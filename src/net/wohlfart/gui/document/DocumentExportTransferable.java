package net.wohlfart.gui.document;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.SwingWorker;

import net.wohlfart.data.IDocumentTO;
import net.wohlfart.data.persis.FolderDAO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * data container for the document using file list flavor, 
 * this also implements the async way to get the data from the database with a swing worker
 * 
 * @author michael
 */ // package private by intention
class DocumentExportTransferable implements Transferable  {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(DocumentExportTransferable.class);
	// DnD support
	private static final DataFlavor[] SUPPORTED_FLAVORS = new DataFlavor[] {DataFlavor.javaFileListFlavor };

	private transient SwingWorker<List<File>, Void> exporter;


	public void preloadData(final IDocumentTO document, final FolderDAO folderDAO) {

		exporter = new SwingWorker<List<File>, Void>() {
			@Override
			protected List<File> doInBackground() {				
				LOG.info("doInBackground() running");
				// FIXME: delete the file on exit:
				String tempDir = System.getProperty("java.io.tmpdir");
				File docFile = new File(tempDir, "" + document.getName());
				
				OutputStream stream = null;
				try {

					boolean append = false;
					stream = new FileOutputStream(docFile, append);
					if (document.getData() == null) {
						folderDAO.loadDocumentContent(document);
					}
					stream.write(document.getData());
					stream.close();
				} catch (IOException | SQLException ex) {
					LOG.error("failed to load data", ex);
				} finally {
					if (stream != null) {
						try {
							stream.close();
						} catch (IOException ignored) {}
					}
				}

				LOG.info("file created '{}', returning as singleton list", docFile);
				return Collections.singletonList(docFile);
			}
		};
		exporter.execute();
	}


	@Override
	public DataFlavor[] getTransferDataFlavors() {
		return SUPPORTED_FLAVORS;
	}

	@Override
	public boolean isDataFlavorSupported(final DataFlavor flavor) {
		for (DataFlavor supportedFlavor : SUPPORTED_FLAVORS) {
			if (flavor.equals(supportedFlavor)) {
				return true;
			}
		}
		LOG.info("requested flavor was '{}' we don't support that", flavor);
		return false;
	}

	/**
	 * needs to return a file list
	 */
	@Override
	public Object getTransferData(final DataFlavor flavor)
			throws UnsupportedFlavorException, IOException {
		LOG.info("getTransferData called");
		if (!isDataFlavorSupported(flavor)) {
			throw new UnsupportedFlavorException(flavor);
		}

		try {
			List<File> result = exporter.get();		
			LOG.info("result was '{}' got it from the exporter", result);
			return result;
		} catch (InterruptedException | ExecutionException ex) {
			throw new IOException("getting data for export failed", ex);
		}
	}

}
