package net.wohlfart;

/**
 * Some infamous global constants.
 * 
 * @author michael
 */
public final class Constants {
	
	// delaying worker threads may be necessary for debugging
	public static final boolean DELAY_WORKERS = false;
	
	public static final int BUILD = 955;
	public static final String BUILD_DATE = "03.03.2012"; //$NON-NLS-1$

	public static final int MAJOR_VERSION = 3;
	public static final int MINOR_VERSION = 0;	

	public static final String VERSION_STRING = MAJOR_VERSION + "." + MINOR_VERSION; //$NON-NLS-1$

	
	private Constants() {}
	
}
