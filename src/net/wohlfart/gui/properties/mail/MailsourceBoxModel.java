package net.wohlfart.gui.properties.mail;

import java.util.Map.Entry;

import javax.swing.DefaultComboBoxModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class MailsourceBoxModel extends DefaultComboBoxModel<IMailBoxFlavor> {
	/**  Logger for this class */
	private static final Logger LOG = LoggerFactory.getLogger(MailsourceBoxModel.class);

	MailsourceBoxModel() {
		LOG.debug("creating model"); //$NON-NLS-1$
		MailBoxFlavourCollection collection = MailBoxFlavourCollection.getInstance();
		for (Entry<String, IMailBoxFlavor> mailsource : collection.entrySet()) {
			LOG.debug("  found: {}", mailsource); //$NON-NLS-1$
			addElement(mailsource.getValue());
		}
		
		setSelectedItem(collection.getNullElement());
	}

}
