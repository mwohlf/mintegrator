package net.wohlfart.gui.table;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import net.wohlfart.Messages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * a popup menu to show/hide table columns
 * the menu stays open so the user can toggle multple columns without reopening the menu
 * idea from Darryl Burke
 * see: http://tips4java.wordpress.com/2010/09/12/keeping-menus-open/
 * 
 * @author michael
 */
@SuppressWarnings("serial")
public class ColumnViewPopupMenu extends JPopupMenu {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(ColumnViewPopupMenu.class);


	ColumnViewPopupMenu(final JTable innerTable) {
		TableModel tableDataModel = innerTable.getModel();
		// TableColumnModel tableColumnModel = innerTable.getColumnModel();

		int count = tableDataModel.getColumnCount();
		for (int column = 0; column < count; column++) {
			final Integer modelIndex = column;
			final String name = Messages.getString(Messages.APPLICATION_DATA_PREFIX + "." + tableDataModel.getColumnName(column));
			add(new CheckBoxMenuItem(name){{
				setSelected(isColumnVisible(innerTable, modelIndex));
				addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent evt) {
						LOG.info("actionPerformed: {}, evt: {}", modelIndex, evt);						
						toggleColumn(innerTable, modelIndex, isSelected());
						//setVisible(true);
					}
				});					
			}});
		} // end for loop	
	}



	//		jMenu.addSeparator();
	//
	//		// create a menu item for the vertical scrollbar
	//		final JCheckBoxMenuItem menuItem = new JCheckBoxMenuItem(Messages.getString("Table.Header.autoResize"));
	//		menuItem.addActionListener(new ActionListener() {
	//			public void actionPerformed(ActionEvent arg0) {
	//				if (menuItem.isSelected()) {
	//					innerTable.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);
	//				} else {
	//					innerTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	//				}
	//				props.setAutoResize(CANDIDATE_TABLE, menuItem.isSelected());
	//			}});
	//		boolean autoResize = props.getAutoResize(CANDIDATE_TABLE);
	//		menuItem.setSelected(autoResize);
	//		jMenu.add(menuItem);
	//
	//		return jMenu;
	//	}
	//

	
	private boolean isColumnVisible(final JTable innerTable, final int modelIndex) {
		//get the column
		int viewColumn = innerTable.convertColumnIndexToView(modelIndex);
		return viewColumn != -1;
	}
	

	private void toggleColumn(final JTable innerTable, final int modelIndex, final boolean makeVisible) {

		if (isColumnVisible(innerTable, modelIndex) && makeVisible) {
			LOG.warn("column is already visible: {}, not changing anything in the view", modelIndex);
			return;
		}
		
		if (!isColumnVisible(innerTable, modelIndex) && !makeVisible) {
			LOG.warn("column is not visible: {}, not changing anything in the view", modelIndex);
			return;
		}

		int viewColumn = innerTable.convertColumnIndexToView(modelIndex);

		if (makeVisible) {
			// make column visible
			LOG.warn("making column visible: {}", modelIndex);			
			TableColumn column = new TableColumn(modelIndex);
			// use a custom header renderer
			column.setHeaderRenderer(new CustomHeaderRenderer());
			innerTable.addColumn(column);
		} else {
			// hide the column
			LOG.warn("hiding column: {}", modelIndex);			
			TableColumn column = innerTable.getColumnModel().getColumn(viewColumn);
			innerTable.removeColumn(column);
		}
	}


	class CheckBoxMenuItem extends JCheckBoxMenuItem {
		public CheckBoxMenuItem(String columnName) {
			super(columnName);
		}

		// show the menu again after the click is done
		@Override
		public void doClick(int pressTime) {
			super.doClick(pressTime);
			LOG.info("doClick: {}", pressTime);				
			ColumnViewPopupMenu.this.setVisible(true);
			// FIXME: somehow the focus is missing on the item after showing the menu again
			this.requestFocus();
			this.requestFocusInWindow();
			this.repaint();
		}
	}

}
