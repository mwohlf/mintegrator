package net.wohlfart.action;

import static net.wohlfart.util.GuiUtilities.getFrame;

import java.awt.event.ActionEvent;

import javax.inject.Singleton;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import net.wohlfart.Constants;
import net.wohlfart.ImageManager;
import net.wohlfart.Messages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author michael
 *
 */
@SuppressWarnings("serial")
@Singleton
public class ShowAbout extends AbstractAction {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(ShowAbout.class);
    /** Platform dependant newline. */
    private static final String NEWLINE = System.getProperty("line.separator"); //$NON-NLS-1$

    
    public ShowAbout() {
        putValue(Action.NAME, Messages.getString("ShowAbout.ActionName")); //$NON-NLS-1$
        putValue(Action.SMALL_ICON, ImageManager.getImageIcon("ShowAbout16")); //$NON-NLS-1$
        putValue(Action.LARGE_ICON_KEY, ImageManager.getImageIcon("ShowAbout24")); //$NON-NLS-1$
        putValue(Action.SHORT_DESCRIPTION, Messages.getString("ShowAbout.ShortDescription")); //$NON-NLS-1$
    }

    @Override
	public void actionPerformed(final ActionEvent evt) {
    	LOG.info("show about action triggered");
    	
    	// this is called in the EDT, so we can create and init UI stuff
    	
        
        JTextArea infoText = new JTextArea();
        infoText.append("MailIntegrator3" + NEWLINE);
        infoText.append("Version: " + Constants.VERSION_STRING + NEWLINE);
        infoText.append("Build: " + Constants.BUILD + NEWLINE);
        infoText.append("Date: " + Constants.BUILD_DATE + NEWLINE);
        // infoText.append("Customer: " + Start.CUSTOMER + NEWLINE);
        infoText.append("Java-Vendor: " + System.getProperty("java.vendor") + NEWLINE);
        infoText.append("Java-Version: " + System.getProperty("java.version") + NEWLINE);
        infoText.append("Java-Home: " + System.getProperty("java.home") + NEWLINE);
        infoText.append("OS-Arch: " + System.getProperty("os.arch") + NEWLINE);
        infoText.append("OS-Name: " + System.getProperty("os.name") + NEWLINE);

        infoText.setEditable(false);
        infoText.setBackground(UIManager.getColor("TextField.inactiveBackground"));

        JOptionPane.showMessageDialog(
                getFrame(),
                infoText,
                "About...",  // title
                JOptionPane.INFORMATION_MESSAGE);     
    }

}
