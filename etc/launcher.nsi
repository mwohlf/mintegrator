; Java Launcher for MailIntegrator
;---------------------------------

;
;the start config in eclipse looks like this:
;
; -Djava.util.logging.config.file=conf/logging.properties
; -Djava.library.path=E:\wohlfart_workspace\mailintegrator\lib
; -ea
;
; with the main class:
;
; net.wohlfart.persman.bm.Start
;
; TODO: include some manifest info for the main class
;

Name "Mintegrator"
Caption "Mintegrator Launcher"
Icon "PersManIcon16.ico"
OutFile "../dist/Mintegrator.exe"

SilentInstall silent
AutoCloseWindow true
ShowInstDetails nevershow
; http://nsis.sourceforge.net/Docs/Chapter4.html we don't need any special permission to start java...
RequestExecutionLevel user

;the libraries we use
; TODO: integrate copyright info
!define INTEGRATOR_JAR "lib\Mintegrator.jar"
!define COMMONS_JAR "lib\commons-codec-1.6.jar;lib\commons-dbcp-1.4.jar;lib\commons-dbutils-1.4.jar;lib\commons-pool-1.5.6.jar"
!define FORMS_JAR "lib\jgoodies-forms-1.4.1.jar;lib\jgoodies-common-1.2.0.jar;lib\jgoodies-looks-2.4.1.jar;lib\jcalendar-1.4.jar"
!define LOGGING_JAR "lib\slf4j-api-1.6.4.jar;lib\slf4j-log4j12-1.6.4.jar;lib\log4j-1.2.16.jar"
!define GROOVY_JAR "lib\groovy-all-1.8.5.jar"
!define JOC_JAR "lib\joc-v3.0.0-basic.jar;lib\moyocore.jar"
!define WELD_JAR "lib\weld-se-1.1.5.Final.jar"
!define DRIVER_JAR "lib\jtds-1.2.5.jar;lib\ojdbc6.jar;lib\derby.jar;lib\derbyclient.jar;lib\hsqldb-2.2.5.jar"
!define HELP_JAR "lib\jhall-2.0.05.jar"


; the jar with the main method to start with
!define MAIN_CLASS "net.wohlfart.Start"


Section ""

  ; build the classpath
  Var /GLOBAL classpath
  StrCpy $classpath '${INTEGRATOR_JAR}'
  StrCpy $classpath '$classpath;${COMMONS_JAR}'
  StrCpy $classpath '$classpath;${FORMS_JAR}'
  StrCpy $classpath '$classpath;${LOGGING_JAR}'
  StrCpy $classpath '$classpath;${GROOVY_JAR}'
  StrCpy $classpath '$classpath;${JOC_JAR}'
  StrCpy $classpath '$classpath;${WELD_JAR}'
  StrCpy $classpath '$classpath;${DRIVER_JAR}'
  StrCpy $classpath '$classpath;${HELP_JAR}'

  ; aggregate the jvm args
  Var /GLOBAL jvm_args
  ; jdk logging not used
  ;StrCpy $jvm_args ' -Djava.util.logging.config.file=conf\logging.properties'
  StrCpy $jvm_args '$jvm_args -Djava.library.path=lib'
  StrCpy $jvm_args '$jvm_args -Dlog4j.configuration=file:conf/log4j.xml'
  ; StrCpy $jvm_args '$jvm_args -ea'
  StrCpy $jvm_args '$jvm_args -splash:gfx/splash.gif'


  Call GetParameters ; check the commandline
  Pop $R1
  ;MessageBox MB_OK|MB_ICONEXCLAMATION "commandline: $R1"

; assume we have no commandline parameter
  StrCpy $R0 "javaw.exe"
  ;MessageBox MB_OK|MB_ICONEXCLAMATION "R1 1: $R1"
  StrCmp $R1 "" startup_application

; assume we have debug option on
  StrCpy $R0 "java.exe"
  ;MessageBox MB_OK|MB_ICONEXCLAMATION "R1 2: $R1"
  StrCpy $jvm_args '$jvm_args -ea' ; enable assertins only on debug
  StrCpy $jvm_args '$jvm_args  -Dlog4j.debug' ; debug logging
  StrCmp $R1 "-debug" startup_application

; assume we want an update
  ;MessageBox MB_OK|MB_ICONEXCLAMATION "R1 3: $R1"
  StrCmp $R1 "-update" do_update
  ; unknown commandline
  MessageBox MB_OK|MB_ICONEXCLAMATION "usage: Mailintegrator.exe [-debug | -update]"
  Quit

do_update:
  Call DoUpdate
  Quit


startup_application:
  Call GetJRE ; find the JRE
  Pop $R0     ; the first element on the stack is the java.exe path

  StrCmp $R0 "" no_jre_found found_jre
no_jre_found:
  MessageBox MB_OK|MB_ICONEXCLAMATION "Keine Java-Laufzeitumgebung gefunden."
  Quit

found_jre:
  ; build the commandline
  StrCpy $0 '"$R0" $jvm_args -classpath "$classpath" ${MAIN_CLASS}'

  ; for debug: write the commandline to a file:
  Push "$0" ;text to write to file
  Push "$EXEDIR\log.txt" ;file to write to
  Call WriteToFile

  SetOutPath $EXEDIR  ; set the cwd for the java program
  Exec $0             ; start the java program in a new process

  ; sleep a bit to prevent multiple starts
  Sleep 5000

SectionEnd



Function .onInit

 ; multiple run protection
 System::Call 'kernel32::CreateMutexA(i 0, i 0, t "mutex") i .r1 ?e'
 Pop $R0

 StrCmp $R0 0 noprevrun
   MessageBox MB_OK|MB_ICONEXCLAMATION "Die Anwendung wurde bereits gestartet."
   Abort
 noprevrun:

FunctionEnd


;;;;;;;;;;;;;;;;;;;;;;;;;; custom functions ,,,,,,,,,,,,,,,,,,,,,,,,


Function WriteToFile
 Exch $0 ;file to write to
 Exch
 Exch $1 ;text to write

  FileOpen $0 $0 a #open file
   FileSeek $0 0 END #go to end
   FileWrite $0 $1 #write to file
  FileClose $0

 Pop $1
 Pop $0
FunctionEnd

;
;  Find JRE (we are looking for a javaw.exe,
;  lets hope it has the right version)
;
;  input: java.exe or javaw.exe in $R0
;  output: path/to/javaX.exe on top of stack
;
Function GetJRE

; see http://nsis.sourceforge.net/A_slightly_better_Java_Launcher

  Push $R0  ; save R0
  Push $R1  ; save R1
  Push $R2  ; save R2

  StrCpy $R2 $R0  ; the bin we seek is in $R2 now

; check the application directory for a decent Java version
;check_appdir:
  ClearErrors
  StrCpy $R0 "$EXEDIR\jre\bin\$R2"
  IfFileExists $R0 jre_found no_appdir
no_appdir:
  ;MessageBox MB_OK|MB_ICONEXCLAMATION "no_appdir"

;check_envvar:
  ClearErrors
  ReadEnvStr $R0 "JAVA_HOME"
  StrCpy $R0 "$R0\bin\$R2"
  IfFileExists $R0 jre_found no_envvar
no_envvar:
  ;MessageBox MB_OK|MB_ICONEXCLAMATION "no_envvar"

;check_registry:
  ClearErrors
  ;ReadRegStr $R1 HKLM "SOFTWARE\JavaSoft\Java Rruntime Environment" "CurrentVersion"
  ReadRegStr $R1 HKLM "SOFTWARE\JavaSoft\Java Runtime Environment" "CurrentVersion"
  ReadRegStr $R0 HKLM "SOFTWARE\JavaSoft\Java Runtime Environment\$R1" "JavaHome"
  StrCpy $R0 "$R0\bin\$R2"
  IfFileExists $R0 jre_found no_registry
no_registry:
  ;MessageBox MB_OK|MB_ICONEXCLAMATION "no_registry"


;nothing_found:
  StrCpy $R0 ""

; some kind of exception here

jre_found:
  Pop $R2   ; restore R2
  Pop $R1   ; restore R1
  Exch $R0  ; exchange the first stack element with the $R0 value so the result is on stack
FunctionEnd

Function DoUpdate
   !define MI_JAR_URL "http://www.persman.de/ftpdocs/Mailintegrator.jar"
   !define HELP_JAR_URL "http://www.persman.de/ftpdocs/integratorHelp.jar"

   !define MI_JAR_FILE "$EXEDIR\lib\Mailintegrator.jar"
   !define HELP_JAR_FILE "$EXEDIR\help\integratorHelp.jar"

   Push $R0
   Push $R1
   Push $R2

   GetFileTime ${MI_JAR_FILE} $R1 $R2
   !define MI_JAR_FILE_OLD "$EXEDIR\lib\Mailintegrator.jar_$R1$R2"
   GetFileTime ${HELP_JAR_FILE} $R1 $R2
   !define HELP_JAR_FILE_OLD "$EXEDIR\help\integratorHelp.jar_$R1$R2"


   MessageBox MB_OK "Updating Mailintegrator Jar-Files from http://www.persman.de/"
   ; move files
   Rename "${MI_JAR_FILE}" "${MI_JAR_FILE_OLD}"
   Rename "${HELP_JAR_FILE}" "${HELP_JAR_FILE_OLD}"

   ; download the Mailintegrator.jar file
   StrCpy $2 "${MI_JAR_FILE}"
   nsisdl::download /TIMEOUT=30000 ${MI_JAR_URL} $2
   Pop $R0 ;Get the return value
   StrCmp $R0 "success" mi_jar_done
   MessageBox MB_OK "Download failed: $R0"
   Goto recover
mi_jar_done:

   ; download the integratorHelp.jar file
   StrCpy $2 "${HELP_JAR_FILE}"
   nsisdl::download /TIMEOUT=30000 ${HELP_JAR_URL} $2
   Pop $R0 ;Get the return value
   StrCmp $R0 "success" help_jar_done
   MessageBox MB_OK "Download failed: $R0"
   Goto recover

help_jar_done:
   MessageBox MB_OK "Update Successfull"
   Quit

recover:
   Rename "${MI_JAR_FILE_OLD}_old" "${MI_JAR_FILE}"
   Rename "${HELP_JAR_FILE_OLD}" "${HELP_JAR_FILE}"
   Quit

FunctionEnd


Function GetParameters

   Push $R0
   Push $R1
   Push $R2
   Push $R3

   StrCpy $R2 1
   StrLen $R3 $CMDLINE

   ;Check for quote or space
   StrCpy $R0 $CMDLINE $R2
   StrCmp $R0 '"' 0 +3
     StrCpy $R1 '"'
     Goto loop
   StrCpy $R1 " "

   loop:
     IntOp $R2 $R2 + 1
     StrCpy $R0 $CMDLINE 1 $R2
     StrCmp $R0 $R1 get
     StrCmp $R2 $R3 get
     Goto loop

   get:
     IntOp $R2 $R2 + 1
     StrCpy $R0 $CMDLINE 1 $R2
     StrCmp $R0 " " get
     StrCpy $R0 $CMDLINE "" $R2

   Pop $R3
   Pop $R2
   Pop $R1
   Exch $R0

FunctionEnd

