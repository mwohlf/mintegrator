package net.wohlfart.data.persis;

import net.wohlfart.data.IDocumentTO;

public class DocumentTO implements IDocumentTO {
	
    private Integer docId;
    private String info;
    private Integer docNumber;
    private byte[] data;
    private Integer gpg;
    private Integer groesse;
    private String mainModule;
    private Integer mainId;
    private String path;
    private String name;
    private String oldDir;
    private Integer id;
    private String groupModule;
    private Integer groupId;
    private Integer writeLock;
    private Integer lockRemark;
    private String type;


    @Override
	public byte[] getData() {
        return data;
    }

    @Override
	public Integer getDocId() {
        return docId;
    }

    @Override
	public Integer getDocNumber() {
        return docNumber;
    }
    @Override
	public Integer getGpg() {
        return gpg;
    }
    @Override
	public Integer getGroesse() {
        return groesse;
    }
    @Override
	public Integer getGroupId() {
        return groupId;
    }
    @Override
	public String getGroupModule() {
        return groupModule;
    }
    @Override
	public Integer getId() {
        return id;
    }
    @Override
	public String getInfo() {
        return info;
    }
    @Override
	public Integer getLockRemark() {
        return lockRemark;
    }
    @Override
	public Integer getMainId() {
        return mainId;
    }
    @Override
	public String getMainModule() {
        return mainModule;
    }
    @Override
	public String getName() {
        return name;
    }
    @Override
	public String getOldDir() {
        return oldDir;
    }
    @Override
	public String getPath() {
        return path;
    }
    @Override
	public String getType() {
        return type;
    }
    @Override
	public Integer getWriteLock() {
        return writeLock;
    }

    @Override
	public void setData(byte[] data) {
        this.data = data;
    }

    @Override
	public void setDocId(Integer docId) {
        this.docId = docId;
    }
    @Override
	public void setDocNumber(Integer docNumber) {
        this.docNumber = docNumber;
    }
    @Override
	public void setGpg(Integer gpg) {
        this.gpg = gpg;
    }
    @Override
	public void setGroesse(Integer groesse) {
        this.groesse = groesse;
    }
    @Override
	public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }
    @Override
	public void setGroupModule(String groupModule) {
        this.groupModule = groupModule;
    }
    @Override
	public void setId(Integer id) {
        this.id = id;
    }
    @Override
	public void setInfo(String info) {
        this.info = info;
    }
    @Override
	public void setLockRemark(Integer lockRemark) {
        this.lockRemark = lockRemark;
    }
    @Override
	public void setMainId(Integer mainId) {
        this.mainId = mainId;
    }
    @Override
	public void setMainModule(String mainModule) {
        this.mainModule = mainModule;
    }
    @Override
	public void setName(String name) {
        this.name = name;
    }
    @Override
	public void setOldDir(String oldDir) {
        this.oldDir = oldDir;
    }
    @Override
	public void setPath(String path) {
        this.path = path;
    }
    @Override
	public void setType(String type) {
        this.type = type;
    }
    @Override
	public void setWriteLock(Integer writeLock) {
        this.writeLock = writeLock;
    }

}