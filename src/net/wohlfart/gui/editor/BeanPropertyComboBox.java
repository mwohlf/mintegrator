package net.wohlfart.gui.editor;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyDescriptor;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JList;

import net.wohlfart.gui.event.ApplicationFocused;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class BeanPropertyComboBox<T> extends JComboBox<T> implements IFocusedApplicationListener {
	/** Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(BeanPropertyComboBox.class);

	private final String propertyName;
	private final LocalPropertyChangeListener listener;
	
	// for the key selection listener
    private String searchFor;
    private long lap;

	private Object bean; // proxy
	private PropertyDescriptor propertyDescriptor;
	private ISelectedValueConverter<T> converter;


	private VetoableChangeSupport changeSupport; // we need to remember the last change support in order to unregister

	private boolean isFiring = false; // endless loop protection


	public BeanPropertyComboBox(final Class<?> clazz, final String propertyName, final ISelectedValueConverter<T> converter) {
		this.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				copyModel2Bean(); // FIXME: maybe we need to use something else here since the client adds action listener too
			}
		});

		this.setRenderer(new DefaultListCellRenderer() {
			@Override
			@SuppressWarnings("unchecked")
			public Component getListCellRendererComponent(@SuppressWarnings("rawtypes") JList list,
					Object value, int index, boolean isSelected,
					boolean cellHasFocus) {
				LOG.debug("rendering triggered for {}", value);
				return super.getListCellRendererComponent(list, 
						converter.convert2ViewString((T)value), index, isSelected, 
						cellHasFocus);
			}			
		});
		
		this.setKeySelectionManager(new KeySelectionManager() {
		    @Override
			public int selectionForKey(char aKey, ComboBoxModel aModel) {
		        long now = new java.util.Date().getTime();
		        if (lap + 500 < now) {
		            searchFor = "" + aKey;
		        }
		        lap = now;
		        String current;
		        for (int i = 0; i < aModel.getSize(); i++) {
		            current = aModel.getElementAt(i).toString().toLowerCase();
		            if (current.startsWith(searchFor)) {
		                return i;
		            }
		        }
		        return -1;
		    }
		});


		this.listener = new LocalPropertyChangeListener();
		this.propertyName = propertyName;
		this.converter = converter;
		setupDescriptor(clazz, propertyName);
	}


	@Override
	protected void fireActionEvent() {
		if (!isFiring) {
			super.fireActionEvent();
		}
	}


	public void setElements(final Collection<T> collection) {
		isFiring = true; // make sure we don't accidently fire a change event
		T sel = (T)getSelectedItem();
		String selView =  converter.convert2ViewString(sel);

		// remove elements that have the same property
		DefaultComboBoxModel<T> model = new DefaultComboBoxModel<>();
		model.addElement(null); // default empty element
		Set<String> set = new HashSet<>();
		for (T element : collection) {
			String s = converter.convert2BeanProperty(element);
			//String s = converter.convert2ViewString(element);
			if (!set.contains(s)) {
				set.add(s);
				model.addElement(element);
			}
		}
		setModel(model);

		// try to re-select whatever has been selected before
		int size = model.getSize();
		T selObject = null;
		if (selView != null) {
			for (int i = 0; i < size; i++) {
				T current = model.getElementAt(i);
				if (selView.equals(converter.convert2ViewString(current))) {
					selObject = current;
					break;
				}
			}
		}
		
		// if nothing matches with the preselected item and we have just
		// one element, select the one element
		if ((size == 2) && (selObject == null)) {
			selObject = model.getElementAt(1);
		}

		setSelectedItem(selObject);
		isFiring = false;
	}

	protected void setupDescriptor(final Class<?> clazz, final String propertyName) {
		try {
			propertyDescriptor = getDescriptor(clazz, propertyName);
			if (propertyDescriptor == null) {
				LOG.error("property descriptor is null for property name '{}'", propertyName);
			}
		} catch (IntrospectionException ex) {
			LOG.error("Error setting property descriptor", ex);
		}
	}

	protected PropertyDescriptor getDescriptor(final Class<?> clazz, final String propertyName)
			throws IntrospectionException {
		// create a property name to descriptor map
		final HashMap<String, PropertyDescriptor> tempDescriptorMap = new HashMap<>();
		BeanInfo beanInfo = Introspector.getBeanInfo(clazz);
		PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
		for (PropertyDescriptor descriptor : propertyDescriptors) {
			tempDescriptorMap.put(descriptor.getName(), descriptor);
		}
		return tempDescriptorMap.get(propertyName);
	}

	protected class LocalPropertyChangeListener implements VetoableChangeListener {
		@Override
		public void vetoableChange(PropertyChangeEvent evt) {
			if (!isFiring) {
				Object beanValue = evt.getNewValue();
				setView((String)beanValue);
			}
		}
	}

	@Override
	public void setFocusedApplication(ApplicationFocused container) {
		this.bean = container.getContent();
		VetoableChangeSupport changeSupport = container.getChangeSupport();

		if (this.changeSupport != null) { // might be null on application start
			this.changeSupport.removeVetoableChangeListener(propertyName, listener);
		}
		this.changeSupport = changeSupport;
		if (this.changeSupport != null) { // might be null on shutdown
			this.changeSupport.addVetoableChangeListener(propertyName, listener);
		}

		isFiring = true; // we don't want the model to fire back
		copyBean2Model();
		isFiring = false;
	}

	protected void copyModel2Bean() {
		LOG.debug("bean is {}, isFiring is {}", bean, isFiring);
		if (isFiring) {
			return;
		}
		if ((BeanPropertyComboBox.this.bean == null)
				|| (BeanPropertyComboBox.this.propertyDescriptor == null)) {
			LOG.info(
					"skipping setter action, bean is {}, propertyDescriptor is {}, host is {}",
					new Object[] { bean, propertyDescriptor,
							BeanPropertyComboBox.this });
			return;
		}
		try {
			isFiring = true;			
			propertyDescriptor.getWriteMethod().invoke(bean, converter.convert2BeanProperty((T)getSelectedItem()));
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
			LOG.error("Error setting text property", ex);
		} finally {
			isFiring = false;
		}
	}

	protected void copyBean2Model() {
		LOG.debug("bean is {}", bean);
		if ((BeanPropertyComboBox.this.bean == null)
				|| (BeanPropertyComboBox.this.propertyDescriptor == null)) {
			LOG.info(
					"skipping setter action, bean is {}, propertyDescriptor is {}, host is {}",
					new Object[] { bean, propertyDescriptor,
							BeanPropertyComboBox.this });
			return;
		}
		try {
			Object beanValue = propertyDescriptor.getReadMethod().invoke(bean, new Object[] {});			
			setView((String)beanValue);						
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
			LOG.error("Error setting text property", ex);
		}
	}

	private void setView(String beanValue) {
		isFiring = true;
		ComboBoxModel<T> model = getModel();

		// try to select whatever has been selected before
		int size = model.getSize();
		T selObject = null;
		if (beanValue != null) {
			for (int i = 0; i < size; i++) {
				T current = model.getElementAt(i);
				if (beanValue.equals(converter.convert2BeanProperty(current))) {
					selObject = current;
					break;
				}
			}
		}

		setSelectedItem(selObject);
		isFiring = false;
	}

	@Override
	public String toString() {
		return getClass().getName() + "@" + Integer.toHexString(hashCode());
	}




}
