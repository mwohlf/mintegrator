package net.wohlfart.gui.properties.look;

import javax.swing.DefaultComboBoxModel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class LookAndFeelComboBoxModel extends DefaultComboBoxModel<LookAndFeelInfo> {
	/**  Logger for this class */
	private static final Logger LOG = LoggerFactory.getLogger(LookAndFeelComboBoxModel.class);

	LookAndFeelComboBoxModel() {
		LOG.debug("creating model");
		LookAndFeelInfo[] installed = UIManager.getInstalledLookAndFeels();
		for (LookAndFeelInfo info : installed) {
			LOG.debug("  found: {}", info.getName());
			addElement(info);
		}
	}
	
}
