package net.wohlfart.gui.table;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyDescriptor;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Observes;
import javax.inject.Singleton;
import javax.swing.table.AbstractTableModel;

import net.wohlfart.data.ICandidateTO;
import net.wohlfart.data.persis.CandidateTO;
import net.wohlfart.gui.event.ApplicationCreated;
import net.wohlfart.gui.event.ApplicationFocused;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Table model specifies which properties from the Application we want to see
 * 
 * @author michael
 */
@SuppressWarnings("serial")
@Singleton
public class ApplicationTableModel extends AbstractTableModel {
	/** Logger for this class */
	private static final Logger LOG = LoggerFactory.getLogger(ApplicationTableModel.class);

	// the descriptors of the currently visible properties/column
	private final ArrayList<PropertyDescriptor> descriptors;

	private List<CandidateTO> dataList = Collections.emptyList();

	private final LocalPropertyChangeListener propertyChangeDispatcher;
	private VetoableChangeSupport changeSupport; // we need to remember the last change support in order to derigister whenever 
	private ICandidateTO application; // proxy

	private static final String[] visibleProperties = {
//		"city",
//		"zipCode",
		"lastName",
		"firstName",
		"emailAddress",
		"phoneNumber",
//		"gender",
		"applicationDate",
//		"dateOfBirth",
//		"medium",
//		"applicationTitle",
//		"jobOfferDescription",
//		"division",	
	};


	ApplicationTableModel() {
		descriptors = new ArrayList<>();
		// private inner class to dispatch property change events to the editors
		propertyChangeDispatcher = new LocalPropertyChangeListener();
	}


    @PostConstruct
    protected void postConstruct() { // NO_UCD
    	LOG.debug("postConstruct() called"); //$NON-NLS-1$
		try {
			initDescriptors(visibleProperties);
		} catch (IntrospectionException ex) {
			LOG.error("failed to initialize the TableModel", ex);
		}	
	}

	private void initDescriptors(final String[] rows) throws IntrospectionException {
		// create a property name to descriptor map
		final HashMap<String, PropertyDescriptor> tempDescriptorMap = new HashMap<>();
		BeanInfo beanInfo = Introspector.getBeanInfo(ICandidateTO.class);
		PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
		for (PropertyDescriptor descriptor : propertyDescriptors) {
			tempDescriptorMap.put(descriptor.getName(), descriptor);
		}
		// collect the properties for the columns
		for (String propertyName : rows) {
			// find the descriptor for the property
			PropertyDescriptor descriptor = tempDescriptorMap.get(propertyName);
			if (descriptor != null) {
				descriptors.add(descriptor);
			} else {
				LOG.warn("the descriptor for the propery with name `{}` could not be found, the table won't contain the property"
						+ "\nthe following properties are available:", propertyName);
				// dump the available names
				for (String key : tempDescriptorMap.keySet()) {
					LOG.warn(" '{}'", key);
				}
			}
		}
	}

	/* accessor for custom code */

	public synchronized ICandidateTO getValueAtRow(final int row) {
		return dataList.get(row);
	}

	public synchronized int getElementIndex(final ICandidateTO element) {
		return dataList.indexOf(element);
	}


	// ---- TableModel methods ----

	@Override
	public int getColumnCount() {
		int result = descriptors.size();
		LOG.debug("getColumnCount is {}", result); //$NON-NLS-1$
		return result;
	}

	@Override
	public int getRowCount() {
		synchronized (this) {
			int result = dataList.size();
			LOG.debug("getRowCount is {} in {}", new Object[] {result, this}); //$NON-NLS-1$
			return result;
		}
	}


	/**
	 *  rowCount starts at 0
	 */
	@Override
	public synchronized Object getValueAt(final int row, final int column) {
		Object returnObject = null;
		try {
			ICandidateTO targetBean = dataList.get(row);
			PropertyDescriptor descriptor = descriptors.get(column);
			Method propertyGetter = descriptor.getReadMethod();
			returnObject = propertyGetter.invoke(targetBean, new Object[] {});
		} catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException ex) {
			LOG.error("failed getting data from the TableModel", ex);
		}
		return returnObject;
	}


	/**
	 * this implementation doesn't care about the row, we just assume the user selected the row before
	 * this method is called and we already got the correct proxy object for the row bean set in
	 * focusedRow...
	 */
	@Override
	public synchronized void setValueAt(final Object value, final int row, final int column) {
		try {
			if (application == null) {
				LOG.error("cant set value {} at {},{}, need a focus in table first in order to receive the proxy", 
						new Object[]{value, row, column});
				return;
			}
			PropertyDescriptor descriptor = descriptors.get(column);
			Method propertySetter = descriptor.getWriteMethod();
			propertySetter.invoke(application, new Object[] {value});
		} catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException ex) {
			LOG.error("failed setting data in the TableModel", ex);
		}
	}


	@Override
	public synchronized String getColumnName(final int column) {
		//String key = "unknown";
		PropertyDescriptor descriptor = descriptors.get(column);
		return descriptor.getName();
		// key = descriptor.getDisplayName();
		// return Messages.getString(ApplicationTableModel.class.getSimpleName() + "." + key);
	}


	@Override
	public int findColumn(final String columnName) {
		int i = 0;
		for (PropertyDescriptor descriptor : descriptors) {
			if (columnName.equals(descriptor.getName())) {
				return i;
			}
			i++;
		}
		return -1;
	}


	@Override
	public synchronized Class<?> getColumnClass(final int column) {
		Class<?> returnClass;// = Object.class;
		PropertyDescriptor descriptor = descriptors.get(column);
		returnClass = descriptor.getPropertyType();
		return returnClass;
	}


	@Override
	public synchronized boolean isCellEditable(final int row, final int column) {
		return true;
	}



	// ---- event trigger ----

	/**
	 * mostly triggered by the open database connection worker
	 */
	public void onFullCandidateCollectionEvent(@Observes final ApplicationListFull container) {
		LOG.info("onFullCandidateCollectionEvent called in {}, container is: {}, content is {} ", 
				new Object[] {this, container, container.get()}); //$NON-NLS-1$	
		dataList = container.get();
		fireTableDataChanged();
	}


	/**
	 * mostly triggered by the open database connection worker
	 */
	public void onAddedCandidateEvent(@Observes final ApplicationCreated container) {
		LOG.info("onAddedCandidateEvent called in {}, container is: {}, content is {} ", 
				new Object[] {this, container, container.get()}); //$NON-NLS-1$		
		dataList.add(container.get());
		fireTableDataChanged();
	}


	public void onFocusedApplicationEvent(@Observes final ApplicationFocused container) {
		LOG.debug("onFocusedApplicationEvent called, container is: '{}' ",  container);

		if (container.isEmpty()) {
			application = null;
			if (changeSupport != null) {
				changeSupport.removeVetoableChangeListener(propertyChangeDispatcher); // remove from the old support
				changeSupport = null;
			}

		} else {
			changeSupport = container.getChangeSupport();
			changeSupport.addVetoableChangeListener(propertyChangeDispatcher);
			application = (ICandidateTO) container.getContent();

		}	
	}



	protected class LocalPropertyChangeListener implements VetoableChangeListener {

		@Override
		public void vetoableChange(PropertyChangeEvent evt) {
			String propertyName = evt.getPropertyName();
			Object value = evt.getNewValue();
			LOG.debug("property change detected name is {}, value is {}", propertyName, value);
		}
	}

}

