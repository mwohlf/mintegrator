package net.wohlfart.console;

import java.awt.EventQueue;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PipedOutputStream;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.DefaultCaret;
import javax.swing.text.Element;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.Style;

@SuppressWarnings("serial")
public class ConsoleDocument extends StyledDocument implements KeyListener  {
	static final int MAX_HISTORY = 3;

	private int currentSelection = -1; // used to find a previous command string, index starting at 0

	// commandLine start position

	// the position where the last enter keystroke was (after the newline that got inserted by enter)
	//  == start of new error
	//  == end of old error
	//  == before commandLine prompt
	private int lastCommit = 0;
	private static final String PROMPT = "> "; //$NON-NLS-1$
	private static final String NEWLINE = "\n"; //$NON-NLS-1$

	private final Caret caret;
	private final ArrayList<String> commandHistory;
	private final ConcurrentLinkedQueue<String> commandSequence;

	public ConsoleDocument(final ConcurrentLinkedQueue<String> commandSequence) {
		super();		
		caret = new CustomCaret();
		commandHistory = new ArrayList<>();
		this.commandSequence = commandSequence;
	}


	public void analyze() {
		//      Get section element
		Element section = getDefaultRootElement();

		// Get number of paragraphs.
		// In a text pane, a span of characters terminated by single
		// newline is typically called a paragraph.
		int paraCount = section.getElementCount();

		// Get index ranges for each paragraph
		for (int i=0; i<paraCount; i++) {
			//            Element e = section.getElement(i);
			//            int rangeStart = e.getStartOffset();
			//            int rangeEnd = e.getEndOffset();
			//            try {
			//                //String para = getText(rangeStart, rangeEnd-rangeStart);
			//                //System.out.println(" element " + i + " from " + rangeStart + " to " + rangeEnd + " content: " + para);
			//            } catch (BadLocationException ex) {
			//                // ignored
			//            }
		}
	}


	protected void createPrompt() {
		try {
			insertString(getLength(), PROMPT, getPromptStyle());
			caret.setDot(getLength());
		} catch (BadLocationException ex) {
			ex.printStackTrace();
		}
	}

	/*
    // returns an input queue with command sequences
    public SynchronousQueue<String> getCommandQueue() {
        return commandQueue;
    }


	 */

	protected void evaluateExpression(final String expression) {

		// remove tabs and blanks
		// FIXME: cleanup this code
		StringBuffer expr = new StringBuffer(expression);
		expr = new StringBuffer(expr.toString().replace('\t', ' '));
		expr = new StringBuffer(expr.toString().replace('\n', ' '));
		expr = new StringBuffer(expr.toString().trim());


		if (expr.length() > 0) {
			commandHistory.add(expr.toString());
			while (commandHistory.size() > MAX_HISTORY) {
				commandHistory.remove(0);
			}
			// pointer to the last element
			currentSelection = commandHistory.size()-1;
			
			commandSequence.add(expr.toString()); // enqueue the new command
		}

	}




	// FIXME: something wrong when scrolling through the history back and forth
	@Override
	public void keyPressed(final KeyEvent evt) {
		switch (evt.getKeyCode()) {

		// return key
		case ( KeyEvent.VK_ENTER ):
			try {
				insertString(getLength(), NEWLINE, getDefaultStyle());
				if ( (evt.getModifiers() & InputEvent.SHIFT_MASK) > 0	) {
					insertString(getLength(), " ", getDefaultStyle()); //$NON-NLS-1$
				} else {
					// get the expression without the prompt
					String expression =getText(
							lastCommit + PROMPT.length(),
							getLength()- lastCommit - PROMPT.length() -1); // minus the just inserted newline

					lastCommit = getLength();
					evaluateExpression(expression);
					createPrompt();
				}
			} catch (BadLocationException ex) {
				ex.printStackTrace();
			}
		evt.consume();
		break;

		// key up
		case ( KeyEvent.VK_UP):
			try {
				if (currentSelection > -1) {	// get last command in sequence
					replace(lastCommit + PROMPT.length(),
							getLength() - lastCommit - PROMPT.length(),
							commandHistory.get(currentSelection),
							getDefaultStyle());
					currentSelection--;
				} else if (currentSelection == -1) {
					replace(lastCommit + PROMPT.length(),
							getLength() - lastCommit - PROMPT.length(),
							"", //$NON-NLS-1$
							getDefaultStyle());
				}
			} catch (BadLocationException ex) {
				ex.printStackTrace();
			}
		evt.consume();  // FIXME: add command history here
		break;

		// key down
		case ( KeyEvent.VK_DOWN):
			try {
				if (currentSelection < (commandHistory.size()-1)) { // get next command in sequence
					currentSelection++;
					replace(lastCommit + PROMPT.length(),
							getLength() - lastCommit - PROMPT.length(),
							commandHistory.get(currentSelection),
							getDefaultStyle());
				} else if (currentSelection == (commandHistory.size()-1)) { // already at last command in sequence
					// no increase, replace with blank
					replace(lastCommit + PROMPT.length(),
							getLength() - lastCommit - PROMPT.length(),
							"", //$NON-NLS-1$
							getDefaultStyle());
				}
			} catch (BadLocationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		evt.consume();  // FIXME: add command history here
		break;



		case ( KeyEvent.VK_LEFT	):
		case ( KeyEvent.VK_BACK_SPACE ):
		case ( KeyEvent.VK_DELETE ):
			// do not delete any evaluated content

			if ((caret.getDot() > lastCommit + PROMPT.length()) && (caret.getMark() >= lastCommit + PROMPT.length())) {
				// go ahead
			} else {
				evt.consume();
			}
		break;

		// check for <ctrl>-V (we need to paste at the end of the document
		case ( KeyEvent.VK_V ):
			if (evt.getModifiers() == InputEvent.CTRL_MASK) {
				caret.setDot(getLength());
			}
		break;
		// check for <ctrl>-X (no cut insite the document)
		case ( KeyEvent.VK_X ):
			if (evt.getModifiers() == InputEvent.CTRL_MASK) {
				evt.consume();
			}
		break;
		default:
		}
	}


	@Override
	public void keyReleased(final KeyEvent evt) {
		// not interested in this event
	}

	@Override
	public void keyTyped(final KeyEvent evt) {
		// no overriding of evaluated content
		// any writing must be completely behind the prompt end
		if ((caret.getDot() >= lastCommit + PROMPT.length()) && (caret.getMark() >= lastCommit + PROMPT.length())) {
			// go ahead
		} else {
			switch (evt.getModifiers()) {
			case InputEvent.ALT_GRAPH_MASK:
			case InputEvent.ALT_MASK:
			case InputEvent.CTRL_MASK:
				break;
			default:
				caret.setDot(getLength());
				break;
			}
		}
	}


	private synchronized void insertLine(final String string, final MutableAttributeSet attr) {
		// show the frame
		final String line = string + NEWLINE;
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					//insertString(getLength(), "\n", promptAttributes);
					insertString(lastCommit, line, attr);
					//insertString(getLength(), "\n", attr);
					// move commit and dor forward since we inserted some text probably before the caret
					lastCommit = lastCommit + line.length();
					caret.setDot(getLength() + line.length());
				} catch (BadLocationException ex) {
					ex.printStackTrace();
				}
			}
		});

	}




	public Caret getCaret() {
		return caret;
	}



	// caret position is dot
	// end of selection is mark
	// FIXME: seems we don't need this subclass
	private static class CustomCaret extends DefaultCaret implements ChangeListener {

		CustomCaret() {
			addChangeListener(this);
		}
		/*
        public void setDot(int dot)  {
            if (dot >= ConsoleDocument.this.promptEnd) {
                super.setDot(dot);
            } else {
                super.setDot(ConsoleDocument.this.promptEnd);
            }
        }

        public void moveDot(int dot) {
            if (dot >= ConsoleDocument.this.promptEnd) {
                super.moveDot(dot);
            } else {
                super.moveDot(ConsoleDocument.this.promptEnd);
            }
        }
		 */
		@Override
		public void stateChanged(ChangeEvent evt) {
			//System.out.println("dot is at: " + getDot() + " mark is at " + getMark() + " evt: " +evt);
		}
	}

	public void setStream(final PipedOutputStream  out, final Style style) {
		try {
			InputStreamReader streamReader = new InputStreamReader(new WaitingInputStream(out));
			final BufferedReader bufferedReader = new BufferedReader(streamReader);

			// FIXME: add a method to interrupt and remove this output stream
			Thread thread = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						String line = null;
						do {
							line = bufferedReader.readLine();
							if (line != null) {
								insertLine(line, style);
							}
						} while (line != null);
					} catch (IOException ex) {
						// TODO Auto-generated catch block
						ex.printStackTrace();
					}
				}
			});
			thread.setDaemon(true);
			thread.start();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
