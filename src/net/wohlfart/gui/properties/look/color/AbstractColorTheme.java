package net.wohlfart.gui.properties.look.color;

class AbstractColorTheme {
	
	private final String id;
	
	AbstractColorTheme(final String id) {
		this.id = id;
	}

	
	public String getId() {
		return id;
	}

	public String getName() {
		return id; // FIXME: do I18n here
	}

	
}
