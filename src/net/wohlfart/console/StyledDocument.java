package net.wohlfart.console;

import java.awt.Color;

import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;

@SuppressWarnings("serial")
public class StyledDocument extends DefaultStyledDocument {

	// user input
    private static final String DEFAULT_STYLE = "DEFAULT_ATTRIBUTES"; //$NON-NLS-1$
    private static final String ERROR_STYLE = "ERROR_ATTRIBUTES"; //$NON-NLS-1$
    private static final String PROMPT_STYLE = "PROMPT_ATTRIBUTES"; //$NON-NLS-1$
    // groovy output
    private static final String OUTPUT_STYLE = "PROMPT_ATTRIBUTES"; //$NON-NLS-1$


    public Style getDefaultStyle() {
        Style style = this.getStyle(DEFAULT_STYLE);
        if (style == null) {
            style = this.addStyle(DEFAULT_STYLE, null);
            StyleConstants.setItalic(style, false);
            StyleConstants.setBold(style, false);
            StyleConstants.setFontFamily(style, "Monospaced"); //$NON-NLS-1$
            StyleConstants.setFontSize(style, 14);
            StyleConstants.setBackground(style, Color.white);
            StyleConstants.setForeground(style, Color.black);
        }
        return style;
    }


    public Style getErrorStyle() {
        Style style = this.getStyle(ERROR_STYLE);
        if (style == null) {
            style = this.addStyle(ERROR_STYLE, null);
            StyleConstants.setItalic(style, false);
            StyleConstants.setBold(style, false);
            StyleConstants.setFontFamily(style, "Monospaced"); //$NON-NLS-1$
            StyleConstants.setFontSize(style, 14);
            StyleConstants.setBackground(style, Color.white);
            StyleConstants.setForeground(style, Color.red);
        }
        return style;
    }

    public Style getPromptStyle() {
        Style style = this.getStyle(PROMPT_STYLE);
        if (style == null) {
            style = this.addStyle(PROMPT_STYLE, null);
            StyleConstants.setItalic(style, false);
            StyleConstants.setBold(style, false);
            StyleConstants.setFontFamily(style, "Monospaced"); //$NON-NLS-1$
            StyleConstants.setFontSize(style, 14);
            StyleConstants.setBackground(style, Color.white);
            StyleConstants.setForeground(style, Color.black);
        }
        return style;
    }

    public Style getOutputStyle() {
        Style style = this.getStyle(OUTPUT_STYLE);
        if (style == null) {
            style = this.addStyle(OUTPUT_STYLE, null);
            StyleConstants.setItalic(style, false);
            StyleConstants.setBold(style, false);
            StyleConstants.setFontFamily(style, "Monospaced"); //$NON-NLS-1$
            StyleConstants.setFontSize(style, 14);
            StyleConstants.setBackground(style, Color.white);
            StyleConstants.setForeground(style, Color.black);
        }
        return style;
    }

}
