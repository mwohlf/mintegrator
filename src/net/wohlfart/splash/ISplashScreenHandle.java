package net.wohlfart.splash;

public interface ISplashScreenHandle {

	void setTotalSteps(int totalCount);

	void setStatus(String string, int currentCount);

}
