package net.wohlfart.gui.editor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyDescriptor;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

import net.wohlfart.Messages;
import net.wohlfart.gui.event.ApplicationFocused;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class BeanPropertyRadioButton extends Box implements IFocusedApplicationListener {
	/** Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(BeanPropertyTextfield.class);

    private static final String FEMALE_VALUE = "W"; //$NON-NLS-1$
    private static final String MALE_VALUE = "M"; //$NON-NLS-1$

    
	private final String propertyName;
	private final LocalPropertyChangeListener listener;

	private Object bean; // proxy
	private PropertyDescriptor propertyDescriptor;

	private VetoableChangeSupport changeSupport; // we need to remember the last change support in order to unregister

	private boolean isFiring = false; // endless loop protection

	
    private final JRadioButton maleRadioButton = new JRadioButton();
    private final JRadioButton femaleRadioButton = new JRadioButton();

    private final ButtonGroup group = new ButtonGroup();


    public BeanPropertyRadioButton(final Class<?> clazz, final String propertyName) {
        super(BoxLayout.X_AXIS);

        maleRadioButton.setText(Messages.getString("GenderChooserEditor.male"));  //$NON-NLS-1$
        femaleRadioButton.setText(Messages.getString("GenderChooserEditor.female")); //$NON-NLS-1$

        add(femaleRadioButton);
        add(maleRadioButton);

        group.add(maleRadioButton);
        group.add(femaleRadioButton);

        femaleRadioButton.addActionListener(new ActionListener() {
            @Override
			public void actionPerformed(final ActionEvent arg0) {
                setView(FEMALE_VALUE);
                copyModel2Bean();
            }
        });
        maleRadioButton.addActionListener(new ActionListener() {
            @Override
			public void actionPerformed(final ActionEvent arg0) {
                setView(MALE_VALUE);
                copyModel2Bean();
           }
        });
        
		this.listener = new LocalPropertyChangeListener();
		this.propertyName = propertyName;
		setupDescriptor(clazz, propertyName);
    }

	protected void setupDescriptor(final Class<?> clazz, final String propertyName) {
		try {
			propertyDescriptor = getDescriptor(clazz, propertyName);
			if (propertyDescriptor == null) {
				LOG.error("property descriptor is null for property name '{}'", propertyName);
			}
		} catch (IntrospectionException ex) {
			LOG.error("Error setting property descriptor", ex);
		}
	}

	protected PropertyDescriptor getDescriptor(final Class<?> clazz, final String propertyName)
			throws IntrospectionException {
		// create a property name to descriptor map
		final HashMap<String, PropertyDescriptor> tempDescriptorMap = new HashMap<>();
		BeanInfo beanInfo = Introspector.getBeanInfo(clazz);
		PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
		for (PropertyDescriptor descriptor : propertyDescriptors) {
			tempDescriptorMap.put(descriptor.getName(), descriptor);
		}
		return tempDescriptorMap.get(propertyName);
	}



    private void setView(final String geschl) {
        group.remove(maleRadioButton);
        group.remove(femaleRadioButton);

        if (null == geschl) {
            maleRadioButton.setSelected(false);
            femaleRadioButton.setSelected(false);
        } else if (geschl.equalsIgnoreCase(FEMALE_VALUE)) {
            group.add(maleRadioButton);
            group.add(femaleRadioButton);
            femaleRadioButton.setSelected(true);
        } else if (geschl.equalsIgnoreCase(MALE_VALUE)) {
            group.add(maleRadioButton);
            group.add(femaleRadioButton);
            maleRadioButton.setSelected(true);
        } else {
            maleRadioButton.setSelected(false);
            femaleRadioButton.setSelected(false);
        }
    }
    
    
	@Override
	public void setFocusedApplication(final ApplicationFocused container) {
		this.bean = container.getContent();
		VetoableChangeSupport changeSupport = container.getChangeSupport();
		
		if (this.changeSupport != null) { // might be null on application start
			this.changeSupport.removeVetoableChangeListener(propertyName, listener);
		}
		this.changeSupport = changeSupport;
		if (this.changeSupport != null) { // might be null on shutdown
			this.changeSupport.addVetoableChangeListener(propertyName, listener);
		}
		
		isFiring = true; // we don't want the model to fire back
		copyBean2Model();
		isFiring = false;
	}


	protected class LocalPropertyChangeListener implements VetoableChangeListener {
		@Override
		public void vetoableChange(PropertyChangeEvent evt) {
			if (!isFiring) {
				Object value = evt.getNewValue();
				setView(value==null?null:value.toString());
			}
		}
	}

	protected void copyModel2Bean() {
		LOG.debug("bean is {}, isFiring is {}", bean, isFiring);
		if (isFiring) {
			return;
		}
		if ((BeanPropertyRadioButton.this.bean == null)
				|| (BeanPropertyRadioButton.this.propertyDescriptor == null)) {
			LOG.info(
					"skipping setter action, bean is {}, propertyDescriptor is {}, host is {}",
					new Object[] { bean, propertyDescriptor,
							BeanPropertyRadioButton.this });
			return;
		}
		try {
			isFiring = true;
			String text = null;
			if (maleRadioButton.isSelected() && !femaleRadioButton.isSelected()) {
				text = MALE_VALUE;
			}
			if (!maleRadioButton.isSelected() && femaleRadioButton.isSelected()) {
				text = FEMALE_VALUE;
			}
			
			propertyDescriptor.getWriteMethod().invoke(bean, text);
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException ex) {
			LOG.error("Error setting text property", ex);
		} finally {
			isFiring = false;
		}
	}

	protected void copyBean2Model() {
		LOG.debug("bean is {}", bean);
		if ((BeanPropertyRadioButton.this.bean == null)
				|| (BeanPropertyRadioButton.this.propertyDescriptor == null)) {
			LOG.info(
					"skipping setter action, bean is {}, propertyDescriptor is {}, host is {}",
					new Object[] { bean, propertyDescriptor,
							BeanPropertyRadioButton.this });
			return;
		}
		try {
			Object value = propertyDescriptor.getReadMethod().invoke(bean, new Object[] {});			
			setView(value==null?null:value.toString());						
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException ex) {
			LOG.error("Error setting text property", ex);
		}
	}

	@Override
	public String toString() {
		return getClass().getName() + "@" + Integer.toHexString(hashCode());
	}

}
