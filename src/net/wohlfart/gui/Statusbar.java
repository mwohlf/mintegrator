package net.wohlfart.gui;

import static javax.swing.BoxLayout.X_AXIS;

import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Observes;
import javax.inject.Singleton;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.event.MouseInputAdapter;

import net.wohlfart.ImageManager;
import net.wohlfart.Messages;
import net.wohlfart.console.Shell;
import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.data.ICandidateTO;
import net.wohlfart.gui.event.ApplicationFocused;
import net.wohlfart.gui.table.ApplicationCollectionFocused;
import net.wohlfart.util.ImageComponent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * application global status bar
 * 
 * @author michael
 */
@SuppressWarnings("serial")
@Singleton
public class Statusbar extends JComponent {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(Statusbar.class);

	// connected and disconnected icons
	private final Image dbConnected;
	private final Image dbDisconnected;
	// connected and disconnected tooltip
	private final String dbConnectedTooltip;
	private final String dbDisconnectedTooltip;

	private final JTextField statusField;
	private final ImageComponent dbIcon;

	// for cleaning up the text after an interval
	private volatile Timer interval;


	public Statusbar() {
		LOG.debug("Statusbar() - init"); //$NON-NLS-1$
		if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("All swing components should be created in the EDT."); //$NON-NLS-1$
		}

		dbConnected = ImageManager.getImage("DBconnectedIcon"); //$NON-NLS-1$
		dbDisconnected = ImageManager.getImage("DBdisconnectedIcon"); //$NON-NLS-1$
		dbConnectedTooltip = Messages.getString("Statusbar.DBconnected"); //$NON-NLS-1$
		dbDisconnectedTooltip = Messages.getString("Statusbar.DBdisconnected"); //$NON-NLS-1$		
		dbIcon = new ImageComponent(dbDisconnected);
		statusField = new JTextField();
	}


	@PostConstruct
	protected void postConstruct() { // NO_UCD
		LOG.debug("postConstruct() called"); //$NON-NLS-1$
		initComponents();
	}


	private void initComponents() {
		setLayout(new BoxLayout(this, X_AXIS));

		statusField.setBorder(new EmptyBorder(0,5,0,0));
		statusField.setEditable(false);
		add(statusField);
		add(dbIcon);

		// --- minor wiring:

		MouseInputAdapter counter = new MouseInputAdapter() {
			@Override
			public void mousePressed(MouseEvent evt) {
				if ( evt.getClickCount() == 3 ) {
					Shell.doExecute();
				}
			}
		};

		statusField.addMouseListener(counter);
		dbIcon.addMouseListener(counter);
		this.addMouseListener(counter);
	}


	public void onAnyDataSourceEvent(@Observes final IBasicDataSource dataSource) { // NO_UCD
		LOG.debug("dataSource.isClosed(): " + ((dataSource==null)?"Datasource is null":dataSource.isClosed()));
		if ( (dataSource != null) && (!dataSource.isClosed()) ) {
			LOG.debug("setIconDbConnected");
			dbIcon.setIcon(dbConnected);
			dbIcon.setToolTipText(dbConnectedTooltip);
			showStatus(dbConnectedTooltip, 2500);
		} else {
			LOG.debug("setIconDbDisconnected");
			dbIcon.setIcon(dbDisconnected);
			dbIcon.setToolTipText(dbDisconnectedTooltip);
			showStatus(dbDisconnectedTooltip, 2500);
		}
	}

	public void onFocusedApplicationEvent(@Observes final ApplicationFocused container) { // NO_UCD
		LOG.debug("onFocusedApplicationEvent called, container is: '{}' ",  container); //$NON-NLS-1$
		final ICandidateTO candidate = (ICandidateTO) container.getContent();
		if ((candidate != null)
				&&  (candidate.getLastName() != null) 
				&&  (candidate.getFirstName() != null)) {
			// only set if there are first- and last name
			final String info = Messages.getString("Statusbar.SelectedCandidateInfo",  //$NON-NLS-1$
					new Object[] {candidate.getLastName(), candidate.getFirstName()});
			showStatus(info, 2500);
		}
	}

	public void onFocusedCandidateCollectionEvent(@Observes final ApplicationCollectionFocused container) { // NO_UCD
		LOG.debug("onFocusedCandidateCollectionEvent called, container is: '{}' ",  container); //$NON-NLS-1$
		final int count =  container.get().size();
		if (count > 0) {
			final String info = Messages.getString("Statusbar.SelectedCandidatesInfo", //$NON-NLS-1$ 
					new Object[] {Integer.valueOf(count)}); 
			showStatus(info, 2500);
		}
	}

	public synchronized void showStatus(final String message) {
		showStatus(message, 0);
	}

	public synchronized void showStatus(final String message, final int time) {
		// we need to run in EDT here
		if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("Need to run in EDT in order to set the status text!");
		}

		// destroy the old timer if it is still running since the text is overridden anyways
		if ((interval != null) && interval.isRunning()) {
			interval.stop();
		}

		if (time > 0) {
			// setup a timer to remove the message if the user provided a time
			interval = new Timer(time, new ActionListener() {
				@Override
				public void actionPerformed(final ActionEvent evt) {
					statusField.setText("");
				}
			});
			interval.setRepeats(false);
			interval.start();
		}
		statusField.setText(message);
	}

}
