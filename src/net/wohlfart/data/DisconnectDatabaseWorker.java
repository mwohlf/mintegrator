package net.wohlfart.data;


import java.awt.EventQueue;
import java.sql.SQLException;

import javax.enterprise.event.Event;
import javax.inject.Inject;

import net.wohlfart.CustomSwingWorker;
import net.wohlfart.Messages;
import net.wohlfart.gui.ErrorPane;
import net.wohlfart.gui.table.ApplicationListFull;
import net.wohlfart.util.GuiUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This swing worker disconnects the database, this is overkill
 *
 * @author michael
 */
public class DisconnectDatabaseWorker extends CustomSwingWorker<Void, Void> {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(DisconnectDatabaseWorker.class);
	
	@Inject 
	private Event<IBasicDataSource> dataSourceEvent;

	@Inject 
	private IBasicDataSource basicDataSource;

	@Inject 
	private Event<ApplicationListFull> fullCandidateListEvent;

	/**
	 * show a ProgressMonitor
	 */
	@Override
	public void doExecute() {
		// this must run in EDT
        if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The method doExecute() must be called from EDT."); //$NON-NLS-1$
		}
		if (basicDataSource.isClosed()) {
			LOG.info("datasource is closed, no disconnect needed, calling the done() method and returning"); //$NON-NLS-1$
			 // cancel the future object, this is needed so the get() method doesn't block 
			cancel(true); // (actually it throws an exception now)
			// done();
			return;
		}
		
		super.doExecute();
	}


	@Override
	protected Void doInBackground() {
		LOG.debug("doInBackground() - start"); //$NON-NLS-1$

		try {		
			setProgress(20);
			if (isCanceled()) {
				return null;
			}

			if (basicDataSource != null) {
				basicDataSource.close();
			}
			setProgress(80);
			if (isCanceled()) {
				return null;
			}

		} catch (SQLException ex) {
			LOG.error("doInBackground()", ex); //$NON-NLS-1$
			GuiUtilities.setDefaultCursor();
			progressMonitor.close();
			ErrorPane.showErrorDialog(ex, Messages.getString("DisconnectDatabaseWorker.SQLException1")); //$NON-NLS-1$

		} catch (Throwable ex) {
			LOG.error("doInBackground()", ex); //$NON-NLS-1$
			GuiUtilities.setDefaultCursor();
			progressMonitor.close();
			ErrorPane.showErrorDialog(ex, Messages.getString("DisconnectDatabaseWorker.SQLException1")); //$NON-NLS-1$

		} finally {
			queue.clearBlockedComponents();
			GuiUtilities.setDefaultCursor();
		}

		LOG.debug("doInBackground() - end"); //$NON-NLS-1$
		return null;
	}

	@Override
	public void done() {
		LOG.debug("invoke done() - start in: {}", this); //$NON-NLS-1$
		super.done();
		fullCandidateListEvent.fire(new ApplicationListFull(null));
		// fire an event with an empty/closed DataSource
		dataSourceEvent.fire(basicDataSource);
	}

}
