/**
 *
 */
package net.wohlfart.mail;

import java.sql.SQLException;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.inject.Singleton;

import net.wohlfart.data.ICandidateTO;
import net.wohlfart.data.persis.CandidateDAO;
import net.wohlfart.properties.IApplicationProperties;
import net.wohlfart.properties.PropertyKeys;

/**
 * this converts an IMailMessage to an ICandidateTO
 * 
 * @author michael
 */
@Singleton
public class ApplicationFactory {

    @Inject
    private CandidateDAO candidateDAO;
	@Inject
	private IApplicationProperties properties;
 
    /** Platform dependent newline.    */
    private static final String NEWLINE = System.getProperty("line.separator");


    // upload a dummy candidate with empty fields
    // then fill the fields and update the candidate
    public ICandidateTO createCandidate(final IMailMessage mail) throws SQLException {
  
        //ICandidateDAO candidateDAO = context.fab.getCandidateDAO();
        ICandidateTO candidate = candidateDAO.createCandidate();

        String emailAddress = mail.getSenderAddress();
        candidate.setEmailAddress(emailAddress);

        String fullName = mail.getSenderName();
        String firstName = getFirstName(fullName);
        String lastName = getLastName(fullName);
        candidate.setFirstName(firstName);
        candidate.setLastName(lastName);

        Date received = mail.getReceivedDate();
        candidate.setApplicationDate(received);

        // do we want the email as remark
        if (properties.getBoolean(PropertyKeys.SAVE_EMAIL_AS_REMARK, true)) {
            String subject = mail.getSubject();
            String remark = mail.getBody();
            //System.out.println("remark size: " + remark.length());
            candidate.setRemark(
                    "Betreff der Email: " + NEWLINE
                    + subject + NEWLINE + NEWLINE
                    + "Inhalt der Email: " + NEWLINE
                    + remark + NEWLINE);  // database has a text field/ LONG in oracle, this means no size limit...
        }

        // FIXME: validate parameters before uploading

//        boolean isValid = context.verifier.validateCandidate(candidateModel);
//        if (isValid) {
//            candidateDAO.updateCandidate(candidate);
//            //candidate.setSynchronized(true); // not sure if this is already done in the DAO
//        } else {
//            // not valid so it can't be synced
//            candidate.setSynchronized(false);
//        }

        return candidate;
    }




    private String getLastName(String fullName) {
        Pattern pattern;
        Matcher matcher;
        String result = fullName;

        // matching "FirstName LastName"
        pattern = Pattern.compile("([a-zA-Z]*) ([a-zA-Z]*)");
        matcher = pattern.matcher(fullName);
        if (matcher.find() && (matcher.groupCount() >= 2)) {
            result =  matcher.group(2);
        }

        // matching "LastName, FirstName"
        pattern = Pattern.compile("([a-zA-Z]*), ([a-zA-Z]*)");
        matcher = pattern.matcher(fullName);
        if (matcher.find() && (matcher.groupCount() >= 1)) {
            result =  matcher.group(1);
        }

        // matching "FirstName.LastName@somewhere.com"
        pattern = Pattern.compile("([a-zA-Z]*)\\.([a-zA-Z]*)@.*");
        matcher = pattern.matcher(fullName);
        if (matcher.find() && (matcher.groupCount() >= 2)) {
            result =  matcher.group(2);
        }


        // make first character uppercase
        if ( result.length() <= 1)  {
            result = result.toUpperCase();
        } else {
            result = result.substring(0, 1).toUpperCase() + result.substring( 1 ).toLowerCase();
        }
        return  result;
    }


    private String getFirstName(String fullName) {
        Pattern pattern;
        Matcher matcher;
        String result = fullName;

        // matching "FirstName LastName"
        pattern = Pattern.compile("([a-zA-Z]*) ([a-zA-Z]*)");
        matcher = pattern.matcher(fullName);
        if (matcher.find() && (matcher.groupCount() >= 1)) {
            result = matcher.group(1);
        }

        // matching "LastName, FirstName"
        pattern = Pattern.compile("([a-zA-Z]*), ([a-zA-Z]*)");
        matcher = pattern.matcher(fullName);
        if (matcher.find() && (matcher.groupCount() >= 2)) {
            result = matcher.group(2);
        }

        // matching "FirstName.LastName@somewhere.com"
        pattern = Pattern.compile("([a-zA-Z]*)\\.([a-zA-Z]*)@.*");
        matcher = pattern.matcher(fullName);
        if (matcher.find() && (matcher.groupCount() >= 1)) {
            result = matcher.group(1);
        }


        // make first character uppercase
        if ( result.length() <= 1)  {
            result = result.toUpperCase();
        } else {
            result = result.substring(0, 1).toUpperCase() + result.substring( 1 ).toLowerCase();
        }
        return result;

    }

}
