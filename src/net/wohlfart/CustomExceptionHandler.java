package net.wohlfart;

import java.lang.Thread.UncaughtExceptionHandler;

import net.wohlfart.gui.ErrorPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * An Exception handler to deal with uncaught Exceptions in Java >1.5.
 *
 *
 * @author michael
 */
public final class CustomExceptionHandler implements UncaughtExceptionHandler {
	/** Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(CustomExceptionHandler.class);

	/**
	 * Constructor, only used in the static install() Method.
	 */
	private CustomExceptionHandler() {
		// static class
	}


	/**
	 * Constructor to install this Exception Handler.
	 */
	static void install() {
		// set the DefaultExceptionHandler
		Thread.setDefaultUncaughtExceptionHandler(new CustomExceptionHandler());
	}

	/**
	 * this is called from the JRE when an uncaught Exception happens.
	 *
	 * @param thread    The Thread where the exception happend
	 * @param throwable The Exception
	 */
	@Override
	public void uncaughtException(final Thread thread, final Throwable throwable) {
		try {
			LOG.error("uncaught exception in thread: {}, stacktrace up next...", thread); //$NON-NLS-1$
			LOG.error("  stacktrace for uncaught exception: ", throwable); //$NON-NLS-1$
			ErrorPane.showErrorDialog(throwable, Messages.getString("DefaultExceptionHandler.DialogTitle")); //$NON-NLS-1$
		} catch (Exception ex) {
			// nothing else to do than dump the stack
			LOG.error("uncaught exeption handler triggered", ex); //$NON-NLS-1$
		}
	}

}
