package net.wohlfart.data;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Properties;

import javax.sql.DataSource;

import net.wohlfart.properties.IApplicationProperties;

public interface IBasicDataSource extends DataSource {

	void close() throws SQLException;

	boolean isClosed();

	Properties getQueries(Class<?> clazz);
	
	// FIXME: maybe we need a notfier to broadcast changes to the modified set of objects
	void addModified(Object object);
	
	boolean isModified(Object object);

	void removeModified(Object object);

	void clearModified();
		
	Collection<Object> getModified();

	boolean configure(IApplicationProperties properties);


}
