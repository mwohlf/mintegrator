/**
 *
 */
package net.wohlfart.mail.selection;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import net.wohlfart.Messages;
import net.wohlfart.mail.IMailMessage;

/**
 * @author michael
 *
 */
@SuppressWarnings("serial")
public class MailTableModel extends AbstractTableModel {

    // all the data
    private final ArrayList<IMailMessage> data;

    // the descriptors of the currently visible properties
    // note: this is empty until syncVisibleProperties is called
    private ArrayList<PropertyDescriptor> descriptors = new ArrayList<>();

    /* package private */
    MailTableModel(ArrayList<IMailMessage> list) {
        data = list;

        ArrayList<String> allProperties = new ArrayList<>();
        allProperties.add("selected");
        allProperties.add("subject");
        allProperties.add("attachmentCount");
        allProperties.add("senderAddress");
        allProperties.add("receivedDate");
        allProperties.add("senderName");
        allProperties.add("imported");
        setupDescriptors(allProperties);
    }


    private synchronized void setupDescriptors(ArrayList<String> properties) {
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(IMailMessage.class);
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            descriptors = new ArrayList<>();
            int count = properties.size();
             // fill the array with nulls:
            for (int i=0; i < count; i++) {
                descriptors.add(null);
            }
            // set the real value set doesn't shift the other values
            for (PropertyDescriptor descr:propertyDescriptors) {
                if (properties.contains(descr.getName())) {
                    int modelIndex = properties.indexOf(descr.getName());
                    descriptors.set(modelIndex, descr);
                }
            }
        } catch (IntrospectionException ex) {
            ex.printStackTrace();
        }
    }


    // TableModel methods

    @Override
	public synchronized int getColumnCount() {
        int returnint = descriptors.size();
        return returnint;
    }

    @Override
	public synchronized int getRowCount() {
        return data.size();
    }

    // rowcount starts at 0
    @Override
	public synchronized Object getValueAt(int row, int column) {
        Object returnObject = null;

        try {
            if (data.size() > row) {
                Object bean = data.get(row);
                PropertyDescriptor descriptor = descriptors.get(column);
                Method propertyGetter = descriptor.getReadMethod();
                returnObject = propertyGetter.invoke(bean, new Object[] {});
            } else {
                assert false:"rowindex too big in MailTabelModel: " + row + "/" + data.size();
            }
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (InvocationTargetException ex) {
            ex.printStackTrace();
        }
        //System.out.println("returned: " + returnObject);
        return returnObject;
    }



    @Override
    public synchronized String getColumnName(int column) {
        //System.out.println("getColumnName: " + column);
        String key = "unknown";
        assert column < descriptors.size(): "column index too big in CandidateTableDataModel to get column name: "
            + column + ">=" + descriptors.size() + this;
        assert column > -1: "column index too small in CandidateTableDataModel to get column name: "
            + column;
        PropertyDescriptor descriptor = descriptors.get(column);
        key = descriptor.getDisplayName();
        return Messages.getString("MailTable.Header." + key);
    }

    @Override
    public synchronized Class<?> getColumnClass(int column) {
        //System.out.println("getColumnClass: " + column);
        Class<?> returnClass; // = Object.class;
        assert column < descriptors.size(): "column index too big in MailTableModel: "
            + column + ">=" + descriptors.size() + this;
        assert column > -1: "column index too small in MailTableModel: "
            + column;
        PropertyDescriptor descriptor = descriptors.get(column);
        returnClass = descriptor.getPropertyType();
        return returnClass;
    }


    /* accessor for custom code */

    public synchronized IMailMessage getValueAtRow(int dataRow) {
        return data.get(dataRow);
    }

}
