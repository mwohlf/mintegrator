package net.wohlfart.data;

import java.awt.event.ActionEvent;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.AbstractAction;
import javax.swing.Action;

import net.wohlfart.ImageManager;
import net.wohlfart.Messages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An Action that disconnect the database
 * 
 * @author michael
 */
@SuppressWarnings("serial")
@Singleton
public class DisconnectDatabaseAction extends AbstractAction {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(DisconnectDatabaseAction.class);
    
    @Inject
    private Instance<DisconnectDatabaseWorker> disconnectDatabaseWorker;

    public DisconnectDatabaseAction() {
        putValue(Action.NAME, Messages.getString("DisconnectDatabase.ActionName")); //$NON-NLS-1$
        putValue(Action.SMALL_ICON, ImageManager.getImageIcon("DisconnectDBIcon16")); //$NON-NLS-1$
        putValue(Action.LARGE_ICON_KEY, ImageManager.getImageIcon("DisconnectDBIcon24")); //$NON-NLS-1$
        putValue(Action.SHORT_DESCRIPTION, Messages.getString("DisconnectDatabase.ShortDescriotion")); //$NON-NLS-1$
    }

    @Override
	public void actionPerformed(final ActionEvent arg0) {
    	LOG.debug("connect databae action triggered");
    	// not a singleton, we pull a new instance each time can't reuse SwingWorkers anyways
        disconnectDatabaseWorker.get() .doExecute();
    }


    public void onAnyDataSourceEvent(@Observes final IBasicDataSource dataSource) {
        LOG.debug("onAnyDataSourceEvent called, checking if the datasource is closed: '{}' "
        		+ "(from dataSource.isClosed())",  dataSource.isClosed());
        setEnabled(!dataSource.isClosed());
    }

}
