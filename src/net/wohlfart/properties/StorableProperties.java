package net.wohlfart.properties;

import java.util.Properties;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * this class adds some features to the properties class:
 * 
 *  - storing the properties as preferences in the user's registry
 * 
 * 
 * 
 * @author michael
 */
@SuppressWarnings("serial")
public class StorableProperties extends Properties {
	/** Logger for this class */
	private final static Logger LOGGER = LoggerFactory.getLogger(StorableProperties.class);

	/** name in the registry */
	private static final String APPLICATION_NODE_NAME = "MailIntegrator";


	void storeInRegistry() throws BackingStoreException {	
		Preferences applicationNode = getApplicationNode();

		for (Object key:keySet()) {
			if (!(key instanceof String)) {
				LOGGER.warn("there is a properties key that is not an instance of string"
						+ "its class is {}, to string returns {} can't persist its value in the database {}",
						new Object[] {key.getClass(), key.toString(), get(key)});
				continue;
			}
			String stringKey = (String) key;

			Object value = get(key);
			if (!(key instanceof String)) {
				LOGGER.warn("there is a properties key that has a value that is not an instance of string"
						+ "its class is {}, to string returns {} its key is {}",
						new Object[] {value.getClass(), value.toString(), stringKey});
				continue;
			}
			String stringValue = (String) get(key);
			applicationNode.put(stringKey, stringValue);
		}
		applicationNode.flush();
	}

	
	void readFromRegistry() throws BackingStoreException {		
		Preferences applicationNode = getApplicationNode();

		String[] keys = applicationNode.keys();
		
		for (String key : keys) {
			String value = applicationNode.get(key, null); // null as  default
			if (value != null) {
				// store the preferences as properties
				put(key, value);
			} else {
				LOGGER.warn("there is a key in the registry that has a null value, key is {}", key);
			}
		}
	}

	
	void cleanRegistry() throws BackingStoreException {
		Preferences applicationNode = getApplicationNode();
		applicationNode.removeNode();
		applicationNode.flush();
	}

	
	
	// -- private helpers
	
	private Preferences getApplicationNode() {
		Preferences userNode = Preferences.userRoot();		
		Preferences applicationNode = userNode.node(APPLICATION_NODE_NAME);
		return applicationNode;
	}

}
