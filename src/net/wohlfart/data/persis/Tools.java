package net.wohlfart.data.persis;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import net.wohlfart.data.IBasicDataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**s
 * @author michael
 *
 */
public class Tools {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(Tools.class);


    /**
     * SQL statement to get the next valid REFNR from the Z_REFNR table
     */
    private static final String GET_REFNR = "select REFNR from Z_REFNR"; //$NON-NLS-1$
    // any other update seems to work too here, maybe there is a triggersomewhere
    private static final String INC_REFNR = "update Z_REFNR set REFNR = REFNR + 1"; //$NON-NLS-1$
    // get max docnumber for a candidate HPT_REFNR is the candidates refnr
    private static final String MAX_DOK_ID = "select max(DOK_ID) from Z_DOKUMENT where HPT_REFNR = ?"; //$NON-NLS-1$


    //
    // bugfix: 15.04.2010 inc refnr then get refnr!!
    //
    public static synchronized  int getRefNr(IBasicDataSource dataSource) throws SQLException {
        Connection connection = dataSource.getConnection();
        connection.setAutoCommit(false);

        // ------------- BUGFIX: get and inc was swapped, first INC then GET !!!
        // inc the refnumber
        PreparedStatement incStmt = connection.prepareStatement(INC_REFNR);
        incStmt.executeUpdate();
        incStmt.close();

        PreparedStatement getStmt = connection.prepareStatement(GET_REFNR);
        ResultSet result = getStmt.executeQuery();

        // get the refnumber
        int refnr = -1;
        if (result.next()) {
            refnr = result.getInt(1);
        } else {
        	throw new SQLException("Can't get a Reference Number, returned result doesn't have a next() value");
            // LOGGER.error("Can't get a Reference Number"); //$NON-NLS-1$
        }
        getStmt.close();

        connection.commit();
        connection.setAutoCommit(true);
        connection.close();

        LOG.info("Returning new refnr: " + refnr);

        return refnr;
    }

    public static Date sqlDate(java.util.Date date) {
        if (date == null) {
            return null;
        }
        return new Date(date.getTime());

    }


    public static synchronized int getNextDocId(Connection connection, int mainRefNr) throws SQLException {
        PreparedStatement getStmt = connection.prepareStatement(MAX_DOK_ID);
        getStmt.setInt(1, mainRefNr);
        ResultSet result = getStmt.executeQuery();

        // get the refnumber
        int nextDocId = 0;
        if (result.next()) {
            nextDocId = result.getInt(1);
        }
        getStmt.close();

        nextDocId++;

        return nextDocId;
    }


    public static String getType(String name) {

        String filename = name.toLowerCase();

        if (filename.length() < 3 ) {
            return "xxx"; //$NON-NLS-1$
        } else if (filename.endsWith(".jpg")) { //$NON-NLS-1$
            return "jpg"; //$NON-NLS-1$
        } else if (filename.endsWith(".gif")) { //$NON-NLS-1$
            return "gif"; //$NON-NLS-1$
        } else if (filename.endsWith(".doc")) { //$NON-NLS-1$
            return "doc"; //$NON-NLS-1$
        } else if (filename.endsWith(".xls")) { //$NON-NLS-1$
            return "xls"; //$NON-NLS-1$
        } else if (filename.endsWith(".pdf")) { //$NON-NLS-1$
            return "pdf"; //$NON-NLS-1$
        } else if (filename.endsWith(".bat")) { //$NON-NLS-1$
            return "bat"; //$NON-NLS-1$
        } else if (filename.endsWith(".txt")) { //$NON-NLS-1$
            return "txt"; //$NON-NLS-1$
        } else if (filename.endsWith(".jpeg")) { //$NON-NLS-1$
            return "jpg"; //$NON-NLS-1$
        } else if (filename.endsWith(".odt")) { //$NON-NLS-1$
            return "odt"; //$NON-NLS-1$
        } else if (filename.endsWith(".ods")) { //$NON-NLS-1$
            return "ods"; //$NON-NLS-1$
        } else if (filename.endsWith(".odp")) { //$NON-NLS-1$
            return "odp"; //$NON-NLS-1$
        } else if (filename.endsWith(".zip")) { //$NON-NLS-1$
            return "zip"; //$NON-NLS-1$
        }
        return "xxx"; //$NON-NLS-1$
    }


    // Returns the contents of the file in a byte array.
    public static byte[] getBytesFromFile(File file) throws IOException {
        InputStream inputStream = new FileInputStream(file);

        // Get the size of the file
        long length = file.length();

        // You cannot create an array using a long type.
        // It needs to be an int type.
        // Before converting to an int type, check
        // to ensure that file is not larger than Integer.MAX_VALUE.
        if (length > Integer.MAX_VALUE) {
            // File is too large
            throw new IOException("File (" //$NON-NLS-1$
                    + file.getName()
                    + ") is too large: " //$NON-NLS-1$
                    + length
                    + " Byte, maximum " //$NON-NLS-1$
                    + Integer.MAX_VALUE
                    + " is allowed "); //$NON-NLS-1$
        }

        // Create the byte array to hold the data
        byte[] bytes = new byte[(int) length];

        // Read in the bytes
        int offset = 0;
        int numRead = 0;
        while ((offset < bytes.length)
                && ((numRead = inputStream.read(bytes, offset, bytes.length - offset)) >= 0) ) {
            offset += numRead;
        }

        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " //$NON-NLS-1$
                    + file.getName());
        }

        // Close the input stream and return bytes
        inputStream.close();
        return bytes;
    }


}
