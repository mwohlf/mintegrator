package net.wohlfart.gui.properties;

import java.util.AbstractList;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;

import net.wohlfart.gui.properties.application.AppPropertiesSheet;
import net.wohlfart.gui.properties.database.DatabaseSheet;
import net.wohlfart.gui.properties.look.LookAndFeelSheet;
import net.wohlfart.gui.properties.mail.MailsourceSheet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * ordered collection of property sheet used by this application
 * this is a weld component, do not instantiate manually unless you know what you are doing
 * 
 * @author michael
 */
@Singleton
public class DefaultPropertySheets extends AbstractList<IPropertySheet> {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(DefaultPropertySheets.class);

	
	@Inject
	private DatabaseSheet databaseSheet;
	
	@Inject
	private LookAndFeelSheet lookAndFeelSheet;
	
	@Inject
	private AppPropertiesSheet appPropertiesSheet;
	
	@Inject
	private MailsourceSheet mailsourcePropertiesSheet;

	
	// initialized in postConstruct
	IPropertySheet[] content;
	
	
	DefaultPropertySheets() {
		// nothing to do
		// note: we can't populate the content here since the elements are not yet injected
	}
	
	@Override
	public IPropertySheet get(int i) {
		return content[i];
	}
	
	@Override
	public int size() {
		return content.length;
	}
	
    @PostConstruct
    protected void postConstruct() { // NO_UCD
    	LOG.debug("postConstruct() called"); //$NON-NLS-1$
		content = new IPropertySheet[] {
				databaseSheet,
				lookAndFeelSheet,
				appPropertiesSheet,
				mailsourcePropertiesSheet
		};		
	}


}
