package net.wohlfart.gui.table;

import java.awt.Dimension;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.Enumeration;
import java.util.EventObject;

import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * View part of the custom table model-view set
 * 
 * 
 * this table prevents editing a table cell if the cell holds the focus and someone clicks
 * a ctrl-<something key> like used for accelerators
 * 
 * @author michael
 */
@SuppressWarnings("serial")
class CustomTable extends JTable {
	/** Logger for this class */
	private static final Logger LOG = LoggerFactory.getLogger(CustomTable.class);

	public CustomTable() {
		super();
	}

	@Override
	public boolean editCellAt(int arg0, int arg1, EventObject evt) {
		if ((evt != null) 
				&& (evt instanceof KeyEvent) 
				&& (((KeyEvent)evt).getModifiers() == InputEvent.CTRL_MASK)) {
			return false;
		}
		return super.editCellAt(arg0, arg1, evt);
	}
	
	@Override
    protected JTableHeader createDefaultTableHeader() {
		//columnModel.setColumnMargin(10);
		
		Enumeration<TableColumn> enumeration = columnModel.getColumns();
		LOG.info("createDefaultTableHeader with columnModel: {}, column count: {}", columnModel, columnModel.getColumnCount());
		while (enumeration.hasMoreElements()) {
			TableColumn elem = enumeration.nextElement();
			LOG.info("   elem: {}", elem);
			elem.setResizable(false);
		}	
        return new JTableHeader(columnModel);
    }
	
	@Override
	protected TableModel createDefaultDataModel() {
		return super.createDefaultDataModel();
	};
		
	
	@Override
	public void setColumnModel(final TableColumnModel columnModel) {
		//columnModel.setColumnMargin(50);
		LOG.debug("setColumnModel with columnModel: {}, column count: {}", columnModel, columnModel.getColumnCount());
		super.setColumnModel(columnModel);
	}

	
	@Override
    public TableColumnModel getColumnModel() {
		LOG.debug("getColumnModel with columnModel: {}, column count: {}", columnModel, columnModel.getColumnCount());
        return super.getColumnModel();
    }

	@Override
	public void setAutoResizeMode(int mode) {
		LOG.debug("setAutoResizeMode called, mode: {}", mode);
		super.setAutoResizeMode(mode);
	};

	
	@Override
	public Dimension getPreferredScrollableViewportSize()
	{	
		Dimension size = super.getPreferredScrollableViewportSize();
		Dimension result = new Dimension(Math.min(getPreferredSize().width, size.width), size.height);
		LOG.debug("getPreferredScrollableViewportSize called, size: {}, result: {}", 
				new Object[] {size, result} );
		return  result;
	}

}
