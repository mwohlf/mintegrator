/**
 *
 */
package net.wohlfart.gui.editor;

import static java.awt.AWTKeyStroke.getAWTKeyStroke;
import static java.awt.KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS;
import static java.awt.KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS;

import java.awt.AWTKeyStroke;
import java.awt.Dimension;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyDescriptor;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import net.wohlfart.gui.event.ApplicationFocused;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author michael
 *
 */
@SuppressWarnings("serial")
public class BeanPropertyTextArea extends JScrollPane implements IFocusedApplicationListener {
	/** Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(BeanPropertyTextArea.class);

	private final String propertyName;
	private final LocalPropertyChangeListener listener;

	private Object bean; // proxy
	private PropertyDescriptor propertyDescriptor;

	private VetoableChangeSupport changeSupport; // we need to remember the last change support in order to unregister

	private boolean isFiring = false; // endless loop protection

    private final JTextArea innerTextArea = new JTextArea();

       
    public BeanPropertyTextArea(final Class<?> clazz, final String propertyName) {
		LOG.info("Constructor called for {}", this);

        AWTKeyStroke  forward =  getAWTKeyStroke(java.awt.event.KeyEvent.VK_TAB, 0);
        AWTKeyStroke  backward =  getAWTKeyStroke(java.awt.event.KeyEvent.VK_TAB, java.awt.event.InputEvent.SHIFT_DOWN_MASK);

        Set<AWTKeyStroke> forwardSet = new HashSet<>();
        forwardSet.add(forward);

        Set<AWTKeyStroke> backwardSet = new HashSet<>();
        backwardSet.add(backward);

        innerTextArea.setFocusTraversalKeys(FORWARD_TRAVERSAL_KEYS, forwardSet);
        innerTextArea.setFocusTraversalKeys(BACKWARD_TRAVERSAL_KEYS, backwardSet);
        setViewportView(innerTextArea);

        innerTextArea.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent arg0) {
				copyModel2Bean();
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				copyModel2Bean();
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				copyModel2Bean();
			}
		});

        innerTextArea.setTransferHandler(new TextTransferHandler());
        innerTextArea.setDragEnabled(true);
        innerTextArea.setLineWrap(false);
        
		this.listener = new LocalPropertyChangeListener();
		this.propertyName = propertyName;
		setupDescriptor(clazz, propertyName);
    }
	
	@Override
	public void setFocusedApplication(final ApplicationFocused container) {
		this.bean = container.getContent();
		VetoableChangeSupport changeSupport = container.getChangeSupport();
		
		if (this.changeSupport != null) { // might be null on application start
			this.changeSupport.removeVetoableChangeListener(propertyName, listener);
		}
		this.changeSupport = changeSupport;
		if (this.changeSupport != null) { // might be null on shutdown
			this.changeSupport.addVetoableChangeListener(propertyName, listener);
		}
		
		isFiring = true; // we don't want the model to fire back
		copyBean2Model(); // FIXME: using a document model might do async changes to the model
		                  //        the change event might be delayed and after the next line!!!
		isFiring = false;
	}

	protected void setupDescriptor(final Class<?> clazz, final String propertyName) {
		try {
			propertyDescriptor = getDescriptor(clazz, propertyName);
			if (propertyDescriptor == null) {
				LOG.error("property descriptor is null for property name '{}'", propertyName);
			}
		} catch (IntrospectionException ex) {
			LOG.error("Error setting property descriptor", ex);
		}
	}

	protected PropertyDescriptor getDescriptor(final Class<?> clazz, final String propertyName)
			throws IntrospectionException {
		// create a property name to descriptor map
		final HashMap<String, PropertyDescriptor> tempDescriptorMap = new HashMap<>();
		BeanInfo beanInfo = Introspector.getBeanInfo(clazz);
		PropertyDescriptor[] propertyDescriptors = beanInfo
				.getPropertyDescriptors();
		for (PropertyDescriptor descriptor : propertyDescriptors) {
			tempDescriptorMap.put(descriptor.getName(), descriptor);
		}
		return tempDescriptorMap.get(propertyName);
	}

	protected class LocalPropertyChangeListener implements VetoableChangeListener {
		@Override
		public void vetoableChange(PropertyChangeEvent evt) {
			if (!isFiring) {
				Object value = evt.getNewValue();
				innerTextArea.setText(value.toString());
			}
		}
	}

    public void setColumns(int columns) {
        innerTextArea.setColumns(columns);
    }

    public void setRows(int rows) {
        innerTextArea.setRows(rows);
    }

    public void setHorizontalScrollbar(final boolean isEnabled) {
        if (isEnabled) {
            setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_AS_NEEDED);
        } else {
            setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_NEVER);
            innerTextArea.setLineWrap(true);
        }
    }
    
    @Override
    public Dimension getMinimumSize() {
    	return innerTextArea.getPreferredScrollableViewportSize();
    };
    
    

	protected void copyModel2Bean() {
		LOG.debug("bean is {}, isFiring is {}", bean, isFiring);
		if (isFiring) {
			return;
		}
		if ((BeanPropertyTextArea.this.bean == null)
				|| (BeanPropertyTextArea.this.propertyDescriptor == null)) {
			LOG.info(
					"skipping setter action, bean is {}, propertyDescriptor is {}, host is {}",
					new Object[] { bean, propertyDescriptor,
							BeanPropertyTextArea.this });
			return;
		}
		try {
			isFiring = true;
			propertyDescriptor.getWriteMethod().invoke(bean, innerTextArea.getText());
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException ex) {
			LOG.error("Error setting text property", ex);
		} finally {
			isFiring = false;
		}
	}

	protected void copyBean2Model() {
		LOG.debug("bean is {}", bean);
		if ((BeanPropertyTextArea.this.bean == null)
				|| (BeanPropertyTextArea.this.propertyDescriptor == null)) {
			LOG.info(
					"skipping setter action, bean is {}, propertyDescriptor is {}, host is {}",
					new Object[] { bean, propertyDescriptor,
							BeanPropertyTextArea.this });
			return;
		}
		try {
			Object value = propertyDescriptor.getReadMethod().invoke(bean,
					new Object[] {});
			innerTextArea.setText(value == null ? "" : value.toString());
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException ex) {
			LOG.error("Error setting text property", ex);
		}
	}

	@Override
	public String toString() {
		return getClass().getName() + "@" + Integer.toHexString(hashCode());
	}

}
