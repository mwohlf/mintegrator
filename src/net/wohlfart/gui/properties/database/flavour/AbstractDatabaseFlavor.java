package net.wohlfart.gui.properties.database.flavour;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.data.ICandidateTO;
import net.wohlfart.data.IFolderTO;
import net.wohlfart.data.IJobOfferTO;
import net.wohlfart.data.IMediumTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// package private
abstract class AbstractDatabaseFlavor {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(AbstractDatabaseFlavor.class);

	private final String id;
	private final HashMap<Class<?>,String> map;

	// package private
	AbstractDatabaseFlavor(final String id) {
		// only one instance per application otherwise we have to override equals/hash ...
		this.id = id;
		this.map = getMap();
	}

	//@Override
	public String getId() {
		return id;
	}

	@SuppressWarnings("serial")
	protected HashMap<Class<?>,String> getMap() {
		return new HashMap<Class<?>,String>() {{ // NO_UCD
			put(ICandidateTO.class, "CandidateTOdefault.xml");
			put(IFolderTO.class, "FolderTOdefault.xml");
			put(IJobOfferTO.class, "JobOfferTOdefault.xml");
			put(IMediumTO.class, "MediumTOdefault.xml");
		}};
	}

	//@Override
	public synchronized Properties getQueries(Class<?> clazz) {
		String filename = getFilenameForClass(clazz);
		if ((filename == null) || (filename.trim().length() == 0)) {
			throw new IllegalArgumentException("filename is null for " + clazz.getCanonicalName());
		}		
		return getPropertiesFromFile(filename);
	}

	private String getFilenameForClass(Class<?> clazz) {
		return map.get(clazz);
	}

	protected Properties getPropertiesFromFile(final String filename) {
		Properties properties = new Properties();
		InputStream is = IBasicDataSource.class.getResourceAsStream(filename);       
		try {
			if (is != null) {
				properties.loadFromXML(is);
				is.close();
			} else {
				LOG.error("empty properties for resource '{}', this is probably not intended", filename);
			}
		} catch (IOException ex) {
			LOG.error("failed to load properties file for SQL select definitions", ex);
		} 
		return properties;
	}

}
