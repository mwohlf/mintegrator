package net.wohlfart.action;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.inject.Singleton;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;

import net.wohlfart.HelpBrokerAdapter;
import net.wohlfart.ImageManager;
import net.wohlfart.Messages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author michael
 *
 */
@SuppressWarnings("serial")
@Singleton
public class HelpAction extends AbstractAction  {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(HelpAction.class);


    public HelpAction() {
        putValue(Action.NAME, Messages.getString("HelpAction.ActionName")); //$NON-NLS-1$
        putValue(Action.SMALL_ICON, ImageManager.getImageIcon("HelpIcon16")); //$NON-NLS-1$
        putValue(Action.LARGE_ICON_KEY, ImageManager.getImageIcon("HelpIcon24")); //$NON-NLS-1$
        putValue(Action.SHORT_DESCRIPTION, Messages.getString("HelpAction.ShortDescription")); //$NON-NLS-1$
        putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_H, InputEvent.CTRL_MASK));
    }

    @Override
	public void actionPerformed(final ActionEvent evt) {
    	LOG.info("help action triggered");
        HelpBrokerAdapter.getInstance().showHelpForAction(evt);
    }

}
