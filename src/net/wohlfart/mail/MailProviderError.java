package net.wohlfart.mail;

@SuppressWarnings("serial")
public class MailProviderError extends RuntimeException {

	public MailProviderError(final String string, final Exception cause) {
		super(string, cause);
	}

	public MailProviderError(final String string) {
		super(string);
	}

}
