/**
 *
 */
package net.wohlfart.mail.outlook;

import java.util.ArrayList;

import net.wohlfart.mail.IMailFolderTreeNode;
import net.wohlfart.mail.IMailMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moyosoft.connector.com.ComponentObjectModelException;
import com.moyosoft.connector.ms.outlook.folder.FoldersCollection;
import com.moyosoft.connector.ms.outlook.folder.FoldersIterator;
import com.moyosoft.connector.ms.outlook.folder.OutlookFolder;
import com.moyosoft.connector.ms.outlook.item.ItemType;

/**
 * @author michael
 *
 */
public class OutlookRootMailFolder implements IMailFolderTreeNode {
    /** Logger for this class */
    private final static Logger LOG = LoggerFactory.getLogger(OutlookRootMailFolder.class);

    private static final String ROOT_FOLDER_NAME = "Outlook";

    private ArrayList<IMailFolderTreeNode> folderArray;

    private final ArrayList<IMailMessage> mailArray = new ArrayList<>();

    // package private
    OutlookRootMailFolder(FoldersCollection folders) {
        setFolderArray(folders);
    }

    @Override
	public String getName() {
        return ROOT_FOLDER_NAME;
    }


    @Override
	public ArrayList<IMailMessage> getMailMessages() {
        return mailArray;
    }


    @Override
	public ArrayList<IMailFolderTreeNode> getSubFolder() {
        return folderArray;
    }




    private void setFolderArray(FoldersCollection folders) {
        folderArray = new ArrayList<>();

        for(FoldersIterator it = folders.iterator(); it.hasNext();) {
            Object object = it.next();
            // Check the item is a folder
            if ( (object != null) && (object instanceof OutlookFolder)) {
                OutlookFolder folder = (OutlookFolder)object;
                try {
                    if (folder.getDefaultItemType() == ItemType.MAIL) {
                        folderArray.add(new OutlookMailSource(folder));
                    }
                } catch (ComponentObjectModelException ex){
                    LOG.error("can't add a folder", ex);
                }
            }
        }
    }

    @Override
	public IMailFolderTreeNode getParentFolder() {
        // no parent folder here
        return null;
    }



}
