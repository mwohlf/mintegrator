/**
 *
 */
package net.wohlfart.mail;

import java.util.ArrayList;

/**
 * this is where we import the mails from
 * 
 * 
 * @author michael
 */
public interface IMailFolderTreeNode {

    String getName();

    ArrayList<IMailMessage> getMailMessages();

    ArrayList<IMailFolderTreeNode> getSubFolder();

    IMailFolderTreeNode getParentFolder();

}
