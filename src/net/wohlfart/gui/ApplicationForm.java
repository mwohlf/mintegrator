package net.wohlfart.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingWorker;

import net.wohlfart.Messages;
import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.data.ICandidateTO;
import net.wohlfart.data.persis.JobOfferDAO;
import net.wohlfart.data.persis.JobOfferSelector;
import net.wohlfart.data.persis.JobOfferTO;
import net.wohlfart.data.persis.MediumDAO;
import net.wohlfart.data.persis.MediumTO;
import net.wohlfart.gui.editor.BeanPropertyComboBox;
import net.wohlfart.gui.editor.BeanPropertyDateChooser;
import net.wohlfart.gui.editor.BeanPropertyRadioButton;
import net.wohlfart.gui.editor.BeanPropertyTextArea;
import net.wohlfart.gui.editor.BeanPropertyTextfield;
import net.wohlfart.gui.editor.IFocusedApplicationListener;
import net.wohlfart.gui.editor.ISelectedValueConverter;
import net.wohlfart.gui.event.ApplicationFocused;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author michael
 */
@SuppressWarnings("serial")
@Singleton
class ApplicationForm extends JComponent {
	/** Logger for this class */
	private static final Logger LOG = LoggerFactory.getLogger(ApplicationForm.class);

	private static final int ZIPFIELD_WIDTH = 70;

	private final BeanPropertyComboBox<MediumTO> medium;
	private final BeanPropertyComboBox<JobOfferTO> applicationTitle;
	private final BeanPropertyComboBox<JobOfferTO> jobOfferDescription;
	private final BeanPropertyComboBox<JobOfferTO> division;

	private final BeanPropertyTextfield emailAddress;
	private final BeanPropertyTextfield phoneNumber;
	private final BeanPropertyTextfield firstName;
	private final BeanPropertyTextfield lastName;
	private final BeanPropertyTextfield zipCode;
	private final BeanPropertyTextfield city;
	private final BeanPropertyTextArea remark;
	private final BeanPropertyTextArea streetAndNumber;
	private final BeanPropertyRadioButton gender;

	private final BeanPropertyDateChooser dateOfBirth;
	private final BeanPropertyDateChooser applicationDate;


	private final Set<IFocusedApplicationListener> listeners = new HashSet<>();
	private final LocalPropertyChangeListener propertyChangeDispatcher;
	// FIXME: we should get this from the Session/DataSource
	private VetoableChangeSupport changeSupport; // we need to remember the last change support in order to unregister 


	@Inject 
	private MediumDAO mediumDAO;
	@Inject
	private JobOfferDAO jobOfferDAO;

	private volatile List<JobOfferTO> jobOffers;
	private volatile List<JobOfferTO> filteredJobOffers;

	private volatile List<MediumTO> mediums;


	ApplicationForm() {
		LOG.debug("ApplicationFormPanel() - init"); //$NON-NLS-1$
		if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("All swing components should be created in the EDT."); //$NON-NLS-1$
		}

		
		// --- setup the medium JComboBox
		final ISelectedValueConverter<MediumTO> mediumConverter = new ISelectedValueConverter<MediumTO>(){
			@Override
			public String convert2ViewString(final MediumTO value) {
				return value==null?" ":value.getShortDescription();
			}			
			@Override
			public String convert2BeanProperty(final MediumTO value) {
				return value==null?null:value.getShortDescription();
			}			
		};
		medium = new BeanPropertyComboBox<>(ICandidateTO.class, "medium", mediumConverter);
		medium.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				// nothing to do here
			}
		});
		listeners.add(medium);


		// --- setup the applicationTitle JComboBox
		final ISelectedValueConverter<JobOfferTO> applicationTitleConverter = new ISelectedValueConverter<JobOfferTO>(){
			@Override
			public String convert2ViewString(final JobOfferTO value) {
				return value==null?" ":value.getProfession();
			}			
			@Override
			public String convert2BeanProperty(final JobOfferTO value) {
				return value==null?null:value.getProfession();
			}			
		};
		applicationTitle = new BeanPropertyComboBox<>(ICandidateTO.class, "applicationTitle",
				applicationTitleConverter);
		applicationTitle.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				String selectedString = applicationTitleConverter
						.convert2BeanProperty((JobOfferTO)applicationTitle.getSelectedItem());
				filteredJobOffers = new ArrayList<>();
				for (JobOfferTO job : jobOffers) {
					String jobString = applicationTitleConverter.convert2BeanProperty(job);
					if ((selectedString == null) || selectedString.equals(jobString)) {
						filteredJobOffers.add(job);
					}
				}
				applicationTitle.setElements(jobOffers);
				jobOfferDescription.setElements(filteredJobOffers);
				division.setElements(filteredJobOffers);
			}
		});
		listeners.add(applicationTitle);


		// --- setup the jobOfferDescription JComboBox
		final ISelectedValueConverter<JobOfferTO> jobOfferDescriptionConverter = new ISelectedValueConverter<JobOfferTO>(){
			@Override
			public String convert2ViewString(final JobOfferTO value) {
				return value==null?" ":value.getShortDescription();
			}			
			@Override
			public String convert2BeanProperty(final JobOfferTO value) {
				return value==null?null:value.getShortDescription();
			}			
		};
		jobOfferDescription = new BeanPropertyComboBox<>(ICandidateTO.class, "jobOfferDescription", 
				jobOfferDescriptionConverter);
		jobOfferDescription.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				String selectedString = jobOfferDescriptionConverter
						.convert2BeanProperty((JobOfferTO)jobOfferDescription.getSelectedItem());
				filteredJobOffers = new ArrayList<>();
				for (JobOfferTO job : jobOffers) {
					String jobString = jobOfferDescriptionConverter.convert2BeanProperty(job);
					if ((selectedString == null) || selectedString.equals(jobString)) {
						filteredJobOffers.add(job);
					}
				}
				applicationTitle.setElements(filteredJobOffers);
				jobOfferDescription.setElements(jobOffers);
				division.setElements(filteredJobOffers);
			}
		});
		listeners.add(jobOfferDescription);


		// --- setup the division JComboBox
		final ISelectedValueConverter<JobOfferTO> divisionConverter = new ISelectedValueConverter<JobOfferTO>(){
			@Override
			public String convert2ViewString(final JobOfferTO value) {
				return value==null?" ":value.getDivision();
			}			
			@Override
			public String convert2BeanProperty(final JobOfferTO value) {
				return value==null?null:value.getDivision();
			}			
		};
		division = new BeanPropertyComboBox<>(ICandidateTO.class, "division",
				divisionConverter);
		division.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				String selectedString = divisionConverter
						.convert2BeanProperty((JobOfferTO)division.getSelectedItem());
				filteredJobOffers = new ArrayList<>();
				for (JobOfferTO job : jobOffers) {
					String jobString = divisionConverter.convert2BeanProperty(job);
					if ((selectedString == null) || selectedString.equals(jobString)) {
						filteredJobOffers.add(job);
					}
				}
				applicationTitle.setElements(filteredJobOffers);
				jobOfferDescription.setElements(filteredJobOffers);
				division.setElements(jobOffers);
			}
		});
		listeners.add(division);


		applicationDate = new BeanPropertyDateChooser(ICandidateTO.class, "applicationDate");
		listeners.add(applicationDate);

		dateOfBirth = new BeanPropertyDateChooser(ICandidateTO.class, "dateOfBirth");
		listeners.add(dateOfBirth);


		firstName = new BeanPropertyTextfield(ICandidateTO.class, "firstName");
		listeners.add(firstName);

		lastName = new BeanPropertyTextfield(ICandidateTO.class, "lastName");
		listeners.add(lastName);

		emailAddress = new BeanPropertyTextfield(ICandidateTO.class, "emailAddress");
		listeners.add(emailAddress);

		phoneNumber = new BeanPropertyTextfield(ICandidateTO.class, "phoneNumber");
		listeners.add(phoneNumber);

		city = new BeanPropertyTextfield(ICandidateTO.class, "city");
		listeners.add(city);

		remark = new BeanPropertyTextArea(ICandidateTO.class, "remark") {{
			setColumns(20);
			setRows(10);
		}
		@Override
		public Dimension getMinimumSize() {
			return getPreferredSize();
		}
		@Override
		public Dimension getMaximumSize() {
			return getPreferredSize();
		}};
		listeners.add(remark);

		streetAndNumber = new BeanPropertyTextArea(ICandidateTO.class, "streetAndNumber") {{
			setColumns(20);
			setRows(2);
			setHorizontalScrollbar(false);
		}
		@Override
		public Dimension getMinimumSize() {
			return getPreferredSize();
		}
		@Override
		public Dimension getMaximumSize() {
			return getPreferredSize();
		}};
		listeners.add(streetAndNumber);

		zipCode = new BeanPropertyTextfield(ICandidateTO.class, "zipCode") {
			private final Dimension dim = new Dimension(ZIPFIELD_WIDTH, (int)super.getMinimumSize().getHeight());
			@Override
			public Dimension getMinimumSize() {
				return dim;
			}
			@Override
			public Dimension getPreferredSize() {
				return dim;
			}
			@Override
			public Dimension getMaximumSize() {
				return dim;
			}
		};
		listeners.add(zipCode);

		gender = new BeanPropertyRadioButton(ICandidateTO.class, "gender");
		listeners.add(gender);


		// private inner class to dispatch property change events to the editors
		propertyChangeDispatcher = new LocalPropertyChangeListener();
	}


    @PostConstruct
    protected void postConstruct() { // NO_UCD
    	LOG.debug("postConstruct() called"); //$NON-NLS-1$
		initComponents();
	}


	private void initComponents() {
		// do the layout
		setLayout(new GridBagLayout());

		// constraints for the labels in the first column
		GridBagConstraints labelConst = new GridBagConstraints();
		labelConst.gridwidth = 1;
		labelConst.insets = new Insets(1,3,0,10);
		labelConst.anchor = GridBagConstraints.WEST;
		labelConst.fill = GridBagConstraints.NONE;
		labelConst.gridx = 0;
		labelConst.gridy = 0;
		labelConst.weightx = labelConst.weighty = 0; 

		// constraints for the input components filling up 5 columns
		GridBagConstraints fullSizeConst = new GridBagConstraints();
		fullSizeConst.gridwidth = 5;
		fullSizeConst.insets = new Insets(1,0,0,1);
		fullSizeConst.anchor = GridBagConstraints.WEST;
		fullSizeConst.fill = GridBagConstraints.HORIZONTAL;
		fullSizeConst.gridx = 1;
		fullSizeConst.gridy = 0;
		// see: http://stackoverflow.com/questions/4226755/java-jscrollpane-doesnt-work-with-gridbaglayout
		fullSizeConst.weightx = 0.5;
		fullSizeConst.weighty = 0; 

		// constrains for some more labels
		GridBagConstraints partSizeConst = new GridBagConstraints();
		partSizeConst.gridwidth = 1;
		partSizeConst.insets = new Insets(1,0,0,1);
		partSizeConst.anchor = GridBagConstraints.WEST;
		partSizeConst.fill = GridBagConstraints.HORIZONTAL;
		partSizeConst.gridx = 0;
		partSizeConst.gridy = 0;
		partSizeConst.weightx = partSizeConst.weighty = 0; 


		// ------------- end constraint definitions ------------


		// --- line start application date and medium
		add(new JLabel(Messages.getString("ApplicationFormPanel.applicationDate")), labelConst);
		partSizeConst.gridx = 1;
		partSizeConst.gridwidth = 1;
		add(applicationDate, partSizeConst);
		partSizeConst.gridx = 2;
		partSizeConst.gridwidth = 4;
		add(new JComponent() {{
			setLayout(new BorderLayout());
			add(new JLabel(Messages.getString("ApplicationFormPanel.medium")), BorderLayout.WEST);
			add(medium, BorderLayout.CENTER);
		}}, partSizeConst);		
		partSizeConst.gridy = labelConst.gridy = ++fullSizeConst.gridy; // ---- line finished,  next line


		// --- application title
		add(new JLabel(Messages.getString("ApplicationFormPanel.applicationProfession")), labelConst);
		add(applicationTitle, fullSizeConst);
		partSizeConst.gridy = labelConst.gridy = ++fullSizeConst.gridy; // ---- line finished,  next line


		// --- job offer description
		add(new JLabel(Messages.getString("ApplicationFormPanel.jobOfferShortDescription")), labelConst);
		add(jobOfferDescription, fullSizeConst);
		partSizeConst.gridy = labelConst.gridy = ++fullSizeConst.gridy; // ---- line finished,  next line


		// --- division/werk
		add(new JLabel(Messages.getString("ApplicationFormPanel.division")), labelConst);
		add(division, fullSizeConst);
		partSizeConst.gridy = labelConst.gridy = ++fullSizeConst.gridy; // ---- line finished,  next line


		// --- first name
		add(new JLabel(Messages.getString("ApplicationFormPanel.firstName")), labelConst);
		add(firstName, fullSizeConst);
		partSizeConst.gridy = labelConst.gridy = ++fullSizeConst.gridy; // ---- line finished,  next line


		// --- last name
		add(new JLabel(Messages.getString("ApplicationFormPanel.lastName")), labelConst);
		add(lastName, fullSizeConst);
		partSizeConst.gridy = labelConst.gridy = ++fullSizeConst.gridy; // ---- line finished,  next line


		// -- address / street and number
		add(new JLabel(Messages.getString("ApplicationFormPanel.streetAndNumber")), labelConst);
		add(streetAndNumber, fullSizeConst);	
		partSizeConst.gridy = labelConst.gridy = ++fullSizeConst.gridy; // ---- line finished,  next line


		// --- zip code and city
		add(new JLabel(Messages.getString("ApplicationFormPanel.zipCode")), labelConst);
		partSizeConst.gridx = 1;
		partSizeConst.gridwidth = 1;
		add(zipCode, partSizeConst);
		partSizeConst.gridx = 2;
		partSizeConst.gridwidth = 4;
		add(new JComponent() {{
			setLayout(new BorderLayout());
			add(new JLabel(Messages.getString("ApplicationFormPanel.city")), BorderLayout.WEST);
			add(city, BorderLayout.CENTER);
		}}, partSizeConst);		
		partSizeConst.gridy = labelConst.gridy = ++fullSizeConst.gridy; // ---- line finished,  next line


		// --- email address
		add(new JLabel(Messages.getString("ApplicationFormPanel.emailAddress")), labelConst);
		add(emailAddress, fullSizeConst);
		partSizeConst.gridy = labelConst.gridy = ++fullSizeConst.gridy; // ---- line finished,  next line


		// --- phone number
		add(new JLabel(Messages.getString("ApplicationFormPanel.phoneNumber")), labelConst);
		add(phoneNumber, fullSizeConst);
		partSizeConst.gridy = labelConst.gridy = ++fullSizeConst.gridy; // ---- line finished,  next line


		// --- date of birth and gender
		add(new JLabel(Messages.getString("ApplicationFormPanel.dateOfBirth")), labelConst);
		partSizeConst.gridx = 1;
		partSizeConst.gridwidth = 1;
		add(dateOfBirth, partSizeConst);
		partSizeConst.gridx = 2;
		partSizeConst.gridwidth = 4;
		add(new JComponent() {{
			setLayout(new BorderLayout());
			add(new JLabel(Messages.getString("ApplicationFormPanel.gender")), BorderLayout.WEST);
			add(gender, BorderLayout.CENTER);
		}}, partSizeConst);			
		partSizeConst.gridy = labelConst.gridy = ++fullSizeConst.gridy; // ---- line finished,  next line


		// --- remark testarea
		add(new JLabel(Messages.getString("ApplicationFormPanel.remark")), labelConst);
		fullSizeConst.fill = GridBagConstraints.BOTH;
		fullSizeConst.weightx = fullSizeConst.weighty = 0.5;  // make it big
		add(remark, fullSizeConst);
		partSizeConst.gridy = labelConst.gridy = ++fullSizeConst.gridy; // ---- line finished,  next line
	}


	public void onFocusedCandidateEvent(@Observes final ApplicationFocused container) { // NO_UCD
		LOG.debug("onFocusedCandidateEvent called, container is: '{}' ",  container); // $NON-NLS-1$

		if (container.isEmpty()) {
			setEnabled(false); // nothing to edit
			if (changeSupport != null) {
				// remove from the old support
				changeSupport.removeVetoableChangeListener(propertyChangeDispatcher); 
				changeSupport = null;
			}

		} else {
			setEnabled(true);
			changeSupport = container.getChangeSupport();
			changeSupport.addVetoableChangeListener(propertyChangeDispatcher);

			// notify all our editors about the new bean
			for (IFocusedApplicationListener listener : listeners) {
				listener.setFocusedApplication(container);
			}
		}
		
		
		
	}


	public void onAnyDataSourceEvent(@Observes final IBasicDataSource dataSource) {
		LOG.debug("onAnyDataSourceEvent called, checking if the datasource is closed: '{}' " //$NON-NLS-1$
				+ "(from dataSource.isClosed())",  dataSource.isClosed()); //$NON-NLS-1$

		if (!dataSource.isClosed()) {
			setupMedium();
			setupJobOffer(JobOfferSelector.getSelectorForKey("SELECT_ALL"));
		} else {
			medium.setModel(new DefaultComboBoxModel<MediumTO>());
			applicationTitle.setModel(new DefaultComboBoxModel<JobOfferTO>());
			jobOfferDescription.setModel(new DefaultComboBoxModel<JobOfferTO>());
			division.setModel(new DefaultComboBoxModel<JobOfferTO>());
		}
	}


	@Override
	public void setEnabled(final boolean enabled) {
		super.setEnabled(enabled);		
		applicationDate.setEnabled(enabled);
		applicationTitle.setEnabled(enabled);
		jobOfferDescription.setEnabled(enabled);
		division.setEnabled(enabled);
		firstName.setEnabled(enabled);
		lastName.setEnabled(enabled);
		dateOfBirth.setEnabled(enabled);
		emailAddress.setEnabled(enabled);
		phoneNumber.setEnabled(enabled);
		remark.setEnabled(enabled);		
	}


	private void setupJobOffer(final JobOfferSelector selector) {
		new SwingWorker<List<JobOfferTO>, Void>() {

			@Override
			protected List<JobOfferTO> doInBackground() throws Exception {
				return jobOfferDAO.readJobOffers(selector);
			}

			@Override
			protected void done() {
				try {
					jobOffers = get();
					applicationTitle.setElements(jobOffers);
					jobOfferDescription.setElements(jobOffers);
					division.setElements(jobOffers);
				} catch (InterruptedException | ExecutionException ex) {
					LOG.error("failed to get data from DB", ex);
				}
			}

		} .execute();		
	}


	private void setupMedium() {	
		new SwingWorker<List<MediumTO>, Void>() {

			@Override
			protected List<MediumTO> doInBackground() throws Exception {
				return mediumDAO.readMedia();
			}

			@Override
			protected void done() {
				try {
					mediums = get();
					medium.setElements(mediums);
				} catch (InterruptedException | ExecutionException ex) {
					LOG.error("failed to get data from DB", ex);
				}
			}

		} .execute();		
	}


	protected class LocalPropertyChangeListener implements VetoableChangeListener {

		@Override
		public void vetoableChange(PropertyChangeEvent evt) {
			String propertyName = evt.getPropertyName();
			Object value = evt.getNewValue();
			// FIXME: nothing to do here yet
			LOG.debug("property change detected name is '{}', value is '{}'",
					propertyName, value);
		}
	}

}
