package net.wohlfart.data;

import java.util.Date;

/**
 * @author michael
 */
public interface IJobOfferTO {

    public abstract String getProfession();

    public abstract String getShortDescription();

    public abstract Integer getId();

    public abstract String getDivision();

    public abstract void setProfession(String jobTitle);

    public abstract void setShortDescription(String shortDescription);

    public abstract void setId(Integer id);

    public abstract void setDivision(String division);

    public abstract Date getStarted();

    public abstract String getFinished();

    public abstract Date getEnd();

    public abstract void setStarted(Date start);

    public abstract void setFinished(String finished);

    public abstract void setEnd(Date end);

}
