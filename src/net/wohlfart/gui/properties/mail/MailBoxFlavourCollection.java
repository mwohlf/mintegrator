package net.wohlfart.gui.properties.mail;

import java.util.HashMap;

@SuppressWarnings("serial")
public final class MailBoxFlavourCollection extends HashMap<String, IMailBoxFlavor> {

	// there keys are  used for i18n lookup and also as ID
	private static final String MOYOSOFT_OUTLOOK = "MOYOSOFT_OUTLOOK"; //$NON-NLS-1$
	private static final String POP = "POP"; //$NON-NLS-1$
	private static final String IMAP = "IMAP"; //$NON-NLS-1$
	private static final String TEXT = "TEXT"; //$NON-NLS-1$
	private static final String NULL = "null"; //$NON-NLS-1$

	
	private static MailBoxFlavourCollection instance;


	private MailBoxFlavourCollection() {
		addMailsource(new MoyosoftOutlook(MOYOSOFT_OUTLOOK));
		addMailsource(new Pop(POP));
		addMailsource(new Imap(IMAP));
		addMailsource(new Text(TEXT));
		addMailsource(new NullMailFlavor(NULL));
	}

	public static synchronized MailBoxFlavourCollection getInstance() {
		if (instance == null) {
			instance = new MailBoxFlavourCollection();
		}
		return instance;
	}


	private void addMailsource(IMailBoxFlavor mailsource) {
		put(mailsource.getId(), mailsource);
	}

	public Object getNullElement() {
		return get(NULL);
	}

}
