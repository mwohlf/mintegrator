package net.wohlfart.gui;

import java.awt.Dimension;
import java.awt.EventQueue;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JToolBar;

import net.wohlfart.action.ApplicationCreateAction;
import net.wohlfart.action.ApplicationDeleteAction;
import net.wohlfart.action.ApplicationUpdateAction;
import net.wohlfart.action.DocumentCreateAction;
import net.wohlfart.action.DocumentDeleteAction;
import net.wohlfart.action.ApplicationEmailImportAction;
import net.wohlfart.data.ConnectDatabaseAction;
import net.wohlfart.data.DisconnectDatabaseAction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author michael
 */
@SuppressWarnings("serial")
@Singleton
public class Toolbar extends JToolBar {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(Toolbar.class);

	@Inject
	private ShutdownAction shutdown;
	public static final String TOOLBUTTON_SHUTDOWN_NAME = "shutdown"; // for testing
	@Inject
	private ApplicationCreateAction createCandidate;
	@Inject
	private ApplicationDeleteAction deleteCandidate;
	@Inject
	private DocumentCreateAction createDocument;
	@Inject
	private DocumentDeleteAction deleteDocument;
	//    @Inject
	//    private SaveDocument saveDocument;
	@Inject
	private ApplicationUpdateAction updateCandidate;
	@Inject
	private ApplicationEmailImportAction importEmailFromFolder;


	//    @Inject
	//    private GlobalCutAction globalCutAction;
	//    @Inject
	//    private GlobalCopyAction globalCopyAction;
	//    @Inject
	//    private GlobalPasteAction globalPasteAction;


	//    @Inject
	//    private SetupDatabase setupDatabase;
	//    @Inject
	//    private SetupIntegrator setupIntegrator;
	//    @Inject
	//    private SetupOutlook setupOutlook;
	//    @Inject
	//    private SaveProperties saveProperties;
	//    @Inject
	//    private LoadProperties loadProperties;


	@Inject
	private ConnectDatabaseAction connectDatabaseAction; 
	@Inject
	private DisconnectDatabaseAction disconnectDatabaseAction;


	//    @Inject
	//    private HelpAction helpAction;
	//    @Inject
	//    private ShowAbout showAbout;



	/* package private */
	Toolbar() {
		LOG.debug("Toolbar() - init"); //$NON-NLS-1$
		if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("All swing components should be created in the EDT."); //$NON-NLS-1$
		}
	}

    @PostConstruct
    protected void postConstruct() { // NO_UCD
    	LOG.debug("postConstruct() called"); //$NON-NLS-1$
		initComponents();
	}

	private void initComponents() {
		add(new TextLessButton(shutdown) {{
			setName(TOOLBUTTON_SHUTDOWN_NAME);
		}});
		add(new TextLessButton(importEmailFromFolder) {{
		}});
		add(Box.createRigidArea(new Dimension(10,10)));
		add(new TextLessButton(createCandidate) {{
		}});
		add(new TextLessButton(deleteCandidate) {{
		}});
		add(Box.createRigidArea(new Dimension(10,10)));
		add(new TextLessButton(createDocument) {{
		}});
		add(new TextLessButton(deleteDocument) {{
		}});
		add(Box.createRigidArea(new Dimension(10,10)));
		add(new TextLessButton(connectDatabaseAction) {{
		}});
		add(new TextLessButton(disconnectDatabaseAction) {{
		}});
		add(Box.createRigidArea(new Dimension(10,10)));
		add(new TextLessButton(updateCandidate) {{
		}});
		add(Box.createGlue());
	}

	private class TextLessButton extends JButton {

		TextLessButton(AbstractAction action) {
			super(action);
		}

		@Override
		public
		String getText() {
			return "";
		}

	}

}
