package net.wohlfart.mail.outlook;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import net.wohlfart.Messages;
import net.wohlfart.mail.FolderTreeModel;
import net.wohlfart.mail.IMailFolderTreeNode;
import net.wohlfart.mail.MailProviderError;
import net.wohlfart.util.GuiUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moyosoft.connector.ms.outlook.Outlook;
import com.moyosoft.connector.ms.outlook.folder.FoldersCollection;


/**
 * view for setting up an Outlook folder
 * this component is not responsible for persisting the folder this means we don't to 
 * anything with the properties
 * 
 * we have a statusArea which can be used to show internal state to the user (e.g. error, ongoing tasks etc.)
 * 
 * 
 * @author michael
 */
@SuppressWarnings("serial")
public class MoyosoftOutlookSetupEditor extends JComponent {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(MoyosoftOutlookSetupEditor.class);

	private final static Dimension PREFERED_TREESIZE =  new Dimension(200,200);

	private final JTree outlookTree;
	private final JTextField folderNameField;
	// to display the error/success message when testing the connection
	private final JTextArea statusArea;

	private IMailFolderTreeNode rootFolder;
	protected volatile boolean initDataModelOngoing = false;

	// the result from the editing:
	protected volatile OutlookMailSource selectedFolder; // instanceof IMailFolderTreeNode 


	public MoyosoftOutlookSetupEditor() {
		// create the components
		outlookTree = createOutlookTree();
		folderNameField = new JTextField(50);
		folderNameField.setEditable(false);
		statusArea = createStatusArea();
		initComponents();
	}


	private void initComponents() {
		// do the layout
		setLayout(new GridBagLayout());

		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(1,0,1,0);
		constraints.anchor = GridBagConstraints.WEST;
		constraints.gridx = 0;
		constraints.gridy = 0;


		constraints.weightx = constraints.weighty = 0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(new JScrollPane() {{
			setViewportView(outlookTree);
			setPreferredSize(PREFERED_TREESIZE);
		}
		@Override
		public Dimension getMinimumSize() {
			return super.getPreferredSize();
		}
		}, constraints);

		constraints.gridy++;
		constraints.weightx = constraints.weighty = 0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(new JLabel(Messages.getString("OutlookDialog.SelectedFolder")), constraints); //$NON-NLS-1$

		constraints.gridy++;
		constraints.weightx = constraints.weighty = 0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(folderNameField, constraints);

		constraints.gridy++;
		constraints.weightx = constraints.weighty = 1;
		constraints.fill = GridBagConstraints.BOTH;
		add(statusArea, constraints);
	}

	/**
	 * to init the tree model, we insert a dummy node first, then try to get the outlook
	 * folder tree...
	 * 
	 * the user might not see the outlook dialog while we hang on the getRooltFolder() call
	 * this might lead to multiple calls to this method, also the cancel might trigger
	 * a data model reset call
	 * 
	 * FIXME: this is an ugly hoping between EDT and worker thread, 
	 *        also sync is not perfect for this method yet
	 */
	public void initDataModel() {
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("Outlook Database backend shouldn't be access in the EDT."); //$NON-NLS-1$
		}
		if (initDataModelOngoing) {
			LOG.error("call to initDataModel was aborded because there is already a call ongoing"); //$NON-NLS-1$
			return;
		}
		initDataModelOngoing = true;

		try {
			EventQueue.invokeAndWait(new Runnable(){
				@Override
				public void run() {
					GuiUtilities.setBusyCursor();
					String label = Messages.getString("MoyosoftOutlookSetupEditor.waitingForOutlook"); //$NON-NLS-1$
					DefaultTreeModel dummy = new DefaultTreeModel(new DefaultMutableTreeNode(label));
					outlookTree.setRootVisible(true);
					outlookTree.setModel(dummy);
				}			
			});		

			// this is where moyosoft gets linked into the whole thing:
			// this might fail e.g if moyosoft or outlook is not available
			final FolderTreeModel model = new FolderTreeModel(getRootFolder());	

			EventQueue.invokeAndWait(new Runnable(){
				@Override
				public void run() {
					outlookTree.setRootVisible(false);
					outlookTree.setModel(model);					
				}			
			});		

		} catch (final Exception ex) {
			EventQueue.invokeLater(new Runnable(){
				@Override
				public void run() {
					LOG.error("can't init tree model, creating error message for user", ex); //$NON-NLS-1$
					// best we can do is just display the localized message
					String error = ex.getLocalizedMessage();
					Throwable rootCause = ex;
					while (rootCause.getCause() != null) {
						rootCause = rootCause.getCause();
					}
					String cause = rootCause.getLocalizedMessage();
					setWarningStatus("" //$NON-NLS-1$
							+ "Error: " + error  + "\n" //$NON-NLS-1$ //$NON-NLS-2$
							+ (ex.equals(rootCause)?"\n":("Cause: " + cause  + "\n"))); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					// finally provide error feedback sound
					UIManager.getLookAndFeel().provideErrorFeedback(null);
				}			
			});		
		} finally {
			GuiUtilities.setDefaultCursor();
			initDataModelOngoing = false;
		}
	}


	private JTextArea createStatusArea() {
		JTextArea result = new JTextArea() {
			// use the label background
			JLabel label;
			{
				setEditable(false);
				setColumns(50);
				setRows(5);
				setPreferredSize(getMinimumSize());
			}			
			@Override
			public Color getBackground() {
				if (label == null) {
					label = new JLabel();
				}
				return label.getBackground();
			}
		};
		result.setLineWrap(true);
		return result;
	}


	private JTree createOutlookTree() {  	
		JTree tree = new JTree(new Object[]{}); // tree with an empty tree model
		tree.setCellRenderer(new OutlookTreeNodeRenderer());
		tree.addTreeSelectionListener(new TreeSelectionListener() {
			@Override
			public void valueChanged(final TreeSelectionEvent evt) {
				TreePath path = evt.getNewLeadSelectionPath();
				// the user closed a node while the selection was within the node
				if (path == null) {
					return; // nothing to do we keep the old selection
				}
				// we have a path that is not null here
				Object element = path.getLastPathComponent();
				if (element instanceof IMailFolderTreeNode) {

					// check if the root folder was selected
					if (element instanceof OutlookRootMailFolder) {
						// root folder is not a valid selection
						selectedFolder = null;
						LOG.debug("invalid or null value selected"); //$NON-NLS-1$
						folderNameField.setText(""); //$NON-NLS-1$
					}

					// check if we have an outlook mail source (this is what we are looking for)
					else if (element instanceof OutlookMailSource) {
						OutlookMailSource folder = (OutlookMailSource)element;
						selectedFolder = folder; //remember the folder for saving later
						LOG.debug("selectedFolder is {}", selectedFolder); //$NON-NLS-1$
						folderNameField.setText(selectedFolder.getName());
					}

					// got something else here
					else {
						LOG.warn("unknown foldertype in FolderChooser: {}", evt); //$NON-NLS-1$
					}
				} else { // not instanceof IMailFolderTreeNode
					LOG.warn("unknown object in FolderChooser, elem is: {}, event is: {}", element, evt); //$NON-NLS-1$
				}
			}
		});
		return tree;
	}





	public Object[] getPathForFolder(final IMailFolderTreeNode folder) {
		Object[] pathObject = null;
		if (folder != null) {
			ArrayList<IMailFolderTreeNode> elements = new ArrayList<>();
			IMailFolderTreeNode pathComponent = folder;
			while (pathComponent != null) {
				elements.add(pathComponent);
				pathComponent = pathComponent.getParentFolder();
			}
			elements.add(getRootFolder());
			// we need a Object[] array with the root at position 0:
			pathObject = new Object[elements.size()];
			// reverse the array:
			for (int i=pathObject.length-1, j=0; i >= 0; i--, j++) {
				// exchange the first and last
				pathObject[i] = elements.get(j);
			}
			//TreePath path = new TreePath(pathObject);
			//tree.setSelectionPath(path);
		}
		return pathObject;
	}


	private synchronized IMailFolderTreeNode getRootFolder() {
		if (rootFolder == null) {
			Outlook outlook =  MoyosoftOutlookHandler.getOutlook();
			FoldersCollection folders = outlook.getFolders();
			rootFolder = new OutlookRootMailFolder(folders);
		}
		return rootFolder;
	}

	public synchronized OutlookMailSource getSelectedFolder() {
		if (initDataModelOngoing)  {
			throw new MailProviderError("ongoing initialization, can't get the selected folder");
		}	
		return selectedFolder;		
	}


	public synchronized void setSelectedFolder(final OutlookMailSource mailSource) {
		if (initDataModelOngoing)  {
			throw new MailProviderError("ongoing initialization, can't set the selected folder");
		}

		if (mailSource == null) {
			LOG.error("nothing configured, mailsource is null");
			return;
		}

		try {
			// this may fail since we might have old folder data in the setups
			Object[] pathObject = getPathForFolder(mailSource);
			TreePath path = new TreePath(pathObject);
			outlookTree.setSelectionPath(path);
			// setup the textfield
			folderNameField.setText(mailSource.getName());
			selectedFolder = mailSource;
		} catch (Exception ex) {
			selectedFolder = null;
		}

	}


	public void testSelectedFolder() {
		if (initDataModelOngoing)  {
			setWarningStatus("Outlook init is ongoing, waiting for Outlook Access");	
			return;
		}			

		if (selectedFolder == null) {
			setWarningStatus("No Folder selected yet");
			return;			
		}

		// counting the mail messages
		try {
			int count = selectedFolder.getMailMessages().size();
			setSuccessStatus("found " + count + " messages \n in folder " + selectedFolder.getName());
		} catch (Exception ex) {
			LOG.error("can't init tree model, creating error message for user", ex); //$NON-NLS-1$
			// best we can do is just display the localized message
			String error = ex.getLocalizedMessage();
			Throwable rootCause = ex;
			while (rootCause.getCause() != null) {
				rootCause = rootCause.getCause();
			}
			String cause = rootCause.getLocalizedMessage();
			setWarningStatus("" //$NON-NLS-1$
					+ "Error: " + error  + "\n" //$NON-NLS-1$ //$NON-NLS-2$
					+ (ex.equals(rootCause)?"\n":("Cause: " + cause  + "\n"))); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
	}


	public Outlook getOutlook() {
//		if (initDataModelOngoing) {
//			setWarningStatus("Outlook init is ongoing, waiting for Outlook Access");	
//			return null;
//		}
		
		Outlook outlook =  MoyosoftOutlookHandler.getOutlook();
		return outlook;
	}


	private void setWarningStatus(final String warningtext) {
		statusArea.setForeground(Color.RED);
		statusArea.setFont(new JLabel() .getFont());
		statusArea.setText(warningtext);
		statusArea.setSelectionStart(0);
		statusArea.setSelectionEnd(0);		
	}

	private void setSuccessStatus(final String successtext) {
		statusArea.setForeground(Color.GREEN);
		statusArea.setFont(new JLabel() .getFont());
		statusArea.setText(successtext);
		statusArea.setSelectionStart(0);
		statusArea.setSelectionEnd(0);		
	}



}
