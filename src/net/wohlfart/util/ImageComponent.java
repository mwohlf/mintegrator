package net.wohlfart.util;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JComponent;

@SuppressWarnings("serial")
public class ImageComponent extends JComponent {

    private ImageIcon icon;

    public ImageComponent(Image image) {
        icon = new ImageIcon(image);
    }

    public void setIcon(Image image) {
        icon = new ImageIcon(image);
        repaint();
    }

    @Override
    public Dimension getSize() {
        return new Dimension(icon.getIconWidth(),icon.getIconHeight());
    }

    @Override
    public Dimension getMinimumSize() {
        return getSize();
    }

    @Override
    public Dimension getMaximumSize() {
        return getSize();
    }

    @Override
    public Dimension getPreferredSize() {
        return getSize();
    }

    /*
     * public Rectangle getBounds(Rectangle rect) {
     * LOGGER.entering(IconFlag.class.getName(), "getBounds"); //
     * this.setBounds(0,0,icon.getIconHeight(),icon.getIconWidth()); //return
     * super.getBounds(rect); return new
     * Rectangle(0,0,icon.getIconHeight(),icon.getIconWidth()); }
     */
    @Override
    public void paintBorder(Graphics g) {
        super.paintBorder(g);
    }

    @Override
    public void paintChildren(Graphics g) {
        super.paintChildren(g);
    }

    @Override
    public void paintComponent(Graphics g) {
        // g.drawImage(icon.getImage(),0,0,null);
        // g.setColor(new Color(0,0,0));
        // g.fillRect(1,1,10,10);
        icon.paintIcon(this, g, 0, 0);
    }

}
