package net.wohlfart.gui.renderer;

import java.awt.Component;
import java.awt.Font;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.data.ICandidateTO;
import net.wohlfart.gui.table.ApplicationTableModel;

@SuppressWarnings("serial")
public class DirtyCellRenderer extends DefaultTableCellRenderer {
	
    private final ApplicationTableModel model;
	private final IBasicDataSource dataSource;


    DirtyCellRenderer(final ApplicationTableModel model, final IBasicDataSource dataSource) {
        this.model = model;
        this.dataSource = dataSource;
    }

    @Override
    public Component getTableCellRendererComponent(
            JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column)  {

        // get a generic rendering from the parent class
        Component result = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        // change the result according to the status of the selected object

        int modelIndex = table.convertRowIndexToModel(row);
        ICandidateTO data = model.getValueAtRow(modelIndex);
        boolean isDirty = dataSource.isModified(data);
        
        // bold font for changed objects
        if (isDirty) {
        	result.setFont(getFont().deriveFont(Font.BOLD));
        }
        
//
//        if (isSelected && !isSynchronized) {
//            result.setFont(getFont().deriveFont(Font.BOLD));
//            result.setBackground(ColorUtil.darker(table.getSelectionBackground()));
//        } else if (isSelected && isSynchronized){
//            result.setBackground(table.getSelectionBackground());
//        } else if (!isSelected && !isSynchronized) {
//            result.setFont(getFont().deriveFont(Font.BOLD));
//            result.setBackground(ColorUtil.darker(table.getBackground()));
//        } else if (!isSelected && isSynchronized) {
//            result.setBackground(table.getBackground());
//        }

        return result;
    }



}
