package net.wohlfart.console;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

/**
 * The overridden read method in this class will not throw "Broken pipe"
 * IOExceptions; It will simply wait for new writers and data. This is used by
 * the JConsole internal read thread to allow writers in different (and in
 * particular ephemeral) threads to write to the pipe.
 * 
 * It also checks a little more frequently than the original read().
 * 
 * Warning: read() will not even error on a read to an explicitly closed pipe
 * (override closed to for that).
 */
public class WaitingInputStream extends PipedInputStream {
    // remember the closed state since we can't query the superclass because
    // the fields are not visible
    boolean closed;

    public WaitingInputStream(PipedOutputStream out) throws IOException {
        super(out);
    }

    @Override
    /*
     * this returns one byte of data from the ringbuffer the write() method
     * inserts the data
     */
    public synchronized int read() throws IOException {
        if (closed) {
            throw new IOException("InputStream closed"); //$NON-NLS-1$
        }

        // check if buffer is empty
        while (super.in < 0) {
            notifyAll(); // wake readers maybe they have more data
            try {
                wait(1000); // the readers start now
            } catch (InterruptedException ex) {
                throw new InterruptedIOException();
            }
        }

        // retrieve the byte
        int ret = super.buffer[super.out] & 0xFF;
        // inc counter inside the buffer
        super.out = (super.out + 1) % super.buffer.length;

        if (super.in == super.out) { // the buffer is empty
            super.in = -1;
        }
        return ret;
    }

    @Override
    public void close() throws IOException {
        closed = true;
        super.close();
    }
}
