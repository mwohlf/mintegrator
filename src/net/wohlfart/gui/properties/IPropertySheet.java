package net.wohlfart.gui.properties;

import java.beans.PropertyChangeListener;

import javax.swing.ImageIcon;
import javax.swing.JComponent;

public interface IPropertySheet {

	ImageIcon getIcon();
	
	// this is used for testing to find the component in the component hierarchy
	// note: this is also implemented by JComponent
	String getName(); 

	String getTitle();

	String getToolTip();

	JComponent getPane();

	// called when all changes should be applied to the system
	void applyAction();

	// called when all changed should be discarded
	void resetAction();

	
	void addPropertyChangeListener(PropertyChangeListener listener);

	void addPropertyChangeListener(String propertyName, PropertyChangeListener listener);

	void removePropertyChangeListener(PropertyChangeListener listener);

	void removePropertyChangeListener(String propertyName, PropertyChangeListener listener);


}
