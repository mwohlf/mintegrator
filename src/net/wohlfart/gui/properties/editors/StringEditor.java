package net.wohlfart.gui.properties.editors;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.lang.reflect.Constructor;

import javax.swing.DefaultCellEditor;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
public class StringEditor extends DefaultCellEditor {

    Class<?>[] argTypes = new Class<?>[]{String.class};
    Constructor<?> constructor;
    Object value;

    public StringEditor() {
        super(new JTextField() {{
        	setFont(getFont().deriveFont(Font.BOLD));
        }});
        setClickCountToStart(2);	
        getComponent().setName("Table.editor");
    }

    @Override
	public boolean stopCellEditing() {
        String s = (String)super.getCellEditorValue();
        // Here we are dealing with the case where a user
        // has deleted the string value in a cell, possibly
        // after a failed validation. Return null, so that
        // they have the option to replace the value with
        // null or use escape to restore the original.
        // For Strings, return "" for backward compatibility.
        if ("".equals(s)) {
            if (constructor.getDeclaringClass() == String.class) {
                value = s;
            }
            super.stopCellEditing();
        }

        try {
            value = constructor.newInstance(new Object[]{s});
        }
        catch (Exception e) {
            ((JComponent)getComponent()).setBorder(new LineBorder(Color.red));
            return false;
        }
        return super.stopCellEditing();
    }

    @Override
	public Component getTableCellEditorComponent(JTable table, Object value,
                                             boolean isSelected,
                                             int row, int column) {
        this.value = null;
        ((JComponent)getComponent()).setBorder(new LineBorder(Color.black));
        try {
            Class<?> type = table.getColumnClass(column);
            // Since our obligation is to produce a value which is
            // assignable for the required type it is OK to use the
            // String constructor for columns which are declared
            // to contain Objects. A String is an Object.
            if (type == Object.class) {
                type = String.class;
            }
            constructor = type.getConstructor(argTypes);
        }
        catch (Exception e) {
            return null;
        }
        return super.getTableCellEditorComponent(table, value, isSelected, row, column);
    }

    @Override
	public Object getCellEditorValue() {
        return value;
    }
}

