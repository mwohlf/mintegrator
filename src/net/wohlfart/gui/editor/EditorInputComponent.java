/**
 *
 */
package net.wohlfart.gui.editor;

/**
 * @author michael
 *
 */
public interface EditorInputComponent<I> {
    I getInput() ;
    void setInput(I text);
    void setMaxLength(int maxLength);
}