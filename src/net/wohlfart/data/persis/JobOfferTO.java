package net.wohlfart.data.persis;

import java.util.Date;

import net.wohlfart.data.IJobOfferTO;

public class JobOfferTO implements IJobOfferTO {
    private Date started;
    private String profession;
    private String finished;
    private Date end;
    private String shortDescription;
    private Integer id;
    private String division;

    @Override
	public String getDivision() {
        return division;
    }

    @Override
	public Date getEnd() {
        return end;
    }

    @Override
	public String getFinished() {
        return finished;
    }
    @Override
	public Integer getId() {
        return id;
    }
    @Override
	public String getProfession() {
        return profession;
    }
    @Override
	public String getShortDescription() {
        return shortDescription;
    }
    @Override
	public Date getStarted() {
        return started;
    }

    @Override
	public void setDivision(String division) {
        this.division = division;
    }

    @Override
	public void setEnd(Date end) {
        this.end = end;
    }
    @Override
	public void setFinished(String finished) {
        this.finished = finished;
    }
    @Override
	public void setId(Integer id) {
        this.id = id;
    }
    @Override
	public void setProfession(String profession) {
        this.profession = profession;
    }
    @Override
	public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }
    @Override
	public void setStarted(Date started) {
        this.started = started;
    }

}