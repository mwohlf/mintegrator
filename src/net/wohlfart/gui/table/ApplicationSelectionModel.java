package net.wohlfart.gui.table;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;
import javax.swing.DefaultListSelectionModel;
import javax.swing.ListSelectionModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
@Singleton // package private by intention
class ApplicationSelectionModel extends DefaultListSelectionModel {
	/** Logger for this class */
	private static final Logger LOG = LoggerFactory.getLogger(ApplicationSelectionModel.class);

	
    @PostConstruct
    protected void postConstruct() { // NO_UCD
    	LOG.debug("postConstruct() called"); //$NON-NLS-1$
		// selection action
		setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	}

}
