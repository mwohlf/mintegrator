/**
 *
 */
package net.wohlfart.worker;

import static net.wohlfart.Constants.DELAY_WORKERS;

import java.awt.EventQueue;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;

import javax.inject.Inject;

import net.wohlfart.CustomSwingWorker;
import net.wohlfart.Messages;
import net.wohlfart.data.IDocumentTO;
import net.wohlfart.data.persis.FolderDAO;
import net.wohlfart.gui.ErrorPane;
import net.wohlfart.util.GuiUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * FIXME: update this worker
 * 
 * @author michael
 */
public class DocumentSaveWorker extends CustomSwingWorker<IDocumentTO, Void> {
    /** Logger for this class */
    private final static Logger LOG = LoggerFactory.getLogger(DocumentSaveWorker.class);

    @Inject 
    private FolderDAO folderDAO;
   

    private volatile File file;
    private volatile IDocumentTO document;
       
    public void doExecute(final IDocumentTO document, final File file) {
		// this must run in EDT
		if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The method doExecute() must be called from EDT."); //$NON-NLS-1$
		}

		if ((document == null) 
				|| (file == null)) {
			LOG.info("create was canceled"); //$NON-NLS-1$
			// cancel the future object, this is needed so the get() method doesn't block 
			cancel(true); // (actually it throws an exception now)
			// done();
			return;				
		}

		this.file = file;
        this.document = document;
        
        super.doExecute();
    }
 

    @Override
    protected IDocumentTO doInBackground() /* throws Exception */{
        LOG.debug("doInBackground() - start"); //$NON-NLS-1$

        try {
            setProgress(10);

            // transfer data from DB to local host
            if (document.getData() == null) {
                if (DELAY_WORKERS) { GuiUtilities.sleep(1); }
                progressMonitor.setProgress(3);
                progressMonitor.setNote(Messages.getString("SaveDocumentDataWorker.Note2")); //$NON-NLS-1$
                //IFolderDAO folderDAO = context.fab.getFolderDAO();
                folderDAO.loadDocumentContent(document);
            }

            // safe the data to the file
            setProgress(40);
            FileOutputStream outStream = new FileOutputStream(file);
            outStream.write(document.getData());
            outStream.close();

        } catch (SQLException ex) {
            progressMonitor.close();
            LOG.error("doInBackground()", ex); //$NON-NLS-1$
            ErrorPane.showErrorDialog(ex, Messages.getString("SaveDocumentDataWorker.SQLException")); //$NON-NLS-1$
        } catch (FileNotFoundException ex) {
            progressMonitor.close();
            LOG.error("doInBackground()", ex); //$NON-NLS-1$
            ErrorPane.showErrorDialog(ex, Messages.getString("SaveDocumentDataWorker.FileNotFoundException")); //$NON-NLS-1$
        } catch (IOException ex) {
            progressMonitor.close();
            LOG.error("doInBackground()", ex); //$NON-NLS-1$
            ErrorPane.showErrorDialog(ex, Messages.getString("SaveDocumentDataWorker.IOException")); //$NON-NLS-1$
        } catch (Throwable ex) {
            progressMonitor.close();
            LOG.error("doInBackground()", ex); //$NON-NLS-1$
            ErrorPane.showErrorDialog(ex, Messages.getString("SaveDocumentDataWorker.Throwable")); //$NON-NLS-1$
        } finally {
            queue.clearBlockedComponents();
            GuiUtilities.setDefaultCursor();
            progressMonitor.close();
            LOG.debug("doInBackground() - finally"); //$NON-NLS-1$
        }
        return null; // everything is done within side effects
    }
    
	@Override
	public void done() {
		super.done();
		LOG.debug("done() called");
	}

}
