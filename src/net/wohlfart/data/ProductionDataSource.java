package net.wohlfart.data;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;

import net.wohlfart.gui.properties.database.flavour.DbFlavourCollection;
import net.wohlfart.gui.properties.database.flavour.IDatabaseFlavor;
import net.wohlfart.properties.IApplicationProperties;
import net.wohlfart.properties.PropertyKeys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * the DataSource implementation for this application, takes values from 
 * the application properties, we create a new instance each time we want to connect
 * this way we always have the latest values from the IApplicationProperties
 * 
 * @author michael
 */
@Singleton
public class ProductionDataSource extends AbstractBasicDataSource { // NO_UCD injected class
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(ProductionDataSource.class);
 
    private IDatabaseFlavor flavor;
   
    @PostConstruct
    protected void postConstruct() { // NO_UCD
    	LOG.debug("postConstruct() called"); //$NON-NLS-1$
    	closed = true; // closed by default
    }
    
    /**
     * configure the DataSource with the current setup from the ApplicationProperties, this method also sets the closed 
     * member in BasicDataSource to false, this way we can reuse this datasource even after it has been closed...
     * 
     * @param properties
     */
    @Override
    public boolean configure(final IApplicationProperties properties) {
    	// get the database flavor
    	String flavorId = properties.getString(PropertyKeys.DATABASE_FLAVOR_ID, null);    	
    	flavor = DbFlavourCollection.getInstance().get(flavorId);
    	if (flavor == null) {
    		// no flavor set, go home
    		LOG.warn("no database flavor found, bailing out of configure"); //$NON-NLS-1$
    		return false;
    	}
    	
    	closed = false;
    	    	
        setDriverClassName(flavor.getDriverClassName());
        setUrl(flavor.getUrl(
        		properties.getString(PropertyKeys.DATABASE_HOST, null), 
        		properties.getInteger(PropertyKeys.DATABASE_PORT, null), 
        		properties.getString(PropertyKeys.DATABASE_NAME, null)
        ));
        
        
        setUsername(properties.getString(PropertyKeys.DATABASE_USER_NAME, null));
        setPassword(String.valueOf(properties.getPassword()));
            
        setDefaultAutoCommit(true);  // need to disable this if we do session semantics...
        setMaxWait(2000); // wait 2 sec if no object is available

        // for debugging no idle connection:
        setMinIdle(3);
        return true;
    }

    @Override
    public Connection getConnection() throws SQLException {
    	LOG.debug("returning connection from datasource, instance is {}", this); //$NON-NLS-1$
    	return super.getConnection();
    }

	@Override
	public Properties getQueries(Class<?> clazz) {
    	if (flavor == null) {
    		// no flavor set, go home
    		LOG.warn("no database flavor found for query lookup"); //$NON-NLS-1$
    		return null;
    	} 	
    	return flavor.getQueries(clazz);
	}

}
