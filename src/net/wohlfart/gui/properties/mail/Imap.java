package net.wohlfart.gui.properties.mail;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.wohlfart.mail.IMailFolderTreeNode;
import net.wohlfart.properties.IApplicationProperties;


class Imap extends AbstractMailsource implements IMailBoxFlavor {
	
	// package private
	Imap(final String id) {
		// only one instance per application otherwise we have to override equals/hash ...
		super(id);
	}

	@Override
	public IMailFolderTreeNode getMailSource(final IApplicationProperties properties) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("serial")
	@Override
	public JComponent getPropertyEditor() {
		return new JPanel(){{ add(new JLabel("Imap"));}};
	}
	
	@Override
	public void initEditor(IApplicationProperties properties) {
		// TODO Auto-generated method stub
	}

	@Override
	public void testEditor(final IApplicationProperties properties) {
		// validate
	}

	@Override
	public void applyEditor(final IApplicationProperties properties) {
		// TODO Auto-generated method stub
	}

	@Override
	public void resetEditor(final IApplicationProperties properties) {
		// TODO Auto-generated method stub	
	}


}
