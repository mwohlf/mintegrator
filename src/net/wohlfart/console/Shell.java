package net.wohlfart.console;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import java.awt.EventQueue;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.swing.JFrame;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;

import net.wohlfart.util.GuiUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author michael
 */
public final class Shell implements Runnable {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(Shell.class);

    // an Object to synchronize on and to notify any threads with...
    private final Object sync = new Object();
    
    private final static String GROOVY_BOOTFILE = "boot.grvy"; //$NON-NLS-1$

    private final ConcurrentLinkedQueue<String> commandSequence;

    private boolean shutdown = false;


    private Thread thread;
    
    
    private final JFrame frame;
    private final Binding binding;
    private final GroovyShell groovyShell;
    private final ConsoleComponent component;




    public static void doExecute() {
    	if (!EventQueue.isDispatchThread()) {
    		throw new IllegalAccessError("The method doExecute() must be called from EDT."); //$NON-NLS-1$
    	}
		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				new Shell() .launch();
				return null;
			}
		}. execute();
    }


    private Shell() {
    	frame = new JFrame();
    	binding = new Binding();
    	groovyShell = new GroovyShell(binding);
        commandSequence = new ConcurrentLinkedQueue<>();
        
        component = new ConsoleComponent(commandSequence);
        frame.add(component);
    }


    public void launch() {
        synchronized (sync) {
        	LOG.info("loading bootfile '{}'", GROOVY_BOOTFILE);
            InputStream input = getClass().getResourceAsStream (GROOVY_BOOTFILE);
            if (input == null) {
            	LOG.error("no bootfile at '{}'", GROOVY_BOOTFILE);
            	return;
            }
            InputStreamReader inputReader = new InputStreamReader(input);
            BufferedReader bin = new BufferedReader(inputReader);
            groovyShell.evaluate(bin);

            // show the frame
            EventQueue.invokeLater(new Runnable() {
                @Override
				public void run() {
                    frame.pack();
                    GuiUtilities.centerOnScreen(frame);
                    frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
                    frame.addWindowListener(new WindowAdapter() {
                        @Override
                        public void windowClosing(WindowEvent arg0) {
                            shutdown = true; // shutdown on window close
                            synchronized (Shell.this) {
                                Shell.this.notify(); // wakeup the thread
                            }
                        }
                    });
                    frame.setVisible(true);
                }
            });
            component.installDrain();


            if ((thread != null) && thread.isAlive()) {
                assert false: "thread is already running"; //$NON-NLS-1$
            // thread is already running
            } else {
                // start the command thread
                thread = new Thread(this);
                thread.setDaemon(true);
                thread.start();
            }
        }

    }



    // add some variable to the groovy environment
    public void setVariable(String string, Object object) {
        binding.setVariable(string, object);
    }




    // invoked by the start() method
    // this waits for a notify and does all the cleanup
    @Override
	public synchronized void run() {
        try {
            while (!shutdown) {
                String command = commandSequence.poll(); // may be null if the queue is empty
                if (command == null) {
                    // the monitors are released and any other thread has access to
                    // this object
                    wait(0);        // waits forever, or till we get a notify
                } else {
                    try {
                        groovyShell.evaluate(command);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
            // we were send a notify if we are here
        } catch (InterruptedException ignore) {
            //
        } finally {
            component.deinstallDrain();
            frame.setVisible(false);
            frame.dispose();
        }
    }

}
