package net.wohlfart.gui.properties.database;

import javax.swing.UIManager;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PortDocumentFilter extends DocumentFilter {
	/**  Logger for this class */
	private static final Logger LOG = LoggerFactory.getLogger(PortDocumentFilter.class);


    @Override
	public void insertString(
    		final FilterBypass filterBypass, 
    		final int offset, 
    		final String string,
    		final AttributeSet attr) throws BadLocationException {
    	LOG.debug("about to insert {}", string); //$NON-NLS-1$
    	if (string != null && !string.matches("[0-9]*")) { //$NON-NLS-1$
    		UIManager.getLookAndFeel().provideErrorFeedback(null);
    		return;
    	}
    	filterBypass.insertString(offset, string, attr);
    }


    @Override
	public void replace(
    		final FilterBypass filterBypass, 
    		final int offset, 
    		final int length, 
    		final String string,
    		final AttributeSet attrs) throws BadLocationException {
    	LOG.debug("about to replace with {}", string); //$NON-NLS-1$
    	if (string != null && !string.matches("[0-9]*")) { //$NON-NLS-1$
    		UIManager.getLookAndFeel().provideErrorFeedback(null);
    		return;
    	}
    	filterBypass.replace(offset, length, string, attrs);
    }

}
