package net.wohlfart.gui.event;

import net.wohlfart.data.persis.CandidateTO;

/**
 * needed to fire events with a null candidate
 */
public class ApplicationCreated {

	private CandidateTO element;

	public ApplicationCreated(CandidateTO element) {
		this.element = element;
	}

	public CandidateTO get() {
		return element;
	}

}
