/**
 *
 */
package net.wohlfart.mail;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author michael
 *
 */
public interface IMailMessage {

    public abstract String getSubject();

    public abstract Integer getAttachmentCount();

    public abstract String getSenderAddress();

    public abstract Date getReceivedDate();

    public abstract String getSenderName();


    public abstract String getBody();

    public abstract ArrayList<IMailAttachment> getAttachments();

    public abstract Boolean getSelected();

    public abstract void setSelected(Boolean selected);

    public abstract Boolean getImported();

    public abstract void setImported(Boolean imported);

}
