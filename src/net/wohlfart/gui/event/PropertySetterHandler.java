package net.wohlfart.gui.event;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;

import net.wohlfart.data.IBasicDataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * lots of reflection magic to turn a simple transfer object into a property change
 * event firing monster bean...
 * 
 * @author michael
 */
// package private
class PropertySetterHandler implements InvocationHandler {
	/** Logger for this class */
	private static final Logger LOG = LoggerFactory.getLogger(PropertySetterHandler.class);

	private static final String SET_METHOD_START = "set";
	private static final String GET_METHOD_START = "get";

	private final VetoableChangeSupport vetoableChangeSupport;

	private final Object delegatee;
	private final IBasicDataSource dataSource;
	private final HashMap<String, PropertyDescriptor> descriptors = new HashMap<>();

	PropertySetterHandler(final Object delegatee, final IBasicDataSource dataSource, final VetoableChangeSupport propertyChangeSupport) {
		this.delegatee = delegatee;
		this.vetoableChangeSupport = propertyChangeSupport;
		this.dataSource = dataSource;

		try {
			BeanInfo beanInfo = Introspector.getBeanInfo(delegatee.getClass());			
			for (PropertyDescriptor descriptor : beanInfo.getPropertyDescriptors()) {
				descriptors.put(descriptor.getName(), descriptor);
			}			
		} catch (IntrospectionException ex) {
			LOG.error("error analzing bean", ex);
		}
	}


	// this reroutes the call from the proxy to the actual delegatee object
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {	
		// its a bad idea to call a method on the proxy, we end up in an endless loop...		
		LOG.debug("before invoking method {} on delegatee {}", method, delegatee);

		// check if we have a standard method call without affecting properties:
		String propertyName = getPropertyNameForBeanMethod(method);
		if (propertyName == null) { // must be something else but no property
			return method.invoke(delegatee, args);		
		}

		LOG.debug("  property is {}", ((propertyName!=null)?propertyName:" - no property found - "));

		if (!isSetter(method)) { // we only care about setter
			LOG.debug("  method is not a setter, ignoring");
			return method.invoke(delegatee, args);		
		}

		// do properties related stuff here:
		Method reader = getReadMethod(propertyName);
		Method writer = getWriteMethod(propertyName);

		Object oldValue = reader.invoke(delegatee, new Object[]{});
		Object newValue = args[0];
		Object result = writer.invoke(delegatee, args);

		if (oldValue == null) { 
			if (newValue == null) {
				return result;
			}
		} else {
			if (oldValue.equals(newValue)) {
				return result;
			}
		}
		
		LOG.debug("detected a property change,"
				+ " property name is '{}'"
				+ "  old value is '{}'"
				+ "  new value is '{}'",
				new Object[] {propertyName, oldValue, newValue});

		// put the object into the set of changed objects if the property was actually changed
		dataSource.addModified(delegatee);
		// do the property change listener stuff:
		vetoableChangeSupport.fireVetoableChange(propertyName, oldValue, newValue);

		return result;
	}


	/**
	 * return the property name for the bean setter/getter method or
	 * null if no property can be found..
	 * 
	 * @param method
	 * @return
	 */
	private String getPropertyNameForBeanMethod(final Method method) {
		String methodName = method.getName();
		if (methodName.startsWith(SET_METHOD_START)
				|| 	methodName.startsWith(GET_METHOD_START)) {

			String propertyName = methodName.substring(3);
			char firstLetter = propertyName.charAt(0);
			// first character after the set/get must be uppercase:
			if (Character.isUpperCase(firstLetter)) {
				// lowerCase the first char
				return "" + Character.toLowerCase(firstLetter) + propertyName.substring(1);
			}
		}
		return null;
	}

	private boolean isSetter(final Method method) {
		String methodName = method.getName();
		return methodName.startsWith(SET_METHOD_START);
	}

	/*
	private boolean isGetter(final Method method) {
		String methodName = method.getName();
		return methodName.startsWith(GET_METHOD_START);
	}
	*/


	private Method getReadMethod(final String propertyName) {
		Method method = null;	
		PropertyDescriptor descriptor = descriptors.get(propertyName);
		if (descriptor != null) {
			method = descriptor.getReadMethod();
		}
		return method;
	}

	private Method getWriteMethod(final String propertyName) {
		Method method = null;	
		PropertyDescriptor descriptor = descriptors.get(propertyName);
		if (descriptor != null) {
			method = descriptor.getWriteMethod();
		}
		return method;
	}


	//  --- VetoableChangeListenerSupport API: 

	// any property
	public void addPropertyChangeListener(VetoableChangeListener listener) { 
		vetoableChangeSupport.addVetoableChangeListener(listener); 
	} 
	public void removePropertyChangeListener(VetoableChangeListener listener) { 
		vetoableChangeSupport.removeVetoableChangeListener(listener); 
	} 
	// named property
	public void addPropertyChangeListener(String propertyName, VetoableChangeListener listener) { 
		vetoableChangeSupport.addVetoableChangeListener(propertyName, listener);
	}
	public void removePropertyChangeListener(String propertyName, VetoableChangeListener listener) { 
		vetoableChangeSupport.removeVetoableChangeListener(propertyName, listener); 
	} 

}
