/**
 *
 */
package net.wohlfart.data;

/**
 * @author michael
 *
 */
public interface IMediumTO {

    public abstract Integer getId();

    public abstract void setId(Integer id);

    public abstract String getShortDescription();

    public abstract void setShortDescription(String description);

    public abstract String getDescription();

    public abstract void setDescription(String description);

}
