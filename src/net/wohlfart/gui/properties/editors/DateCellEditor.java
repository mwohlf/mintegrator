package net.wohlfart.gui.properties.editors;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.JFormattedTextField;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellEditor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class DateCellEditor extends AbstractCellEditor implements TableCellEditor  {
	/** Logger for this class */
	private static final Logger LOGGER = LoggerFactory.getLogger(DateCellEditor.class);


	public static final String DATE_FORMAT_STRING = "dd.MM.yyyy";

	private final JFormattedTextField textField;

	public DateCellEditor() {
		textField = new JFormattedTextField(new SimpleDateFormat(DATE_FORMAT_STRING));
		textField.setBorder(new EmptyBorder(0,0,0,0));
		textField.setVerifyInputWhenFocusTarget(false);

		// stop cell editing for any action event (return key for example)
		// this enables the editor to stop editing after the return key
		// has been pressed only once...
		textField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent evt) {
				LOGGER.debug(evt.toString());
				stopCellEditing();
			}
		});
	}


	@Override
	public Component getTableCellEditorComponent(
			JTable table,
			Object value,
			boolean isSelected,
			int row,
			int column) {

		textField.setValue(value);
		return textField;
	}

	@Override
	public Object getCellEditorValue() {
		return textField.getValue();
	}

	@Override
	public boolean isCellEditable(EventObject anEvent) {
		if (anEvent instanceof MouseEvent) {
			return ((MouseEvent)anEvent).getClickCount() >= 2;
		}
		return true;
	}
}
