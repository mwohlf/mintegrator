package net.wohlfart.gui.event;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Shutdown {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(Shutdown.class);

	private Date date;
	public Shutdown(final Date date) {
		this.date = date;
		LOG.debug("shutdown event created at {}", this.date.getTime());
	}		
}

