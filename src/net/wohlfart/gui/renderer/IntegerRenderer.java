package net.wohlfart.gui.renderer;

import java.awt.Component;

import javax.swing.JTable;

import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.gui.table.ApplicationTableModel;

@SuppressWarnings("serial")
public class IntegerRenderer extends DirtyCellRenderer  {

    public IntegerRenderer(final ApplicationTableModel model, final IBasicDataSource dataSource) {
        super(model, dataSource);
    }

    @Override
    public Component getTableCellRendererComponent(
            JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column) {

        return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

    }

}
