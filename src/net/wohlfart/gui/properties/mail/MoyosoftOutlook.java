package net.wohlfart.gui.properties.mail;

import static net.wohlfart.properties.PropertyKeys.OUTLOOK_ENTRY_ID;
import static net.wohlfart.properties.PropertyKeys.OUTLOOK_STORE_ID;

import java.awt.EventQueue;

import javax.swing.JComponent;

import net.wohlfart.mail.IMailFolderTreeNode;
import net.wohlfart.mail.outlook.MoyosoftOutlookSetupEditor;
import net.wohlfart.mail.outlook.OutlookMailSource;
import net.wohlfart.properties.IApplicationProperties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moyosoft.connector.com.ComponentObjectModelException;
import com.moyosoft.connector.ms.outlook.Outlook;
import com.moyosoft.connector.ms.outlook.folder.OutlookFolder;
import com.moyosoft.connector.ms.outlook.folder.OutlookFolderID;


/**
 * this class links the custom OutlookMailSource object to the ApplicationProperties and to the 
 * native Outlook folder
 * 
 * @author michael
 */
public class MoyosoftOutlook extends AbstractMailsource implements IMailBoxFlavor {
	/** Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(MoyosoftOutlook.class);
	
	private final MoyosoftOutlookSetupEditor moyosoftOutlookSetupEditor;

	// package private
	MoyosoftOutlook(final String id) {
		// only one instance per application otherwise we have to override equals/hash ...
		super(id);
		moyosoftOutlookSetupEditor = new MoyosoftOutlookSetupEditor();
	}

	@Override
	public IMailFolderTreeNode getMailSource(final IApplicationProperties properties) {
		return loadOutlookMailSource(properties);
	}


	@Override
	public JComponent getPropertyEditor() {
		return moyosoftOutlookSetupEditor;
	}
	
	@Override
	public void initEditor(final IApplicationProperties properties) {
		// this needs to run off the EDT!
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("Outlook Database backend shouldn't be access in the EDT."); //$NON-NLS-1$
		}
		moyosoftOutlookSetupEditor.initDataModel();
	}
	
	@Override
	public void resetEditor(final IApplicationProperties properties) {
		OutlookMailSource mailSource = loadOutlookMailSource(properties);
		moyosoftOutlookSetupEditor.setSelectedFolder(mailSource);
		moyosoftOutlookSetupEditor.testSelectedFolder();
	}


	@Override
	public void testEditor(final IApplicationProperties properties) {
		moyosoftOutlookSetupEditor.testSelectedFolder();
	}
	
	@Override
	public void applyEditor(final IApplicationProperties properties) {		
		saveOutlookMailSource(properties, moyosoftOutlookSetupEditor.getSelectedFolder());
	}

	
	
	// persistence methods --- 
	
	private void saveOutlookMailSource(final IApplicationProperties properties, final OutlookMailSource mailSource) {
		if (mailSource == null) {
			properties.delete(OUTLOOK_ENTRY_ID);
			properties.delete(OUTLOOK_STORE_ID);
			return;
		}

		String endtryId = mailSource.getOutlookEntryID();
		String storeId = mailSource.getOutlookStoreID();

		properties.setProperty(OUTLOOK_ENTRY_ID, endtryId);
		properties.setProperty(OUTLOOK_STORE_ID, storeId);	
	}
	
	
	private OutlookMailSource loadOutlookMailSource(final IApplicationProperties properties) {
		LOG.debug("loadOutlookMailSource");
		OutlookMailSource mailSource = null;
		try {				
			String outlookEntryId = properties.getString(OUTLOOK_ENTRY_ID, null);
			String outlookStoreId = properties.getString(OUTLOOK_STORE_ID, null);

			// return null if we are waiting for outlook access to get approved
			Outlook outlook =  moyosoftOutlookSetupEditor.getOutlook();
			if ((outlookEntryId != null) && (outlookStoreId != null) && (outlook != null)) {
				OutlookFolderID folderId = new OutlookFolderID(outlookEntryId, outlookStoreId);
				OutlookFolder folder = outlook.getFolder(folderId);
				mailSource = new OutlookMailSource(folder);
			} 
		} catch (ComponentObjectModelException ex) {
			LOG.warn("no Outlook folder found, we have to prompt the user", ex); //$NON-NLS-1$
		} catch (Throwable ex) {
			LOG.warn("Outlook API Problem", ex); //$NON-NLS-1$
		}
		return mailSource;
	}
	
	


}
