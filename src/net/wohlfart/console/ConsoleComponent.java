package net.wohlfart.console;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;


@SuppressWarnings("serial")
public class ConsoleComponent extends JPanel {

	private final JTextPane textPane;
	private final ConsoleDocument doc;
	private PrintStream originalOut;
	private PrintStream originalErr;

	public ConsoleComponent(final ConcurrentLinkedQueue<String> commandSequence) {
		super();
		textPane = new JTextPane();
		doc = new ConsoleDocument(commandSequence);
		initComponents();
	}       


	private void initComponents() {
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(100,100));
		textPane.setCaret(doc.getCaret());
		textPane.setStyledDocument(doc);
		textPane.addKeyListener(doc);
		textPane.setMargin(new Insets(1,1,1,1));
		doc.createPrompt();
		add(new JScrollPane(textPane), BorderLayout.CENTER);
	}
	
	public void analyze() {
		doc.analyze();
	}

	@Override
	public void requestFocus() {
		super.requestFocus();
		// propagate focus to the textPane
		textPane.requestFocus();
	}

	public void installDrain() {
		PipedOutputStream out1 = new PipedOutputStream();
		originalOut = System.out;
		System.setOut(new PrintStream(out1));
		doc.setStream(out1, doc.getOutputStyle());

		PipedOutputStream out2 = new PipedOutputStream();
		originalErr = System.err;
		System.setErr(new PrintStream(out2));
		doc.setStream(out2, doc.getErrorStyle());
	}

	public void deinstallDrain() {
		System.setOut(originalOut);
		System.setErr(originalErr);
	}
}




