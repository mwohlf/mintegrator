default run configuration:

-Dlog4j.debug
-Dlog4j.configuration=file:conf/log4j.xml
-Djava.library.path=lib
-ea


open issues:
------------


Oracle connect
--------------
host: 192.168.2.156
port: 1521
login: PERSIS/PERSIS

MS-SQL connect
--------------
host: localhost
port: 1433
login: PERSIS/PERSIS


download link:
http://dl.element5.com/d/1e801ba004e02328/4A72B8C2/41754/joc-basic-2_1_3.zip

the second add still is being triggered by DECORATEE_CHANGED event

        selectedCandidate.addPropertyChangeListener(BeanDecorator.DECORATEE_CHANGED, this);
        selectedCandidate.addPropertyChangeListener(this);


addPropertyChangeListener(this); without a property string listens to all property events

ant scp update:
install jsch-xxx and update ant homedir

swing icons:
http://forum.java.sun.com/thread.jspa?forumID=57&threadID=564246
JLabel label = new JLabel(UIManager.getDefaults().getIcon("OptionPane.informationIcon"));


Java help:
http://docs.sun.com/source/819-0913/dev/csh.html#HelpBroker

datevalidation check with blocking eventqueue:
http://www.javaworld.com/javatips/jw-javatip89_p.html?page=2

the following says run MyApp in a JVM and allocate a minimum of 5 Megabytes and a maximum of 15 Megabytes off of the heap in order to do so.

   java -Xms5m -Xmx15m MyApp

nullsoft ftp/http/https network plugin!
for downloading updates!


troubleshooting swing:
http://java.sun.com/javase/6/webnotes/trouble/TSG-Desktop/html/swing.html#gdled

eventqueue:
http://www.javaworld.com/javatips/jw-javatip89_p.html

To test if it is in fact a threading issue, on a Windows machine, you can press ctrl-break to get a thread dump, and it will notify you of any deadlocks that exist.

progressMonitor is not modal !!
we can still trigger the same action again...



java.lang.IllegalStateException: Pool not open
Stacktrace:
org.apache.commons.pool.BaseObjectPool.assertOpen(BaseObjectPool.java:78)
org.apache.commons.pool.impl.GenericObjectPool.borrowObject(GenericObjectPool.java:781)
org.apache.commons.dbcp.PoolingDataSource.getConnection(PoolingDataSource.java:95)
org.apache.commons.dbcp.BasicDataSource.getConnection(BasicDataSource.java:540)
org.apache.commons.dbutils.QueryRunner.prepareConnection(QueryRunner.java:195)
org.apache.commons.dbutils.QueryRunner.query(QueryRunner.java:306)
net.wohlfart.data.persis.CandidateDAO.readAllCandidates(CandidateDAO.java:196)
net.wohlfart.worker.ConnectDatabaseWorker.doInBackground(ConnectDatabaseWorker.java:84)
net.wohlfart.worker.ConnectDatabaseWorker.doInBackground(ConnectDatabaseWorker.java:1)
javax.swing.SwingWorker$1.call(Unknown Source)
java.util.concurrent.FutureTask$Sync.innerRun(Unknown Source)
java.util.concurrent.FutureTask.run(Unknown Source)
javax.swing.SwingWorker.run(Unknown Source)
java.util.concurrent.ThreadPoolExecutor$Worker.runTask(Unknown Source)
java.util.concurrent.ThreadPoolExecutor$Worker.run(Unknown Source)
java.lang.Thread.run(Unknown Source)
