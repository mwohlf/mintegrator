package net.wohlfart.gui.properties.mail;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.wohlfart.mail.IMailFolderTreeNode;
import net.wohlfart.properties.IApplicationProperties;

class Text extends AbstractMailsource implements IMailBoxFlavor {

	// package private
	Text(final String id) {
		super(id);
	}

	@Override
	public IMailFolderTreeNode getMailSource(final IApplicationProperties properties) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("serial")
	@Override
	public JComponent getPropertyEditor() {
		// we need some kind of pop browser here
		return new JPanel(){{ add(new JLabel("Text"));}};
	}
	
	@Override
	public void initEditor(IApplicationProperties properties) {
		// TODO Auto-generated method stub
	}

	@Override
	public void testEditor(final IApplicationProperties properties) {
		// validate
	}

	@Override
	public void applyEditor(final IApplicationProperties properties) {
		// TODO Auto-generated method stub
	}

	@Override
	public void resetEditor(final IApplicationProperties properties) {
		// TODO Auto-generated method stub	
	}

}
