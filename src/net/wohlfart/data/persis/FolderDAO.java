package net.wohlfart.data.persis;

import static net.wohlfart.data.persis.Tools.getBytesFromFile;
import static net.wohlfart.data.persis.Tools.getNextDocId;
import static net.wohlfart.data.persis.Tools.getRefNr;
import static net.wohlfart.data.persis.Tools.getType;

import java.awt.EventQueue;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.inject.Inject;

import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.data.ICandidateTO;
import net.wohlfart.data.IDocumentFileTO;
import net.wohlfart.data.IDocumentTO;
import net.wohlfart.data.IFolderTO;
import net.wohlfart.properties.ApplicationPropertiesImpl;
import net.wohlfart.properties.PropertyKeys;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author michael
 */
public class FolderDAO {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(FolderDAO.class);

	@Inject
	private IBasicDataSource basicDataSource;


	private static final String DEFAULT_GROUP = "Mailanhang"; //$NON-NLS-1$
	private static final String DEFAULT_DESCRIPTION = "Bewerbung"; //$NON-NLS-1$


	public FolderDAO() {
	}



	public List<DocumentTO> readAllDocuments(ICandidateTO candidate) throws SQLException {
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The DataBase shouldn't be access in the EDT."); //$NON-NLS-1$
		}
		Properties properties = basicDataSource.getQueries(IFolderTO.class);

		BeanListHandler<DocumentTO> handler = new BeanListHandler<>(DocumentTO.class);

		// create Query Runner for a given DataSource
		QueryRunner run = new QueryRunner(basicDataSource);

		String sql = properties.getProperty("FIND_DOCUMENTS_FOR_CANDIDATE");
		// returns result from the Handler
		ArrayList<DocumentTO> result = (ArrayList<DocumentTO>) run.query(
				sql, 
				handler,
				candidate.getId()
				);

		LOG.debug("returned {} documents from the database, select was \"{}\"", 
				result.size(), sql);
		
		return result;

//		//System.err.println("result found: " + result);
//
//		// remove old documents
//		list.setDecoratee(list.getEmptyList());
//		ArrayList<IDocumentTO> clone = list.getDecorateeClone();
//
//		// fresh from the DB everything is in sync
//		for (IDocumentTO element: result) {
//			//System.out.println("attachment found: " + document);
//			element.setSynchronized(true);
//			clone.add(element);
//		}
//		list.setDecoratee(clone);
	}


	public IDocumentTO attachDocument(final ICandidateTO candiate, final IDocumentFileTO document) throws SQLException, IOException {
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The DataBase shouldn't be access in the EDT."); //$NON-NLS-1$
		}
		Properties properties = basicDataSource.getQueries(IFolderTO.class);

		Connection connection = null;


		ApplicationPropertiesImpl props = ApplicationPropertiesImpl.getInstance();
		String description = props.getProperty(PropertyKeys.DOC_DESCRIPTION);
		if (description.trim().length() == 0) {
			description = DEFAULT_DESCRIPTION;
		}
		String group = props.getProperty(PropertyKeys.DOC_GROUP);
		if (group.trim().length() == 0) {
			group = DEFAULT_GROUP;
		}



		try {

			// changed: prefetch the refids since we would block the refid table otherwise

			// new refnr for folder entry
			int folderId = getRefNr(basicDataSource);

			// new refnr for document
			int documentId = getRefNr(basicDataSource);


			// set auto commit to false since we are doing a transaction
			connection = basicDataSource.getConnection();
			connection.setAutoCommit(false);

			QueryRunner run = new QueryRunner();


			// the candidated refnr
			int candidateId = candiate.getId();
			// running docId number for each candidate
			int docId = getNextDocId(connection, candidateId);
			// the binary data
			byte[] cache = getBytesFromFile(document.getFile());


			String anlage = "DB:\\BM\\" + candidateId + "\\Anhang\\" + document.getName();
			// insert folder
			Object[] arg1 = {
					folderId,    // REFNR
					candidateId, // BWR_REFNR
					documentId,  // DOK_REFNR
					docId,       // DOK_ID
					anlage,      // ANLAGE //$NON-NLS-1$ //$NON-NLS-2$
					description, // BEZ
					group,       // GRUPPE
					1,           // SORT1
					1            // SORT2
			};
			run.update(connection, properties.getProperty("INSERT_FOLDER_ELEMENT"), arg1); //$NON-NLS-1$


			// insert document
			Object[] arg2 = {
					documentId,             // REFNR
					candidateId,    // HPT_REFNR
					candidateId,    // SATZ_REFNR
					"ANLAGEN",      // DOK_INFO //$NON-NLS-1$
					getType(document.getName()),  // TYP
					docId,          // DOK_ID
					// using 0 here seems not to work with the windows persis:
					1,              // DOK_NO
					cache.length,   // GROESSE
					document.getName(),       // NAME
					"BM",           // HPT_MODUL //$NON-NLS-1$
					"ANLAGEN",      // MAIN_PATH //$NON-NLS-1$
					"BM",           // SATZ_MODUL //$NON-NLS-1$
					0,              // GPG
					0,              // SCHRSPERR
					0,              // SPERRVERM
					"C:\\TEMP",     // OLD_VZ //$NON-NLS-1$
					cache           // DOKUMENT
			};
			// FIXME: check cache size
			//System.out.println("FIXME: check cache size; cache size: " + cache.length );
			run.update(connection, properties.getProperty("INSERT_DOCUMENT"), arg2); //$NON-NLS-1$


			connection.commit();
			connection.setAutoCommit(true);
			return getDocument(documentId);

		} catch (Throwable ex) {
			// something went wrong here
			LOG.error("exception", ex); //$NON-NLS-1$
			if (connection != null) {
				connection.rollback();
			}
		} finally {
			if (connection != null) {
				connection.setAutoCommit(true);
				connection.close(); // we need to close the connection in order to reuse it
			}
		}
		return null;

	}

	/**
	 * @throws SQLException
	 * @see net.wohlfart.data.IFolderDAO#getDocuments(net.wohlfart.data.ICandidateTO, net.wohlfart.BeanListDecorator)
	 */

	private IDocumentTO getDocument(int refnr) throws SQLException {
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The DataBase shouldn't be access in the EDT."); //$NON-NLS-1$
		}
		Properties properties = basicDataSource.getQueries(IFolderTO.class);

		BeanHandler<DocumentTO> handler = new BeanHandler<>(DocumentTO.class);

		// create Query Runner for a given DataSource
		QueryRunner run = new QueryRunner(basicDataSource);

		// returns result from the Handler
		IDocumentTO result = run.query(
				properties.getProperty("FIND_DOCUMENTS_FOR_ID"),  //$NON-NLS-1$
				handler,
				new Object[] {refnr});

		return result;

	}


	/**

	 */
	public void deleteDocument(IDocumentTO document) throws SQLException {
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The DataBase shouldn't be access in the EDT."); //$NON-NLS-1$
		}
		Properties properties = basicDataSource.getQueries(IFolderTO.class);

		//System.out.println("deleting: " + document);

		// create Query Runner for a given DataSource
		QueryRunner run = new QueryRunner(basicDataSource);

		// delete attachments first
		run.update( properties.getProperty("DELETE_ATTACHMENT"), new Object[] {document.getId()}); //$NON-NLS-1$

		// delete folders
		run.update( properties.getProperty("DELETE_FOLDER_ENTRY"), new Object[] {document.getId()}); //$NON-NLS-1$

	}


	/**
	 * @throws SQLException
	 * @see net.wohlfart.data.IDocumentDAO#loadDocumentContent(net.wohlfart.data.IDocumentTO)
	 */
	public void loadDocumentContent(IDocumentTO document) throws SQLException {
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The DataBase shouldn't be access in the EDT."); //$NON-NLS-1$
		}
		Properties properties = basicDataSource.getQueries(IFolderTO.class);
		Connection connection = null;

		try {
			connection = basicDataSource.getConnection();

			PreparedStatement stmt = connection.prepareStatement(properties.getProperty("LOAD_DOCUMENT_CONTENT")); //$NON-NLS-1$
			stmt.setInt(1, document.getId());
			ResultSet result = stmt.executeQuery();

			byte[] cache = null;
			if (result.next()) {
				cache = result.getBytes(1);
			} else {
				System.err.println("Can't get a Reference Number"); //$NON-NLS-1$
			}

			document.setData(cache);
			stmt.close();
		} catch (Throwable ex) {
			// something went wrong here
			LOG.error("exception", ex); //$NON-NLS-1$
			if (connection != null) {
				connection.rollback();
			}
		} finally {
			if (connection != null) {
				connection.setAutoCommit(true);
				connection.close(); // we need to close the connection in order to reuse it
			}
		}
	}
}
