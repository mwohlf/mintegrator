package net.wohlfart.gui.properties;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingWorker;

import net.wohlfart.Messages;
import net.wohlfart.util.GuiUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * A dialog implementation that shows a set of property sheets, no injections here
 * can me manually insxtanciated
 * 
 * 
 * @author michael
 */
@SuppressWarnings("serial")
class PropertySheetsDialog extends JDialog {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(PropertySheetsDialog.class);

	// package private, needed for testing to pick up 
	static final String TABBED_PANE_NAME = "tabbedPane"; //$NON-NLS-1$ // NO_UCD

	// min size for the inner panel
	private static final Dimension MINIMUM_DIM = new Dimension(200,200);

	private final JToolBar iconBar;
	private final JPanel tabbedPane;

	// don't use static here if we ever have multiple instances
	private final CardLayout cardLayout = new CardLayout();

	// sheets, we need to notify them on ok/cancel
	private final Set<IPropertySheet> sheets = new HashSet<>();
	private final DispatchAction okAction = new OkAction();
	private final DispatchAction cancelAction = new CancelAction();

	// we create a uid by counting the pages
	private int pageCounter = 0;

	public PropertySheetsDialog() {
		super(GuiUtilities.getFrame());
		if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("All swing components should be created in the EDT."); //$NON-NLS-1$
		}

		setTitle(Messages.getString("PropertySheetsDialog.title")); //$NON-NLS-1$
		// we trigger a cancel on all sheets when closed
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(final WindowEvent evt) {
				ActionEvent event = new ActionEvent(PropertySheetsDialog.this, ActionEvent.ACTION_PERFORMED, "closing"); //$NON-NLS-1$
				cancelAction.actionPerformed(event);
			}
		});

		// border layout
		setModalityType(ModalityType.APPLICATION_MODAL);
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		// button bar at the border
		iconBar = new JToolBar(){{
			setOrientation(JToolBar.VERTICAL);
			setFloatable(false);
			//setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
			setBackground(Color.WHITE);
		}};			
		contentPane.add(iconBar, BorderLayout.WEST);	

		// ok / cancel button bar
		JPanel buttons = new JPanel() {{
			setLayout(new FlowLayout(FlowLayout.RIGHT));
			add(Box.createHorizontalStrut(100));
			add(new JButton(cancelAction));
			add(new JButton(okAction));
		}};
		contentPane.add(buttons, BorderLayout.SOUTH);

		// tabbedPane in the center
		tabbedPane = new JPanel() {{
			setName(TABBED_PANE_NAME); // for testing
			setMinimumSize(MINIMUM_DIM);
			setLayout(cardLayout);
			setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		}};		
		contentPane.add(tabbedPane, BorderLayout.CENTER);
	}



	/**
	 * add an ordered collection of property sheets
	 * 
	 * @param pagelist
	 */
	synchronized void addPages(final List<IPropertySheet> pagelist) {
		for (IPropertySheet sheet : pagelist) {
			addPage(sheet);
		}
	}


	/**
	 * add a properties sheet, synchronized needed for the pageCounter, 
	 * this method must run in the EDT
	 * 
	 * @param icon used for the button might be null
	 * @param name header/title for the sheet
	 * @param tooltip tooltip (not used yet)
	 * @param page the actual JPanle in use for this page
	 */
	private synchronized void addPage(final IPropertySheet sheet) {
		if(!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("All swing components should be created in the EDT."); //$NON-NLS-1$
		}
		iconBar.add(new JButton(sheet.getIcon()){{
			if (sheet.getIcon()==null) {
				setText(sheet.getTitle());
			}
			setName(sheet.getName()); // need this to find the buttons in the tests
			setToolTipText(sheet.getToolTip());
			addActionListener(new ActionListener() {
				// need to keep this as an instance variable
				String value = Integer.toString(pageCounter);
				@Override
				public void actionPerformed(final ActionEvent evt) {				
					cardLayout.show(tabbedPane, value);
				}				
			});
		}});

		final JLabel header = new JLabel(sheet.getTitle());
		header.setBorder(BorderFactory.createEmptyBorder(5,5,15,5));
		tabbedPane.add(
				new JComponent() {{
					setLayout(new BorderLayout());
					add(header, BorderLayout.NORTH);
					add(sheet.getPane(), BorderLayout.CENTER);
				}}, 
				Integer.toString(pageCounter));
		sheets.add(sheet);
		pageCounter++;
	}


	@Override
	public void setVisible(boolean isVisible) {
		if (isVisible) {
			setLocationRelativeTo(null); // center
		}
		super.setVisible(isVisible);
	}



	private abstract class DispatchAction extends AbstractAction {

		@Override
		public void actionPerformed(ActionEvent evt) {
			// ..careful this is EDT
			PropertySheetsDialog.this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			new SwingWorker<Void, Void>() {
				@Override
				protected Void doInBackground() throws Exception {
					for (IPropertySheet sheet : sheets) {
						// something might go wrong when saving a sheet, make sure this doesn't affect the other sheets:
						try {
							notifySheet(sheet);
						} catch (Throwable th) {
							LOG.error("failed to dispacth OK action to the property sheets", th); //$NON-NLS-1$
						} 
					}
					return null;
				}			
				@Override
				protected void done() {
					PropertySheetsDialog.this.setCursor(Cursor.getDefaultCursor());
					PropertySheetsDialog.this.setVisible(false);
					PropertySheetsDialog.this.dispose();
				};				
			} .execute();
		}

		abstract void notifySheet(IPropertySheet sheet);

	}


	private class CancelAction extends DispatchAction {

		public CancelAction() {
			putValue(Action.NAME, Messages.getString("PropertySheetsDialog.CancelAction.ActionName")); //$NON-NLS-1$
		}

		// called within a loop outside the EDT for each sheet:
		@Override
		void notifySheet(IPropertySheet sheet) {
			sheet.resetAction();
		}

	}


	private class OkAction extends DispatchAction {

		public OkAction() {
			putValue(Action.NAME, Messages.getString("PropertySheetsDialog.OkAction.ActionName")); //$NON-NLS-1$
		}

		// called withing a loop outside the EDT for each sheet:
		@Override
		void notifySheet(IPropertySheet sheet) {
			sheet.applyAction();
		}

	}



}
