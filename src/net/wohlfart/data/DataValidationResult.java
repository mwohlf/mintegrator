package net.wohlfart.data;

public class DataValidationResult {

	public static final DataValidationResult VALID = new DataValidationResult() {
		@Override
		public boolean isValid() {
			return true;
		}
	};
	
	
	public boolean isValid() {
		return false;
	}
	
	
	// return an user readable error message
	public String getErrorMessage() {
		return "validation error";
	}

}
