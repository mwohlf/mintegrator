package net.wohlfart.gui.properties.look;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public final class LookAndFeelInfoCollection extends HashMap<String, LookAndFeelInfo> {
	/**  Logger for this class */
	private static final Logger LOG = LoggerFactory.getLogger(LookAndFeelInfoCollection.class);

	private static LookAndFeelInfoCollection instance;

	// we use the name of the info class of the LookAndFeelInfo as id since it should be unique
	private LookAndFeelInfoCollection() {
		LookAndFeelInfo[] installed = UIManager.getInstalledLookAndFeels();
		for (LookAndFeelInfo info : installed) {
			LOG.debug("  found: {} getClassName(): {}", new Object[] {info.getClassName(), info.getName()});
			put(info.getClassName(), info);
		}
	}

	public static synchronized LookAndFeelInfoCollection getInstance() {
		if (instance == null) {
			instance = new LookAndFeelInfoCollection();
		}
		return instance;
	}


	public LookAndFeelInfo getCurrentLookAndFeelInfo() {
		LookAndFeel lookAndFeel = UIManager.getLookAndFeel();
		String className = lookAndFeel.getClass().getCanonicalName();
		return get(className);
	}
	
	@Override
	public LookAndFeelInfo get(final Object lookAndFeelClassname) {
		LookAndFeelInfo result = super.get(lookAndFeelClassname);
		if (result == null) {
			LOG.warn("lookup for look and feel failed, the lookAndFeelClassname/key was {} "
					+ "the following look and feels are available...", lookAndFeelClassname);
			Set<Entry<String, LookAndFeelInfo>> entries = this.entrySet();
			for (Entry<String, LookAndFeelInfo> entry : entries) {
				LOG.warn("  key: {} value: {}", entry.getKey(), entry.getValue());
			}
		}
		return result;
	}

}
