/**
 *
 */
package net.wohlfart;

import java.awt.EventQueue;

import javax.swing.JComponent;
import javax.swing.RepaintManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A RepaintManager to make sure all access to UI Components happens in the EDT,
 * this is for testing since it slows down Swing quite a bit.
 * 
 * this code is from Alexander Potochkin
 * see: http://weblogs.java.net/blog/alexfromsun/archive/2006/02/debugging_swing.html
 */
public class CheckThreadViolationRepaintManager extends RepaintManager {
	/** Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(CheckThreadViolationRepaintManager.class);

    private boolean completeCheck = true;
    
    
	public static void install() {
		RepaintManager.setCurrentManager(new CheckThreadViolationRepaintManager());
	}


    public boolean isCompleteCheck() {
        return completeCheck;
    }
    
    public void setCompleteCheck(final boolean completeCheck) {
        this.completeCheck = completeCheck;
    }

    @Override
    public synchronized void addInvalidComponent(final JComponent component) {
        checkThreadViolations(component);
        super.addInvalidComponent(component);
    }

    @Override
    public void addDirtyRegion(final JComponent component, int x, int y, int w, int h) {
         checkThreadViolations(component);
         super.addDirtyRegion(component, x, y, w, h);
    }


    private void checkThreadViolations(final JComponent component) {
        if (!EventQueue.isDispatchThread()
                && (completeCheck || component.isShowing())) {
        	
            final Exception exception = new Exception();
            boolean repaint = false;
            boolean fromSwing = false;
            final StackTraceElement[] stackTrace = exception.getStackTrace();
            for (StackTraceElement st : stackTrace) {
                if (repaint && st.getClassName().startsWith("javax.swing.")) { //$NON-NLS-1$
                    fromSwing = true;
                }
                if ("repaint".equals(st.getMethodName())) { //$NON-NLS-1$
                    repaint = true;
                }
            }
            if (repaint && !fromSwing) {
                //no problems here, since repaint() is thread safe
                return;
            }
            LOG.error("thread violation", exception);
        }
    }

}
