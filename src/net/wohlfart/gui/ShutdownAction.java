/**
 *
 */
package net.wohlfart.gui;

import java.awt.event.ActionEvent;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.AbstractAction;
import javax.swing.Action;

import net.wohlfart.ImageManager;
import net.wohlfart.Messages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An Action to fire up the ShutdownWorker.
 * 
 * @author michael
 */
@SuppressWarnings("serial")
@Singleton
public class ShutdownAction extends AbstractAction {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(ShutdownAction.class);

    @Inject
    private Instance<ShutdownWorker> shutdownWorker;

    public ShutdownAction() {
        putValue(Action.NAME, Messages.getString("Shutdown.ActionName")); //$NON-NLS-1$
        putValue(Action.SMALL_ICON, ImageManager.getImageIcon("ShutdownIcon16")); //$NON-NLS-1$
        putValue(Action.LARGE_ICON_KEY, ImageManager.getImageIcon("ShutdownIcon24")); //$NON-NLS-1$
        putValue(Action.SHORT_DESCRIPTION, Messages.getString("Shutdown.ShortDescription")); //$NON-NLS-1$
    }

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
	public void actionPerformed(final ActionEvent evt) {
    	LOG.info("shutdown action triggered by event {}", evt);
        shutdownWorker.get() .execute();
    }

}
