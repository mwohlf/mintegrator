/**
 *
 */
package net.wohlfart.mail.selection;

import static net.wohlfart.properties.ApplicationPropertiesImpl.MAIL_SELECTION_DIALOG;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import net.wohlfart.mail.IMailFolderTreeNode;
import net.wohlfart.mail.IMailMessage;
import net.wohlfart.util.GuiUtilities;

/**
 * @author michael
 *
 */
@SuppressWarnings("serial")
public class MailSelectionDialog  extends JDialog implements ListSelectionListener  {

    private final JButton selectButton = new JButton("�bernehmen");
    private boolean selectedButton;

    private final JButton cancelButton = new JButton("Abbrechen");

    private ArrayList<IMailMessage> selectedMails = new ArrayList<>();

    private final ListSelectionModel selectionModel;

    private final MailTable mailTable;

    private final MailTableModel tableModel;



    private final ArrayList<IMailMessage> mails;

    /* use static initializer */
    protected MailSelectionDialog(Frame frame, IMailFolderTreeNode folder) {
        super(frame);
        setLayout(new BorderLayout());
        setTitle("Bitte Emails ausw�hlen");

        mails = folder.getMailMessages();
        tableModel = new MailTableModel(mails);
        selectionModel = new ToggleListSelectionModel();

        selectionModel.addListSelectionListener(this);

        mailTable = new MailTable(tableModel);
        mailTable.setListSelectionModel(selectionModel);

        add(mailTable, BorderLayout.CENTER);
        JPanel buttons = createButtonContainer();
        add(buttons, BorderLayout.SOUTH);

        selectButton.addActionListener(new ActionListener() {
            @Override
			public void actionPerformed(final ActionEvent evt) {
                selectedButton = true;
                setVisible(false);
            }
        });

        cancelButton.addActionListener(new ActionListener() {
            @Override
			public void actionPerformed(final ActionEvent evt) {
                selectedButton = false;
                setVisible(false);
            }
        });


        getRootPane().setDefaultButton(selectButton);

        setModal(true);
    }




    private JPanel createButtonContainer() {
        JPanel buttons = new JPanel();
        buttons.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 10));
        buttons.add(selectButton);
        buttons.add(cancelButton);
        return buttons;
    }


    public static ArrayList<IMailMessage> selectMailsFromFolder(IMailFolderTreeNode folder) {
        final MailSelectionDialog dialog = new MailSelectionDialog(GuiUtilities.getFrame(), folder);

        // load the prefs or center on screen and pack, if there are no prefs
        GuiUtilities.loadWindowProperties(MAIL_SELECTION_DIALOG, dialog);
        // add a component listener to write back the position and size changes
        GuiUtilities.addComponentListener(MAIL_SELECTION_DIALOG, dialog);
        // blocking until user hit ok or cancel button
        dialog.setVisible(true);
        return dialog.getSelectedMails();
    }


    private ArrayList<IMailMessage> getSelectedMails() {
        if (selectedButton) {
            return selectedMails;
        }
        return new ArrayList<>();
    }

    // selection changed, refresh the selected mails
    @Override
	public void valueChanged(ListSelectionEvent evt) {
        if (evt.getValueIsAdjusting()) {
            return;
        }

        ArrayList<IMailMessage> newSelectedMails = new ArrayList<>();

        int start = selectionModel.getMinSelectionIndex();
        int end = selectionModel.getMaxSelectionIndex();

        // set selected property for all mails that are selected now
        //ArrayList<IMailData> candidateSet = new ArrayList<IMailData>();
        for (int i = start; i <= end; i++) {
            if (selectionModel.isSelectedIndex(i)) {
                int dataRow = mailTable.convertRowIndexToModel(i);
                IMailMessage mail = tableModel.getValueAtRow(dataRow);
                mail.setSelected(true);
                newSelectedMails.add(mail);
            }
        }

        // clear selected property for all mails that are no longer selected
        for (IMailMessage mail: selectedMails) {
            if (!newSelectedMails.contains(mail)) {
                mail.setSelected(false);
            }
        }

        selectedMails = newSelectedMails;
    }



}
