package net.wohlfart.data.persis;

import net.wohlfart.data.IMediumTO;

/**
 * @author michael
 */
public class MediumTO implements IMediumTO {

    private String description;
    private Integer id;
    private String shortDescription;

    @Override
	public String getDescription() {
        return description;
    }

    @Override
	public Integer getId() {
        return id;
    }

    @Override
	public String getShortDescription() {
        return shortDescription;
    }

    @Override
	public void setDescription(String description) {
        this.description = description;
    }

    @Override
	public void setId(Integer id) {
        this.id = id;
    }

    @Override
	public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

}
