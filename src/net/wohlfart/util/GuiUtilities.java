/**
 *
 */
package net.wohlfart.util;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.IOException;
import java.util.HashMap;

import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import net.wohlfart.properties.ApplicationPropertiesImpl;
import net.wohlfart.properties.PropertyKeys;

/**
 * @author michael
 *
 */
public class GuiUtilities {

    public static final Cursor BUSY_CURSOR = new Cursor(Cursor.WAIT_CURSOR);
    public static final Cursor DEFAULT_CURSOR = new Cursor(Cursor.DEFAULT_CURSOR);



    public static final void centerOnScreen(Window window) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension windowSize = window.getSize();

        if (windowSize.height > screenSize.height) {
            windowSize.height = screenSize.height;
        }

        if (windowSize.width > screenSize.width) {
            windowSize.width = screenSize.width;
        }

        window.setLocation((screenSize.width - windowSize.width) / 2,
                (screenSize.height - windowSize.height) / 2);
    }


    // this method should return the root frame of this application
    public static Frame getFrame() {
        Frame[] frames = Frame.getFrames();
        for (Frame element : frames) {
            if (element.isVisible()) {
                return element;
            }
        }
        return null;
    }

    // If a string is on the system clipboard, this method returns it;
    // otherwise it returns null.
    public static String getClipboard() {
        Transferable t = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);

        try {
            if ((t != null) && t.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                String text = (String)t.getTransferData(DataFlavor.stringFlavor);
                return text;
            }
        } catch (UnsupportedFlavorException e) {
        } catch (IOException e) {
        }
        return null;
    }

    // This method writes a string to the system clipboard.
    // otherwise it returns null.
    public static void setClipboard(String str) {
        StringSelection ss = new StringSelection(str);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
    }

    /**
     *
     * method to save the location and position of a dialog or frame
     *
     * @param key
     * @param window
     */
    public static void saveWindowProperties(String key, Window window) {
        ApplicationPropertiesImpl properties = ApplicationPropertiesImpl.getInstance();
        properties.setDimension(PropertyKeys.DIMENSION_FORMAT.format(new Object[] {key}), window.getSize());
        properties.setPoint(PropertyKeys.POINT_FORMAT.format(new Object[] {key}), window.getLocation());
    }


    public static void loadWindowProperties(String windowUid, Window window) {
        ApplicationPropertiesImpl properties = ApplicationPropertiesImpl.getInstance();
        
        // set the dimension or pack the window
        Dimension dim = properties.getDimension(PropertyKeys.DIMENSION_FORMAT.format(new Object[] {windowUid}), null);
        if (dim != null) {
            window.setSize(properties.getDimension(PropertyKeys.DIMENSION_FORMAT.format(new Object[] {windowUid})));
        } else {
            window.pack();
        }

        // set the location or center on screen
        Point point = properties.getPoint(PropertyKeys.POINT_FORMAT.format(new Object[] {windowUid}), null);
        if (point != null) {
            window.setLocation(properties.getPoint(PropertyKeys.POINT_FORMAT.format(new Object[] {windowUid})));
        } else {
            centerOnScreen(window);
        }
    }
    /* use the following sequence to get a persistent window properties:

    // load the prefs or center on screen and pack
    SwingUtilities.loadWindowProperties(MAIL_SELECTION_DIALOG, dialog);
    // add a component listener to write back the position and size changes
    SwingUtilities.addComponentListener(MAIL_SELECTION_DIALOG, dialog);

    ...
    dialog.setVisible(true);
     */



    /**
     *
     *
     * @param mail_selection_dialog
     * @param dialog
     */
    public static void addComponentListener(final String key, final Window dialog) {
        dialog.addComponentListener(new ComponentListener() {
            @Override
			public void componentMoved(ComponentEvent evt) {
                save();
            }
            @Override
			public void componentResized(ComponentEvent evt) {
                save();
            }
            @Override
			public void componentShown(ComponentEvent evt) {
                // ignore
            }
            @Override
			public void componentHidden(ComponentEvent evt) {
                // ignore
            }
            public void save() {
                GuiUtilities.saveWindowProperties(key, dialog);
            }
        });
    }



    /**
     *
     * method to save the properties of the tabel columns
     *
     *
     * @param windowUid
     * @param table
     */
    public static void saveColumnProperties(String windowUid, JTable table, TableModel dataModel) {
        ApplicationPropertiesImpl properties = ApplicationPropertiesImpl.getInstance();
        TableColumnModel tableColumnModel = table.getColumnModel();

        int count = dataModel.getColumnCount();
        // note: i is the model column index
        for (int modelIndex=0; modelIndex < count; modelIndex++) {
            int position = table.convertColumnIndexToView(modelIndex);
            if (position < 0) {
                // the column is no longer visible, the property for this is saved in the
                // checkbox action listener
            } else {
                // save the models position for the visible column
                properties.setInteger(PropertyKeys.MODEL_COLUMN_POSITION_FORMAT.format(new Object[] {windowUid,modelIndex}), position);
                // save the width for the visible column
                TableColumn tableColumn = tableColumnModel.getColumn(position);
                int width = tableColumn.getWidth();
                properties.setInteger(PropertyKeys.VIEW_COLUMN_WIDTH_FORMAT.format(new Object[] {windowUid,position}), width);
            }
        }
    }

    /**
     *
     * method to restore the table column properties
     *
     * @param tableUid
     * @param table
     */
    public static void loadColumnProperties(String tableUid, JTable table, TableModel dataModel) {
        ApplicationPropertiesImpl props = ApplicationPropertiesImpl.getInstance();
        TableColumnModel columnModel = table.getColumnModel();



        // cleanup all columns from the column model
        int columns = columnModel.getColumnCount();
        for (int modelIndex = 0; modelIndex < columns; modelIndex++) {
            TableColumn column = columnModel.getColumn(modelIndex);
            columnModel.removeColumn(column);
        }

        HashMap<Integer,TableColumn> columnHash = new HashMap<>();

        // number of column data we have in the table model
        int count = dataModel.getColumnCount();
        // get info for each column in the tablemodel
        int visibleColumns = 0;
        for (int modelIndex = 0; modelIndex < count; modelIndex++) {
            boolean columnIsVisible = props.getColumnIsVisible(tableUid, modelIndex);
            if (columnIsVisible) {
                visibleColumns++;
                TableColumn column = new TableColumn(modelIndex);

                Integer position = props.getInteger(PropertyKeys.MODEL_COLUMN_POSITION_FORMAT.format(new Object[] {tableUid, modelIndex}), 0);
                // move the column right if the position is already taken
                while (columnHash.containsKey(position)) {
                    position++;
                }
                columnHash.put(position, column);
            }
        }

        // adding columns from left to right
        for (int i=0; i<visibleColumns; i++) {
            TableColumn column = columnHash.get(Integer.valueOf(i));
            // adding to the table installs the header too
            // and adds the column to the column model
            table.addColumn(column);
        }

        // setting the visible column widths
        for (int i=0; i<visibleColumns; i++) {
            TableColumn column = columnModel.getColumn(i);
            Integer width = props.getInteger(PropertyKeys.VIEW_COLUMN_WIDTH_FORMAT.format(new Object[] {tableUid, i}), 70);
            column.setPreferredWidth(width);
        }

//        boolean autoResize = props.getAutoResize(tableUid);
//        if (autoResize) {
//            table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
//        } else {
//            table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
//        }


    }


    /**
     * @param mail_table_header
     * @param innerTable
     */
    public static void addColumnModelListener(final String key, final JTable table) {
        JTableHeader tableHeader = table.getTableHeader();
        TableColumnModel columnModel = tableHeader.getColumnModel();
        columnModel.addColumnModelListener(new TableColumnModelListener() {
            @Override
			public void columnAdded(TableColumnModelEvent evt) {
                //System.out.println("columnAdded from: " + evt.getFromIndex() + " to: " + evt.getToIndex());
                save();
            }
            @Override
			public void columnMarginChanged(ChangeEvent evt) {
                //System.out.println("columnMarginChanged: " + evt);
                save();
            }
            @Override
			public void columnMoved(TableColumnModelEvent evt) {
                int from = evt.getFromIndex();
                int to = evt.getToIndex();
                if ( from != to ) {
                    save();
                }
            }
            @Override
			public void columnRemoved(TableColumnModelEvent evt) {
                //System.out.println("columnRemoved from: " + evt.getFromIndex() + " to: " + evt.getToIndex());
                save();
            }
            @Override
			public void columnSelectionChanged(ListSelectionEvent evt) {
                // ignored
            }
            public void save() {
                //System.out.println("save for column listener model called");
                GuiUtilities.saveColumnProperties(key, table, table.getModel());
            }
        });
    }


    // FIXME: remove this ugly code:
    public static void setBusyCursor() {
    	Frame frame = getFrame();
    	if (frame != null) { // frame might be closed already
    		frame.setCursor(BUSY_CURSOR);
    	}
    }

    // FIXME: remove this ugly code:
    public static void setDefaultCursor() {
    	Frame frame = getFrame();
    	if (frame != null) { // frame might be closed already
    		frame.setCursor(DEFAULT_CURSOR);
    	}
    }


    /**
     * delay for debugging
     * @param i
     */
    public static void sleep(int i) {
        try {
            Thread.sleep(i);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

}
