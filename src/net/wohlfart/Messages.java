package net.wohlfart;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This class implements the standard i18n Message pattern.
 *
 * @author michael
 */
public final class Messages {

    /**  Logger for this class.  */
    private static final Logger LOGGER = LoggerFactory.getLogger(Messages.class);

    /**
     * The Resource bundle.
     */
    private static final String BUNDLE_NAME = "net.wohlfart.messages";  //$NON-NLS-1$

    /**
     * The resource bundle for key to message or string mappings.
     */
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

    // prefix for toggle menu item & table header
	public static final String APPLICATION_DATA_PREFIX = "ApplicationDataTable";

    /**
     * The hidden constructor.
     */
    private Messages() {
    	throw new AssertionError("Messages is not supposed to be instanciated, use the static helper methods please"); //$NON-NLS-1$
    }

    /**
     * Accessor Method to find parameterless Strings in the Resources.
     *
     * @param key A String key to find a matching value Object.
     *
     * @return a value string
     */
    public static String getString(final String key) {
        LOGGER.debug("looking for key " + key); //$NON-NLS-1$

        try {
        	String value = RESOURCE_BUNDLE.getString(key);
            LOGGER.debug("found value for key: '{}' -> '{}'", key, value); //$NON-NLS-1$
            return value;
        } catch (MissingResourceException e) {
        	String value = '!' + key + '!';
            LOGGER.warn("Can't find String resource for key: '{}' creating dummy string '{}'", key, value); //$NON-NLS-1$
            return value;
        }
    }


    /**
     * Accessor Method to find Strings and combine them with parameters in the Resources.
     *
     * @param key A String key to find a matching value Object.
     *
     * @return a value string
     */
    public static String getString(final String key, final Object[] fields) {
        LOGGER.debug("looking for key " + key); //$NON-NLS-1$

        try {
            String value = RESOURCE_BUNDLE.getString(key);
            LOGGER.debug("found value for key: '{}' -> '{}'", key, value); //$NON-NLS-1$
            MessageFormat messageFormat = new MessageFormat(value);
            return messageFormat.format(fields);
        } catch (MissingResourceException e) {
        	String value = '!' + key + '!';
            LOGGER.warn("Can't find String resource for key: '{}' creating dummy string '{}'", key, value); //$NON-NLS-1$
            return value;
        }
    }

}
