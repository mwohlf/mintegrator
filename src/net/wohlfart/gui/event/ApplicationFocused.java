package net.wohlfart.gui.event;

import java.beans.VetoableChangeSupport;

import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.data.ICandidateTO;

/**
 * Application Focused is a single application that is currently being worked on
 * we need to register property change listeners on the application bean in order
 * to sync the table data with the form data, we also need this object to be able to
 * fire an event whenever no candidate is selected (get() returns null)
 * 
 * this event is created at 3 point in the application:
 * - value changed in the application list table
 * - create application action
 * - delete application action (null value there)
 * 
 */
public class ApplicationFocused implements IChangeSupportWrapper {

	private final Object element; // the transfer object
	private final VetoableChangeSupport vetoableChangeSupport; // propertyChangeSupport

	// FIXME: this is ugly, we need a constructor without having to drag around the whole datasource...
	//        one solution would be to add the change support right after getting the bean from the database
	public ApplicationFocused(final ICandidateTO element, final IBasicDataSource dataSource) {
		if (element != null) {
			this.vetoableChangeSupport = new VetoableChangeSupport(element);
			this.element = PropertyChangeSupportProxyFactory.createPropertyChangeSupportProxy(element, dataSource, vetoableChangeSupport);		
		} else {
			this.vetoableChangeSupport = null;
			this.element = null;
		}
	}
	
	@Override
	public boolean isEmpty() {
		return (element == null);
	}

	@Override
	public Object getContent() {
		return element;
	}

	@Override
	public VetoableChangeSupport getChangeSupport() {
		return vetoableChangeSupport;
	}

}
