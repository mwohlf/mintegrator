/**
 *
 */
package net.wohlfart.mail.selection;

import javax.swing.DefaultListSelectionModel;
import javax.swing.ListSelectionModel;

/**
 * @author michael
 *
 */
@SuppressWarnings("serial")
public class ToggleListSelectionModel extends DefaultListSelectionModel  {



    private boolean gestureStarted;

    public ToggleListSelectionModel() {
        setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    }


    @Override
    public void setSelectionInterval(int index0, int index1) {
        if (isSelectedIndex(index0) && !gestureStarted) {
            super.removeSelectionInterval(index0, index1);
        } else {
            super.addSelectionInterval(index0, index1);
        }
        gestureStarted = true;
    }

    @Override
    public void setValueIsAdjusting(boolean isAdjusting) {
        if (isAdjusting == false) {
            gestureStarted = false;
        }
        // don't call super!:
        //super.setValueIsAdjusting(isAdjusting);
    }


}
