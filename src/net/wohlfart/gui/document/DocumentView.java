package net.wohlfart.gui.document;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.UIManager;

import net.wohlfart.Messages;
import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.data.IDocumentTO;
import net.wohlfart.data.persis.FolderDAO;
import net.wohlfart.gui.event.DocumentFocused;
import net.wohlfart.worker.DocumentOpenWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * simple filesystem like view for attached documents
 * all events are forwarded to the parent container
 * 
 * @author michael
 */
@SuppressWarnings("serial")
class DocumentView extends JComponent implements DragGestureListener  {
	/** Logger for this class */
	private static final Logger LOG = LoggerFactory.getLogger(DocumentView.class);

	private static final int MIN_WIDTH = 250;
	private static final int MAX_WIDTH = 2048;
	private static final int PANEL_HEIGHT = 60;
	
	@Inject 
	private Event<DocumentFocused> documentFocusEvent;
	@Inject // to open a document on the local system 
	private Instance<DocumentOpenWorker> documentOpenWorker;
	@Inject 
	private IBasicDataSource dataSource;
	@Inject
	private FolderDAO folderDAO;
	
	
	private IDocumentTO selectedDoc;  // the document that has focus at the moment
	private IDocumentTO document; // data model for this view

	public DocumentView() {
		// this must run in EDT
		if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The method doExecute() must be called from EDT."); //$NON-NLS-1$
		}
	}
	
	public void setModel(IDocumentTO document) {
		this.document = document;
	}

	
	@PostConstruct
	protected void postConstruct() {
		initComponents();
	}

	private void initComponents() {
		//setTransferHandler(new DocumentTransferHandler());
		setBorder(BorderFactory.createEtchedBorder());
		setMinimumSize(new Dimension(MIN_WIDTH, PANEL_HEIGHT));
		setPreferredSize(new Dimension(MIN_WIDTH, PANEL_HEIGHT));
		setMaximumSize(new Dimension(MAX_WIDTH, PANEL_HEIGHT));

		addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(final MouseEvent evt) {
				if (((evt.getModifiers() & InputEvent.CTRL_MASK) != 0) 
						&& isSelected()) {
					// deselect if we are selected and control-clicked
					LOG.debug("unselecting doc {}", selectedDoc);
					documentFocusEvent.fire(new DocumentFocused(null, dataSource));
					//documentPanel.setSelectedDocument(null); 
				} else {
					// select otherwise
					//documentPanel.setSelectedDocument(document);
					LOG.debug("selecting doc {}", document);
					documentFocusEvent.fire(new DocumentFocused(document, dataSource));
				}
			}

			@Override
			public void mouseClicked(final MouseEvent evt) {
				// check for double-clicks
				if ((evt.getClickCount() == 2) && (!evt.isPopupTrigger())) {
					//documentPanel.setSelectedDocument(document);
					documentFocusEvent.fire(new DocumentFocused(document, dataSource));
					//documentPanel.openDocument(document);
					documentOpenWorker.get() .doExecute(document);
				}
			}
		});

		setFocusable(true);
		// DragSource dragSource = new DragSource();
		DragSource.getDefaultDragSource()
		.createDefaultDragGestureRecognizer(this,  // component
				DnDConstants.ACTION_COPY_OR_MOVE,  // actions
				this); // DragGestureListener
	}

	@Override
	public void dragGestureRecognized(final DragGestureEvent evt) {
		// create a transferable object and call startDrag()
		//DocumentTransferHandler.setOwnDragInProcess(true);
		LOG.info("drag detected, starting the worker...");
		DocumentExportTransferable export = new DocumentExportTransferable();
		export.preloadData(document, folderDAO);
		evt.startDrag(null, export);
	}


	@Override
	public void paintBorder(Graphics g) {
		super.paintBorder(g);
	}

	@Override
	public void paintChildren(Graphics g) {
		super.paintChildren(g);
	}

	@Override
	public void paintComponent(Graphics g) {
		
		Graphics2D g2 = (Graphics2D)g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		                    RenderingHints.VALUE_ANTIALIAS_ON);

		final int lineOne = 15;
		final int lineTwo = 35;
		final int lineStart = 70;


		// paint selectionBackground depending on the selection status
		if (isSelected()) {
			g.setColor(UIManager.getColor("Table.selectionBackground"));
			Dimension size = getSize();
			g.fillRect(0, 0, size.width, size.height);
			g.setColor(UIManager.getColor("Table.selectionForeground"));
		} else {
			g.setColor(UIManager.getColor("Table.Foreground"));
		}

		//g.setFont(Fonts.getFont(Fonts.STANDARD_BOLD_FONT));
		g.drawString(getNameString(), lineStart, lineOne);  //x-position and borderline of String
		//g.setFont(Fonts.getFont(Fonts.STANDARD_FONT));
		String sizeString = Messages.getString("DocumentView.Size");
		//String lastChangeString = Messages.getString("DocumentView.LastChange");
		g.drawString(sizeString + getSizeString(), lineStart, lineTwo);  //x-position and borderline of String
		// FIXME: implement different ImageFactories
		Icon icon = ThumbnailFactory.getInstance().createThumbnail(document);
		icon.paintIcon(this, g, 1, 1);
	}
	
	
	// return true if the current doc is the selected doc
	private boolean isSelected() {
		return (selectedDoc != null) 
				&& (document != null)
				&& document.getId().equals(selectedDoc.getId());
	}
	
	
	// -- needed for rendering

	private String getNameString() {
		return document.getName();//.getName();
	}

	
	
	private String getSizeString() {

		final int k = 10;
		final int m = 20;

		final double kByte = (1<<k);
		final double mByte = (1<<m);

		String result = null;
		int size = document.getGroesse();//.getGroesse();

		DecimalFormat decFormat = new DecimalFormat("###.00");

		if (size > mByte) { // MByte
			result = decFormat.format(size/mByte) + " MByte";
		} else if (size > kByte) { // kByte
			result = decFormat.format(size/kByte) + " kByte";
		} else {  // Byte
			result = size + " Byte";
		}
		return result;
	}

	// can't use @Observes here CDI would want to instantiate or lookup in some kind of context...
	// so this method is called inthe parent container
	public void onFocusedDocumentEvent(final DocumentFocused container) {
		this.selectedDoc = (IDocumentTO) container.getContent();
		LOG.debug("onFocusedDocumentEvent called in {}, selected doc is {}", this, selectedDoc); //$NON-NLS-1$
	}

}
