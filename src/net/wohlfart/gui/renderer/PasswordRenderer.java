package net.wohlfart.gui.renderer;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

@SuppressWarnings("serial")
public class PasswordRenderer extends DefaultTableCellRenderer  {

    @Override
    public Component getTableCellRendererComponent(
            JTable table,
            Object value,
            boolean isSelected,
            boolean hasFocus,
            int row, int column) {

        return super.getTableCellRendererComponent(table, "**********", isSelected, hasFocus, row, column);
    }

}
