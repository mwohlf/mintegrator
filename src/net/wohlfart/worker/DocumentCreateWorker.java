/**
 *
 */
package net.wohlfart.worker;

import java.awt.EventQueue;
import java.io.File;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.swing.JOptionPane;

import net.wohlfart.CustomSwingWorker;
import net.wohlfart.Messages;
import net.wohlfart.data.ICandidateTO;
import net.wohlfart.data.IDocumentFileTO;
import net.wohlfart.data.IDocumentTO;
import net.wohlfart.data.persis.DocumentDAO;
import net.wohlfart.data.persis.DocumentTO;
import net.wohlfart.data.persis.FolderDAO;
import net.wohlfart.gui.ErrorPane;
import net.wohlfart.gui.event.DocumentCreated;
import net.wohlfart.util.GuiUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * swing worker to upload a document into the database
 * 
 * @author michael
 */
public class DocumentCreateWorker extends CustomSwingWorker<Void, IDocumentTO> {
	/** Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(DocumentCreateWorker.class);

	@Inject
	private FolderDAO folderDAO;
	@Inject
	private DocumentDAO documentDAO;
	@Inject 
	private Event<DocumentCreated> addedDocumentEvent;


	private volatile File[] files;
	private volatile ICandidateTO application;


	public void doExecute(final File[] selectedFiles, final ICandidateTO application) {
		// this must run in EDT
		if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The method doExecute() must be called from EDT."); //$NON-NLS-1$
		}

		if ((selectedFiles == null) 
				|| (selectedFiles.length <= 0)
				|| (application == null)) {
			LOG.info("create was canceled"); //$NON-NLS-1$
			// cancel the future object, this is needed so the get() method doesn't block 
			cancel(true); // (actually it throws an exception now)
			// done();
			return;				
		}

		this.application = application;
		this.files = Arrays.copyOf(selectedFiles, selectedFiles.length);
		
		super.doExecute();

	}

	@Override
	protected Void doInBackground() {
		LOG.debug("doInBackground() - start"); //$NON-NLS-1$

		try {
			setProgress(10);
			if (isCanceled()) {
				return null;
			}

			final int total = files.length;
			int uploaded = 0;

			String note;
			for (File file : files) {
				uploaded++;

				note = Messages.getString("UploadDocumentsWorker.Note2",   //$NON-NLS-1$
						new Object[] {uploaded, total});

				// we have a range of 80
				setProgress(10 + (80 * uploaded / total));
				setNote(note);

				final IDocumentFileTO documentFile = documentDAO.createDocumentFile(file);
				IDocumentTO document = folderDAO.attachDocument(
						application,
						documentFile);

				LOG.debug("document attached: {}", document); //$NON-NLS-1$
				publish(document);	

				if (isCanceled()) {
					break;
				}

			}

			// get the saved documents to be in sync with the DB
			folderDAO.readAllDocuments(application);

		} catch (SQLException ex) {
			LOG.error("doInBackground()", ex);
			progressMonitor.close();
			JOptionPane.showMessageDialog(
					null,
					ex.getMessage(),
					Messages.getString("UploadDocumentsWorker.SQLException"), //$NON-NLS-1$
					JOptionPane.ERROR_MESSAGE);
		} catch (Throwable th) {
			LOG.error("doInBackground()", th);
			GuiUtilities.setDefaultCursor();
			progressMonitor.close();
			ErrorPane.showErrorDialog(th, Messages.getString("DeleteSelectedCandidatesWorker.Throwable")); //$NON-NLS-1$
		} finally {
			queue.clearBlockedComponents();
			GuiUtilities.setDefaultCursor();
			LOG.info("doInBackground() - finally");
		}

		return null; 
	}



	/**
	 * called in the EDT with the fetched data
	 */
	@Override
	protected void process(List<IDocumentTO> chunks) {	
		// FIXME: instead of directly calling the documentPanel we need to 
		// use an event here
		for (IDocumentTO doc : chunks) {
			addedDocumentEvent.fire(new DocumentCreated((DocumentTO)doc));
		}
	}

	@Override
	protected void done() {
		super.done();
		addedDocumentEvent.fire(new DocumentCreated(null));
	}
//
//	@Override
//	public void done() {
//		super.done();
//		// EDT here
//		// FIXME: instead of directly calling the documentPanel we need to 
//		// use an event here
//		documentPanel.updateView(application);
//	}

}
