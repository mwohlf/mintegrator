package net.wohlfart.action;

import static net.wohlfart.util.GuiUtilities.setClipboard;

import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;

import net.wohlfart.ImageManager;
import net.wohlfart.Messages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * @author michael
 */
@SuppressWarnings("serial")
@Singleton
public class GlobalCopyAction extends AbstractAction implements CaretListener {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(GlobalCopyAction.class);

	private static final String PERMANENT_FOCUS_OWNER = "permanentFocusOwner"; //$NON-NLS-1$
	private static final KeyStroke KEYSTROKE = KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK);
	
	public GlobalCopyAction() {
		putValue(Action.NAME, Messages.getString("GlobalCopyAction.ActionName")); //$NON-NLS-1$
		putValue(Action.SMALL_ICON, ImageManager.getImageIcon("CopyIcon16")); //$NON-NLS-1$
		putValue(Action.LARGE_ICON_KEY, ImageManager.getImageIcon("CopyIcon24")); //$NON-NLS-1$
		putValue(Action.SHORT_DESCRIPTION, Messages.getString("GlobalCopyAction.ShortDescription")); //$NON-NLS-1$
		putValue(Action.ACCELERATOR_KEY, KEYSTROKE);
	}
	
    @PostConstruct
    protected void postConstruct() { // NO_UCD
    	LOG.debug("postConstruct() called"); //$NON-NLS-1$
		// patch the default keymap for JTextComponents
		// http://www.java-tips.org/java-se-tips/javax.swing/how-to-map-actions-with-keystrokes.html
		Keymap defaultKeymap = JTextComponent.getKeymap(JTextComponent.DEFAULT_KEYMAP);			
		defaultKeymap.addActionForKeyStroke(
				KEYSTROKE, 
	    		this);

		// keep the CaretListener on the focus owner all the time...
		KeyboardFocusManager keyboardFocusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
		keyboardFocusManager.addPropertyChangeListener(PERMANENT_FOCUS_OWNER, new PropertyChangeListener() {
			// usually this event is fired twice first with empty getNewValue then with empty getOldValue
			@Override 
			public void propertyChange(PropertyChangeEvent evt) {
				LOG.debug("permanent focus owner changed: {}", evt); //$NON-NLS-1$

				Object oldValue = evt.getOldValue();
				if (oldValue != null) {
					if (oldValue instanceof JTextComponent) {
						JTextComponent textComponent = (JTextComponent)oldValue;
						textComponent.removeCaretListener(GlobalCopyAction.this);
					}
				}

				Object newValue = evt.getNewValue();
				if (newValue != null) {
					if (newValue instanceof JTextComponent) {
						JTextComponent textComponent = (JTextComponent)newValue;
						textComponent.addCaretListener(GlobalCopyAction.this);
					}
				}
			}			
		});
		setEnabled(false);
	}


	@Override
	public void actionPerformed(final ActionEvent evt) {
		LOG.info("copy event happend: {}", evt); //$NON-NLS-1$
		Object object = KeyboardFocusManager.getCurrentKeyboardFocusManager().getPermanentFocusOwner();
		if (object instanceof JTextComponent) {
			JTextComponent textComponent = (JTextComponent)object;
			String selected = textComponent.getSelectedText();
			if (selected.length() > 0) {
				setClipboard(selected);
			}
		} else {
			LOG.info("can't copy, focus owner is not a JTextComponent: {}", object); //$NON-NLS-1$
		}
	}

	@Override
	public void caretUpdate(final CaretEvent evt) {
		// check for a selection, no selection, no copy enabled:
		if (evt.getDot() != evt.getMark()) {
			setEnabled(true);
		} else {
			setEnabled(false);
		}
	}

}
