package net.wohlfart.data;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;

import net.wohlfart.ImageManager;
import net.wohlfart.Messages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An Action that connects to the database by invoking a SwingWorker.
 * 
 * @author michael
 */
@SuppressWarnings("serial")
@Singleton
public class ConnectDatabaseAction extends AbstractAction {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(ConnectDatabaseAction.class);
  
    @Inject
    private Instance<ConnectDatabaseWorker> connectDatabaseWorker;

    public ConnectDatabaseAction() {
        putValue(Action.NAME, Messages.getString("ConnectDatabase.ActionName")); //$NON-NLS-1$
        putValue(Action.SMALL_ICON, ImageManager.getImageIcon("ConnectDBIcon16")); //$NON-NLS-1$
        putValue(Action.LARGE_ICON_KEY, ImageManager.getImageIcon("ConnectDBIcon24")); //$NON-NLS-1$
        putValue(Action.SHORT_DESCRIPTION, Messages.getString("ConnectDatabase.ActionShortDescription")); //$NON-NLS-1$
        putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_MASK));
    }

    @Override
	public void actionPerformed(final ActionEvent evt) {
    	LOG.debug("connect database action triggered"); //$NON-NLS-1$
        // can't use a singleton, this pulls a new instance of a SwingWorker each time
        connectDatabaseWorker.get() .doExecute();
    }

  
    public void onAnyDataSourceEvent(@Observes final IBasicDataSource dataSource) {
        LOG.debug("onAnyDataSourceEvent called, checking if the datasource is closed: '{}' " //$NON-NLS-1$
        		+ "(from dataSource.isClosed())", dataSource.isClosed()); //$NON-NLS-1$
        setEnabled(dataSource.isClosed());
    }
    
}
