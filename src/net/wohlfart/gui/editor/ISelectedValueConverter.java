package net.wohlfart.gui.editor;


/**
 * simple interface for converting an Object (usually from a JComboBox) into
 * a String property for UI rendering, its also used to set the property of a bean
 * in case user selected a specific JComboBox element and we need to set a bean property
 * which is string
 * 
 * @author michael
 */
public interface ISelectedValueConverter<T> {
	
	abstract String convert2ViewString(T object);
	
	abstract String convert2BeanProperty(T object);
	
}
