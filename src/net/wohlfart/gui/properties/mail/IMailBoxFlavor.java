package net.wohlfart.gui.properties.mail;

import javax.swing.JComponent;

import net.wohlfart.mail.IMailFolderTreeNode;
import net.wohlfart.properties.IApplicationProperties;

public interface IMailBoxFlavor {

	public abstract String getId();


	// the user frontend to setup a specific mailbox flavor
	public abstract JComponent getPropertyEditor();	


	// called before the editor is displayed to the user, this way some
	// expensive operations can be performed lazy
	public abstract void initEditor(IApplicationProperties properties);

	// called when the user hits the reset button, try to get the data from the properties
	public abstract void resetEditor(IApplicationProperties properties);
	
	// try to test the selected editor parameters
	public abstract void testEditor(IApplicationProperties properties);
	
	// an implementation of the mailbox flavor is responsible to store its parameters in the properties
	public abstract void applyEditor(IApplicationProperties properties);

	
	// MailSource is where we import mails from, a mailbox flavor must recover
	// the mail source from the properties or return null if nothing has been configured yet
	// or the recovery fails for whatever reason...
	public abstract IMailFolderTreeNode getMailSource(IApplicationProperties properties);



}
