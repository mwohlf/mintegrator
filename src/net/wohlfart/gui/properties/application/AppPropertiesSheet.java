package net.wohlfart.gui.properties.application;

import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.lang.reflect.InvocationTargetException;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import net.wohlfart.ImageManager;
import net.wohlfart.Messages;
import net.wohlfart.gui.properties.AbstractPropertySheet;
import net.wohlfart.properties.IApplicationProperties;
import net.wohlfart.properties.PropertyKeys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Application Properties settings panel
 * 
 * 
 * @author michael
 */
@SuppressWarnings("serial")
@Singleton
public class AppPropertiesSheet extends AbstractPropertySheet {
	/**  Logger for this class */
	private static final Logger LOG = LoggerFactory.getLogger(AppPropertiesSheet.class);

	public static final String NAME = "AppPropertiesSheet"; // for lookup in the component hierarchy //$NON-NLS-1$

	//   private final JComboBox jobOffersChoice;
	private final JTextField docDescription;
	private final JTextField docGroup;

	private final JCheckBox safeDeleteCheck;
	private final JCheckBox safeImportCheck;
	private final JCheckBox quitCheck;

	private final JCheckBox saveEmailAsRemarkCheck;
	private final JCheckBox saveEmailAsFileCheck;

	private final JCheckBox markEmailsCheck;


	@Inject
	private IApplicationProperties properties;



	AppPropertiesSheet() {
		super(ImageManager.getImageIcon("NewApplicationIcon24"),  //$NON-NLS-1$
				Messages.getString("AppPropertiesSheet.title"),   //$NON-NLS-1$
				Messages.getString("AppPropertiesSheet.tooltip")); //$NON-NLS-1$
		setName(NAME);
		// setup the finals
		docDescription = new JTextField();
		docDescription.setName(PropertyKeys.DOC_DESCRIPTION);
		docGroup = new JTextField();
		docGroup.setName(PropertyKeys.DOC_GROUP);

		safeDeleteCheck = new JCheckBox(Messages.getString("AppPropertiesSheet.safeDeleteCheckLabel")); //$NON-NLS-1$
		safeDeleteCheck.setName(PropertyKeys.CONFIRM_DELETE);
		safeImportCheck = new JCheckBox(Messages.getString("AppPropertiesSheet.safeImportCheckLabel")); //$NON-NLS-1$
		safeImportCheck.setName(PropertyKeys.CONFIRM_SELECTION);
		quitCheck = new JCheckBox(Messages.getString("AppPropertiesSheet.quitCheckLabel")); //$NON-NLS-1$
		quitCheck.setName(PropertyKeys.CONFIRM_QUIT);

		saveEmailAsRemarkCheck = new JCheckBox(Messages.getString("AppPropertiesSheet.saveEmailAsRemarkCheckLabel")); //$NON-NLS-1$
		saveEmailAsRemarkCheck.setName(PropertyKeys.SAVE_EMAIL_AS_REMARK_CHECK);
		saveEmailAsFileCheck = new JCheckBox(Messages.getString("AppPropertiesSheet.saveEmailAsFileCheck")); //$NON-NLS-1$
		saveEmailAsFileCheck.setName(PropertyKeys.SAVE_EMAIL_AS_FILE_CHECK);

		markEmailsCheck = new JCheckBox(Messages.getString("AppPropertiesSheet.markEmailsCheckLabel")); //$NON-NLS-1$
		markEmailsCheck.setName(PropertyKeys.MARK_EMAILS_CHECK);
	}

	@PostConstruct
	protected void postConstruct() { // NO_UCD
		LOG.debug("postConstruct() called"); //$NON-NLS-1$
		initWiring();
		initComponents();
	}


	private void initWiring() {
		// reset the fields not much else to wire here
		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() throws Exception {
				resetAction(); // can't run in EDT, need a swingworker for this
				return null;
			}
		} .execute();
	}

    protected void initComponents() { // NO_UCD
    	LOG.debug("postConstruct() called"); //$NON-NLS-1$
		// do the layout
		setLayout(new GridBagLayout());
		setName(NAME);

		GridBagConstraints labelColumn = new GridBagConstraints();
		labelColumn.insets = new Insets(5,3,0,10);
		labelColumn.anchor = GridBagConstraints.WEST;
		GridBagConstraints editorColumn = new GridBagConstraints();
		editorColumn.insets = new Insets(1,10,0,3);
		editorColumn.anchor = GridBagConstraints.WEST;
		editorColumn.fill = GridBagConstraints.HORIZONTAL;
		editorColumn.weightx = 0.5;
		labelColumn.gridy = editorColumn.gridy=0;

		// docDescription
		add(new JLabel(Messages.getString("AppPropertiesSheet.docDescriptionLabel")) //$NON-NLS-1$
		{{
			setLabelFor(docDescription);
		}}, labelColumn);
		labelColumn.gridy = ++editorColumn.gridy;
		add(docDescription, editorColumn);
		labelColumn.gridy = ++editorColumn.gridy;

		// docGroup
		add(new JLabel(Messages.getString("AppPropertiesSheet.docGroupLabel")) //$NON-NLS-1$
		{{
			setLabelFor(docGroup);
		}}, labelColumn);
		labelColumn.gridy = ++editorColumn.gridy;
		add(docGroup, editorColumn);
		labelColumn.gridy = ++editorColumn.gridy;

		// docDescription
		add(new JLabel(Messages.getString("AppPropertiesSheet.validationChecksLabel")), labelColumn); //$NON-NLS-1$
		labelColumn.gridy = ++editorColumn.gridy;
		add(safeDeleteCheck, editorColumn);
		labelColumn.gridy = ++editorColumn.gridy;
		add(safeImportCheck, editorColumn);
		labelColumn.gridy = ++editorColumn.gridy;
		add(quitCheck, editorColumn);
		labelColumn.gridy = ++editorColumn.gridy;

		// email handling
		add(new JLabel(Messages.getString("AppPropertiesSheet.processingEmailLabel")), labelColumn); //$NON-NLS-1$
		labelColumn.gridy = ++editorColumn.gridy;
		add(saveEmailAsRemarkCheck, editorColumn);
		labelColumn.gridy = ++editorColumn.gridy;
		add(saveEmailAsFileCheck, editorColumn);
		labelColumn.gridy = ++editorColumn.gridy;

		// outlook checkmark
		add(new JLabel(Messages.getString("AppPropertiesSheet.outlookMarkerLabel")), labelColumn); //$NON-NLS-1$
		labelColumn.gridy = ++editorColumn.gridy;
		add(markEmailsCheck, editorColumn);
		labelColumn.gridy = ++editorColumn.gridy;


		// -- filler component

		// add a filler component	
		GridBagConstraints filler = new GridBagConstraints();
		filler.gridy = labelColumn.gridy = editorColumn.gridy;
		filler.gridwidth = 2;
		filler.weightx = filler.weighty = 0.5;
		add(Box.createGlue(), filler);
		filler.gridy = labelColumn.gridy = ++editorColumn.gridy;
	}

	// this is called outside the EDT
	@Override
	public void resetAction() {
		LOG.info("resetting values in {} ", NAME); //$NON-NLS-1$	
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The method resetAction() needs to run outside the EDT"); //$NON-NLS-1$
		}

		// we need to set the values in the EDT
		try {
			EventQueue.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					docDescription.setText(properties.getString(PropertyKeys.DOC_DESCRIPTION, "")); //$NON-NLS-1$
					docGroup.setText(properties.getString(PropertyKeys.DOC_GROUP, "")); //$NON-NLS-1$		
					safeDeleteCheck.setSelected(properties.getBoolean(PropertyKeys.CONFIRM_DELETE, true));		
					safeImportCheck.setSelected(properties.getBoolean(PropertyKeys.CONFIRM_SELECTION, true));		
					quitCheck.setSelected(properties.getBoolean(PropertyKeys.CONFIRM_QUIT, true));		
					saveEmailAsRemarkCheck.setSelected(properties.getBoolean(PropertyKeys.SAVE_EMAIL_AS_REMARK_CHECK, true));		
					saveEmailAsFileCheck.setSelected(properties.getBoolean(PropertyKeys.SAVE_EMAIL_AS_FILE_CHECK, true));		
					markEmailsCheck.setSelected(properties.getBoolean(PropertyKeys.MARK_EMAILS_CHECK, true));		
				}});
		} catch (InvocationTargetException | InterruptedException ex) {
			LOG.warn("error setting values, exception is ignored", ex); //$NON-NLS-1$
		}
	}

	@Override
	public void applyAction() {
		LOG.info("applying values in {} ", NAME); //$NON-NLS-1$
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The method applyAction() needs to run outside the EDT"); //$NON-NLS-1$
		}

		// we need to set the values in the EDT
		try {
			EventQueue.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					properties.setString(PropertyKeys.DOC_DESCRIPTION, docDescription.getText().trim());
					properties.setString(PropertyKeys.DOC_GROUP, docGroup.getText().trim());		
					properties.setBoolean(PropertyKeys.CONFIRM_DELETE, safeDeleteCheck.isSelected());
					properties.setBoolean(PropertyKeys.CONFIRM_SELECTION, safeImportCheck.isSelected());
					properties.setBoolean(PropertyKeys.CONFIRM_QUIT, quitCheck.isSelected());
					properties.setBoolean(PropertyKeys.SAVE_EMAIL_AS_REMARK_CHECK, saveEmailAsRemarkCheck.isSelected());
					properties.setBoolean(PropertyKeys.SAVE_EMAIL_AS_FILE_CHECK, saveEmailAsFileCheck.isSelected());
					properties.setBoolean(PropertyKeys.MARK_EMAILS_CHECK, markEmailsCheck.isSelected());		
				}});
		} catch (InvocationTargetException | InterruptedException ex) {
			LOG.warn("error setting values, exception is ignored", ex); //$NON-NLS-1$
		}
	}

}
