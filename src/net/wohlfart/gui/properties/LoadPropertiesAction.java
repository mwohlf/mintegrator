/**
 *
 */
package net.wohlfart.gui.properties;

import java.awt.event.ActionEvent;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.SwingWorker;

import net.wohlfart.ImageManager;
import net.wohlfart.Messages;
import net.wohlfart.gui.ErrorPane;
import net.wohlfart.properties.IApplicationProperties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Load the applications properties with included SwingWorker...
 * 
 * @author michael
 */
@SuppressWarnings("serial")
@Singleton
public class LoadPropertiesAction extends AbstractAction {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(LoadPropertiesAction.class);

	@Inject
	private IApplicationProperties properties;

    public LoadPropertiesAction() {
        putValue(Action.NAME, Messages.getString("LoadProperties.ActionName")); //$NON-NLS-1$
        putValue(Action.SMALL_ICON, ImageManager.getImageIcon("LoadPropertiesIcon16")); //$NON-NLS-1$
        putValue(Action.LARGE_ICON_KEY, ImageManager.getImageIcon("LoadPropertiesIcon24")); //$NON-NLS-1$
        putValue(Action.SHORT_DESCRIPTION, Messages.getString("LoadProperties.ShortDescription")); //$NON-NLS-1$
    }

    @Override
	public void actionPerformed(final ActionEvent evt) {
		loadProperties();
    }
    
	/**
	 * this method can be called from another class in order to trigger
	 * a save for the properties (e.g. during shutdown)
	 */
	public void loadProperties() {
		loadProperties(properties);
	}

	private void loadProperties(final IApplicationProperties properties) {
		// fire up a SwingWorker to do the job
		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() throws Exception {
				try {
					properties.load();
				} catch (Throwable th) {
					LOG.error("failed to store properties", th);
					ErrorPane.showErrorDialog(th, Messages.getString("SaveProperties.Failed"));
				}
				return null;
			}} .execute();
	}

}
