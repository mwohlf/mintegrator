package net.wohlfart.gui.table;

import java.util.Collections;
import java.util.List;

import net.wohlfart.data.persis.CandidateTO;

/**
 * needed to fire events with a null candidate
 */
public class ApplicationListFull {

	private List<CandidateTO> elements = Collections.emptyList();

	public ApplicationListFull(final List<CandidateTO> elements) {
		if (elements == null) {
			this.elements = Collections.emptyList();
		} else {
			this.elements = elements;
		}
	}

	
	public List<CandidateTO> get() {
		return elements;
	}

}
