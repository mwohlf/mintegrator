/**
 *
 */
package net.wohlfart.gui.editor;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.im.InputContext;
import java.io.IOException;

import javax.swing.JComponent;
import javax.swing.TransferHandler;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;

@SuppressWarnings("serial")
public class TextTransferHandler extends TransferHandler {

    @Override
    public void exportToClipboard(JComponent comp, Clipboard clipboard, int action) throws IllegalStateException {

        if (comp instanceof JTextComponent) {
            JTextComponent text = (JTextComponent)comp;
            int p0 = text.getSelectionStart();
            int p1 = text.getSelectionEnd();
            if (p0 != p1) {
                try {
                    Document doc = text.getDocument();
                    String srcData = doc.getText(p0, p1 - p0);
                    StringSelection contents = new StringSelection(srcData);

                    clipboard.setContents(contents, null);

                    if (action == TransferHandler.MOVE) {
                        doc.remove(p0, p1 - p0);
                    }
                } catch (BadLocationException ble) {}
            }
        }
    }

    @Override
    public boolean importData(JComponent comp, Transferable t) {

        if (comp instanceof JTextComponent) {
            DataFlavor flavor = getFlavor(t.getTransferDataFlavors());

            if (flavor != null) {
                InputContext ic = comp.getInputContext();
                if (ic != null) {
                    ic.endComposition();
                }
                try {
                    String data = (String)t.getTransferData(flavor);

                    ((JTextComponent)comp).replaceSelection(data);
                    return true;
                } catch (UnsupportedFlavorException ufe) {
                } catch (IOException ioe) {
                }
            }
        }
        return false;
    }

    @Override
    public boolean canImport(JComponent comp, DataFlavor[] transferFlavors) {

        JTextComponent c = (JTextComponent)comp;
        if (!(c.isEditable() && c.isEnabled())) {
            return false;
        }
        return (getFlavor(transferFlavors) != null);
    }

    @Override
    public int getSourceActions(JComponent c) {

        return COPY_OR_MOVE;
    }

    private DataFlavor getFlavor(DataFlavor[] flavors) {

        if (flavors != null) {
            for (DataFlavor element : flavors) {
                if (element.equals(DataFlavor.stringFlavor)) {
                    return element;
                }
            }
        }
        return null;
    }

    @Override
    public Transferable createTransferable(JComponent comp) {

        if (comp instanceof JTextComponent) {
            JTextComponent text = (JTextComponent)comp;
            int p0 = text.getSelectionStart();
            int p1 = text.getSelectionEnd();
            if (p0 != p1) {
                try {
                    Document doc = text.getDocument();
                    String srcData = doc.getText(p0, p1 - p0);
                    StringSelection contents = new StringSelection(srcData);
                    return contents;

                } catch (BadLocationException ble) {}
            }
        }
        return null;
    }

}
