package net.wohlfart.gui.properties.database.flavour;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.HashMap;

import net.wohlfart.data.ICandidateTO;
import net.wohlfart.data.IFolderTO;
import net.wohlfart.data.IJobOfferTO;
import net.wohlfart.data.IMediumTO;
import net.wohlfart.properties.PropertyKeys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class Oracle extends AbstractDatabaseFlavor implements IDatabaseFlavor {
	private static final Logger LOG = LoggerFactory.getLogger(Oracle.class);

	//private static final String FILE_EXTENSION = "oracle";
	private static final String DEFAULT_PORT = "1521";
	private static final String DB_PROTOCOL = "jdbc:oracle:thin:";
	private static final String DB_DRIVERCLASS = "oracle.jdbc.OracleDriver";
	private static final String DB_URL_FORMAT = "{0}@{1}:{2,number,#}:{3}";   // {0}: protocol, {1}: host, {2}: port, {3}: dbName  

	// package private
	Oracle(final String id) {
		// only one instance per application otherwise we have to override equals/hash ...
		super(id);
	}
	
	@Override
	@SuppressWarnings("serial")
	protected HashMap<Class<?>,String> getMap() {
		return new HashMap<Class<?>,String>() {{ // NO_UCD
			put(ICandidateTO.class, "CandidateTOoracle.xml");
			put(IFolderTO.class, "FolderTOoracle.xml");
			put(IJobOfferTO.class, "JobOfferTOdefault.xml");
			put(IMediumTO.class, "MediumTOdefault.xml");
		}};
	}

	@Override
	public String getUrl(final String host, final Integer port, final String dbName) {
		return MessageFormat.format(DB_URL_FORMAT, new Object[] {
				DB_PROTOCOL, host, port, dbName
		});
	}

//	@Override
//	public String getFileExtension() {
//		return FILE_EXTENSION;
//	}

	@Override
	public String getDriverClassName() {
		return DB_DRIVERCLASS;
	}

	@Override
	public String getDefaultValue(final String key, final String defaultValue) {
		switch (key) {
		case PropertyKeys.DATABASE_PORT:
			return DEFAULT_PORT;
		case PropertyKeys.DATABASE_HOST:
			return "192.168.178.28";
		case PropertyKeys.DATABASE_NAME:
			return "XE";
		case PropertyKeys.DATABASE_USER_NAME:
			return "PERSIS";
		case PropertyKeys.DATABASE_PASSWORD:
			return "PERSIS";
		default:
			return defaultValue;
		}
	}

	
	@Override
	public void validateConnection(final Connection connection) throws SQLException {
		String timestampSelect = "select systimestamp from dual";		
	    Statement statement = connection.createStatement();
	    ResultSet resultSet = statement.executeQuery(timestampSelect);
	    resultSet.next();
	    String timestamp = resultSet.getString(1);
	    if (timestamp == null || timestamp.length() == 0) {
	    	throw new SQLException("can't get time form DB, select was {}", timestampSelect);
	    }
	    LOG.info("validated database connection, timestamp selected: {}, select was {}", timestamp, timestampSelect);
	    resultSet.close();
	    statement.close();
	}

}
