package net.wohlfart.properties;

import java.text.MessageFormat;

/**
 * FIXME: we should convert this into an enumeration...
 * 
 * collection of keys that are used to store data in the properties file
 * 
 * @author michael
 */
public class PropertyKeys {

	public static final String DRIVER_PROTOCOL = "DRIVER_PROTOCOL";
	public static final String DRIVER_CLASS_NAME = "DRIVER_CLASS_NAME";
	public static final String DATABASE_USER_NAME = "DATABASE_USER_NAME";
	public static final String DATABASE_PASSWORD = "DATABASE_PASSWORD";


    public static final String DATABASE_TYPE = "DATABASE_TYPE";

	public static final String DATABASE_HOST = "DATABASE_HOST";
	public static final String DATABASE_PORT = "DATABASE_PORT";
	public static final String DATABASE_NAME = "DATABASE_NAME";
	public static final String DATABASE_URL_FORMAT = "DATABASE_URL_FORMAT";


	public static final String JOB_OFFER_SELECTION = "JOB_OFFER_SELECTION";

	public static final String AUTO_CONNECT = "AUTO_CONNECT";
	public static final String CONFIRM_QUIT = "CONFIRM_QUIT";
	public static final String CONFIRM_DELETE = "CONFIRM_DELETE";
	public static final String CONFIRM_SELECTION = "CONFIRM_SELECTION";

	public static final String SAVE_EMAIL_AS_FILE = "SAVE_EMAIL_AS_FILE";
	public static final String SAVE_EMAIL_AS_REMARK = "SAVE_EMAIL_AS_REMARK";

	public static final String MAIL_SOURCE_FLAVOR = "MAIL_SOURCE_FLAVOR";

	public static final String OUTLOOK_ENTRY_ID = "OUTLOOK_ENTRY_ID";
	public static final String OUTLOOK_STORE_ID = "OUTLOOK_STORE_ID";

	public static final String MARK_IMPORTED_EMAILS = "MARK_IMPORTED_EMAILS";

	public static final String DOC_DESCRIPTION = "DOC_DESCRIPTION"; // goes into the BEZ field in INET_BWR_ATT
	public static final String DOC_GROUP = "DOC_GROUP"; // goes into the GROUP field in INET_BWR_ATT

	// divider locations
	public static final String VERTICAL_SPLIT_PANE = "VERTICAL_SPLIT_PANE";
	public static final String HORIZONTAL_SPLIT_PANE = "HORIZONTAL_SPLIT_PANE";



	// prefix for the mailselection dialog
	public static final String MAIL_TABLE = "MAIL_TABLE";
	// prefix for the candidate dialog
	public static final String CANDIDATE_TABLE = "CANDIDATE_TABLE";


	// format keys to store column sort and width information for a table
	public static final MessageFormat MODEL_COLUMN_POSITION_FORMAT = new MessageFormat("{0}_MODEL_COLUMN_INDEX_{1}");
	static final MessageFormat MODEL_COLUMN_VISIBLE_FORMAT = new MessageFormat("{0}_MODEL_COLUMN_VISIBLE_{1}");
	public static final MessageFormat VIEW_COLUMN_WIDTH_FORMAT = new MessageFormat("{0}_VIEW_COLUMN_WIDTH_{1}");

	// standard point and dimension format keys
	public static final MessageFormat DIMENSION_FORMAT = new MessageFormat("{0}_DIMENSION");
	public static final MessageFormat POINT_FORMAT = new MessageFormat("{0}_POINT");
	static final MessageFormat AUTOREZIZE_FORMAT = new MessageFormat("{0}_AUTORESIZE");
	public static final String PROPERTIES_FILE_TAG = "PROPERTIES_FILE_TAG";
	
	
	
	// used in AppPropertiesSheet
	//public static final String SAFE_DELETE_CHECK = "SAFE_DELETE_CHECK";
	//public static final String SAFE_IMPORT_CHECK = "SAFE_IMPORT_CHECK";
	//public static final String QUIT_CHECK = "QUIT_CHECK";
	public static final String SAVE_EMAIL_AS_REMARK_CHECK = "SAVE_EMAIL_AS_REMARK_CHECK";
	public static final String SAVE_EMAIL_AS_FILE_CHECK = "SAVE_EMAILS_AS_FILE_CHECK";
	public static final String MARK_EMAILS_CHECK = "MARK_EMAILS_CHECK";
	
	
	public static final String BASE_FRAME_DIMENSION = "BASE_FRAME_DIMENSION";
	public static final String BASE_FRAME_LOCATION = "BASE_FRAME_LOCATION";
	public static final String DATABASE_FLAVOR_ID = "DATABASE_FLAVOR_ID";
	
	public static final String LOOK_AND_FEEL_INFO = "LOOK_AND_FEEL_INFO";
	public static final String LOOK_AND_FEEL_THEME = "LOOK_AND_FEEL_THEME";
	


}
