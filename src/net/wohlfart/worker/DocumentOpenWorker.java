/**
 *
 */
package net.wohlfart.worker;

import static net.wohlfart.Constants.DELAY_WORKERS;
import static net.wohlfart.util.GuiUtilities.getFrame;

import java.awt.Desktop;
import java.awt.EventQueue;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;

import javax.inject.Inject;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import net.wohlfart.CustomEventQueue;
import net.wohlfart.Messages;
import net.wohlfart.data.IDocumentTO;
import net.wohlfart.data.persis.FolderDAO;
import net.wohlfart.gui.ErrorPane;
import net.wohlfart.util.GuiUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author michael
 *
 */
public class DocumentOpenWorker extends SwingWorker<Void, Void> {
	/** Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(DocumentOpenWorker.class);

	private static final String TEMPFILE_PREFIX = "MailIntegrator."; //$NON-NLS-1$

	// FIXME: move this to a config file
	private static final String[] RUN_ENDINGS = new String[] { ".com",".exe", ".sh", ".bash"  }; //$NON-NLS-1$ //$NON-NLS-2$

	// pre 1.6 way to open files:
	// http://www.michael-thomas.com/java/javacert/MyLaunchDefaultBrowser.html
	// http://java.sun.com/developer/technicalArticles/J2SE/Desktop/javase6/desktop_api/
	private static final String WIN_CMD = "rundll32 url.dll,FileProtocolHandler "; //$NON-NLS-1$

	@Inject
	private FolderDAO folderDAO;

	// custom event queue for blocking user interaction while SwingWorker is running
	private final CustomEventQueue queue = CustomEventQueue.getInstance();


	private volatile IDocumentTO document;


	public void doExecute(final IDocumentTO document) {
		// this must run in EDT
		if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The method doExecute() must be called from EDT."); //$NON-NLS-1$
		}

		if (document == null) {
			LOG.info("open was canceled"); //$NON-NLS-1$
			// cancel the future object, this is needed so the get() method doesn't block 
			cancel(true); // (actually it throws an exception now)
			// done();
			return;	
		}
		GuiUtilities.setBusyCursor();
		this.document = document;

		// fire up the swing worker
		execute();
	}

	protected boolean isCanceled() {
		if (DELAY_WORKERS) { GuiUtilities.sleep(2000); }
		return false;
	}

	@Override
	protected Void doInBackground() {
		LOG.debug("doInBackground() - start"); //$NON-NLS-1$

		try {

			// transfer data from DB to local host
			if (document.getData() == null) {
				folderDAO.loadDocumentContent(document);
			}
			// save the data as tempfile
			File tmpFile = File.createTempFile(
					TEMPFILE_PREFIX,
					document.getName());

			// check if the tempfile is executable
			if (isExecutable(document.getName())) {
				LOG.info("executable file won't be opened " + document.getName()); //$NON-NLS-1$
				JOptionPane.showMessageDialog(
						getFrame(),
						Messages.getString("OpenDocumentDataWorker.FileIsExecutable")); //$NON-NLS-1$
				return null; // no opening
			}

			FileOutputStream outStream = new FileOutputStream(tmpFile);
			outStream.write(document.getData());
			outStream.close();

			if (Desktop.isDesktopSupported() 
					&& Desktop.getDesktop().isSupported(Desktop.Action.OPEN)) {
				openWithDesktopSupport(tmpFile);
			} else {
				// backup, FIXME: doesn't work on linux
				openNoDesktopSupport(tmpFile);
			}

		} catch (SQLException | IOException ex) {
			LOG.error("doInBackground()", ex); //$NON-NLS-1$
			ErrorPane.showErrorDialog(ex, Messages.getString("OpenDocumentDataWorker.SQLException")); //$NON-NLS-1$
		} catch (Throwable th) {
			LOG.error("doInBackground()", th); //$NON-NLS-1$
			ErrorPane.showErrorDialog(th, Messages.getString("OpenDocumentDataWorker.Throwable")); //$NON-NLS-1$
		} finally {
			queue.clearBlockedComponents();
			GuiUtilities.setDefaultCursor();
			LOG.debug("doInBackground() - finally"); //$NON-NLS-1$
		}
		return null;

	}


	private void openWithDesktopSupport(final File tmpFile) throws IOException {
		Desktop desktop = Desktop.getDesktop();
		desktop.open(tmpFile);
	}

	// this is plan B in case Desktop is not supported
	private void openNoDesktopSupport(final File tmpFile) throws IOException {
		String cmd = WIN_CMD + tmpFile.getAbsolutePath();
		Runtime.getRuntime().exec(cmd);
	}


	private boolean isExecutable(String filename) {
		boolean executable = false;
		for (String end : RUN_ENDINGS) {
			executable = executable | filename.endsWith(end);
		}
		return executable;
	}

}
