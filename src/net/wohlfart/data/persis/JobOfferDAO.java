package net.wohlfart.data.persis;

import java.awt.EventQueue;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.inject.Inject;

import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.data.IJobOfferTO;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author michael
 */
public class JobOfferDAO  {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(JobOfferDAO.class);

    @Inject
    private IBasicDataSource basicDataSource;


    public JobOfferDAO() {
    }



    public List<JobOfferTO> readJobOffers(JobOfferSelector selector) throws SQLException {
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The DataBase shouldn't be access in the EDT."); //$NON-NLS-1$
		}

		Properties properties = basicDataSource.getQueries(IJobOfferTO.class);

    	BeanListHandler<JobOfferTO> handler = new BeanListHandler<>(JobOfferTO.class);
        QueryRunner run = new QueryRunner(basicDataSource);

        //System.out.println("selecting for: " + selector.getStatementKey());
        String sql = properties.getProperty(selector.getStatementKey());
        // returns result from the Handler
        ArrayList<JobOfferTO> result = (ArrayList<JobOfferTO>) run.query(
                sql,
                handler);
     
        LOG.debug("returned {} job offers from the database, select was \"{}\"", 
                result.size(), sql);

        return result;
   }

}
