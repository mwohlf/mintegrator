package net.wohlfart.data.persis;

import java.sql.SQLException;

@SuppressWarnings("serial")
public class CreationFailedException extends SQLException {

	public CreationFailedException(String string, Throwable th) {
		super(string, th);
	}

}
