/**
 *
 */
package net.wohlfart;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Custom event queue for HotKey management and component blocking.
 * Put UI elements in the blockedComponents to block user interaction
 * or in the unblockedComponents to allow, this is used for semi-modal dialogs
 * 
 * see: http://www.javaworld.com/javaworld/javatips/jw-javatip89.html
 *      http://stackoverflow.com/questions/6479730/how-to-disable-almost-all-components-in-a-jframe
 *      
 * @author michael
 */
public final class CustomEventQueue extends EventQueue {
	/** Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(CustomEventQueue.class);
	// singleton
	private static CustomEventQueue instance;

	private final Set<Component> blockedComponents = new HashSet<>();
	private final Set<Component> unblockedComponents = new HashSet<>();
	
	// next in the chain of command
	private final EventQueue systemEventQueue = Toolkit.getDefaultToolkit().getSystemEventQueue();


	public static synchronized CustomEventQueue getInstance() {
		if( instance == null ) {
			instance = new CustomEventQueue();
		}
		return instance;
	}

	private CustomEventQueue() {
		LOG.debug("installing custom eventqueu"); //$NON-NLS-1$
		systemEventQueue.push( this );
	}

	public void clearBlockedComponents() {
		setBlockedComponents(Collections.<Component>emptySet());
	}

	public void setBlockedComponents(final Set<Component> blockedComponents ) {
		this.blockedComponents.clear();
		this.blockedComponents.addAll(blockedComponents);
	}

	public void setBlockedComponents(final Component[] blockedComponents ) {
		this.blockedComponents.clear();
		for (Component component:blockedComponents) {
			this.blockedComponents.add(component);
		}
	}

	public void setUnblockedComponents(Component[] unblockedComponents) {
		this.blockedComponents.clear();
		for (Component component:unblockedComponents) {
			this.unblockedComponents.add(component);
		}
	}


	/** 
	 * main entry point into this class, this method is called on most user actions
	 */
	@Override
	protected void dispatchEvent(final AWTEvent event) {

		if (event instanceof MouseEvent) {
			handle((MouseEvent) event);
		}

		else if (event instanceof KeyEvent) {
			handle((KeyEvent) event);
		}

		else {
			super.dispatchEvent(event);
		}
	}
	

	private void handle(MouseEvent mouseEvent) {
		int id = mouseEvent.getID();
		switch (id) {
		case MouseEvent.MOUSE_ENTERED:
		case MouseEvent.MOUSE_EXITED:
		case MouseEvent.MOUSE_MOVED:
		case MouseEvent.MOUSE_RELEASED:
			super.dispatchEvent(mouseEvent);
			return;
		case MouseEvent.MOUSE_DRAGGED:
		case MouseEvent.MOUSE_PRESSED:
		case MouseEvent.MOUSE_CLICKED:
		default:
			int x = mouseEvent.getX();
			int y = mouseEvent.getY();
			Component component = mouseEvent.getComponent();
			// get deepest element, 
			component = SwingUtilities.getDeepestComponentAt(component, x, y);
			while (component != null) {
				// then walk upward to check if we find a component in the parents
				if (unblockedComponents.contains(component)) {
					super.dispatchEvent( mouseEvent );
					return;
				}
				// that is either blocked or not blocked
				if (blockedComponents.contains(component)) {
					mouseEvent.consume(); // blocked
					if (id == MouseEvent.MOUSE_CLICKED) {
						Toolkit.getDefaultToolkit().beep();
					}
					return;
				}
				component = component.getParent();
			}
			super.dispatchEvent( mouseEvent );
		}
	}


	private void handle(KeyEvent keyEvent) {
		// never block key events, this is not 100% perfect but it takes a lot of
		// work by the user to tab into a blocked textfield etc...
		super.dispatchEvent( keyEvent );
	}

}

