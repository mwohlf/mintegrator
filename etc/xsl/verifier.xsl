<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:output
            method="text"
            encoding="utf-8"/>


    <xsl:template match="/transform">

     <!-- package -->
     <xsl:text>package net.wohlfart.data.persis;</xsl:text>  <!-- this must match to the destination folder -->
     <xsl:text>&#10;</xsl:text>
     <xsl:text>&#10;</xsl:text>

     <!-- imports -->
     <xsl:text>import java.util.HashMap;</xsl:text>
     <xsl:text>&#10;</xsl:text>
     <xsl:text>&#10;</xsl:text>

     <!-- class start -->
    <xsl:text>public class Fields {</xsl:text>
     <xsl:text>&#10;</xsl:text>
     <xsl:text>&#10;</xsl:text>

     <xsl:for-each select="table">
       <xsl:apply-templates select="." />
     </xsl:for-each>

     <!-- class end -->
    <xsl:text>&#10;}&#10;</xsl:text>

    </xsl:template>


    <xsl:template match="table">

     <xsl:text>    public static final HashMap&lt;String, String> </xsl:text>
     <xsl:value-of select='translate(substring(@custom, 1, 1),"ABCDEFGHIJKLMNOPQRSTUVWXYZ","abcdefghijklmnopqrstuvwxyz")'/>
     <xsl:value-of select='substring(@custom, 2)'/>
     <xsl:text> = new HashMap&lt;String, String>(); </xsl:text>
     <xsl:text>&#10;</xsl:text>

     <xsl:text>    static {&#10;</xsl:text>
     <xsl:for-each select="column">
       <xsl:apply-templates select="." >
         <xsl:with-param name='original' select='../@original' />
         <xsl:with-param name='custom' select='../@custom' />
       </xsl:apply-templates>
     </xsl:for-each>
     <xsl:text>    }&#10;</xsl:text>
     <xsl:text>&#10;</xsl:text>
    </xsl:template>


    <xsl:template match="column">
     <xsl:param name='original' />
     <xsl:param name='custom' />

     <xsl:text>        </xsl:text>
     <xsl:value-of select='translate(substring($custom, 1, 1),"ABCDEFGHIJKLMNOPQRSTUVWXYZ","abcdefghijklmnopqrstuvwxyz")'/>
     <xsl:value-of select='substring($custom, 2)'/>
     <xsl:text>.put(</xsl:text>
     <xsl:text>"</xsl:text>
     <xsl:value-of select='translate(substring(@custom, 1, 1),"ABCDEFGHIJKLMNOPQRSTUVWXYZ","abcdefghijklmnopqrstuvwxyz")'/>
     <xsl:value-of select='substring(@custom, 2)'/>
     <xsl:text>"</xsl:text>
     <xsl:text>,</xsl:text>
     <xsl:text>"</xsl:text>
     <xsl:value-of select="$original" />
     <xsl:text>.</xsl:text>
     <xsl:value-of select="@original" />
     <xsl:text>");  //$NON-NLS-1$ //$NON-NLS-2$</xsl:text>

     <xsl:text>&#10;</xsl:text>
    </xsl:template>



</xsl:stylesheet>









