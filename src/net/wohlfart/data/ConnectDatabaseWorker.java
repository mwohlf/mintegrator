package net.wohlfart.data;

import java.awt.EventQueue;
import java.net.ConnectException;
import java.net.NoRouteToHostException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.swing.JOptionPane;

import net.wohlfart.CustomSwingWorker;
import net.wohlfart.Messages;
import net.wohlfart.data.persis.CandidateDAO;
import net.wohlfart.data.persis.CandidateTO;
import net.wohlfart.gui.ErrorPane;
import net.wohlfart.gui.table.ApplicationListFull;
import net.wohlfart.properties.IApplicationProperties;
import net.wohlfart.properties.PropertyKeys;
import net.wohlfart.util.GuiUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * connects to the DB, shows a progress monitor, sets a wait cursor, blocks user input
 *
 *
 * @author michael
 *
 */
public class ConnectDatabaseWorker extends CustomSwingWorker<List<CandidateTO>, Void> {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(ConnectDatabaseWorker.class);

	@Inject
	private CandidateDAO candidateDAO;

	@Inject 
	private Event<IBasicDataSource> dataSourceEvent;

	@Inject 
	private IApplicationProperties properties;

	@Inject 
	private IBasicDataSource basicDataSource;

	@Inject 
	private Event<ApplicationListFull> fullCandidateListEvent;


	/**
	 * show a ProgressMonitor
	 */
	@Override
	public void doExecute() {
		// this must run in EDT
		if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The method doExecute() must be called from EDT."); //$NON-NLS-1$
		}
		if (!basicDataSource.isClosed()) {
			LOG.info("datasource is open already, no disconnect needed, calling the done() method"); //$NON-NLS-1$
			cancel(true); // cancel the future object
			// done();
			return;
		}
		LOG.info("connecting database...");
		super.doExecute();
	}


	@Override
	protected List<CandidateTO> doInBackground() {
		LOG.info("doInBackground() - start"); //$NON-NLS-1$
		setProgress(10);

		Connection connection = null;
		try {
			setProgress(20);
			if (isCanceled()) {
				return null;
			}

			// this also sets close to false in the DataSource
			boolean success = basicDataSource.configure(properties);
			setProgress(30);
			if (isCanceled()) {
				return null;
			}
			
			
			if (!success) {
				setProgress(100);
				progressMonitor.close();
				JOptionPane.showMessageDialog(GuiUtilities.getFrame(),
						Messages.getString("ConnectDatabaseWorker.CheckDataSourceConfig"), //$NON-NLS-1$
						Messages.getString("ConnectDatabaseWorker.DataSourceConfigError"), //$NON-NLS-1$
						JOptionPane.ERROR_MESSAGE);			
				basicDataSource.close();
				return null;
			}

			connection = basicDataSource.getConnection(); // this might block for several seconds
			if (isCanceled()) {
				basicDataSource.close();
				return null;
			}
						
			LOG.debug("doInBackground() - connected: {}", connection.toString()); //$NON-NLS-1$
			setProgress(40);
			if (isCanceled()) {
				basicDataSource.close();
				return null;
			}

			// alright seems to work, let the user know
			setNote(Messages.getString("ConnectDatabaseWorker.Note2")); //$NON-NLS-1$
			setProgress(50);
			if (isCanceled()) {
				LOG.debug("doInBackground() - canceled4"); //$NON-NLS-1$
				basicDataSource.close();
				return null;
			}

			setNote(Messages.getString("ConnectDatabaseWorker.Note3")); //$NON-NLS-1$
			List<CandidateTO> candidates = candidateDAO.readAllCandidates();
			setProgress(60);
			if (isCanceled()) {
				LOG.debug("doInBackground() - canceled4"); //$NON-NLS-1$
				basicDataSource.close();
				return null;
			}
			
			return candidates;

		} catch (SQLException ex) {
			LOG.error("doInBackground() - SQLException", ex); //$NON-NLS-1$
			GuiUtilities.setDefaultCursor();
			progressMonitor.close();
			try {
				basicDataSource.close();
			} catch (SQLException ignored) {}
			displayNiceError(ex);

		} catch (Throwable ex) {
			LOG.error("doInBackground() - Throwable", ex); //$NON-NLS-1$
			GuiUtilities.setDefaultCursor();
			progressMonitor.close();
			try {
				basicDataSource.close(); // FIXME: use try-autoclose here
			} catch (SQLException ignored) {}
			ErrorPane.showErrorDialog(ex, Messages.getString("ConnectDatabaseWorker.Throwable")); //$NON-NLS-1$

		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ignored) {}
			}
			queue.clearBlockedComponents();
			GuiUtilities.setDefaultCursor();
			progressMonitor.close();
		}
		LOG.debug("doInBackground() - end"); //$NON-NLS-1$
		return null;
	}


	/**
	 * this method is executed on the EDT and called by swing after the doInBackground method is finished
	 */
	@Override
	protected void done() {
		super.done();
		LOG.debug("invoke done() - start in: {}", this); //$NON-NLS-1$
		try {
			List<CandidateTO> result = get();
			fullCandidateListEvent.fire(new ApplicationListFull(result));
		} catch (InterruptedException | ExecutionException ex) {
			LOG.warn("error getting swing worker result", ex);
		}		
		dataSourceEvent.fire(basicDataSource);
	}


	protected void displayNiceError(final SQLException ex) {

		Throwable firstCause = ex;
		while (firstCause.getCause() != null) {
			firstCause = firstCause.getCause();
		}

		if (firstCause instanceof ConnectException) {
			JOptionPane.showMessageDialog(GuiUtilities.getFrame(),
					Messages.getString("ConnectDatabaseWorker.ConnectException.message"), //$NON-NLS-1$
					Messages.getString("ConnectDatabaseWorker.ConnectException"), //$NON-NLS-1$
					JOptionPane.ERROR_MESSAGE);

		} else if (firstCause instanceof NoRouteToHostException) {
			String unknownHostname = Messages.getString("ConnectDatabaseWorker.noRouteToHost"); //$NON-NLS-1$
			String hostname = properties.getString(PropertyKeys.DATABASE_HOST, unknownHostname);
			JOptionPane.showMessageDialog(GuiUtilities.getFrame(),
					Messages.getString("ConnectDatabaseWorker.NoRouteToHostException.message", new Object[] {hostname}), //$NON-NLS-1$
					Messages.getString("ConnectDatabaseWorker.NoRouteToHostException"), //$NON-NLS-1$
					JOptionPane.ERROR_MESSAGE);

		} else if (firstCause instanceof UnknownHostException) {
			String unknownHostname = Messages.getString("ConnectDatabaseWorker.unknownHostname"); //$NON-NLS-1$
			String hostname = properties.getString(PropertyKeys.DATABASE_HOST, unknownHostname);
			JOptionPane.showMessageDialog(GuiUtilities.getFrame(),
					Messages.getString("ConnectDatabaseWorker.UnknownHostException.message", new Object[] {hostname}), //$NON-NLS-1$
					Messages.getString("ConnectDatabaseWorker.UnknownHostException"), //$NON-NLS-1$
					JOptionPane.ERROR_MESSAGE);

		} else if (firstCause instanceof SocketTimeoutException) {
			String unknownHostname = Messages.getString("ConnectDatabaseWorker.socketTimeout"); //$NON-NLS-1$
			String hostname = properties.getString(PropertyKeys.DATABASE_HOST, unknownHostname);
			JOptionPane.showMessageDialog(GuiUtilities.getFrame(),
					Messages.getString("ConnectDatabaseWorker.SocketTimeoutException.message", new Object[] {hostname}), //$NON-NLS-1$
					Messages.getString("ConnectDatabaseWorker.SocketTimeoutException"), //$NON-NLS-1$
					JOptionPane.ERROR_MESSAGE);

		} else {
			JOptionPane.showMessageDialog(GuiUtilities.getFrame(),
					ex.getMessage(),
					Messages.getString("ConnectDatabaseWorker.SQLException"), //$NON-NLS-1$
					JOptionPane.ERROR_MESSAGE);
		}
	}

}
