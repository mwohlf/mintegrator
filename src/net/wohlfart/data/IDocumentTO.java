package net.wohlfart.data;

/**
 * @author michael
 *
 */
public interface IDocumentTO {

    public abstract byte[] getData();

    public abstract Integer getDocId();

    public abstract Integer getDocNumber();

    public abstract Integer getGpg();

    public abstract Integer getGroesse();

    public abstract Integer getGroupId();

    public abstract String getGroupModule();

    public abstract Integer getId();

    public abstract String getInfo();

    public abstract Integer getLockRemark();

    public abstract Integer getMainId();

    public abstract String getMainModule();

    public abstract String getName();

    public abstract String getOldDir();

    public abstract String getPath();

    public abstract String getType();

    public abstract Integer getWriteLock();

    public abstract void setData(byte[] data);

    public abstract void setDocId(Integer docId);

    public abstract void setDocNumber(Integer docNumber);

    public abstract void setGpg(Integer gpg);

    public abstract void setGroesse(Integer groesse);

    public abstract void setGroupId(Integer groupId);

    public abstract void setGroupModule(String groupModule);

    public abstract void setId(Integer id);

    public abstract void setInfo(String info);

    public abstract void setLockRemark(Integer lockRemark);

    public abstract void setMainId(Integer mainId);

    public abstract void setMainModule(String mainModule);

    public abstract void setName(String name);

    public abstract void setOldDir(String oldDir);

    public abstract void setPath(String path);

    public abstract void setType(String type);

    public abstract void setWriteLock(Integer writeLock);

}