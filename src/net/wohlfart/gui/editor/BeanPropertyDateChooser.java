package net.wohlfart.gui.editor;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyDescriptor;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;

import javax.swing.Box;
import javax.swing.BoxLayout;

import net.wohlfart.gui.event.ApplicationFocused;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.toedter.calendar.JDateChooser;

@SuppressWarnings("serial")
public class BeanPropertyDateChooser extends Box implements IFocusedApplicationListener {
	/** Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(BeanPropertyDateChooser.class);

	private final String propertyName;
	private final LocalPropertyChangeListener listener;
	
	private final JDateChooser chooser;

	private Object bean; // proxy
	private PropertyDescriptor propertyDescriptor;


	private VetoableChangeSupport changeSupport; // we need to remember the last change support in order to unregister

	private boolean isFiring = false; // endless loop protection

	
	public BeanPropertyDateChooser(final Class<?> clazz, final String propertyName) {
		super(BoxLayout.X_AXIS);

		chooser = new JDateChooser();
		chooser.setDateFormatString("dd.MM.yyyy");
		chooser.getDateEditor().addPropertyChangeListener("date", new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				copyModel2Bean();
			}		
		});


		add(chooser);

		this.listener = new LocalPropertyChangeListener();
		this.propertyName = propertyName;
		setupDescriptor(clazz, propertyName);
	}

	protected void setView(final Object object) {
		isFiring = true; // we don't want the model to fire back
		chooser.setDate((Date)object);
		isFiring = false;
	}


	protected void setupDescriptor(final Class<?> clazz, final String propertyName) {
		try {
			propertyDescriptor = getDescriptor(clazz, propertyName);
			if (propertyDescriptor == null) {
				LOG.error("property descriptor is null for property name '{}'", propertyName);
			}
		} catch (IntrospectionException ex) {
			LOG.error("Error setting property descriptor", ex);
		}
	}

	protected PropertyDescriptor getDescriptor(final Class<?> clazz, final String propertyName)
			throws IntrospectionException {
		// create a property name to descriptor map
		final HashMap<String, PropertyDescriptor> tempDescriptorMap = new HashMap<>();
		BeanInfo beanInfo = Introspector.getBeanInfo(clazz);
		PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
		for (PropertyDescriptor descriptor : propertyDescriptors) {
			tempDescriptorMap.put(descriptor.getName(), descriptor);
		}
		return tempDescriptorMap.get(propertyName);
	}

	protected class LocalPropertyChangeListener implements VetoableChangeListener {
		@Override
		public void vetoableChange(PropertyChangeEvent evt) {
			if (!isFiring) {
				Object value = evt.getNewValue();
				setView(value);
			}
		}
	}

	@Override
	public void setFocusedApplication(final ApplicationFocused container) {
		this.bean = container.getContent();
		VetoableChangeSupport changeSupport = container.getChangeSupport();

		if (this.changeSupport != null) { // might be null on application start
			this.changeSupport.removeVetoableChangeListener(propertyName, listener);
		}
		this.changeSupport = changeSupport;
		if (this.changeSupport != null) { // might be null on shutdown
			this.changeSupport.addVetoableChangeListener(propertyName, listener);
		}

		isFiring = true; // we don't want the model to fire back
		copyBean2Model();
		isFiring = false;
	}

	protected void copyModel2Bean() {
		LOG.debug("bean is {}, isFiring is {}", bean, isFiring);
		if (isFiring) {
			return;
		}
		if ((BeanPropertyDateChooser.this.bean == null)
				|| (BeanPropertyDateChooser.this.propertyDescriptor == null)) {
			LOG.info(
					"skipping setter action, bean is {}, propertyDescriptor is {}, host is {}",
					new Object[] { bean, propertyDescriptor,
							BeanPropertyDateChooser.this });
			return;
		}
		try {
			isFiring = true;
			propertyDescriptor.getWriteMethod().invoke(bean, chooser.getDate());
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException ex) {
			LOG.error("Error setting text property", ex);
		} finally {
			isFiring = false;
		}
	}

	protected void copyBean2Model() {
		LOG.debug("bean is {}", bean);
		if ((BeanPropertyDateChooser.this.bean == null)
				|| (BeanPropertyDateChooser.this.propertyDescriptor == null)) {
			LOG.info(
					"skipping setter action, bean is {}, propertyDescriptor is {}, host is {}",
					new Object[] { bean, propertyDescriptor,
							BeanPropertyDateChooser.this });
			return;
		}
		try {
			Object value = propertyDescriptor.getReadMethod().invoke(bean, new Object[] {});			
			setView(value);						
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException ex) {
			LOG.error("Error setting text property", ex);
		}
	}

	@Override
	public String toString() {
		return getClass().getName() + "@" + Integer.toHexString(hashCode());
	}

}
