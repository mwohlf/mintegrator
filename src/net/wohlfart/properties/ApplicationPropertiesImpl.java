/**
 *
 */
package net.wohlfart.properties;

import static java.lang.Boolean.parseBoolean;
import static net.wohlfart.properties.PropertyKeys.AUTOREZIZE_FORMAT;
import static net.wohlfart.properties.PropertyKeys.AUTO_CONNECT;
import static net.wohlfart.properties.PropertyKeys.CONFIRM_DELETE;
import static net.wohlfart.properties.PropertyKeys.CONFIRM_QUIT;
import static net.wohlfart.properties.PropertyKeys.CONFIRM_SELECTION;
import static net.wohlfart.properties.PropertyKeys.DATABASE_HOST;
import static net.wohlfart.properties.PropertyKeys.DATABASE_NAME;
import static net.wohlfart.properties.PropertyKeys.DATABASE_PASSWORD;
import static net.wohlfart.properties.PropertyKeys.DATABASE_PORT;
import static net.wohlfart.properties.PropertyKeys.DATABASE_URL_FORMAT;
import static net.wohlfart.properties.PropertyKeys.DATABASE_USER_NAME;
import static net.wohlfart.properties.PropertyKeys.DOC_DESCRIPTION;
import static net.wohlfart.properties.PropertyKeys.DOC_GROUP;
import static net.wohlfart.properties.PropertyKeys.DRIVER_PROTOCOL;
import static net.wohlfart.properties.PropertyKeys.MODEL_COLUMN_VISIBLE_FORMAT;
import static net.wohlfart.properties.PropertyKeys.SAVE_EMAIL_AS_FILE;
import static net.wohlfart.properties.PropertyKeys.SAVE_EMAIL_AS_REMARK;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Point;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;

import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * singleton
 * 
 * application wide properties file
 * for a discussion about singleton scope versus application scope see:
 * http://docs.jboss.org/weld/reference/latest/en-US/html/scopescontexts.html#d0e1923
 *
 *  example connection string MS-SQL: "jdbc:jtds:sqlserver://localhost:1433/PERSIS"
 *  oracle example: Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@hostname:1526:orcl", "scott", "tiger");
 *                  @machineName:port:SID,   userid,  password
 *
 *  protocol, host, port, database
 *  Oracle: jdbc:oracle:thin:@fiji:1521:my10g
 *  MS-SQL-Server
 *  public static final MessageFormat DATABASE_URL_FORMAT = new MessageFormat("{0}//{1}:{2}/{3}");
 *
 *
 * @author michael
 */
@SuppressWarnings("serial")
@Singleton
public class ApplicationPropertiesImpl extends StorableProperties implements IApplicationProperties {
	/** Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(ApplicationPropertiesImpl.class);

	private static ApplicationPropertiesImpl applicationPropertiesImpl;

	// comment for the xml properties file generation
	private static final String COMMENT_STRING = "this is a generated file, do not edit unless you know what you are doing";

	// cipher for the password encryption  
	private static final SimpleCipher CIPHER = SimpleCipher.createDefaultCipher();

	// file and dir to store the properties in
	private static final String PROPERTIES_FILENAME = "properties.xml";
	private static final String CONF_DIRECTORY = "conf";


	private static final String FILE_SEPARATOR = System.getProperty("file.separator");

	/** The working directory is the location in the file system from where the java command was invoked. */
	private static final String CURRENT_DIRECTORY = System.getProperty("user.dir");


	// key for the mail selection dialog
	public static final String MAIL_SELECTION_DIALOG = "MAIL_SELECTION_DIALOG";
	public static final String BASE_FRAME = "BASE_FRAME";


	// setup dialogs all resizeable
	public static final String SETUP_DATABASE_DIALOG = "SETUP_DATABASE_DIALOG";
	public static final String SETUP_OUTLOOK_DIALOG = "SETUP_OUTLOOK_DIALOG";
	public static final String SETUP_INTEGRATOR_DIALOG = "SETUP_INTEGRATOR_DIALOG";
	public static final String SETUP_PROPERTIES_DIALOG = "SETUP_PROPERTIES_DIALOG";

	private void loadDefaults() {

		try {
			InputStream input = getClass().getResourceAsStream ("default.properties");
			Properties properties = new Properties();
			properties.load(input);
			
			put(DATABASE_USER_NAME, properties.getProperty(DATABASE_USER_NAME));
			put(DATABASE_HOST, properties.getProperty(DATABASE_HOST));
			put(DATABASE_PORT, properties.getProperty(DATABASE_PORT));
			put(DATABASE_NAME, properties.getProperty(DATABASE_NAME));
			// WurfelZucker
			//put(DATABASE_PASSWORD, "WIm0qIgzgWkIs2287q2NyQ==");
			put(DATABASE_PASSWORD, properties.getProperty(DATABASE_PASSWORD));

			put(AUTO_CONNECT, parseBoolean(properties.getProperty(AUTO_CONNECT)));
			put(CONFIRM_QUIT, parseBoolean(properties.getProperty(CONFIRM_QUIT)));
			put(CONFIRM_DELETE, parseBoolean(properties.getProperty(CONFIRM_DELETE)));
			put(CONFIRM_SELECTION, parseBoolean(properties.getProperty(CONFIRM_SELECTION)));
			put(SAVE_EMAIL_AS_FILE, parseBoolean(properties.getProperty(SAVE_EMAIL_AS_FILE)));
			put(SAVE_EMAIL_AS_REMARK, parseBoolean(properties.getProperty(SAVE_EMAIL_AS_REMARK)));
			//put(OUTLOOK_NODE_NAME, "");
			//put(OUTLOOK_ENTRY_ID, "empty");
			//put(OUTLOOK_STORE_ID, "empty");
			put(DOC_DESCRIPTION, properties.getProperty(DOC_DESCRIPTION));
			put(DOC_GROUP, properties.getProperty(DOC_GROUP));

			
		} catch (IOException ex) {
			LOG.error("can'T load defaults", ex);
		}

	}


	// FIXME: remove any calls to this method, we use CDI to inject the properties
	public static synchronized ApplicationPropertiesImpl getInstance() {
		if (applicationPropertiesImpl == null) {
			applicationPropertiesImpl = new ApplicationPropertiesImpl();
		}
		return applicationPropertiesImpl;
	}


	/* private */ protected ApplicationPropertiesImpl() {
		loadDefaults();
		load();
	}

	/**  we try to find the resources in the conf directory. */
	private String getFilename() {
		return CURRENT_DIRECTORY
				+ FILE_SEPARATOR
				+ CONF_DIRECTORY
				+ FILE_SEPARATOR
				+ PROPERTIES_FILENAME;
	}

	@Override
	public synchronized void save() {
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The method save() must not be invoked in the EDT!"); //$NON-NLS-1$
		}

		LOG.info("save triggered for ApplicationProperties"); //$NON-NLS-1$
		try (FileOutputStream fileOutputStream = new FileOutputStream(getFilename())) {
			// debug:
			for (Object key:keySet()) {
				LOG.info("  persisting: {} -> {}", key, get(key)); //$NON-NLS-1$
			}
			storeToXML(fileOutputStream, COMMENT_STRING);

		} catch (IOException ex) {
			LOG.error("exception while saving properties", ex); //$NON-NLS-1$
		} 
	}

	public void delete() {
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("the method delete() must not be invoked in the EDT!"); //$NON-NLS-1$
		}

		boolean success = new File(getFilename()).delete();
		if (success) {
			LOG.debug("deleted properties file");
		}
	}


	@Override
	public synchronized void load() {
		File file = new File(getFilename());
		if (file.exists() && file.canRead()) {
			if (file.length() <= 0) {
				LOG.warn("properties file is empty, running with default properties");
			} else {
				try {
					// stored properties from the file...
					loadFromXML(new FileInputStream(file));
					// debug:
					for (Object key:keySet()) {
						LOG.debug("  loaded: {} -> {}", key, get(key));
					}
					// don't forget to decrypt the password!
				} catch (Exception ex) {
					LOG.warn("can't parse properties file", ex);
				}
			}
		} else {
			LOG.warn("can't read properties file");
		}
	}


	//////////////// properties access ////////////////

	// we must not return null, the DataSource won't accept null for passwords
	@Override
	public String getPassword() {
		String password = null;
		String encrypted = getProperty(DATABASE_PASSWORD);
		if ((encrypted != null) && (encrypted.trim().length() > 0)) {
			password = CIPHER.decrypt(encrypted); 
		}
		if (password == null || password.trim().length() == 0) {
			LOG.warn("password is empty or null :\"{}\"", password);
			password = "";
		}
		return password;
	}
	@Override
	public void setPassword(String password) {
		setProperty(DATABASE_PASSWORD, CIPHER.encrypt(password));
	}
	@Override
	public void deletePassword() {
		remove(DATABASE_PASSWORD);
	}




	public Boolean getBoolean(String key) {
		return getBoolean(key, false);
	}
	@Override
	public Boolean getBoolean(String key, Boolean defaultValue) {
		if (containsKey(key)) {
			return Boolean.valueOf(getProperty(key));
		}
		return defaultValue;
	}
	@Override
	public void setBoolean(String key, Boolean value) {
		super.put(key, value.toString());
	}




	public String getString(String key) {
		return getString(key, null);
	}
	@Override
	public String getString(String key, String defaultValue) {
		if (containsKey(key)) {
			return getProperty(key);
		}
		return defaultValue;
	}
	@Override
	public void setString(String key, String value) {
		super.put(key, value);
	}







	public Point getPoint(String key) {
		return getPoint(key, new Point(0,0));
	}
	public Point getPoint(String key, Point defaultValue) {
		if (containsKey(key + ".point.x") && containsKey(key + ".point.y")) {
			return new Point(getInteger(key + ".point.x"), getInteger(key + ".point.y"));
		}
		return defaultValue;
	}
	public void setPoint(String key, Point value) {
		setInteger(key + ".point.x", value.x);
		setInteger(key + ".point.y", value.y);
	}




	public synchronized Dimension getDimension(String key) {
		return getDimension(key, new Dimension(0,0));
	}
	@Override
	public synchronized Dimension getDimension(String key, Dimension defaultValue) {
		LOG.debug("this: {}", this);
		if (containsKey(key + ".dimension.width") && containsKey(key + ".dimension.height")) {
			return new Dimension(getInteger(key + ".dimension.width"), getInteger(key + ".dimension.height"));
		}
		LOG.debug("returning default Dimension for key {} is {}", key, defaultValue);
		return defaultValue;
	}
	@Override
	public synchronized void setDimension(String key, Dimension value) {
		setInteger(key + ".dimension.width", value.width);
		setInteger(key + ".dimension.height", value.height);
		LOG.debug("this: {}", this);
	}




	public Integer getInteger(String key) {
		return getInteger(key, 0);
	}
	@Override
	public Integer getInteger(String key, Integer defaultValue) {
		if (containsKey(key)) {
			Integer result = new Integer(getProperty(key));
			LOG.debug("getting integer for key {} is {}", key, result);
			return result;
		}
		LOG.debug("returning default integer value for key {} is {}", key, defaultValue);
		return defaultValue;
	}
	@Override
	public void setInteger(String key, Integer value) {
		LOG.debug("setting integer for key {} to {}", key, value);
		setProperty(key, value.toString());
	}




	@Override
	public boolean containsKey(Object object) {
		return super.containsKey(object);
	}
	@Override
	public boolean contains(Object object) {
		return super.containsKey(object);
	}


	@Override
	public String getDatabaseUrl() {
		return
				new MessageFormat(getProperty(DATABASE_URL_FORMAT))
		.format(new Object[] {
				getProperty(DRIVER_PROTOCOL),
				getProperty(DATABASE_HOST),
				getProperty(DATABASE_PORT),
				getProperty(DATABASE_NAME)
		});
	}


	// table visible properties
	public boolean getColumnIsVisible(String tableUid, Integer modelIndex) {
		return getBoolean(MODEL_COLUMN_VISIBLE_FORMAT.format(new Object[] {tableUid, modelIndex}), true);
	}
	public void setColumnIsVisible(String tableUid, Integer modelIndex, boolean isVisible) {
		setBoolean(MODEL_COLUMN_VISIBLE_FORMAT.format(new Object[] {tableUid, modelIndex}), isVisible);
	}



	// table resize properties
	public boolean getAutoResize(String tableUid) {
		return getBoolean(AUTOREZIZE_FORMAT.format(new Object[] {tableUid}), false);
	}
	public void setAutoResize(String tableUid, boolean isOn) {
		setBoolean(AUTOREZIZE_FORMAT.format(new Object[] {tableUid}), isOn);
	}



	@Override
	public void delete(String key) {
		remove(key);
	}




}
