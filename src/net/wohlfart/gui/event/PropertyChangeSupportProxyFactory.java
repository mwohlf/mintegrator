package net.wohlfart.gui.event;

import java.beans.VetoableChangeSupport;
import java.lang.reflect.Proxy;

import net.wohlfart.data.IBasicDataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertyChangeSupportProxyFactory {
	/** Logger for this class */
	private static final Logger LOG = LoggerFactory.getLogger(PropertyChangeSupportProxyFactory.class);

	
	public static<T> T createPropertyChangeSupportProxy(T bean, IBasicDataSource dataSource, VetoableChangeSupport support) {
		
		LOG.debug("creating a proxy...");
		LOG.debug("  bean is {}", bean);
			
		Class<?> clazz = bean.getClass();
		ClassLoader loader = clazz.getClassLoader();
		LOG.debug("  class is {}", bean.getClass().getSimpleName());
				
		PropertySetterHandler setterHandler = new PropertySetterHandler(bean, dataSource,support);
		
		Class<?>[] interfaces = clazz.getInterfaces();
		
		T proxy = (T) Proxy.newProxyInstance(loader, interfaces, setterHandler);
		
		return proxy;
	}

}
