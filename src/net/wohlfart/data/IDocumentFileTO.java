package net.wohlfart.data;


import java.io.File;



/**
 * adding the file component to the document class
 * 
 * @author michael
 */
public interface IDocumentFileTO extends IDocumentTO {

    File getFile();

    void setFile(File file);

}
