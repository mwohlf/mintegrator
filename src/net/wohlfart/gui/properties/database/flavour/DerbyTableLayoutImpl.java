package net.wohlfart.gui.properties.database.flavour;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


/**
 * create a databaseLayout for derby from scratch
 * 
 * @author michael
 */
public class DerbyTableLayoutImpl implements ITableLayout {


	// --- INET_BWR table ---

	private static final String CREATE_INET_BWR = ""
			+ "create table INET_BWR ("
			+ " BEM VARCHAR(2550),"
			+ " BWG_VOM DATE,"
			+ " BWG_ALS VARCHAR(255),"
			+ " ANSCHORT VARCHAR(255),"
			+ " ANSCHLAND VARCHAR(255),"
			+ " GEBTAG DATE,"
			+ " WERK1 VARCHAR(255),"
			+ " EMAIL VARCHAR(255),"
			+ " VNAME VARCHAR(255),"
			+ " GESCHL VARCHAR(255),"
			+ " GEAEND_VON VARCHAR(255),"			
			+ " GEAEND_AM TIMESTAMP,"			
			+ " REFNR INTEGER,"
			+ " PRJ_KBEZ VARCHAR(255),"
			+ " NAME VARCHAR(255),"
			+ " AUFMERKSAM1 VARCHAR(255),"
			+ " TELEFON1 VARCHAR(255),"
			+ " ANSCH VARCHAR(255),"
			+ " ANSCHPLZ VARCHAR(255)"
			+ ")";

	private static final String DROP_INET_BWR = ""
			+ "drop table INET_BWR";


	// --- Z_DOCUMENT table ---

	private static final String CREATE_Z_DOKUMENT = ""
			+ "create table Z_DOKUMENT ("
			+ " REFNR INTEGER, "
			+ " HPT_REFNR INTEGER, "
			+ " SATZ_REFNR INTEGER, "
			+ " DOK_INFO VARCHAR(256), "
			+ " TYP VARCHAR(256), "
			+ " DOK_ID INTEGER, "
			+ " DOK_NO INTEGER, "
			+ " GROESSE INTEGER, "
			+ " NAME VARCHAR(256), "
			+ " HPT_MODUL VARCHAR(256), "
			+ " MAIN_PATH VARCHAR(256), "
			+ " SATZ_MODUL VARCHAR(256), "
			+ " GPG INTEGER, "
			+ " SCHRSPERR INTEGER, "
			+ " SPERRVERM INTEGER, "
			+ " OLD_VZ VARCHAR(256), "
			+ " DOKUMENT BLOB, " 
			+ " GEAEND_AM TIMESTAMP, "
			+ " GEAEND_VON VARCHAR(255) "
			+ ")";

	private static final String DROP_Z_DOKUMENT = ""
			+ "drop table Z_DOKUMENT";


	// --- INET_BWR_ATT	---

	private static final String CREATE_INET_BWR_ATT = ""
			+ "create table INET_BWR_ATT ("
			+ " REFNR INTEGER,"
			+ " BWR_REFNR INTEGER,"
			+ " DOK_REFNR INTEGER,"
			+ " DOK_ID INTEGER,"
			+ " ANLAGE VARCHAR(255),"
			+ " BEZ VARCHAR(255),"
			+ " GRUPPE VARCHAR(255),"
			+ " SORT1 INTEGER,"
			+ " SORT2 INTEGER,"
			+ " GEAEND_AM TIMESTAMP,"
			+ " GEAEND_VON VARCHAR(255)"
			+ ")";

	private static final String DROP_INET_BWR_ATT = ""
			+ "drop table INET_BWR_ATT";


	// --- ST_PROJEKT


	private static final String CREATE_ST_PROJEKT = ""
			+ "create table ST_PROJEKT ("
			+ " REFNR INTEGER,"
			+ " KBEZ VARCHAR(255),"
			+ " BERUF VARCHAR(255),"
			+ " WERK VARCHAR(255),"
			+ " BEGINN DATE,"
			+ " ERLEDIGT DATE,"
			+ " ERLEDIGT_AM DATE"
			+ ")";

	private static final String[] FILL_ST_PROJEKT = {
		// @formatter:off
		"insert into ST_PROJEKT values (2001, '201', '201 - w1 - projekt 1', 'werk1', " 
		+ "'2006-09-10-00.00.00', '2006-09-10-00.00.00', '2006-09-10-00.00.00') ",
		
		"insert into ST_PROJEKT values (2002, '202', '202 - w1 - projekt 2', 'werk1', " 
		+ "'2006-09-10-00.00.00', '2006-09-10-00.00.00', '2006-09-10-00.00.00') ",
		
		"insert into ST_PROJEKT values (2003, '203', '203 - w1 - projekt 3', 'werk1', "
		+ "'2006-09-10-00.00.00', '2006-09-10-00.00.00', '2006-09-10-00.00.00') ",
		
		"insert into ST_PROJEKT values (2004, '204', '204 - w2 - projekt 4', 'werk2', "
	    + "'2006-09-10-00.00.00', '2006-09-10-00.00.00', '2006-09-10-00.00.00') ",
	    
		"insert into ST_PROJEKT values (2005, '205', '205 - w2 - projekt 5', 'werk2', "
		+ "'2006-09-10-00.00.00', '2006-09-10-00.00.00', '2006-09-10-00.00.00') ",
	
  	    "insert into ST_PROJEKT values (2006, '206', '206 - w2 - projekt 6', 'werk2', "
        + "'2006-09-10-00.00.00', '2006-09-10-00.00.00', '2006-09-10-00.00.00') ",
	    
		"insert into ST_PROJEKT values (2007, '207', '207 - w2 - projekt 7', 'werk2', "
		+ "'2006-09-10-00.00.00', '2006-09-10-00.00.00', '2006-09-10-00.00.00') ",
	
  	    "insert into ST_PROJEKT values (2008, '208', '208 - w3 - projekt 8', 'werk3', "
        + "'2006-09-10-00.00.00', '2006-09-10-00.00.00', '2006-09-10-00.00.00') ",
    
	    "insert into ST_PROJEKT values (2009, '209', '209 - w3 - projekt 9', 'werk3', "
	    + "'2006-09-10-00.00.00', '2006-09-10-00.00.00', '2006-09-10-00.00.00') ",};
	// @formatter:on



	private static final String DROP_ST_PROJEKT = ""
			+ "drop table ST_PROJEKT";


	// --- ST_MEDIUM ---

	private static final String CREATE_ST_MEDIUM = ""
			+ "create table ST_MEDIUM ("
			+ " REFNR INTEGER,"
			+ " KBEZ VARCHAR(255),"
			+ " BEZ VARCHAR(255),"
			+ " AUFMERKSAM VARCHAR(1)"
			+ ")";

	private static final String[] FILL_ST_MEDIUM = {
		"insert into ST_MEDIUM values (1001, '101', '101 - medium 1', '1') ",
		"insert into ST_MEDIUM values (1002, '102', '102 - medium 2', '1') ",
		"insert into ST_MEDIUM values (1003, '103', '103 - medium 3', '1') ",
		"insert into ST_MEDIUM values (1004, '104', '104 - medium 4', '1') ",
		"insert into ST_MEDIUM values (1005, '105', '105 - medium 5', '1') ",};


	private static final String DROP_ST_MEDIUM = ""
			+ "drop table ST_MEDIUM";




	// --- Z_REFNR ---

	private static final String CREATE_Z_REFNR = ""
			+ "create table Z_REFNR ("
			+ " REFNR INTEGER"
			+ ")";

	private static final String INIT_Z_REFNR = ""
			+ "insert into Z_REFNR (REFNR) values (1)";

	private static final String DROP_Z_REFNR = ""

			+ "drop table Z_REFNR";




	// create a HSQL database
	@Override
	public void setupDatabaseTables(final Connection connection) throws SQLException {
		PreparedStatement stmt;
		
		
		// setup the INET_BWR table
		stmt = connection.prepareStatement(CREATE_INET_BWR);
		stmt.execute();
		
		// setup the ST_MEDIUM table
		stmt = connection.prepareStatement(CREATE_ST_MEDIUM);
		stmt.execute();
		for (String s : FILL_ST_MEDIUM) {
			stmt = connection.prepareStatement(s);
			stmt.execute();					
		}

		// setup the ST_PROJEKT table
		stmt = connection.prepareStatement(CREATE_ST_PROJEKT);
		stmt.execute();
		for (String s : FILL_ST_PROJEKT) {
			stmt = connection.prepareStatement(s);
			stmt.execute();					
		}
		
		
		
		
		// setup the Z_DOKUMENT table
		stmt = connection.prepareStatement(CREATE_Z_DOKUMENT);
		stmt.execute();
		// setup the Z_DOKUMENT table
		stmt = connection.prepareStatement(CREATE_INET_BWR_ATT);
		stmt.execute();
		
		
		// setup the Z_REFNR table
		stmt = connection.prepareStatement(CREATE_Z_REFNR);
		stmt.execute();
		stmt = connection.prepareStatement(INIT_Z_REFNR);
		stmt.execute();

		connection.commit();
	}



	// destroy the HSQL database
	@Override
	public void teardownDatabaseTables(final Connection connection) throws SQLException {
		PreparedStatement stmt;

		stmt = connection.prepareStatement(DROP_INET_BWR);
		stmt.execute();
		stmt = connection.prepareStatement(DROP_ST_MEDIUM);
		stmt.execute();
		stmt = connection.prepareStatement(DROP_ST_PROJEKT);
		stmt.execute();
		stmt = connection.prepareStatement(DROP_Z_DOKUMENT);
		stmt.execute();
		stmt = connection.prepareStatement(DROP_INET_BWR_ATT);
		stmt.execute();
		stmt = connection.prepareStatement(DROP_Z_REFNR);
		stmt.execute();
		connection.commit();
	}


}
