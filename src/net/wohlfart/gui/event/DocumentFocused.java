package net.wohlfart.gui.event;

import java.beans.VetoableChangeSupport;

import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.data.IDocumentTO;

/**
 * DocumentFocused is a single document that currently has the focus in the document view
 */
public class DocumentFocused implements IChangeSupportWrapper {

	private final Object element; // the transfer object
	private final VetoableChangeSupport vetoableChangeSupport; // propertyChangeSupport

	// FIXME: this is ugly, we need a constructor without having to drag around the whole datasource...
	//        one solution would be to add the change support right after getting the bean from the database
	public DocumentFocused(final IDocumentTO element, final IBasicDataSource dataSource) {
		if (element != null) {
			this.vetoableChangeSupport = new VetoableChangeSupport(element);
			this.element = PropertyChangeSupportProxyFactory.createPropertyChangeSupportProxy(element, dataSource, vetoableChangeSupport);		
		} else {
			this.vetoableChangeSupport = null;
			this.element = null;
		}
	}
	
	@Override
	public boolean isEmpty() {
		return (element == null);
	}

	@Override
	public Object getContent() {
		return element;
	}

	@Override
	public VetoableChangeSupport getChangeSupport() {
		return vetoableChangeSupport;
	}

}
