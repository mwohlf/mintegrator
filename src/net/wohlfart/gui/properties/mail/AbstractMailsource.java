package net.wohlfart.gui.properties.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract class AbstractMailsource {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(AbstractMailsource.class);

	private final String id;

	
	// package private
	AbstractMailsource(final String id) {
		// only one instance per application otherwise we have to override equals/hash ...
		LOG.debug("init for id {}", id);
		this.id = id;
	}
	
	//@Override
	public String getId() {
		return id;
	}

}
