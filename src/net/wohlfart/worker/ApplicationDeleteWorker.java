/**
 *
 */
package net.wohlfart.worker;


import static net.wohlfart.util.GuiUtilities.getFrame;

import java.awt.EventQueue;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;

import net.wohlfart.CustomSwingWorker;
import net.wohlfart.Messages;
import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.data.ICandidateTO;
import net.wohlfart.data.persis.CandidateDAO;
import net.wohlfart.data.persis.CandidateTO;
import net.wohlfart.gui.ErrorPane;
import net.wohlfart.gui.event.ApplicationFocused;
import net.wohlfart.gui.table.ApplicationListFull;
import net.wohlfart.properties.IApplicationProperties;
import net.wohlfart.properties.PropertyKeys;
import net.wohlfart.util.GuiUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * deletes all candidates that are selected
 * prompts  "are you sure", wait cursor, blocks user input
 *
 * @author michael
 */
public class ApplicationDeleteWorker extends CustomSwingWorker<Void, ICandidateTO> {
	/** Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(ApplicationDeleteWorker.class);

	@Inject
	private CandidateDAO candidateDAO;
	@Inject
	private IBasicDataSource dataSource; // to keep track of data object changes

	@Inject 
	private IApplicationProperties properties;
	@Inject 
	private Event<ApplicationListFull> fullApplications;
	@Inject 
	private Event<ApplicationFocused> applicationFocusEvent;


	// the selected applications we want to delete
	private volatile Collection<ICandidateTO> selected;
	// all applications
	private volatile List<CandidateTO> total;


	// used in the validation dialog to store the user reply
	private volatile int reply = JOptionPane.NO_OPTION;


	/**
	 * show a ProgressMonitor
	 * @param collection2 
	 */
	public void doExecute(final Collection<ICandidateTO> selected, 
			final List<CandidateTO> total) {
		// this must run in EDT
		if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The method doExecute() must be called from EDT."); //$NON-NLS-1$
		}
		try {
			if (!confirmedDeleteIfNeeded() || (selected.size() == 0)) {
				LOG.info("delete was canceled"); //$NON-NLS-1$
				// cancel the future object, this is needed so the get() method doesn't block 
				cancel(true); // (actually it throws an exception now)
				// done();
				return;				
			}
		} catch (InvocationTargetException | InterruptedException ex) {
			LOG.error("doExecute()", ex); //$NON-NLS-1$
		} 

		this.selected = selected;
		this.total = total;
		
		super.doExecute();
	}


	@Override
	protected Void doInBackground() {
		LOG.info("doInBackground() - start"); //$NON-NLS-1$

		try {
			setProgress(10);
			if (isCanceled()) {
				return null;
			}

			int total = selected.size();
			int deleted = 0;
			Iterator<ICandidateTO> it = selected.iterator();
			while (it.hasNext()) {
				if (isCanceled()) {
					return null;
				}
				ICandidateTO candidate = it.next();
				candidateDAO.deleteCandidate(candidate);
				deleted ++;

				// publish the deleted application to remove them from the data model
				publish(candidate);

				String note = Messages.getString("DeleteSelectedCandidatesWorker.Note2",   //$NON-NLS-1$
						new Object[] {deleted, total});

				// we have a range of 80
				setProgress(10 + (80 * deleted / total));
				setNote(note);
			}
			setProgress(95);

		} catch (SQLException ex) {
			LOG.error("doInBackground()", ex);
			progressMonitor.close();
			JOptionPane.showMessageDialog(
					null,
					ex.getMessage(),
					Messages.getString("DeleteSelectedCandidatesWorker.SQLException"), //$NON-NLS-1$
					JOptionPane.ERROR_MESSAGE);
		} catch (Throwable th) {
			LOG.error("doInBackground()", th);
			GuiUtilities.setDefaultCursor();
			progressMonitor.close();
			ErrorPane.showErrorDialog(th, Messages.getString("DeleteSelectedCandidatesWorker.Throwable")); //$NON-NLS-1$
		} finally {
			queue.clearBlockedComponents();
			GuiUtilities.setDefaultCursor();
			LOG.info("doInBackground() - finally");
		}

		return null;
	}


	/**
	 * called in the EDT with the deleted data
	 */
	@Override
	protected void process(List<ICandidateTO> chunks) {		
		total.removeAll(chunks);
		fullApplications.fire(new ApplicationListFull(total));
	}


	@Override
	public void done() {
		super.done();
		// remove the single element focus
		applicationFocusEvent.fire(new ApplicationFocused(null, dataSource));
	}

	/**
	 * invoke a confirm dialog
	 * @return true if the user really wants to quit the application
	 */
	private boolean confirmedDeleteIfNeeded() throws InvocationTargetException, InterruptedException {
		// read from the properties:
		boolean confirmDelete = properties.getBoolean(PropertyKeys.CONFIRM_DELETE, true);
		if (!confirmDelete)  {
			return true; // the settings say there is no confirm needed, just quit already			
		}

		// need to be created in the EDT:
		final JCheckBox confirm = new JCheckBox(Messages.getString("DeleteSelectedCandidate.QueryCheckbox")); //$NON-NLS-1$
		confirm.setBorder(BorderFactory.createEmptyBorder(20, 1, 1, 1));
		confirm.setSelected(confirmDelete);
		reply = JOptionPane.showConfirmDialog(getFrame(),
				new Object[] { 
			Messages.getString("DeleteSelectedCandidate.QuitConfirm"),   //$NON-NLS-1$
			confirm 
		}, 
		Messages.getString("DeleteSelectedCandidate.DialogTitle"),  //$NON-NLS-1$
		JOptionPane.YES_NO_OPTION,
		JOptionPane.QUESTION_MESSAGE);
		// remember the confirm reply:
		properties.setBoolean(PropertyKeys.CONFIRM_DELETE, confirm.isSelected());

		LOG.info("user replied delete validation dialog: {}", reply); //$NON-NLS-1$
		return (reply == JOptionPane.YES_OPTION);		
	}

}
