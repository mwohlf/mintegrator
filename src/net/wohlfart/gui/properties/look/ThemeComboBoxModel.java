package net.wohlfart.gui.properties.look;

import java.util.Map.Entry;

import javax.swing.DefaultComboBoxModel;

import net.wohlfart.gui.properties.look.color.ColorThemeCollection;
import net.wohlfart.gui.properties.look.color.IColorTheme;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class ThemeComboBoxModel extends DefaultComboBoxModel<IColorTheme> {
	/**  Logger for this class */
	private static final Logger LOG = LoggerFactory.getLogger(ThemeComboBoxModel.class);

	ThemeComboBoxModel() {
		LOG.debug("creating model");
		ColorThemeCollection collection = ColorThemeCollection.getInstance();
		for (Entry<String, IColorTheme> theme : collection.entrySet()) {
			LOG.debug("  found: {}", theme);
			addElement(theme.getValue());
		}
	}

}
