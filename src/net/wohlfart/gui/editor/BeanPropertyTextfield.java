/**
 *
 */
package net.wohlfart.gui.editor;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyDescriptor;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import net.wohlfart.gui.event.ApplicationFocused;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author michael
 */
@SuppressWarnings("serial")
public class BeanPropertyTextfield extends JTextField implements IFocusedApplicationListener {
	/** Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(BeanPropertyTextfield.class);

	private final String propertyName;
	private final LocalPropertyChangeListener listener;

	private Object bean; // proxy
	private PropertyDescriptor propertyDescriptor;

	private VetoableChangeSupport changeSupport; // we need to remember the last change support in order to unregister

	private boolean isFiring = false; // endless loop protection

	public BeanPropertyTextfield(final Class<?> clazz, final String propertyName) {
		LOG.info("Constructor called for {}", this);
		getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(final DocumentEvent evt) {
				copyModel2Bean();
			}
			@Override
			public void insertUpdate(final DocumentEvent evt) {
				copyModel2Bean();
			}
			@Override
			public void removeUpdate(final DocumentEvent evt) {
				copyModel2Bean();
			}
		});
		setTransferHandler(new TextTransferHandler());
		setDragEnabled(true);
		this.listener = new LocalPropertyChangeListener();
		this.propertyName = propertyName;
		setupDescriptor(clazz, propertyName);
	}	
	
	@Override
	public void setFocusedApplication(final ApplicationFocused container) {
		this.bean = container.getContent();
		VetoableChangeSupport changeSupport = container.getChangeSupport();
		
		if (this.changeSupport != null) { // might be null on application start
			this.changeSupport.removeVetoableChangeListener(propertyName, listener);
		}
		this.changeSupport = changeSupport;
		if (this.changeSupport != null) { // might be null on shutdown
			this.changeSupport.addVetoableChangeListener(propertyName, listener);
		}
		
		isFiring = true; // we don't want the model to fire back
		copyBean2Model();
		isFiring = false;
	}

	protected void setupDescriptor(final Class<?> clazz, final String propertyName) {
		try {
			propertyDescriptor = getDescriptor(clazz, propertyName);
			if (propertyDescriptor == null) {
				LOG.error("property descriptor is null for property name '{}'", propertyName);
			}
		} catch (IntrospectionException ex) {
			LOG.error("Error setting property descriptor", ex);
		}
	}

	protected PropertyDescriptor getDescriptor(final Class<?> clazz, final String propertyName)
			throws IntrospectionException {
		// create a property name to descriptor map
		final HashMap<String, PropertyDescriptor> tempDescriptorMap = new HashMap<>();
		BeanInfo beanInfo = Introspector.getBeanInfo(clazz);
		PropertyDescriptor[] propertyDescriptors = beanInfo
				.getPropertyDescriptors();
		for (PropertyDescriptor descriptor : propertyDescriptors) {
			tempDescriptorMap.put(descriptor.getName(), descriptor);
		}
		return tempDescriptorMap.get(propertyName);
	}

	protected class LocalPropertyChangeListener implements VetoableChangeListener {
		@Override
		public void vetoableChange(PropertyChangeEvent evt) {
			if (!isFiring) {
				Object value = evt.getNewValue();
				setText(value.toString());
			}
		}
	}

	protected void copyModel2Bean() {
		LOG.debug("bean is {}, isFiring is {}", bean, isFiring);
		if (isFiring) {
			return;
		}
		if ((BeanPropertyTextfield.this.bean == null)
				|| (BeanPropertyTextfield.this.propertyDescriptor == null)) {
			LOG.info(
					"skipping setter action, bean is {}, propertyDescriptor is {}, host is {}",
					new Object[] { bean, propertyDescriptor,
							BeanPropertyTextfield.this });
			return;
		}
		try {
			isFiring = true;
			propertyDescriptor.getWriteMethod().invoke(bean, getText());
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException ex) {
			LOG.error("Error setting text property", ex);
		} finally {
			isFiring = false;
		}
	}

	protected void copyBean2Model() {
		LOG.debug("bean is {}", bean);
		if ((BeanPropertyTextfield.this.bean == null)
				|| (BeanPropertyTextfield.this.propertyDescriptor == null)) {
			LOG.info(
					"skipping setter action, bean is {}, propertyDescriptor is {}, host is {}",
					new Object[] { bean, propertyDescriptor,
							BeanPropertyTextfield.this });
			return;
		}
		try {
			Object value = propertyDescriptor.getReadMethod().invoke(bean,
					new Object[] {});
			setText(value == null ? "" : value.toString());
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException ex) {
			LOG.error("Error setting text property", ex);
		}
	}

	@Override
	public String toString() {
		return getClass().getName() + "@" + Integer.toHexString(hashCode());
	}

}
