package net.wohlfart.data;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import javax.enterprise.event.Observes;
import javax.inject.Singleton;

import net.wohlfart.data.persis.CandidateTO;
import net.wohlfart.data.persis.DocumentTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The propose of this class is to provide a verification for the
 * field lengths of the data.
 */
@Singleton
public class DataValidator  {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(DataValidator.class);

	private static final String SET_METHOD_START = "set";
	private static final String GET_METHOD_START = "get";
	
	
	private static final Class<?>[] KNOWN_CLASSES = {CandidateTO.class, DocumentTO.class};

	// mapping class/type --> property --> property size
	private final HashMap<Class<?>, HashMap<String, Integer>> columnSizes = new HashMap<>();
	

	
	// check all properties of the bean
	public DataValidationResult validate(final Object bean) {
		return DataValidationResult.VALID;
	}
	
	
	// check the named property of the bean
	public DataValidationResult validate(final Object bean, final String propertyName) {
		return DataValidationResult.VALID;
	}

	

    public void onAnyDataSourceEvent(@Observes final IBasicDataSource dataSource) {
        LOG.debug("onAnyDataSourceEvent called, checking if the datasource is closed: '{}' "
        		+ "(from dataSource.isClosed())",  dataSource.isClosed());
        
        // whatever happens we need to clear first
        columnSizes.clear();
        
        if (!dataSource.isClosed()) {
        	// we might have to fill the map again
        	initFieldsForClasses(dataSource, KNOWN_CLASSES);       	
        }
   
        // --- dump the content: 
        Set<Entry<Class<?>, HashMap<String, Integer>>> entries = columnSizes.entrySet();
        for (Entry<Class<?>, HashMap<String, Integer>> entry : entries) {       	
        	LOG.info("--- {}", entry.getKey());        	
        	Set<Entry<String, Integer>> properties = entry.getValue().entrySet();
        	for (Entry<String, Integer> property : properties) {
        		LOG.info("{} -> {}", property.getKey(), property.getValue());
        	}     	
        }
    }
  

	private void initFieldsForClasses(
			final IBasicDataSource dataSource, 
			final Class<?>... clazzSet) {	
		for (Class<?> clazz : clazzSet) {
			Method[] methods = clazz.getDeclaredMethods();			
			LOG.info("init {}", clazz);
			HashMap<String, Integer> fieldSized = getFieldSize(dataSource, methods);
			columnSizes.put(clazz, fieldSized);
		}		
	}
	
	
	// calculate a map to match property names to database sizes
	private HashMap<String, Integer> getFieldSize(final IBasicDataSource dataSource, final Method[] methods) {
		HashMap<String, Integer> map = new HashMap<>();		
		for (Method method : methods) {
			DatabaseColumn databaseField = method.getAnnotation(DatabaseColumn.class);
			if (databaseField != null) {
				String column = databaseField.column();
				String table = databaseField.table();
				LOG.debug("init DB-field '{}.{}'", table, column);
				
				String name = getAttributeName(method);
				Integer size = getFieldSize(dataSource, table, column);
				
				if ((name != null) && (size != null)) {
					map.put(name, size);
				}
			}
		}		
		return map;
	}


	// returns null if no attribute can be found
	private String getAttributeName(final Method method) {
		String methodName = method.getName();
		if (methodName.startsWith(SET_METHOD_START)
				|| 	methodName.startsWith(GET_METHOD_START)) {

			String propertyName = methodName.substring(3);
			char firstLetter = propertyName.charAt(0);
			// first character after the set/get must be uppercase:
			if (Character.isUpperCase(firstLetter)) {
				// lowerCase the first char
				return "" + Character.toLowerCase(firstLetter) + propertyName.substring(1);
			}
		}
		return null;
	}


	// return null if no size can be detected or the type has no size ?!
	private Integer getFieldSize(final IBasicDataSource dataSource, final String table, final String column) {
		Integer columnSize = null;
		
		try (Connection connection = dataSource.getConnection();)  {		
			DatabaseMetaData metaData = connection.getMetaData();
			
			ResultSet columns = metaData.getColumns(null, null, table, column);
			columns.next();
			
			String columnName = columns.getString("COLUMN_NAME");
			LOG.debug("columnName is {}", columnName);
            
            String columnType = columns.getString("TYPE_NAME");
            LOG.debug("columnType is {}", columnType);

            columnSize = columns.getInt("COLUMN_SIZE");
            LOG.debug("columnSize is {}", columnSize);         
			
		} catch (final SQLException ex) {
			LOG.warn("can't get size for column '{}.{}' error and stacktrace follows...", table, column);
			LOG.error("...exception while trying to get size for column", ex);
		} 
			
		return columnSize;
	}

}
