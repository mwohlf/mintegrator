package net.wohlfart.gui.properties.look.color;

import javax.swing.plaf.metal.MetalLookAndFeel;

public interface IColorTheme {
	
	String getId();

	String getName();

	void apply(MetalLookAndFeel metalLookAndFeel);


}
