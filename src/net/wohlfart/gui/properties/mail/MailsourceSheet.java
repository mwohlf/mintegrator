package net.wohlfart.gui.properties.mail;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.SwingWorker;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

import net.wohlfart.ImageManager;
import net.wohlfart.Messages;
import net.wohlfart.gui.properties.AbstractPropertySheet;
import net.wohlfart.properties.IApplicationProperties;
import net.wohlfart.properties.PropertyKeys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
@Singleton
public class MailsourceSheet extends AbstractPropertySheet {
	/**  Logger for this class */
	private static final Logger LOG = LoggerFactory.getLogger(MailsourceSheet.class);

	 // for lookup in the component hierarchy 
	public static final String NAME = "MailsourceSheet";//$NON-NLS-1$

	private final JComboBox<IMailBoxFlavor> mailsourceSelect;
	
	private volatile IMailBoxFlavor selectedFlavor;
	
	private volatile boolean initOngoing = false; // flag to indicate that we are initializing 
	
	@Inject
	private IApplicationProperties properties;

	MailsourceSheet() {
		super(ImageManager.getImageIcon("CopyOutlookIcon24"),  //$NON-NLS-1$
			  Messages.getString("MailsourceSheet.title"),   //$NON-NLS-1$
			  Messages.getString("MailsourceSheet.tooltip")); //$NON-NLS-1$
		setName(NAME);
		// setup the finals
		mailsourceSelect = createMailsourceSelect();
	}

	@PostConstruct
	protected void postConstruct() { // NO_UCD
		LOG.debug("postConstruct() called"); //$NON-NLS-1$
		initWiring();
		initComponents();
	}


    private void initWiring() {
		// reset the fields not much else to wire here
		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() throws Exception {
				resetAction(); // can't run in EDT, need a swingworker for this
				return null;
			}
		} .execute();
	}


    protected void initComponents() { // NO_UCD
    	LOG.debug("postConstruct() called"); //$NON-NLS-1$
		// do the layout
		setName(NAME);
		setLayout(new GridBagLayout());

		GridBagConstraints labelColumn = new GridBagConstraints();
		labelColumn.insets = new Insets(1,3,0,10);
		labelColumn.anchor = GridBagConstraints.WEST;
		GridBagConstraints editorColumn = new GridBagConstraints();
		editorColumn.insets = new Insets(1,0,0,10);
		editorColumn.anchor = GridBagConstraints.WEST;
		editorColumn.gridx = 1;
		editorColumn.fill = GridBagConstraints.HORIZONTAL;
		labelColumn.gridy = editorColumn.gridy=0;

		
		// --- start editors ---
	

		// the MailSource combobox
		add(new JLabel(Messages.getString("MailsourceSheet.mailsourceSelectLabel")) //$NON-NLS-1$
		{{
			setLabelFor(mailsourceSelect);
		}}, labelColumn);
		add(mailsourceSelect, editorColumn);
		labelColumn.gridy = ++editorColumn.gridy;
		
		
		// CardLayout for the specific configurations
		final CardLayout cardLayout = new CardLayout();
		final JComponent propertyEditor = new JComponent(){{setLayout(cardLayout);}};
		
		ComboBoxModel<IMailBoxFlavor> model = mailsourceSelect.getModel();
		int size = model.getSize();
		for (int i = 0 ; i < size ; i++) {
			IMailBoxFlavor elem = model.getElementAt(i);
			String id = elem.getId();
			propertyEditor.add(elem.getPropertyEditor(), id);
		}
		
		// some wiring to switch the card when the user selects a flavor
		mailsourceSelect.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt) {
				selectedFlavor = (IMailBoxFlavor)mailsourceSelect.getModel().getSelectedItem();
				cardLayout.show(propertyEditor, selectedFlavor.getId());
				// update the selections off the EDIT
				new SwingWorker<Void, Void>() {
					@Override
					protected Void doInBackground() {
						initOngoing = true;
						try {
							initAction(); // this might block for a long time
						} finally {
							initOngoing = false;
						}
						return null;
					}
				}.execute();
			}		
		});
				
		add(propertyEditor, editorColumn);
		labelColumn.gridy = ++editorColumn.gridy;
		
		
		// ------------------- filler and button bar
		
		// add a filler component	
		GridBagConstraints filler = new GridBagConstraints();
		filler.gridy = labelColumn.gridy = editorColumn.gridy;
		filler.gridwidth = 2;
		filler.weightx = filler.weighty = 0.5;
		add(Box.createGlue(), filler);

		// "restore default" and "apply" buttons:
		GridBagConstraints buttons = new GridBagConstraints();
		buttons.gridy = filler.gridy + 1;
		buttons.gridwidth = 2;
		buttons.fill = GridBagConstraints.HORIZONTAL;
		JComponent buttonBar = createButtonBar();
		add(buttonBar, buttons);
		
		// initially update the selections off the EDIT
		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				initOngoing = true;
				try {
					initAction(); // this might block for a long time
				} finally {
					initOngoing = false;
				}
				return null;
			}
		}.execute();
	}
	

	@SuppressWarnings("unchecked")
	private JComboBox<IMailBoxFlavor> createMailsourceSelect() {
		JComboBox<IMailBoxFlavor> result =  new JComboBox<>();
		result.setModel(new MailsourceBoxModel());
		// pick the name to render the cell
		result.setRenderer(new BasicComboBoxRenderer() {
			@Override
			public Component getListCellRendererComponent(final JList list,
					Object value, int index, boolean isSelected,
					boolean cellHasFocus) {
				if (value instanceof IMailBoxFlavor) {
					String newValue = ((IMailBoxFlavor) value).getId();
					return super.getListCellRendererComponent(list, newValue, index, isSelected, cellHasFocus);
				} else {
					LOG.warn("invalid rendering value"); // just a fallback
					return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				}		
			}			
		});		
		return result;	
	}


	private JComponent createButtonBar() {
		Box result = Box.createHorizontalBox();
		// filler
		result.add(Box.createHorizontalGlue());
		// reset button
		result.add(new JButton(new TestAction()));
		// reset button
		result.add(new JButton(new ResetAction()));
		// apply button
		result.add(new JButton(new ApplyAction()));
		// some padding
		result.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
		return result;
	}


	
	// this method is called outside the EDT to test a database connection
	protected void testAction() {
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("Outlook Database backend shouldn't be access in the EDT."); //$NON-NLS-1$
		}
		LOG.info("testAction triggered");		 //$NON-NLS-1$		
		selectedFlavor = (IMailBoxFlavor)mailsourceSelect.getModel().getSelectedItem();
		selectedFlavor.testEditor(properties);
	}
	
	private void initAction() {
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("Outlook Database backend shouldn't be access in the EDT."); //$NON-NLS-1$
		}
		LOG.info("init values in {} ", NAME); //$NON-NLS-1$
		selectedFlavor = (IMailBoxFlavor)mailsourceSelect.getModel().getSelectedItem();
		selectedFlavor.initEditor(properties);
	}
	
	// also called in the init method
	@Override
	public void resetAction() {
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("Outlook Database backend shouldn't be access in the EDT."); //$NON-NLS-1$
		}
		LOG.info("resetting values in {} ", NAME); //$NON-NLS-1$
		
		selectedFlavor = null;
		String sourceId = properties.getString(PropertyKeys.MAIL_SOURCE_FLAVOR, null);
		if (sourceId != null) {
			selectedFlavor = MailBoxFlavourCollection.getInstance().get(sourceId);
		}  // fallback in case something went wrong with the properties
		if (selectedFlavor == null) {
			selectedFlavor = (IMailBoxFlavor)MailBoxFlavourCollection.getInstance().getNullElement();
		}
		if (!initOngoing) {
			mailsourceSelect.setSelectedItem(selectedFlavor);
		}
		selectedFlavor.resetEditor(properties);
	}
	
	@Override
	public void applyAction() {
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("Outlook Database backend shouldn't be access in the EDT."); //$NON-NLS-1$
		}
		LOG.info("applying values in {} ", NAME); //$NON-NLS-1$
		selectedFlavor = (IMailBoxFlavor)mailsourceSelect.getModel().getSelectedItem();
		selectedFlavor.applyEditor(properties);
		// persist the flavor:
		properties.setString(PropertyKeys.MAIL_SOURCE_FLAVOR, selectedFlavor.getId());
	}



	// custom action to test the database
	protected class TestAction extends AbstractAction {

		public TestAction() {
			putValue(Action.NAME, Messages.getString("MailsourceSheet.TestAction.ActionName")); //$NON-NLS-1$
		}

		@Override
		public void actionPerformed(ActionEvent evt) {
			MailsourceSheet.this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			new SwingWorker<Void, Void>() {
				@Override
				protected Void doInBackground() throws Exception {
					try {
						testAction();
					} catch (Throwable th) {
						LOG.error("error while performing test action in database sheet", th); //$NON-NLS-1$
					} 
					return null;
				}
				@Override
				protected void done() {
					MailsourceSheet.this.setCursor(Cursor.getDefaultCursor());
				};				
			}.execute();
		}
	}

}
