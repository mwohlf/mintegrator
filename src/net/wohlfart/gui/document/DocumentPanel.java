package net.wohlfart.gui.document;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import net.wohlfart.action.DocumentCreateAction;
import net.wohlfart.action.DocumentDeleteAction;
import net.wohlfart.action.DocumentSaveAction;
import net.wohlfart.data.ICandidateTO;
import net.wohlfart.data.IDocumentTO;
import net.wohlfart.data.persis.DocumentTO;
import net.wohlfart.gui.event.ApplicationFocused;
import net.wohlfart.gui.event.DocumentCreated;
import net.wohlfart.gui.event.DocumentFocused;
import net.wohlfart.worker.DocumentCreateWorker;
import net.wohlfart.worker.DocumentFolderReadWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * document list view
 * - adding documents creates a new subcomponent
 * - keeps the current focused component
 * 
 * @author michael
 */
@SuppressWarnings("serial")
@Singleton
public class DocumentPanel extends JComponent {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(DocumentPanel.class);

	private static final DataFlavor[] SUPPORTED_FLAVORS = new DataFlavor[] {DataFlavor.javaFileListFlavor};

	private static final int acceptableDropActions = DnDConstants.ACTION_COPY;

	@Inject
	private DocumentCreateAction createDocument;
	@Inject
	private DocumentDeleteAction deleteDocument;
	@Inject
	private DocumentSaveAction saveDocument;


	@Inject // to create a document form a file 
	private Instance<DocumentCreateWorker> documentCreateWorker;
	@Inject // to read a application data folder from the database
	private Instance<DocumentFolderReadWorker> documentFolderReadWorker;
	@Inject
	private Instance<DocumentView> documentView;


	private volatile boolean isFiring = false;
	private volatile ICandidateTO candidate;


	// the container for all documents 
	private final JPanel documentContainer;
	// scrollable container
	private final JScrollPane innerScrollPane;
	// import handler
	//private final DocImportTransferHandler transferHandler;
	// drop action listener
	private final DropListener dropListener;



	public DocumentPanel() {
		LOG.debug("DocumentPanel() - init"); //$NON-NLS-1$
		if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("All swing components should be created in the EDT."); //$NON-NLS-1$
		}
		documentContainer = new JPanel();
		innerScrollPane = new JScrollPane();
		//transferHandler = new DocImportTransferHandler();
		dropListener = new DropListener();
	}




	@PostConstruct
	protected void postConstruct() { // NO_UCD
		LOG.debug("postConstruct() called"); //$NON-NLS-1$ 
		initComponents();
	}


	private void initComponents() {
		// setup the layout    	
		setLayout(new BorderLayout());

		setComponentPopupMenu(new JPopupMenu() {{
			add(createDocument);
			add(deleteDocument);
			add(saveDocument);
		}});

		setDropTarget(new DocDropTarget(this, dropListener));		
		//setTransferHandler(transferHandler);

		innerScrollPane.setViewportView(documentContainer);
		add(innerScrollPane, BorderLayout.CENTER);

		BoxLayout boxLayout = new BoxLayout(documentContainer, BoxLayout.Y_AXIS);
		documentContainer.setLayout(boxLayout);

		innerScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		innerScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
	}

	// called by the SwingWorker in the EDT
	private void addDocument(final IDocumentTO doc) {
		// this must run in EDT
		if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The method doExecute() must be called from EDT."); //$NON-NLS-1$
		}
		DocumentView view = documentView.get(); // create an instance
		view.setModel(doc);
		documentContainer.add(view);		
		view.setDropTarget(new DocDropTarget(view, dropListener));	
	}



	// called by the drop listner
	private void importFiles(final List<File> files) {
		// FIXME: for now only import any file that is in the list
		// we might have to process folders later...
		List<File> arrayList = new ArrayList<>();
		for (File file : files) {
			if (file.isFile() && file.canRead()) {
				arrayList.add(file);
			}
		}
		File[] selectedFiles = arrayList.toArray(new File[arrayList.size()]);		
		documentCreateWorker.get() .doExecute(selectedFiles, candidate);	
	}


	/**
	 * mostly triggered by the open database connection worker
	 */
	public void onAddedDocumentEvent(@Observes final DocumentCreated container) { // NO_UCD
		LOG.info("onAddedDocumentEvent called in {}, container is: {}, content is {} ", 
				new Object[] {this, container, container.get()}); //$NON-NLS-1$		
		// we always send a null document last to make sure the repaint is triggered only once and also when there is no
		// doc attached
		DocumentTO doc = container.get();
		if (doc == null) {
			revalidate();
			repaint();	
		} else {
			addDocument(container.get());
		}
	}



	public void onFocusedDocumentEvent(@Observes final DocumentFocused container) { // NO_UCD
		LOG.info("onFocusedDocumentEvent called, triggered repaint()"); //$NON-NLS-1$
		Component[] comps = documentContainer.getComponents();
		for (Component comp  : comps) {
			if (comp instanceof DocumentView) {
				((DocumentView) comp).onFocusedDocumentEvent(container);
			}
		}
		repaint();
	}


	public void onFocusedApplicationEvent(@Observes final ApplicationFocused container) { // NO_UCD
		LOG.info("onFocusedApplicationEvent called, container is: {} , isFiring: {}", 
				new Object[] {container, isFiring});
		candidate = (ICandidateTO) container.getContent();	
		updateView(candidate);
	}


	private void updateView(final ICandidateTO candidate) {
		// remove the content
		Component[] comps = documentContainer.getComponents();
		for (Component comp  : comps) {
			documentContainer.remove(comp);
		}
		// let a SwingWorker populate this component, also repaints/revalidates this
		// also can handle a null candidate
		documentFolderReadWorker.get() .doExecute(candidate);
	}


	private class DropListener implements DropTargetListener {

		@Override
		public void dragEnter(DropTargetDragEvent evt) {
			LOG.debug("dragEnter()");
			evt.acceptDrag(acceptableDropActions);
		}
		@Override
		public void dragOver(DropTargetDragEvent evt) {
			LOG.debug("dragOver()");
			evt.acceptDrag(acceptableDropActions);      
		}
		@Override
		public void dropActionChanged(DropTargetDragEvent evt) {
			LOG.debug("dropActionChanged()");
			evt.acceptDrag(acceptableDropActions);     
		}
		@Override
		public void dragExit(DropTargetEvent evt) {
			LOG.debug("dragExit()");
		}
		@Override
		@SuppressWarnings("unchecked")
		public void drop(DropTargetDropEvent evt) {
			LOG.debug("drop event triggered");			
			evt.acceptDrop(acceptableDropActions);
			Transferable incoming = evt.getTransferable();
			LOG.debug("incoming transferable: '{}'", incoming);
			try {
				Object data = incoming.getTransferData(SUPPORTED_FLAVORS[0]);
				LOG.debug("incoming data: '{}'", data);
				List<File> list = (List<File>) data;
				LOG.debug("file list: '{}'", list);	// these are files or directories that are dropped into the Panel
				DocumentPanel.this.importFiles(list);
			} catch (UnsupportedFlavorException | IOException ex) {
				LOG.error("exception during drop action", ex);
				evt.rejectDrop();
			}
		}
	}


	private static class DocDropTarget extends DropTarget {

		public DocDropTarget(final Component component, final DropTargetListener listener) {  
			super(component,                             // view target
					DocumentPanel.acceptableDropActions,   // ops
					listener,       // listener
					true);                                 // accept drops
		}
	}

}
