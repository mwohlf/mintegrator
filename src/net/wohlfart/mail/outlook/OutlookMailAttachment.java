/**
 *
 */
package net.wohlfart.mail.outlook;

import java.io.File;
import java.io.IOException;

import net.wohlfart.mail.IMailAttachment;

import com.moyosoft.connector.ms.outlook.attachment.OutlookAttachment;

/**
 * @author michael
 *
 */
public class OutlookMailAttachment implements IMailAttachment {

    private final OutlookAttachment data;

    public OutlookMailAttachment(OutlookAttachment data) {
        this.data = data;
    }

    @Override
	public String getFilename() {
        return data.getDisplayName();
    }

    @Override
	public File getTmpFile() throws IOException {
        File file = File.createTempFile("PersMan", data.getDisplayName());
        data.saveAsFile(file);
        return file;
    }

}
