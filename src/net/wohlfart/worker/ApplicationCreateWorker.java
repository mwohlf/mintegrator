/**
 *
 */
package net.wohlfart.worker;

import java.awt.EventQueue;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.swing.JOptionPane;

import net.wohlfart.CustomSwingWorker;
import net.wohlfart.Messages;
import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.data.ICandidateTO;
import net.wohlfart.data.persis.CandidateDAO;
import net.wohlfart.data.persis.CandidateTO;
import net.wohlfart.gui.ErrorPane;
import net.wohlfart.gui.event.ApplicationCreated;
import net.wohlfart.gui.event.ApplicationFocused;
import net.wohlfart.gui.table.ApplicationCollectionFocused;
import net.wohlfart.util.GuiUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author michael
 *
 */
public class ApplicationCreateWorker extends CustomSwingWorker<ICandidateTO, Void> {
    /** Logger for this class */
    private final static Logger LOG = LoggerFactory.getLogger(ApplicationCreateWorker.class);

    @Inject
    private CandidateDAO candidateDAO;
	@Inject
	private IBasicDataSource dataSource; // to keep track of data object changes
	
	@Inject 
	private Event<ApplicationCreated> addedCandidateEvent;
	@Inject 
	private Event<ApplicationFocused> applicationFocusEvent;
	@Inject 
	private Event<ApplicationCollectionFocused> candidateCollectionEvent;


	/**
	 * show a ProgressMonitor
	 */
	@Override
	public void doExecute() {
		// this must run in EDT
        if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The method doExecute() must be called from EDT."); //$NON-NLS-1$
		}
        
        super.doExecute();
	}


    @Override
    protected ICandidateTO doInBackground() {
        LOG.debug("doInBackground() - start"); //$NON-NLS-1$
        
        try {

			setProgress(10);
			if (isCanceled()) {
				return null;
			}

            progressMonitor.setProgress(30);
            ICandidateTO candidate = candidateDAO.createCandidate();
            
			setProgress(70);
			if (isCanceled()) {
				return null;
			}

            return candidate;

        } catch (SQLException ex) {
            LOG.error("doInBackground()", ex);
			GuiUtilities.setDefaultCursor();
            progressMonitor.close();
            JOptionPane.showMessageDialog(GuiUtilities.getFrame(),
                    ex.getMessage(),
                    Messages.getString("CreateSingleApplicationWorker.SQLException"),
                    JOptionPane.ERROR_MESSAGE);
            
        } catch (Throwable ex) {
            LOG.error("doInBackground()", ex);
			GuiUtilities.setDefaultCursor();
            progressMonitor.close();
            ErrorPane.showErrorDialog(ex, Messages.getString("CreateSingleApplicationWorker.Throwable")); //$NON-NLS-1$

        } finally {
            queue.clearBlockedComponents();
            GuiUtilities.setDefaultCursor();
        }
        LOG.debug("doInBackground() - end");
        return null;
    }


    @Override
    // this runs in the swing thread
    public void done() {
    	super.done();
        try {
            ICandidateTO newApplication = get();
            if (newApplication == null) {
                return;
            }

            // fire an application added
            addedCandidateEvent.fire(new ApplicationCreated((CandidateTO)newApplication));
            // focus on the new application
            applicationFocusEvent.fire(new ApplicationFocused(newApplication, dataSource));
            // reduce the focused list to the one new application
            candidateCollectionEvent.fire(new ApplicationCollectionFocused(Arrays.asList(newApplication)));
            
        } catch (InterruptedException | ExecutionException ex) {
            LOG.error("done()", ex);
            ErrorPane.showErrorDialog(ex, Messages.getString("CreateSingleApplicationWorker.Throwable")); //$NON-NLS-1$
        }
    }

}
