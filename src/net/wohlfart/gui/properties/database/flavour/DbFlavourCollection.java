package net.wohlfart.gui.properties.database.flavour;

import java.util.HashMap;


@SuppressWarnings("serial")
public final class DbFlavourCollection extends HashMap<String, IDatabaseFlavor> {

	// there keys are  used for i18n lookup and also as ID
	private static final String HSQL = "HSQL"; //$NON-NLS-1$
	private static final String MS_SQL = "MSSQL"; //$NON-NLS-1$
	private static final String MY_SQL = "MYSQL"; //$NON-NLS-1$
	private static final String ORACLE = "ORACLE"; //$NON-NLS-1$
	private static final String POSTGRESQL = "PostgreSQL"; //$NON-NLS-1$
	private static final String DERBY_EMBEDDED = "Derby-embedded"; //$NON-NLS-1$
	private static final String DERBY_CLIENT = "Derby-net"; //$NON-NLS-1$
	private static final String NULL = "null"; //$NON-NLS-1$
	

	private static DbFlavourCollection instance;


	private DbFlavourCollection() {
		addFlavour(new MsSql(DbFlavourCollection.MS_SQL));
		addFlavour(new MySql(DbFlavourCollection.MY_SQL));
		addFlavour(new Oracle(DbFlavourCollection.ORACLE));
		addFlavour(new HSqlDb(DbFlavourCollection.HSQL));
		addFlavour(new PostgreSql(DbFlavourCollection.POSTGRESQL));
		addFlavour(new DerbyEmbedded(DbFlavourCollection.DERBY_EMBEDDED));
		addFlavour(new DerbyClient(DbFlavourCollection.DERBY_CLIENT));
		addFlavour(new NullFlavor(DbFlavourCollection.NULL));
	}

	public static synchronized DbFlavourCollection getInstance() {
		if (instance == null) {
			instance = new DbFlavourCollection();
		}
		return instance;
	}


	private void addFlavour(IDatabaseFlavor databaseFlavour) {
		put(databaseFlavour.getId(), databaseFlavour);
	}

	public Object getNullElement() {
		return get(NULL);
	}


}
