package net.wohlfart.action;

import static net.wohlfart.util.GuiUtilities.getClipboard;

import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.FlavorEvent;
import java.awt.datatransfer.FlavorListener;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;

import net.wohlfart.ImageManager;
import net.wohlfart.Messages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * @author michael
 *
 */
@SuppressWarnings("serial")
@Singleton
public class GlobalPasteAction extends AbstractAction {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(GlobalPasteAction.class);

	private static final KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_MASK);

	public GlobalPasteAction() {
		putValue(Action.NAME, Messages.getString("GlobalPasteAction.ActionName")); //$NON-NLS-1$
		putValue(Action.SMALL_ICON, ImageManager.getImageIcon("PasteIcon16")); //$NON-NLS-1$
		putValue(Action.LARGE_ICON_KEY, ImageManager.getImageIcon("PasteIcon24")); //$NON-NLS-1$
		putValue(Action.SHORT_DESCRIPTION, Messages.getString("GlobalPasteAction.ShortDescription")); //$NON-NLS-1$
		putValue(Action.ACCELERATOR_KEY, keyStroke);
	}

    @PostConstruct
    protected void postConstruct() { // NO_UCD
    	LOG.debug("postConstruct() called"); //$NON-NLS-1$
		// patch the default keymap for JTextComponents
		// http://www.java-tips.org/java-se-tips/javax.swing/how-to-map-actions-with-keystrokes.html
		Keymap defaultKeymap = JTextComponent.getKeymap(JTextComponent.DEFAULT_KEYMAP);			
		defaultKeymap.addActionForKeyStroke(
				keyStroke, 
				this);

		Toolkit.getDefaultToolkit().getSystemClipboard().addFlavorListener(new FlavorListener(){
			@Override
			public void flavorsChanged(FlavorEvent evt) {
				checkClipboardFlavors();
			}
		});
		checkClipboardFlavors();
	}


	@Override
	public void actionPerformed(final ActionEvent evt) {
		LOG.info("paste event happend: {}", evt); //$NON-NLS-1$
		Object object = KeyboardFocusManager.getCurrentKeyboardFocusManager().getPermanentFocusOwner();
		if (object instanceof JTextComponent) {
			JTextComponent textComponent = (JTextComponent)object;
			String clipboard = getClipboard();
			textComponent.replaceSelection(clipboard);
		} else {
			LOG.info("can't paste, focus owner is not a JTextComponent: {}", object); //$NON-NLS-1$
		}
	}


	private void checkClipboardFlavors() {
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		DataFlavor[] flavors = clipboard.getAvailableDataFlavors();
		boolean containsStringFlavor = false;
		for (DataFlavor flavor: flavors) {
			containsStringFlavor = containsStringFlavor || flavor.equals(DataFlavor.stringFlavor);
		}

		// FIXME: there is more than just String copy-paste
		if (!containsStringFlavor) {
			setEnabled(false);
			return;
		}

		boolean isEnabled = false;
		try {
			Object object =  clipboard.getData(DataFlavor.stringFlavor);
			if ( (object != null)
					&& (object instanceof String)
					&& (((String)object).length() > 0) ) {
				isEnabled = true;
			} 
		} catch(UnsupportedFlavorException | IOException ex) {
			LOG.warn("problem while checking clipboard content", ex); //$NON-NLS-1$
		} finally {
			setEnabled(isEnabled);
		}
	}

}
