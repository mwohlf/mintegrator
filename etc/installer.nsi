Name "PersMan Mintegrator"

#
# this file needs to be iso-latin-1 encoded in order for the umlauts to work!
#

# Included files
!include Sections.nsh
!include MUI2.nsh

SetCompressor lzma

;Request application privileges for Windows Vista
RequestExecutionLevel admin

; replace the NSIS branding text
BrandingText "PersMan Software-Vertrieb"

; we want to see the installation details
ShowInstDetails show

# Defines
!define REGKEY "SOFTWARE\$(^Name)"
!define VERSION 3.0
!define COMPANY PersMan
!define URL http://www.persman.de/

# MUI defines
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install-blue.ico"
!define MUI_STARTMENUPAGE_REGISTRY_ROOT HKLM
!define MUI_STARTMENUPAGE_REGISTRY_KEY ${REGKEY}
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME StartMenuGroup
!define MUI_STARTMENUPAGE_DEFAULTFOLDER "Mintegrator"
!define MUI_FINISHPAGE_RUN $INSTDIR\Mintegrator.exe
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"

# install page title needs more space:
!define MUI_WELCOMEPAGE_TITLE_3LINES

# Variables
Var StartMenuGroup

# Installer pages
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_STARTMENU Application $StartMenuGroup
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

# Installer languages
!insertmacro MUI_LANGUAGE German

# Installer attributes
OutFile ../dist/MI-installer.exe
InstallDir "$PROGRAMFILES\Mintegrator"
CRCCheck on
XPStyle on
ShowInstDetails hide
VIProductVersion 3.0.0.0
VIAddVersionKey ProductName "mintegrator"
VIAddVersionKey ProductVersion "${VERSION}"
VIAddVersionKey CompanyName "${COMPANY}"
VIAddVersionKey CompanyWebsite "${URL}"
VIAddVersionKey FileVersion ""
VIAddVersionKey FileDescription ""
VIAddVersionKey LegalCopyright ""
InstallDirRegKey HKLM "${REGKEY}" Path
ShowUninstDetails show


########################################################################################

# Installer sections
Section "Mintegrator" SEC0000
    SetOutPath $INSTDIR\conf
    SetOverwrite on
    File /r ..\dist\conf\*
    SetOutPath $INSTDIR\gfx
    File /r ..\dist\gfx\*
#    SetOutPath $INSTDIR\help
#    File /r ..\dist\help\*
    SetOutPath $INSTDIR\lib
    File /r ..\dist\lib\*
    SetOutPath $INSTDIR
    File ..\dist\Mintegrator.exe
    WriteRegStr HKLM "${REGKEY}\Components" Mintegrator 1
    AccessControl::GrantOnFile "$INSTDIR" "(S-1-5-32-545)" "FullAccess"
SectionEnd

Section "JRE 1.7.0" SEC0001
    SetOutPath $INSTDIR\jre
    SetOverwrite on
    File /r ..\dist\jre\*
    WriteRegStr HKLM "${REGKEY}\Components" "JRE 1.7.0" 1
SectionEnd



########################################################################################


Section -post SEC0002
    WriteRegStr HKLM "${REGKEY}" Path $INSTDIR
    SetOutPath $INSTDIR
    WriteUninstaller $INSTDIR\uninstall.exe
    !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    SetOutPath $SMPROGRAMS\$StartMenuGroup
    CreateShortCut "$DESKTOP\Mintegrator.lnk" "$INSTDIR\Mintegrator.exe" ""
    CreateShortCut "$SMPROGRAMS\$StartMenuGroup\Start $(^Name).lnk" $INSTDIR\Mintegrator.exe
    CreateShortcut "$SMPROGRAMS\$StartMenuGroup\Uninstall $(^Name).lnk" $INSTDIR\uninstall.exe
    !insertmacro MUI_STARTMENU_WRITE_END
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" DisplayName "$(^Name)"
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" DisplayVersion "${VERSION}"
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" Publisher "${COMPANY}"
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" URLInfoAbout "${URL}"
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" DisplayIcon $INSTDIR\uninstall.exe
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" UninstallString $INSTDIR\uninstall.exe
    WriteRegDWORD HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" NoModify 1
    WriteRegDWORD HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" NoRepair 1
SectionEnd



# modern install componenten descriptions
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
  !insertmacro MUI_DESCRIPTION_TEXT ${SEC0000} "PersMan Mintegrator sowie Datenbankschnittstellen, ben�tigt zur Ausf�hrung eine Java Laufzeitumgebung."
  !insertmacro MUI_DESCRIPTION_TEXT ${SEC0001} "Eine aktuelle Java Laufzeitumgebung (JRE), diese JRE wird nicht in das Betriebssystem eingebunden."
!insertmacro MUI_FUNCTION_DESCRIPTION_END





# Macro for selecting uninstaller sections
!macro SELECT_UNSECTION SECTION_NAME UNSECTION_ID
    Push $R0
    ReadRegStr $R0 HKLM "${REGKEY}\Components" "${SECTION_NAME}"
    StrCmp $R0 1 0 next${UNSECTION_ID}
    !insertmacro SelectSection "${UNSECTION_ID}"
    GoTo done${UNSECTION_ID}
next${UNSECTION_ID}:
    !insertmacro UnselectSection "${UNSECTION_ID}"
done${UNSECTION_ID}:
    Pop $R0
!macroend

# Uninstaller sections
Section /o "un.JRE 1.7.0" UNSEC0001
    RmDir /r /REBOOTOK $INSTDIR\jre
    DeleteRegValue HKLM "${REGKEY}\Components" "JRE 1.7.0"
SectionEnd

Section /o "un.Mintegrator" UNSEC0000
    Delete /REBOOTOK $INSTDIR\Mintegrator.exe
    RmDir /r /REBOOTOK $INSTDIR\log
    RmDir /r /REBOOTOK $INSTDIR\lib
#    RmDir /r /REBOOTOK $INSTDIR\help
    RmDir /r /REBOOTOK $INSTDIR\gfx
    RmDir /r /REBOOTOK $INSTDIR\conf
    RmDir /r /REBOOTOK $INSTDIR
    DeleteRegValue HKLM "${REGKEY}\Components" Mintegrator
SectionEnd

Section un.post UNSEC0002
    DeleteRegKey HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)"
    Delete /REBOOTOK "$DESKTOP\Mintegrator.lnk"
    Delete /REBOOTOK "$SMPROGRAMS\$StartMenuGroup\Start $(^Name).lnk"
    Delete /REBOOTOK "$SMPROGRAMS\$StartMenuGroup\Uninstall $(^Name).lnk"
    Delete /REBOOTOK $INSTDIR\uninstall.exe
    DeleteRegValue HKLM "${REGKEY}" StartMenuGroup
    DeleteRegValue HKLM "${REGKEY}" Path
    DeleteRegKey /IfEmpty HKLM "${REGKEY}\Components"
    DeleteRegKey /IfEmpty HKLM "${REGKEY}"
    RmDir /REBOOTOK $SMPROGRAMS\$StartMenuGroup
    RmDir /REBOOTOK $INSTDIR
    Push $R0
    StrCpy $R0 $StartMenuGroup 1
    StrCmp $R0 ">" no_smgroup
no_smgroup:
    Pop $R0
SectionEnd

# Installer functions
Function .onInit
    InitPluginsDir
FunctionEnd


# Uninstaller functions
Function un.onInit
    SetAutoClose true
    ReadRegStr $INSTDIR HKLM "${REGKEY}" Path
    !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuGroup
    !insertmacro SELECT_UNSECTION "Mintegrator" ${UNSEC0000}
    !insertmacro SELECT_UNSECTION "JRE 1.7.0" ${UNSEC0001}
FunctionEnd


