package net.wohlfart;

import org.jboss.weld.environment.se.StartMain;
import org.jboss.weld.environment.se.WeldContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
 * 
 * @author michael
 */
public final class Start {

	private static final Logger LOG = LoggerFactory.getLogger(Start.class);


	/**  
	 * A private constructor since this class is not meant to be instantiated ever. 
	 */
	private Start() {
		throw new AssertionError("call the main() method to run this application"); //$NON-NLS-1$
	}


	/**
	 * start the whole application, 
	 * we need the container in order to do some  stuff in the EDT
	 * otherwise we could have just used
	 * StartMain.main(String[]) to bootup the container
	 * 
	 * @param args command line parameters
	 */
	public static void main(String[] args)
	{
		LOG.debug("running main"); //$NON-NLS-1$
		WeldContainer container = new StartMain(args).go();     
		LOG.debug("created the container"); //$NON-NLS-1$
		Core core = container.instance().select(Core.class).get();
		LOG.debug("got the core"); //$NON-NLS-1$
		LOG.debug("launching the core..."); //$NON-NLS-1$
		core.launch(container);
		LOG.debug("...returned from launching the core"); //$NON-NLS-1$
	}

}
