/**
 *
 */
package net.wohlfart.mail;

import java.io.File;
import java.io.IOException;

/**
 * @author michael
 *
 */
public interface IMailAttachment {


    // the filename may be different than the name of the TmpFile
    String getFilename();

    // the data are stored in a tmp file
    File getTmpFile() throws IOException;


    //IDocumentFileTO getDocumentFileTO();

}
