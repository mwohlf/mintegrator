package net.wohlfart.properties;

import java.awt.Dimension;

public interface IApplicationProperties {
	
	// --- special getter
	
	public abstract String getDatabaseUrl();

	public abstract String getPassword();

	
	
	// ---  Object getter/setter
	
	public abstract String getProperty(final String key, final String defaultValue);
	
	public abstract Object setProperty(final String key, final String value);


	
	// ---  String getter/setter

	public abstract String getString(String key, String defaultValue);
	
	public abstract void setString(String key, String value);

	
	
	// ---   Integer getter/setter
	
	public abstract Integer getInteger(String key, Integer defaultValue);
	
	public abstract void setInteger(String key, Integer value);

	
	
	// ---  Boolean getter/setter

	public abstract Boolean getBoolean(String key, Boolean defaultValue);

	public abstract void setBoolean(String key, Boolean value);

	
	
	// ---   Dimension getter/setter
	
	public abstract Dimension getDimension(String key, Dimension defaultValue);

	public abstract void setDimension(String key, Dimension value);

	
	
	
	public abstract void delete(String key);

	public abstract void setPassword(String password);

	public abstract void deletePassword();

	
	
	public abstract void save();

	public abstract void load();


	
}
