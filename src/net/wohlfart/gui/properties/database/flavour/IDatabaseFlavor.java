package net.wohlfart.gui.properties.database.flavour;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public interface IDatabaseFlavor {
	
	String getId();
	
	String getUrl(final String host, final Integer port, final String dbName);
	
	//String getFileExtension();

	String getDriverClassName(); // FIXME: make sure we call DriverManager.deregisterDriver on that classname

	// used to return default values for port, database name, etc
	String getDefaultValue(String key, String defaultValue);

	// throws an exception if something is wrong with the database, also used to setup the DB
	// in case we have a embedded DB
	void validateConnection(Connection connection) throws SQLException;

	// this is used by the DAO to get the queries 
	Properties getQueries(Class<?> clazz);

}
