package net.wohlfart.gui.editor;

import net.wohlfart.gui.event.ApplicationFocused;

public interface IFocusedApplicationListener {

	public abstract void setFocusedApplication(final ApplicationFocused container);

}
