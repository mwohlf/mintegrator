package net.wohlfart.data.persis;

import java.io.File;
import java.io.IOException;

import net.wohlfart.data.IDocumentFileTO;
import net.wohlfart.mail.IMailAttachment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author michael
 */
public class DocumentDAO {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(DocumentDAO.class);

    public DocumentDAO() {
    }


    public IDocumentFileTO createDocumentFile(final File file, final String docName) {
    	LOG.debug("creating document from file {} with name {}", 
    			new Object[] {file, docName});
        return new DocumentFileTO(file, docName);
    }

    public IDocumentFileTO createDocumentFile(final File file) {
    	LOG.debug("creating document for file {}", file);
        return new DocumentFileTO(file, file.getName());
    }

    public IDocumentFileTO createDocumentFile(final IMailAttachment attachment) throws IOException {
    	LOG.debug("creating document for attachment {}", attachment);
        return new DocumentFileTO(attachment.getTmpFile(), attachment.getFilename());
    }

}
