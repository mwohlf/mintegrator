package net.wohlfart.action;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;

import net.wohlfart.ImageManager;
import net.wohlfart.Messages;
import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.worker.ApplicationCreateWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Creates a single application out of thin air
 * 
 * @author michael
 */
@SuppressWarnings("serial")
@Singleton
public class ApplicationCreateAction extends AbstractAction {
    /**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(ApplicationCreateAction.class);

    @Inject
    private Instance<ApplicationCreateWorker> createSingleCandidateWorker;

	private boolean dataSourceConnected;


    public ApplicationCreateAction() {
        putValue(Action.NAME, Messages.getString("CreateCandidate.ActionName")); //$NON-NLS-1$
        putValue(Action.SMALL_ICON, ImageManager.getImageIcon("NewApplicationIcon16")); //$NON-NLS-1$
        putValue(Action.LARGE_ICON_KEY, ImageManager.getImageIcon("NewApplicationIcon24")); //$NON-NLS-1$
        putValue(Action.SHORT_DESCRIPTION, Messages.getString("CreateCandidate.ShortDescription")); //$NON-NLS-1$
        putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
    }

    @Override
	public void actionPerformed(final ActionEvent evt) {
        createSingleCandidateWorker.get() .doExecute();
    }

    private void doEnableCheck() {
    	setEnabled(dataSourceConnected);
    }
    	
    public void onAnyDataSourceEvent(@Observes final IBasicDataSource dataSource) {
        LOG.debug("onAnyDataSourceEvent called, checking if the datasource is closed: '{}' " //$NON-NLS-1$
        		+ "(from dataSource.isClosed())", dataSource.isClosed()); //$NON-NLS-1$
        dataSourceConnected = !dataSource.isClosed();
        doEnableCheck();
    }

}
