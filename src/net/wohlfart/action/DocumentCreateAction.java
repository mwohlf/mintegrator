package net.wohlfart.action;

import static net.wohlfart.util.GuiUtilities.getFrame;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Collection;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.KeyStroke;

import net.wohlfart.ImageManager;
import net.wohlfart.Messages;
import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.data.ICandidateTO;
import net.wohlfart.gui.table.ApplicationCollectionFocused;
import net.wohlfart.worker.DocumentCreateWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * simple action to fire up a file loader
 */
@SuppressWarnings("serial")
@Singleton
public class DocumentCreateAction extends AbstractAction {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(DocumentCreateAction.class);
    
    @Inject
    private Instance<DocumentCreateWorker> documentCreateWorker;

    private final JFileChooser jFileChooser;
	private boolean dataSourceConnected;
	private Collection<ICandidateTO> focusedApplications;


    public DocumentCreateAction() {
        putValue(Action.NAME, Messages.getString("CreateDocument.ActionName")); //$NON-NLS-1$
        putValue(Action.SMALL_ICON, ImageManager.getImageIcon("AddDocumentsIcon16")); //$NON-NLS-1$
        putValue(Action.LARGE_ICON_KEY, ImageManager.getImageIcon("AddDocumentsIcon24")); //$NON-NLS-1$
        putValue(Action.SHORT_DESCRIPTION, Messages.getString("CreateDocument.ShortDescription")); //$NON-NLS-1$
        putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_M, InputEvent.CTRL_MASK));
        
        jFileChooser = new JFileChooser();
        jFileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
        // enable selection of multiple files
        jFileChooser.setMultiSelectionEnabled(true);
        jFileChooser.setDialogTitle(Messages.getString("CreateDocument.DialogTitle")); //$NON-NLS-1$
      
        doEnableCheck();
    }

    @Override
    public void actionPerformed(ActionEvent evt) {

        // reset the selection
        jFileChooser.setSelectedFiles(new File[]{});
        int chooserResult = jFileChooser.showOpenDialog(getFrame());

        if (chooserResult == JFileChooser.APPROVE_OPTION) {
            File[] selectedFiles = jFileChooser.getSelectedFiles();            
            ICandidateTO application = focusedApplications.toArray(new ICandidateTO[1])[0];          
            documentCreateWorker.get() .doExecute(selectedFiles, application);
        }
    }
    
    private void doEnableCheck() {
    	int count = focusedApplications==null?0:focusedApplications.size();
    	setEnabled(dataSourceConnected && (count == 1));
    }
    	
	public void onFocusedCandidateCollectionEvent(@Observes final ApplicationCollectionFocused container) {
		LOG.debug("onFocusedCandidateCollectionEvent called, container is: '{}' ",  container); //$NON-NLS-1$
		focusedApplications = container.get();
		doEnableCheck();
	}

    public void onAnyDataSourceEvent(@Observes final IBasicDataSource dataSource) {
        LOG.debug("onAnyDataSourceEvent called, checking if the datasource is closed: '{}' " //$NON-NLS-1$
        		+ "(from dataSource.isClosed())", dataSource.isClosed()); //$NON-NLS-1$
        dataSourceConnected = !dataSource.isClosed();
        doEnableCheck();
    }

}
