package net.wohlfart.gui.renderer;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;

@SuppressWarnings("serial")
public class StringArrayRenderer extends DefaultTableCellRenderer {
    final JTextField field = new JTextField();

    public StringArrayRenderer() {
    }

    @Override
    public Component getTableCellRendererComponent(
            JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column)  {

        if ((value != null) && (value.getClass() != null) && (value.getClass().isArray())) {
            String[] array = (String[]) value;
            StringBuffer buffer = new StringBuffer();
            for (int i = 0; i < array.length; i++) {
                buffer.append(array[i]);
                if (i < array.length - 1) {
                    buffer.append(", ");
                }
            }
            return super.getTableCellRendererComponent(
                    table, buffer.toString(), isSelected, hasFocus, row, column);
        }
        return super.getTableCellRendererComponent(
                table, value, isSelected, hasFocus, row, column);

    }

}
