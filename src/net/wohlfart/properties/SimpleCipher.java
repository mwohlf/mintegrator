package net.wohlfart.properties;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;


/**
 * 
 * 
 * 
 * @author michael
 */
public class SimpleCipher {

	private static final String KEY_FAB =  "PBEWithMD5AndDES";

	private static final String DEFAULT_CRYPT_KEY = "PersMan";

	private final Cipher encrypter;
	private final Cipher decrypter;

	private static final byte[] SALT = {
		(byte)0x45, (byte)0x45, (byte)0x45, (byte)0x45,
		(byte)0x45, (byte)0x45, (byte)0x45, (byte)0x45  };

	private static final int ITERATION_COUNT = 19;


	public static SimpleCipher createDefaultCipher() {
		return new SimpleCipher(DEFAULT_CRYPT_KEY);
	}

	public SimpleCipher(String passPhrase) {
		try {
			if (passPhrase == null) {
				throw new IllegalArgumentException("please provide a non null passphrase");
			}
			// Create the key
			KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), SALT, ITERATION_COUNT);
			SecretKey key = SecretKeyFactory.getInstance(KEY_FAB).generateSecret(keySpec);
			encrypter = Cipher.getInstance(key.getAlgorithm());
			decrypter = Cipher.getInstance(key.getAlgorithm());

			// Prepare the parameter to the ciphers
			AlgorithmParameterSpec paramSpec = new PBEParameterSpec(SALT, ITERATION_COUNT);

			// Create the ciphers
			encrypter.init(Cipher.ENCRYPT_MODE, key, paramSpec);
			decrypter.init(Cipher.DECRYPT_MODE, key, paramSpec);
		} catch (InvalidAlgorithmParameterException | InvalidKeySpecException 
				| NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException ex) {
			throw new UnsupportedOperationException("Exception while initializing SimpleCipher", ex);
		} 
	}


	public String encrypt(String string) {
		try {
			if (string == null) {
				throw new IllegalArgumentException("can't encrypt null");
			}
			// Encode the string into bytes using utf-8
			byte[] utf8 = string.getBytes("UTF8");
			// Encrypt
			byte[] enc = encrypter.doFinal(utf8);
			// Encode bytes to base64 to get a string
			return new org.apache.commons.codec.binary.Base64().encodeToString(enc);
		} catch (BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException ex) {
			throw new UnsupportedOperationException("Exception while initializing SimpleCipher", ex);
		} 
	}

	public String decrypt(String string) {
		try {
			if (string == null) {
				throw new IllegalArgumentException("can't encrypt null");
			}
			// decode base64 to get bytes
			byte[] dec = new org.apache.commons.codec.binary.Base64().decode(string);
			// decrypt
			byte[] utf8 = decrypter.doFinal(dec);
			// Decode using utf-8
			return new String(utf8, "UTF8");
		} catch (BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException ex) {
			throw new UnsupportedOperationException("Exception while initializing SimpleCipher", ex);
		} 
	}

}
