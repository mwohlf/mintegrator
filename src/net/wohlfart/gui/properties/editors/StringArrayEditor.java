package net.wohlfart.gui.properties.editors;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.util.EventObject;
import java.util.StringTokenizer;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

@SuppressWarnings("serial")
public class StringArrayEditor extends AbstractCellEditor implements TableCellEditor {

    final JTextField field = new JTextField();

    public StringArrayEditor() {
    }

    @Override
	public Component getTableCellEditorComponent(
            JTable table, Object value,
            boolean isSelected, int row, int column) {

        if ((value != null) && value.getClass().isArray()) {
            String[] array = (String[]) value;
            StringBuffer buffer = new StringBuffer();
            for (int i = 0; i < array.length; i++) {
                buffer.append(array[i]);
                if (i < array.length - 1) {
                    buffer.append(", ");
                }
            }

            field.setText(buffer.toString());
        }
        return field;
    }

    //	Enables the editor only for double-clicks.
    @Override
    public boolean isCellEditable(EventObject evt) {
        if (evt instanceof MouseEvent) {
            return ((MouseEvent)evt).getClickCount() >= 2;
        }
        return true;
    }


    @Override
	public Object getCellEditorValue() {
        // we have an array here
        StringTokenizer tokenizer = new StringTokenizer(field.getText(), ",");
        Object[] values = new String[tokenizer.countTokens()];
        int i = 0;
        while (tokenizer.hasMoreElements()) {
            values[i] = tokenizer.nextToken().trim();
            i++;
        }
        return values;
    }
}
