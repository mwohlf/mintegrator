package net.wohlfart.action;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.Collection;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;

import net.wohlfart.ImageManager;
import net.wohlfart.Messages;
import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.data.ICandidateTO;
import net.wohlfart.gui.table.ApplicationCollectionFocused;
import net.wohlfart.worker.ApplicationUpdateWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author michael
 *
 */
@SuppressWarnings("serial")
@Singleton
public class ApplicationUpdateAction extends AbstractAction {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(ApplicationUpdateAction.class);
    
    @Inject
    private Instance<ApplicationUpdateWorker> updateCandidatesWorker;

	private boolean isDataSourceConnected;
	private Collection<ICandidateTO> focusedApplications;

	
    public ApplicationUpdateAction() {
        putValue(Action.NAME, Messages.getString("UpdateCandidate.ActionName")); //$NON-NLS-1$
        putValue(Action.SMALL_ICON, ImageManager.getImageIcon("SaveApplicationIcon16")); //$NON-NLS-1$
        putValue(Action.LARGE_ICON_KEY, ImageManager.getImageIcon("SaveApplicationIcon24")); //$NON-NLS-1$
        putValue(Action.SHORT_DESCRIPTION, Messages.getString("UpdateCandidate.ShortDescription")); //$NON-NLS-1$
        putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
    }

    @Override
	public void actionPerformed(ActionEvent evt) {
		// this creates a new instance
        updateCandidatesWorker.get() .doExecute(focusedApplications);
    }
    
    private void doEnableCheck() {
    	int count = focusedApplications==null?0:focusedApplications.size();
    	setEnabled(isDataSourceConnected && (count >= 1));
    	// fix the name
    	putValue(Action.NAME, Messages.getString("UpdateCandidate.ActionNameChoice",  //$NON-NLS-1$
    			new Object[] {Integer.valueOf(count)}));
    }
    	
	public void onFocusedCandidateCollectionEvent(@Observes final ApplicationCollectionFocused container) {
		LOG.debug("onFocusedCandidateCollectionEvent called, container is: '{}' ",  container); //$NON-NLS-1$
		focusedApplications = container.get();
		doEnableCheck();
	}

    public void onAnyDataSourceEvent(@Observes final IBasicDataSource dataSource) {
        LOG.debug("onAnyDataSourceEvent called, checking if the datasource is closed: '{}' " //$NON-NLS-1$
        		+ "(from dataSource.isClosed())", dataSource.isClosed()); //$NON-NLS-1$
        isDataSourceConnected = !dataSource.isClosed();
        doEnableCheck();
    }

}
