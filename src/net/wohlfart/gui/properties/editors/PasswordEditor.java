package net.wohlfart.gui.properties.editors;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

@SuppressWarnings("serial")
public class PasswordEditor extends AbstractCellEditor implements TableCellEditor {

	final JPasswordField field = new JPasswordField();

	public PasswordEditor() {
	}

	@Override
	public Component getTableCellEditorComponent(
			JTable table, Object value,
			boolean isSelected, int row, int column) {

			if (value != null) {
				field.setText(value.toString());
			}
			return field;
	}

	//	Enables the editor only for double-clicks.
	@Override
	public boolean isCellEditable(EventObject evt) {
		if (evt instanceof MouseEvent) {
			return ((MouseEvent)evt).getClickCount() >= 2;
		}
		return true;
	}


	@Override
	public Object getCellEditorValue() {
		return field.getPassword();
	}
}
