package net.wohlfart.data.persis;

import java.util.Date;

import net.wohlfart.data.DatabaseColumn;
import net.wohlfart.data.ICandidateTO;

public class CandidateTO implements ICandidateTO {
	
    private String streetAndNumber;
    private String country;
    private String city;
    private String zipCode;
    private String remark;
    private String applicationTitle;
    private Date applicationDate;
    private String emailAddress;
    private Date dateOfBirth;
    private String gender;
    private String lastName;
    private String jobOfferDescription;
    private Integer id;
    private String phoneNumber;
    private String firstName;
    private String division;
    private String medium;

    //private boolean isSynced = false;

    @Override
    @DatabaseColumn(table="INET_BWR", column="BWG_VOM")
    public Date getApplicationDate() {
        return applicationDate;
    }
    @Override
    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    
    @Override
    @DatabaseColumn(table="INET_BWR", column="BWG_ALS")
    public String getApplicationTitle() {
        return applicationTitle;
    }
    @Override
    public void setApplicationTitle(String applicationTitle) {
        this.applicationTitle = applicationTitle;
    }


    @Override
    @DatabaseColumn(table="INET_BWR", column="ANSCHORT")
    public String getCity() {
        return city;
    }
    @Override
    public void setCity(String city) {
        this.city = city;
    }


    @Override
    @DatabaseColumn(table="INET_BWR", column="ANSCHLAND")
    public String getCountry() {
        return country;
    }
    @Override
    public void setCountry(String country) {
        this.country = country;
    }

    
    @Override
    @DatabaseColumn(table="INET_BWR", column="GEBTAG")
    public Date getDateOfBirth() {
        return dateOfBirth;
    }
    @Override
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    
    @Override
    @DatabaseColumn(table="INET_BWR", column="WERK1")
    public String getDivision() {
        return division;
    }
    @Override
    public void setDivision(String division) {
        this.division = division;
    }

    
    @Override
    @DatabaseColumn(table="INET_BWR", column="WERK1")
    public String getEmailAddress() {
        return emailAddress;
    }    
    @Override
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    
    @Override
    @DatabaseColumn(table="INET_BWR", column="VNAME")
    public String getFirstName() {
        return firstName;
    }
    @Override
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    
    @Override
    @DatabaseColumn(table="INET_BWR", column="GESCHL")
    public String getGender() {
        return gender;
    }
    @Override
    public void setGender(String gender) {
        this.gender = gender;
    }

    
    @Override
	@DatabaseColumn(table="INET_BWR", column="REFNR")
    public Integer getId() {
        return id;
    }
    @Override
	public void setId(Integer id) {
        this.id = id;
    }

    
    @Override
    @DatabaseColumn(table="INET_BWR", column="PRJ_KBEZ")
    public String getJobOfferDescription() {
        return jobOfferDescription;
    }
    @Override
    public void setJobOfferDescription(String jobOfferDescription) {
        this.jobOfferDescription = jobOfferDescription;
    }

    
    @Override
    @DatabaseColumn(table="INET_BWR", column="NAME")
    public String getLastName() {
        return lastName;
    }
    @Override
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    
    @Override
    @DatabaseColumn(table="INET_BWR", column="AUFMERKSAM1")
    public String getMedium() {
        return medium;
    }
    @Override
    public void setMedium(String medium) {
        this.medium = medium;
    }

    
    @Override
    @DatabaseColumn(table="INET_BWR", column="TELEFON1")
    public String getPhoneNumber() {
        return phoneNumber;
    }
    @Override
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    
    @Override
    @DatabaseColumn(table="INET_BWR", column="BEM")
    public String getRemark() {
        return remark;
    }
    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    
    @Override
    @DatabaseColumn(table="INET_BWR", column="ANSCH")
    public String getStreetAndNumber() {
        return streetAndNumber;
    }
    @Override
    public void setStreetAndNumber(String streetAndNumber) {
        this.streetAndNumber = streetAndNumber;
    }

    
    @Override
    @DatabaseColumn(table="INET_BWR", column="ANSCHPLZ")
    public String getZipCode() {
        return zipCode;
    }
    @Override
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

}
