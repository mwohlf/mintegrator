/**
 *
 */
package net.wohlfart.mail.outlook;

import java.util.ArrayList;

import net.wohlfart.mail.IMailFolderTreeNode;
import net.wohlfart.mail.IMailMessage;

import com.moyosoft.connector.com.Dispatch;
import com.moyosoft.connector.ms.outlook.folder.FoldersCollection;
import com.moyosoft.connector.ms.outlook.folder.FoldersIterator;
import com.moyosoft.connector.ms.outlook.folder.OutlookFolder;
import com.moyosoft.connector.ms.outlook.item.ItemsCollection;
import com.moyosoft.connector.ms.outlook.item.ItemsIterator;
import com.moyosoft.connector.ms.outlook.item.OutlookItem;
import com.moyosoft.connector.ms.outlook.mail.OutlookMail;
import com.moyosoft.connector.ms.outlook.util.ObjectClass;

/**
 * custom data model for an outlook folder
 * 
 * @author michael
 */
public class OutlookMailSource implements IMailFolderTreeNode {

	private final OutlookFolder outlookNativeFolder;

	private ArrayList<IMailFolderTreeNode> folderArray;
	private ArrayList<IMailMessage> mailArray;

	public OutlookMailSource(final OutlookFolder folder) {
		outlookNativeFolder = folder;
	}


	public String getOutlookEntryID() {
		return outlookNativeFolder.getFolderId().getEntryId();
	}

	public String getOutlookStoreID() {
		return outlookNativeFolder.getFolderId().getStoreId();
	}

	public String getNodeName() {
		return outlookNativeFolder.getName();
	}




	@Override
	public String getName() {
		return outlookNativeFolder.getName();
	}

	public boolean isSelectable() {
		return true;
	}

	@Override
	public ArrayList<IMailMessage> getMailMessages() {
		if (mailArray == null) {
			setMailArray();
		}
		return mailArray;
	}

	@Override
	public ArrayList<IMailFolderTreeNode> getSubFolder() {
		return subFolder();
	}


	private void setMailArray() {
		ItemsCollection items = outlookNativeFolder.getItems();
		mailArray = new ArrayList<>();

		for(ItemsIterator it = items.iterator(); it.hasNext();) {
			Object object = it.next();
			// Check the item is a folder
			if ( (object != null) && (object instanceof OutlookItem)) {
				OutlookItem item = (OutlookItem)object;
				if (item.getType().isMail() ) {
					OutlookMail mail = (OutlookMail)item;
					mailArray.add(new OutlookMailData(mail));
				}
			}
		}
	}


	private ArrayList<IMailFolderTreeNode> subFolder() {
		if (folderArray == null) {
			setFolderArray();
		}
		return folderArray;
	}


	private void setFolderArray() {
		FoldersCollection foldersCollection = outlookNativeFolder.getFolders();
		folderArray = new ArrayList<>();

		for(FoldersIterator it = foldersCollection.iterator(); it.hasNext();) {
			Object object = it.next();
			// Check the item is a folder
			if ( (object != null) && (object instanceof OutlookFolder)) {
				OutlookFolder ofolder = (OutlookFolder)object;
				if (ofolder.getDefaultItemType().isMail()) {
					folderArray.add(new OutlookMailSource(ofolder));
				}
			}
		}
	}


	@Override
	public int hashCode() {
		int hashCode = 0;
		hashCode += getOutlookEntryID().hashCode();
		hashCode += getOutlookStoreID().hashCode();
		hashCode += getNodeName().hashCode();
		return hashCode;
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof OutlookMailSource) {
			OutlookMailSource that = (OutlookMailSource)object;
			return getOutlookEntryID().equals(that.getOutlookEntryID())
					&& getOutlookStoreID().equals(that.getOutlookStoreID())
					&& getNodeName().equals(that.getNodeName());
		}
		return false;
	}



	@Override
	public OutlookMailSource getParentFolder() {
		Dispatch parentFolderDispatch = outlookNativeFolder.getDispatch().invokeGetter("Parent").getDispatch();

		if (parentFolderDispatch == null) {
			return null;
		}

		int objClassValue = parentFolderDispatch.invokeGetter("Class").getInt();

		if(objClassValue == ObjectClass.FOLDER.getTypeValue())   {
			return new OutlookMailSource(new OutlookFolder(parentFolderDispatch));
		}
		return null;
	}

}

