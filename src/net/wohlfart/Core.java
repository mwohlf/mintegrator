package net.wohlfart;

import java.awt.EventQueue;
import java.awt.Frame;
import java.util.Date;
import java.util.concurrent.CancellationException;

import javax.annotation.PostConstruct;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.AnnotatedType;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.InjectionTarget;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.UIManager;

import net.wohlfart.data.ConnectDatabaseWorker;
import net.wohlfart.data.DisconnectDatabaseWorker;
import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.gui.BaseFrame;
import net.wohlfart.gui.ErrorPane;
import net.wohlfart.gui.event.Shutdown;
import net.wohlfart.gui.properties.SavePropertiesAction;
import net.wohlfart.mail.outlook.MoyosoftOutlookHandler;
import net.wohlfart.properties.ApplicationPropertiesImpl;
import net.wohlfart.properties.PropertyKeys;
import net.wohlfart.splash.ISplashScreenHandle;

import org.jboss.weld.environment.se.WeldContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * A weld component picked up in the Start.main() method, central entrance point for this application
 * 
 * @author michael
 */
@Singleton
public class Core implements Runnable {
	/** Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(Core.class);

	// sync for the shutdown trigger
	private final Object sync = new Object();

	///// the following variables are accessed by multiple threads and need to be volatile:
	//
	// flag to shutdown
	private volatile boolean shutdown = false;

	// need to shutdown on exit
	// private volatile Weld weld;
	// can not inject this variable since it has to be created in the EDT:
	private volatile BaseFrame baseFrame;

	// ---  CDI injected variables:
	
	@Inject
	private ISplashScreenHandle splashScreenHandle;
	
	@Inject
	private SavePropertiesAction savePropertiesAction;
	
	@Inject
	private Instance<ConnectDatabaseWorker> connectDatabaseWorker;
	
	@Inject
	private Instance<DisconnectDatabaseWorker> disconnectWorker;
	
	@Inject
	private Event<IBasicDataSource> dataSourceEvent;
	
	@Inject 
	private IBasicDataSource basicDataSource;



	@PostConstruct
	protected void postConstruct() { // NO_UCD
		LOG.debug("postConstruct() called"); //$NON-NLS-1$
	}




	/**
	 * this method should be executed while the splashscreen is shown,
	 * it performs some basic injections and fires up a EDT Thread to show 
	 * the main window
	 */
	void launch(final WeldContainer weldContainer) {

		if (LOG.isDebugEnabled()) {
			LOG.debug("custom repaint installed"); //$NON-NLS-1$
			CheckThreadViolationRepaintManager.install();
		}

		try {
			// some changes to the look and feel
			// changing the title of the progress monitor
			UIManager.put("ProgressMonitor.progressText", Messages.getString("ProgressMonitor.progressText")); //$NON-NLS-1$ //$NON-NLS-2$

			/* ---- init splashscreen ---- */
			splashScreenHandle.setTotalSteps(7);
			splashScreenHandle.setStatus(Messages.getString("Engine.initSplashScreen"), 1); //$NON-NLS-1$
			// install a default ExceptionHandler
			CustomExceptionHandler.install();

			/* ---- load properties ---- */
			splashScreenHandle.setStatus(Messages.getString("Engine.loadingProperties"), 2); //$NON-NLS-1$
			ApplicationPropertiesImpl.getInstance(); 


			/* ---- load images ---- */
			splashScreenHandle.setStatus(Messages.getString("Engine.loadingImages"), 3); //$NON-NLS-1$
			ImageManager.preloadImages();

			/* ---- init help broker ---- */
			splashScreenHandle.setStatus(Messages.getString("Engine.initHelpBroker"), 4); //$NON-NLS-1$
			HelpBrokerAdapter helpAdapter = HelpBrokerAdapter.getInstance();
			LOG.info("initialize help adapter"); //$NON-NLS-1$
			helpAdapter.initialize();
			LOG.info("returned from help adapter"); //$NON-NLS-1$

			splashScreenHandle.setStatus(Messages.getString("Engine.setupSwingXML"), 5); //$NON-NLS-1$
			// the creation of the main frame has to happen in the EDT
			EventQueue.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					// standard way to create an object:
					// baseFrame = new BaseFrame();
					// weld way to create an object:
					LOG.debug("getting bean manager"); //$NON-NLS-1$
					BeanManager beanManager = weldContainer.getBeanManager();
					LOG.debug("creating annotation type"); //$NON-NLS-1$
					AnnotatedType<BaseFrame> type = beanManager.createAnnotatedType(BaseFrame.class);
					LOG.debug("creating injection target"); //$NON-NLS-1$
					InjectionTarget<BaseFrame> injectionTarget = beanManager.createInjectionTarget(type);
					LOG.debug("creating creational context"); //$NON-NLS-1$
					CreationalContext<BaseFrame> creationalContext = beanManager.createCreationalContext(null);
					LOG.debug("producing base frame"); //$NON-NLS-1$
					baseFrame = injectionTarget.produce(creationalContext);
					LOG.debug("injecting base frame"); //$NON-NLS-1$
					injectionTarget.inject(baseFrame, creationalContext);
					LOG.debug("calling post construct"); //$NON-NLS-1$
					injectionTarget.postConstruct(baseFrame);
				}
			});


			splashScreenHandle.setStatus(Messages.getString("Engine.startMainThread"), 7); //$NON-NLS-1$

			// start a new thread doing this applications work
			// this thread blocks in the run method until shutdown is called
			new Thread(this).start(); // calls the run method...

		} catch (Exception ex) {
			ex.printStackTrace();
			ErrorPane.showErrorDialog(ex, "Error during application launch"); //$NON-NLS-1$
		}

	}

	public void onShutdownEvent(@Observes final Shutdown shutdownEvent) { // NO_UCD
		LOG.info("shutdown event caught {}", shutdownEvent); //$NON-NLS-1$
		shutdown();
	}


	/** this kills flags the main thread to shut down */
	private void shutdown() {
		LOG.debug("shutdown invoked"); //$NON-NLS-1$
		synchronized (sync) { // get lock on the sync object
			LOG.debug("inside shutdown sync"); //$NON-NLS-1$
			shutdown = true;
			sync.notify(); // wake anyone up who is waiting on sync
		}
		LOG.info("shutdown initialized"); //$NON-NLS-1$
	}

	// invoked by the start() method
	// this waits for a notify and does all the cleanup, this is not the EDT and might block
	// to wait for other components to cleanup and finish their work
	@Override
	public void run() {
		LOG.debug("run invoked"); //$NON-NLS-1$

		try {
			//final ApplicationContext context = ApplicationContext.getInstance();

			// show the frame in a swing thread, invoke later should work too but adds
			// a bit of complexity since we have more concurrency...
			EventQueue.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					assert baseFrame != null : "baseframe is null at startup"; //$NON-NLS-1$

					// TODO:
					// load the preferences or center on screen and pack, if there
					// are no preferences
					//GuiUtilities.loadWindowProperties(BASE_FRAME, baseFrame);
					// add a component listener to write back the position
					// and size changes
					//GuiUtilities.addComponentListener(BASE_FRAME, baseFrame);

					baseFrame.setVisible(true);
					baseFrame.toFront();
					dataSourceEvent.fire(basicDataSource);

					// do we need to connect right away ?
					ApplicationPropertiesImpl properties = ApplicationPropertiesImpl.getInstance();

					boolean autoConnect = properties.getBoolean(PropertyKeys.AUTO_CONNECT, Boolean.FALSE).booleanValue();
					if (autoConnect) {
						connectDatabaseWorker.get() .doExecute();
					}
				}
			});

			synchronized (sync) {
				LOG.debug("inside sync"); //$NON-NLS-1$
				while (!shutdown) {
					LOG.debug("inside shutdown poll"); //$NON-NLS-1$
					// the monitors are released and any other thread has access to this object
					sync.wait(0); // waits forever, or till we get a notify
					LOG.info("after notify"); //$NON-NLS-1$
				}
			}
			// we were send a notify if we are here
			LOG.debug("run method finished, about to leave the run method"); //$NON-NLS-1$

			LOG.debug("calling savePropertiesAction"); //$NON-NLS-1$
			savePropertiesAction.saveProperties();


			final DisconnectDatabaseWorker disconnectDatabaseWorker = disconnectWorker.get();
			// disconnecting the database
			EventQueue.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					disconnectDatabaseWorker.doExecute(); // needs EDT for progress bar etc.
				}
			});

			// XXX: maybe we need a worker for this, should be the same semantics as the database disconnection
			// FIXME: get the mailSource brand here might be something else than outlook
			MoyosoftOutlookHandler.disposeOutlook();
			
			try {
				// this blocks until the worker finished or is canceled
				if (!disconnectDatabaseWorker.isCancelled()) {
					LOG.info("waiting for database disconnect (disconnectDatabaseWorker) at: {}",  //$NON-NLS-1$
							new Date() );
					disconnectDatabaseWorker.get();
					LOG.info("finished waiting for database disconnect (disconnectDatabaseWorker) at: {}",  //$NON-NLS-1$
							new Date() );
				} else {
					LOG.info("Db was already disconnected or disconnect was canceled, continue shutdown.."); //$NON-NLS-1$
				}
			} catch (CancellationException ignore) {
				LOG.error("CancellationException", ignore); //$NON-NLS-1$
			};


			// finally close the main frame
			baseFrame.setVisible(false);

			LOG.info("shutdown finished -- clean"); //$NON-NLS-1$
		} catch (InterruptedException ex) {
			LOG.error("wait interrupted during shutdown, bailing out", ex); //$NON-NLS-1$
		} catch (Throwable ex) {
			LOG.error("unspecified problem during shutdown, bailing out", ex); //$NON-NLS-1$
		} finally {
			// disposing all active frames, this should end the application
			Frame[] activeFrames = Frame.getFrames();
			for (Frame frame : activeFrames) {
				frame.setVisible(false);
				frame.dispose();
			}
		}
	}

}
