/**
 *
 */
package net.wohlfart.mail.outlook;

import java.awt.Component;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import net.wohlfart.mail.IMailFolderTreeNode;


/**
 * @author michael
 *
 */
@SuppressWarnings("serial")
public class OutlookTreeNodeRenderer extends DefaultTreeCellRenderer {

    @Override
    public Component getTreeCellRendererComponent(
            JTree tree,
            Object value,
            boolean sel,
            boolean expanded,
            boolean leaf,
            int row,
            boolean focus)  {

    	String string;
        if (value instanceof IMailFolderTreeNode) {
            string = ((IMailFolderTreeNode)value).getName();
        } else {
        	string = value==null?"":value.toString(); //$NON-NLS-1$
        }

        super.getTreeCellRendererComponent(
                tree,
                string,
                sel,
                expanded,
                leaf,
                row,
                focus);

        return this;
    }

}
