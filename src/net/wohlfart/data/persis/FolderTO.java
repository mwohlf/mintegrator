package net.wohlfart.data.persis;

import net.wohlfart.data.IFolderTO;

public class FolderTO implements IFolderTO {
	
    private String attachment;
    private String title;
    private Integer candidateId;
    private Integer docId;
    private Integer documentId;
    private String group;
    private Integer id;
    private Integer sort1;
    private Integer sort2;

    @Override
    public String getAttachment() {
        return attachment;
    }
    
    @Override
    public void setAttachment(final String attachment) {
        this.attachment = attachment;
    }


    @Override
    public Integer getCandidateId() {
        return candidateId;
    }
    
    @Override
    public void setCandidateId(final Integer candidateId) {
        this.candidateId = candidateId;
    }
 

    @Override
    public Integer getDocId() {
        return docId;
    }
    
    @Override
    public void setDocId(final Integer docId) {
        this.docId = docId;
    }

    
    
    @Override
    public Integer getDocumentId() {
        return documentId;
    }
    @Override
    public void setDocumentId(final Integer documentId) {
        this.documentId = documentId;
    }

    
    @Override
    public String getGroup() {
        return group;
    }
    @Override
    public void setGroup(final String group) {
        this.group = group;
    }

    @Override
    public Integer getId() {
        return id;
    }
    @Override
    public void setId(final Integer id) {
        this.id = id;
    }

    @Override
    public Integer getSort1() {
        return sort1;
    }
    @Override
    public void setSort1(final Integer sort1) {
        this.sort1 = sort1;
    }

    @Override
    public Integer getSort2() {
        return sort2;
    }
    @Override
    public void setSort2(final Integer sort2) {
        this.sort2 = sort2;
    }

    @Override
    public String getTitle() {
        return title;
    }
    @Override
    public void setTitle(String title) {
        this.title = title;
    }

}