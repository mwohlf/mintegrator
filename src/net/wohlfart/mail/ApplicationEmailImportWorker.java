package net.wohlfart.mail;

import static net.wohlfart.Constants.DELAY_WORKERS;
import static net.wohlfart.util.GuiUtilities.getFrame;

import java.awt.EventQueue;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;

import net.wohlfart.CustomSwingWorker;
import net.wohlfart.Messages;
import net.wohlfart.data.ICandidateTO;
import net.wohlfart.data.IDocumentFileTO;
import net.wohlfart.data.persis.CandidateTO;
import net.wohlfart.data.persis.DocumentDAO;
import net.wohlfart.data.persis.FolderDAO;
import net.wohlfart.gui.ErrorPane;
import net.wohlfart.gui.event.ApplicationCreated;
import net.wohlfart.gui.properties.mail.IMailBoxFlavor;
import net.wohlfart.gui.properties.mail.MailBoxFlavourCollection;
import net.wohlfart.mail.selection.MailSelectionDialog;
import net.wohlfart.properties.ApplicationPropertiesImpl;
import net.wohlfart.properties.IApplicationProperties;
import net.wohlfart.properties.PropertyKeys;
import net.wohlfart.util.GuiUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author michael
 */
public class ApplicationEmailImportWorker extends CustomSwingWorker<ArrayList<ICandidateTO>, ICandidateTO> {
	/** Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(ApplicationEmailImportWorker.class);

	private static final String NEWLINE = System.getProperty("line.separator"); //$NON-NLS-1$

	private static final String TEMPFILE_PREFIX = "PersMan"; //$NON-NLS-1$
	private static final String TEMPFILE_POSTFIX = "Email"; //$NON-NLS-1$

	private static final String DEFAULT_EMAIL_FILENAME = "Email.txt"; //$NON-NLS-1$

	@Inject
	private ApplicationFactory candidateFactory;
	@Inject
	private IApplicationProperties properties;

	@Inject
	private FolderDAO folderDAO;
	@Inject
	private DocumentDAO documentDAO;

	@Inject 
	private Event<ApplicationCreated> addedCandidateEvent;
	//@Inject 
	//private Event<ApplicationFocused> applicationFocusEvent; // FIXME: select the imported 
	//@Inject 
	//private Event<ApplicationCollectionFocused> candidateCollectionEvent;  // FIXME: select the imported 


	/**
	 * show a ProgressMonitor
	 */
	@Override
	public void doExecute() {
		// this must run in EDT
		if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The method doExecute() must be called from EDT."); //$NON-NLS-1$
		}	
		super.doExecute();
	}

	@Override
	protected ArrayList<ICandidateTO> doInBackground() {
		LOG.debug("doInBackground() - start"); //$NON-NLS-1$

		try {

			// make sure we have a valid mailfolder
			IMailFolderTreeNode mailFolder = getValidatedMailFolder();

			// if the folder is null the user canceled or the source is invalid or both
			if (mailFolder == null) {
				LOG.info("doInBackground() - null folder"); //$NON-NLS-1$
				JOptionPane.showMessageDialog(
						getFrame(),
						Messages.getString("EmailFromDialogWorker.FolderIsNull")); //$NON-NLS-1$
				return null; // bail out
			}

			// check if there are some mail in the folder for import
			if (mailFolder.getMailMessages().size() <= 0) {
				LOG.info("doInBackground() - no emails"); //$NON-NLS-1$
				JOptionPane.showMessageDialog(
						getFrame(),
						Messages.getString("EmailFromDialogWorker.NoMailsFound",  //$NON-NLS-1$
								new Object[] {mailFolder.getName()})); 
				return null;
			}


			// folder is not null and there are some mail in the folder
			// so prompt the user for some selections
			ArrayList<IMailMessage> selected = MailSelectionDialog.selectMailsFromFolder(mailFolder);

			// check if the user selected something, otherwise we can skip the import
			if (selected.size() <= 0) {
				LOG.info("no mails selected"); //$NON-NLS-1$
				return null;
			}

			// we may need to query the user if we should really import the mails
			if (!showImportQuery(selected)) {
				LOG.info("import canceled"); //$NON-NLS-1$
				return null;
			}

			// everything looks good here, lets do some work and import the selected emails
			// the progress monitor is set up in this method
			return importMailArray(selected);

		} catch (Throwable ex) {
			LOG.error("doInBackground()", ex); //$NON-NLS-1$
			ErrorPane.showErrorDialog(ex, Messages.getString("EmailFromDialogWorker.Throwable")); //$NON-NLS-1$
		} finally {
			queue.clearBlockedComponents();
			GuiUtilities.setDefaultCursor();
			LOG.debug("doInBackground() - finally"); //$NON-NLS-1$
		}
		return null;
	}


	/**
	 * get the if it is configured, validate it if not prompt the user,
	 * return null if nothing valid can be found...
	 * 
	 * @return
	 */
	private IMailFolderTreeNode getValidatedMailFolder() {

		String flavorId = properties.getString(PropertyKeys.MAIL_SOURCE_FLAVOR, null);
		if (flavorId == null) {
			LOG.info("no mailsource flavorId found, check config");
			return null;
		}
		IMailBoxFlavor mailboxFlavor = MailBoxFlavourCollection.getInstance().get(flavorId);
		if (mailboxFlavor == null) {
			LOG.info("mailboxFlavor is null, check config, flavorId was {}", flavorId);
			return null;
		}
		IMailFolderTreeNode mailSource = mailboxFlavor.getMailSource(properties);

		if (!isValid(mailSource)) {
			LOG.info("invalid mailsource, check config, flavorId was {}, mailSource is {}", flavorId, mailSource);
			return null;
		}
		return mailSource;
	}

	private boolean isValid(final IMailFolderTreeNode mailFolderTreeNode) {
		if (mailFolderTreeNode == null) {
			return false;
		}
		// TODO: check the folder here
		return true;
	}

	/**
	 * bulk import for emails
	 *
	 * @param selected
	 * @return
	 */
	private ArrayList<ICandidateTO> importMailArray(final ArrayList<IMailMessage> selected) {

		ArrayList<ICandidateTO> imported = new ArrayList<>();
		// check if there is something to import at all
		if (selected == null || selected.size() == 0) {
			LOG.info("import list is empty, nothing to import");
			return imported;
		}  	
		LOG.info("starting email import");

		int totalCount = selected.size();

		setNote(Messages.getString("EmailFromDialogWorker.Note1", new Object[] {Integer.valueOf(totalCount)}));
		setProgress(5);

		boolean markEmails = properties.getBoolean(PropertyKeys.MARK_IMPORTED_EMAILS, false);
		boolean attachEmail = properties.getBoolean(PropertyKeys.SAVE_EMAIL_AS_FILE, true);


		try {
			int i = 0;
			for (IMailMessage mail : selected) {

				// candidate properties
				progressMonitor.setProgress(10 * i + 2);
				if (DELAY_WORKERS) { GuiUtilities.sleep(200); }
				// this stores the candidate in the DB
				ICandidateTO candidate = candidateFactory.createCandidate(mail);
				publish(candidate); // publish partial result

				imported.add(candidate);  // add to final result

				// check if we are interrupted
				if (progressMonitor.isCanceled()) {
					return imported;
				}

				progressMonitor.setNote(Messages.getString(
						"EmailFromDialogWorker.Note2",  //$NON-NLS-1$
						new Object[] {
								Integer.valueOf(i+1),  // counting starts at 0
								Integer.valueOf(totalCount),
								mail.getSenderAddress()}));

				// safe email as attachment ?
				if (attachEmail) {

					String subject = mail.getSubject();
					String remark = mail.getBody();
					String mailString = Messages.getString(
							"EmailFromDialogWorker.EmailFormat",  //$NON-NLS-1$
							new Object[] {NEWLINE, subject, remark});
					// check if we are interrupted
					if (progressMonitor.isCanceled()) {
						return imported;
					}

					try {
						// create a file to attach
						File file = File.createTempFile(TEMPFILE_PREFIX, TEMPFILE_POSTFIX);
						FileWriter fileWriter = new FileWriter(file);
						fileWriter.write(mailString);
						fileWriter.flush();
						fileWriter.close();
						IDocumentFileTO documentFile = documentDAO.createDocumentFile(file, DEFAULT_EMAIL_FILENAME);
						folderDAO.attachDocument(candidate, documentFile);
					} catch (IOException ex) {
						LOG.error("error on document save, trying next document", ex);
					}

				}  // end save email as file

				// attachments
				ArrayList<IMailAttachment> attachments = mail.getAttachments();
				progressMonitor.setProgress(10 * i + 7);
				for (IMailAttachment attachment : attachments) {
					progressMonitor.setNote(Messages.getString(
							"EmailFromDialogWorker.Note3",   //$NON-NLS-1$
							new Object[] {attachment.getFilename()}));
					if (DELAY_WORKERS) { GuiUtilities.sleep(5); }
					IDocumentFileTO documentFile = documentDAO.createDocumentFile(attachment);
					folderDAO.attachDocument(candidate, documentFile);

					// check if we are interrupted
					if (progressMonitor.isCanceled()) {
						return imported;
					}
				}
				i++;
				// check if we are interrupted
				if (progressMonitor.isCanceled()) {
					return imported;
				}

				if (markEmails) {
					// finished importing an email
					mail.setImported(true); // mark this email as follow up, user can mark it as completetd later
				}
			} // loop the selected emails

			return imported;

		} catch (Exception ex) {
			progressMonitor.close();
			LOG.error("unexpected exception", ex); //$NON-NLS-1$
			JOptionPane.showMessageDialog(
					null,
					ex.getMessage(),
					Messages.getString("EmailFromDialogWorker.Exception"), //$NON-NLS-1$
					JOptionPane.ERROR_MESSAGE);
		} finally {
			progressMonitor.close();
		}

		return imported;

	}

	/**
	 * @return
	 */
	private boolean showImportQuery(ArrayList<IMailMessage> selected) {
		ApplicationPropertiesImpl properties = ApplicationPropertiesImpl.getInstance();
		boolean confirmSelection = properties.getBoolean(PropertyKeys.CONFIRM_SELECTION);

		if (confirmSelection == false) {
			return true; // no confirm, just do it
		}

		String info = Messages.getString("EmailFromDialogWorker.ImportSelectedMailInfo", new Object[]{Integer.valueOf(selected.size())}); //$NON-NLS-1$
		String query = Messages.getString("EmailFromDialogWorker.ImportSelectedMailQuery"); //$NON-NLS-1$

		JCheckBox confirm = new JCheckBox(Messages.getString("EmailFromDialogWorker.ConfirmImport")); //$NON-NLS-1$
		confirm.setSelected(confirmSelection);

		int reply = JOptionPane.showConfirmDialog(getFrame(),
				new Object[] { info, query, confirm },
				Messages.getString("EmailFromDialogWorker.DoImport"), //$NON-NLS-1$
				JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE);

		// remember the confirm reply:
		properties.setBoolean(PropertyKeys.CONFIRM_SELECTION, confirm.isSelected());

		if (reply == JOptionPane.NO_OPTION) {
			return false; // bail out
		}

		return true; // do it

	}

	
	
	@Override
	protected void process(List<ICandidateTO> batch) {		
		for (ICandidateTO newApplication: batch) {
			// fire an application added
			addedCandidateEvent.fire(new ApplicationCreated((CandidateTO)newApplication));
		}
	}

	/**
	 * this is called in the Swing thread after the doInBackground finished
	 *
	 * @see javax.swing.SwingWorker#done()
	 */
	@Override
	public void done() {
		super.done();
		try {
			ArrayList<ICandidateTO> newList = get();
			if (newList == null) {
				return;
			}
			// update the list of selected candidates
			// context.selectedCandidateListModel.setDecoratee(newList);
            
			
			// focus onthe new:
			//candidateCollectionEvent.fire(new ApplicationCollectionFocused(Arrays.asList(newApplication)));

		} catch (InterruptedException | ExecutionException ex) {
			LOG.error("done()", ex); //$NON-NLS-1$
		}
	}

}
