/**
 *
 */
package net.wohlfart.gui;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Window;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import net.wohlfart.Messages;
import net.wohlfart.util.GuiUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * simple class to display a throwable
 * 
 * @author michael
 */
public class ErrorPane {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(ErrorPane.class);

	/** Platform dependent newline.    */
	private static final String NEWLINE = System.getProperty("line.separator"); //$NON-NLS-1$
	
	/** char count after we cut off the error message 
	 * FIXME: this should depend on L&F and the size of the used font... 
	 */
	private static final int LINE_CUTOFF = 50;


	/**
	 * fire up a Error Dialog in the EDT...
	 * 
	 * @param throwable
	 * @param title
	 */
	public static final void showErrorDialog(final Throwable throwable, final String title) {
		EventQueue.invokeLater(new Runnable(){
			@Override
			public void run() {
				showErrorDialogEDT(throwable, title);
			}  		
		});	
	}


	/**
	 * this method blocks s long as the error dialog is shown...
	 * 
	 * @param throwable
	 * @param title
	 */
	private static final void showErrorDialogEDT(final Throwable throwable, final String title) {
		LOG.info("showing error dialog for throwable {}", new Object[] {throwable});

		//// create a nice header label with name of the throwable class and message as description
		// we want maximum 40 characters
		String message = throwable.getLocalizedMessage();
		message = (message == null)?"":message;
		int length = message.length();
		String exceptionClassname = throwable.getClass().getCanonicalName();
		int cutoff = Math.min(length, LINE_CUTOFF);

		JLabel shortDescription = new JLabel(
				Messages.getString((length > cutoff)?"ErrorPane.DescriptionDots":"ErrorPane.Description", 
				new Object[] {exceptionClassname, message.substring(0, cutoff)}));
		
		
		////  label over the textarea
		final JLabel detailsLabel = new JLabel(Messages.getString("ErrorPane.Details")); //$NON-NLS-1$
		detailsLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));


		//// textarea with the whole stacktrace
		@SuppressWarnings("serial")
		final JTextArea textArea = new JTextArea() {{
			setLineWrap(false);
			setEditable(false);
			append(throwable.toString());
			append(NEWLINE);
			append(NEWLINE);
			append(Messages.getString("ErrorPane.Stacktrace")); //$NON-NLS-1$
			append(NEWLINE);
		}};
		for (StackTraceElement element: throwable.getStackTrace()) {
			textArea.append(element.toString());
			textArea.append(NEWLINE);
		}
		final JScrollPane stacktraceArea = new JScrollPane(textArea);
		stacktraceArea.setPreferredSize(new Dimension(450, 270));
		
		// see: http://blogs.oracle.com/scblog/entry/tip_making_joptionpane_dialog_resizable
		// we want the dialog to be resizable
		stacktraceArea.addHierarchyListener(new HierarchyListener() {
            @Override
			public void hierarchyChanged(final HierarchyEvent evt) {
                Window window = SwingUtilities.getWindowAncestor(stacktraceArea);
                if (window instanceof Dialog) {
                    Dialog dialog = (Dialog)window;
                    if (!dialog.isResizable()) {
                        dialog.setResizable(true);
                    }
                }
            }
        });


		// this blocks unless the dialog is closed, which is not a problem if we are in the EDT...
		JOptionPane.showMessageDialog(
				GuiUtilities.getFrame(),
				new Object[] {
					shortDescription, 
					detailsLabel, 
					stacktraceArea},
				title,
				JOptionPane.ERROR_MESSAGE);
	}

}
