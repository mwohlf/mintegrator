package net.wohlfart.gui.properties.database;

import java.util.Map.Entry;

import javax.swing.DefaultComboBoxModel;

import net.wohlfart.gui.properties.database.flavour.DbFlavourCollection;
import net.wohlfart.gui.properties.database.flavour.IDatabaseFlavor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class DbFlavourBoxModel extends DefaultComboBoxModel<IDatabaseFlavor> {
	/**  Logger for this class */
	private static final Logger LOG = LoggerFactory.getLogger(DbFlavourBoxModel.class);

	DbFlavourBoxModel() {
		super();
		LOG.debug("creating model"); //$NON-NLS-1$
		final DbFlavourCollection collection = DbFlavourCollection.getInstance();
		for (Entry<String, IDatabaseFlavor> flavour : collection.entrySet()) {
			LOG.debug("  found: {}", flavour); //$NON-NLS-1$
			addElement(flavour.getValue());
		}
		setSelectedItem(collection.getNullElement());
	}

}
