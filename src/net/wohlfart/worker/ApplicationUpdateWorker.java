/**
 *
 */
package net.wohlfart.worker;


import java.awt.EventQueue;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.swing.JOptionPane;

import net.wohlfart.CustomSwingWorker;
import net.wohlfart.Messages;
import net.wohlfart.data.DataValidationResult;
import net.wohlfart.data.DataValidator;
import net.wohlfart.data.ICandidateTO;
import net.wohlfart.data.persis.CandidateDAO;
import net.wohlfart.gui.ErrorPane;
import net.wohlfart.gui.table.ApplicationDataTable;
import net.wohlfart.util.GuiUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * updates all candidates that are selected into the database
 * 
 * @author michael
 */
public class ApplicationUpdateWorker extends CustomSwingWorker<Void, ICandidateTO> {
	/** Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(ApplicationUpdateWorker.class);


	@Inject
	private CandidateDAO candidateDAO;
	@Inject
	private DataValidator dataValidator;
	@Inject
	private ApplicationDataTable applicationDataTable;

	// the selected applications we want to update in the DB
	private volatile Collection<ICandidateTO> selected;

	public void doExecute(final Collection<ICandidateTO> selected) {
		// this must run in EDT
		if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The method doExecute() must be called from EDT."); //$NON-NLS-1$
		}

		if (selected.size() == 0) {
			LOG.info("nothing to update"); //$NON-NLS-1$
			cancel(true); // (actually it throws an exception now)
			// done();
			return;				
		}

		this.selected = selected;

		super.doExecute();	
	}


	@Override
	protected Void doInBackground() /* throws Exception */{
		LOG.debug("doInBackground() - start"); //$NON-NLS-1$

		try {
			setProgress(10);
			if (isCanceled()) {
				return null;
			}

			int i = 0;
			int count = selected.size();
			for (ICandidateTO candidate : selected) {
				i++; // i > 0 and i <= count (needed as divider...)
				if (isCanceled()) {
					break;
				}
				// max progress is 100, we reserve 10 to 90 range of 80 for all candidates			
				setProgress(10 + (int) Math.round(((double)i / (double)count) * 80d)) ;
				setNote(Messages.getString(
						"UpdateCandidatesWorker.Note2",
						new Object[] {Integer.valueOf(count), Integer.valueOf(i)}));

				if (isCanceled()) {
					break;
				}

				// this is where the crash happens when we violate DB constraints
				// so we try to avoid this by using a validator
				DataValidationResult result = dataValidator.validate(candidate);
				if (!result.isValid()) {
					// TODO: show some error here
				} else {
					// this also removes the candidate from the change list
					candidateDAO.updateCandidate(candidate);	
					// need to tell the table to rerender the row with the updated 
					// application
					publish(candidate);
				}

				if (isCanceled()) {
					break;
				}
			}
			if (isCanceled()) {
				return null;
			}
			setProgress(95);

		} catch (SQLException ex) {
			LOG.error("doInBackground()", ex);
			progressMonitor.close();
			JOptionPane.showMessageDialog(
					null,
					ex.getMessage(),
					Messages.getString("UpdateCandidatesWorker.SQLException"), //$NON-NLS-1$
					JOptionPane.ERROR_MESSAGE);
		} catch (Throwable th) {
			LOG.error("doInBackground()", th);
			GuiUtilities.setDefaultCursor();
			progressMonitor.close();
			ErrorPane.showErrorDialog(th, Messages.getString("UpdateCandidatesWorker.Throwable")); //$NON-NLS-1$
		} finally {
			queue.clearBlockedComponents();
			GuiUtilities.setDefaultCursor();
			LOG.info("doInBackground() - finally");
		}

		return null;
	}

	// runs in the EDT
	@Override
	protected void process(List<ICandidateTO> chunks) {		
		for (ICandidateTO candidate : chunks) {
			applicationDataTable.repaintRow(candidate);
		}
	}

	@Override
	public void done() {
		super.done();
	}
}
