/**
 *
 */
package net.wohlfart.mail;

import java.util.ArrayList;
import java.util.Vector;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * a tree model for a mail/folder data provider
 * 
 * FIXME: cleanup this class
 * @author michael
 */
public class FolderTreeModel implements TreeModel {
    private final IMailFolderTreeNode rootFolder;

    private final Vector<TreeModelListener> treeModelListeners = new Vector<>();

    public FolderTreeModel(IMailFolderTreeNode rootFolder) {
        this.rootFolder = rootFolder;
    }


    @Override
	public Object getRoot() {
        return rootFolder;
    }


    @Override
	public Object getChild(Object folder, int index) {
        assert (folder instanceof IMailFolderTreeNode): "can't get children for nonfolder: " + folder;
        
        //System.out.println("getChild: " + folder + " " + index);
        IMailFolderTreeNode mailFolder = (IMailFolderTreeNode)folder;
        return mailFolder.getSubFolder().get(index);
    }


    @Override
	public int getChildCount(Object folder) {
        assert (folder instanceof IMailFolderTreeNode): "can't get children for nonfolder: " + folder;
        //System.out.println("getChildCount: " + folder);
        IMailFolderTreeNode mailFolder = (IMailFolderTreeNode)folder;
        return mailFolder.getSubFolder().size();
    }

    /**
     * @see javax.swing.tree.TreeModel#getIndexOfChild(java.lang.Object, java.lang.Object)
     */
    @Override
	public int getIndexOfChild(Object parent, Object child) {
        assert (parent instanceof IMailFolderTreeNode): "can't get children for nonfolder: " + parent;
        assert (child instanceof IMailFolderTreeNode): "child is not a folder: " + child;
        //System.out.println("getChild, parent: " + parent + " child: " + child);
        IMailFolderTreeNode mailFolder = (IMailFolderTreeNode)parent;
        ArrayList<IMailFolderTreeNode> folderArray = mailFolder.getSubFolder();
        return folderArray.indexOf(child);
    }


    @Override
	public boolean isLeaf(Object folder) {
        assert (folder instanceof IMailFolderTreeNode): "can't get children for nonfolder: " + folder;
        //System.out.println("isLeaf: " + folder);
        IMailFolderTreeNode mailFolder = (IMailFolderTreeNode)folder;
        return (mailFolder.getSubFolder().size() == 0);
    }



    /**
     * @see javax.swing.tree.TreeModel#valueForPathChanged(javax.swing.tree.TreePath, java.lang.Object)
     */
    @Override
	public void valueForPathChanged(TreePath path, Object newValue) {
        System.out.println("*** valueForPathChanged : " + path + " --> " + newValue);
    }






    @Override
	public void addTreeModelListener(final TreeModelListener listener) {
        treeModelListeners.addElement(listener);
    }

    // removes a listener
    @Override
	public void removeTreeModelListener(final TreeModelListener listener) {
        treeModelListeners.removeElement(listener);
    }
}
