package net.wohlfart.action;

import static net.wohlfart.util.GuiUtilities.getFrame;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;

import net.wohlfart.ImageManager;
import net.wohlfart.Messages;
import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.data.IDocumentTO;
import net.wohlfart.gui.event.DocumentFocused;
import net.wohlfart.worker.DocumentSaveWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author michael
 *
 */
@SuppressWarnings("serial")
@Singleton
public class DocumentSaveAction extends AbstractAction {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(DocumentSaveAction.class);

	@Inject
	private Instance<DocumentSaveWorker> documentSaveWorker;

	private boolean isDataSourceConnected;
	private IDocumentTO document;

    private final JFileChooser jFileChooser;

    public DocumentSaveAction() {
        putValue(Action.NAME, Messages.getString("SaveDocument.ActionName")); //$NON-NLS-1$
        putValue(Action.SMALL_ICON, ImageManager.getImageIcon("SaveDocumentIcon16")); //$NON-NLS-1$
        putValue(Action.LARGE_ICON_KEY, ImageManager.getImageIcon("SaveDocumentIcon24")); //$NON-NLS-1$
        putValue(Action.SHORT_DESCRIPTION, Messages.getString("SaveDocument.ShortDescription")); //$NON-NLS-1$
        
        jFileChooser = new JFileChooser();
        jFileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
        jFileChooser.setDialogTitle(Messages.getString("SaveDocument.DialogTitle")); //$NON-NLS-1$
 
        doEnableCheck();
    }

    @Override
	public void actionPerformed(final ActionEvent evt) {

        File file = new File(document.getName());
        jFileChooser.setSelectedFile(file);

        int chooserResult = jFileChooser.showSaveDialog(getFrame());

        if (chooserResult == JFileChooser.APPROVE_OPTION) {
            File saveFile = jFileChooser.getSelectedFile();
            documentSaveWorker.get() .doExecute(document, saveFile);
        }

    }

	private void doEnableCheck() {
		setEnabled(isDataSourceConnected && (document != null));  	
	}

	public void onFocusedDocumentEvent(@Observes final DocumentFocused container) { // NO_UCD
		LOG.info("onFocusedDocumentEvent called"); //$NON-NLS-1$
		document = (IDocumentTO) container.getContent();
		doEnableCheck();
	}


	public void onAnyDataSourceEvent(@Observes final IBasicDataSource dataSource) { // NO_UCD
		LOG.debug("onAnyDataSourceEvent called, checking if the datasource is closed: '{}' " //$NON-NLS-1$
				+ "(from dataSource.isClosed())", dataSource.isClosed()); //$NON-NLS-1$
		isDataSourceConnected = !dataSource.isClosed();
		doEnableCheck();
	}

}
