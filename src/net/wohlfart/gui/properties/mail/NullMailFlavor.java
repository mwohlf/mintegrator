package net.wohlfart.gui.properties.mail;

import java.awt.Color;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import net.wohlfart.mail.IMailFolderTreeNode;
import net.wohlfart.properties.IApplicationProperties;

public class NullMailFlavor implements IMailBoxFlavor {
	
	private final String id;
	// to display the error/success message when testing the connection
	private final JTextArea statusArea;


	// package private
	NullMailFlavor(final String id) {
		// only one instance per application otherwise we have to override equals/hash ...
		this.id = id;
		statusArea = createStatusArea();
	}
	
	@Override
	public void initEditor(IApplicationProperties properties) {
		// TODO Auto-generated method stub
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public JComponent getPropertyEditor() {
		return statusArea;
	}

	@Override
	public IMailFolderTreeNode getMailSource(final IApplicationProperties properties) {
		throw new UnsupportedOperationException("no valid mail flavor selected"); //$NON-NLS-1$
	}

	@Override
	public void resetEditor(final IApplicationProperties properties) {
		statusArea.setForeground(Color.BLACK);
		statusArea.setFont(new JLabel() .getFont());
		statusArea.setText("");
	}

	@Override
	public void testEditor(final IApplicationProperties properties) {
		statusArea.setForeground(Color.RED);
		statusArea.setFont(new JLabel() .getFont());
		statusArea.setText(" - please select -");
	}

	@Override
	public void applyEditor(final IApplicationProperties properties) {
		statusArea.setForeground(Color.RED);
		statusArea.setFont(new JLabel() .getFont());
		statusArea.setText(" - please select -");
		//throw new UnsupportedOperationException("no valid database flavor selected"); //$NON-NLS-1$
	}

	
	private JTextArea createStatusArea() {
		@SuppressWarnings("serial")
		JTextArea result = new JTextArea() {
			// use the label background
			JLabel label;
			{
				setEditable(false);
				setColumns(50);
				setRows(5);
				setPreferredSize(getMinimumSize());
			}			
			@Override
			public Color getBackground() {
				if (label == null) {
					label = new JLabel();
				}
				return label.getBackground();
			}
		};
		result.setLineWrap(true);
		return result;
	}

}
