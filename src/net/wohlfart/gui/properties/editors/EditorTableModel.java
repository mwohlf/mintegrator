package net.wohlfart.gui.properties.editors;

import java.awt.Component;
import java.beans.BeanInfo;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.swing.table.AbstractTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class EditorTableModel extends AbstractTableModel{
    /** Logger for this class  */
    private static final Logger LOG = LoggerFactory.getLogger(EditorTableModel.class);

    //ArrayList<PropertyDescriptor> propertyDescriptors = new ArrayList<PropertyDescriptor>();
    PropertyDescriptor[] moreDescriptors;

    protected Object bean;

    public EditorTableModel() {
    }

    public void setData(BeanInfo beanInfo, Object bean) {
        /*
        PropertyDescriptor[] moreDescriptors;

        moreDescriptors = beanInfo.getPropertyDescriptors();
        for (PropertyDescriptor desc:moreDescriptors) {
            propertyDescriptors.add(desc);
        }

        BeanInfo[] moreBeanInfo = beanInfo.getAdditionalBeanInfo();

        if (moreBeanInfo != null) {
            for  (BeanInfo info:moreBeanInfo) {
                moreDescriptors = info.getPropertyDescriptors();
                for (PropertyDescriptor desc:moreDescriptors) {
                    propertyDescriptors.add(desc);
                }
            }
        }
*/
        moreDescriptors = beanInfo.getPropertyDescriptors();
        this.bean = bean;
        fireTableDataChanged();
    }

    @Override
    public Class<?> getColumnClass(int column) {
    	LOG.debug("int column=" + column + " - start");
        Class<?> returnClass;
        switch (column) {
        case 0:
            returnClass = String.class;
            break;
        case 1:
            returnClass = Component.class;
            break;
        default:
            returnClass = String.class;
            break;
        }
        LOG.debug("int - end");
        return returnClass;
    }

    @Override
	public int getColumnCount() {
    	LOG.debug("start");
        int returnint = 2;
        LOG.debug("end");
        return returnint;
    }

    @Override
    public String getColumnName(int column) {
    	LOG.debug("int column=" + column + " - start");
        String returnString;
        switch (column) {
        case 0:
            returnString = "Einstellung";
            break;
        case 1:
            returnString = "Wert";
            break;
        default:
            returnString = "unknown";
        }
        LOG.debug("int - end");
        return returnString;
    }

    @Override
	public int getRowCount() {
    	LOG.debug("start");
        int returnint = 0;
        if (moreDescriptors != null) {
            returnint = moreDescriptors.length; //.size();
        }
        LOG.debug("end");
        return returnint;
    }

    @Override
	public Object getValueAt(int row, int column) {
    	LOG.debug("int row=" + row + ", int column=" + column + " - start");
        Object returnObject = null;
        try {
            switch (column) {
            case 0:
                returnObject = moreDescriptors[row].getName();
                if (moreDescriptors[row].getPropertyType().isArray()) {
                    returnObject = moreDescriptors[row].getDisplayName();
                } else {
                    //returnObject = propertyDescriptors[row].getName();
                    returnObject = moreDescriptors[row].getDisplayName();
                }
                break;
            case 1:
                Method propertyGetter = moreDescriptors[row].getReadMethod();
                returnObject = propertyGetter.invoke(bean, (Object[])null);
                break;
            default:
                returnObject = "unknown";
            }
        } catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException ex) {
        	LOG.error("exception", ex); //$NON-NLS-1$
        }
        LOG.debug("int, int - end");
        return returnObject;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
    	LOG.debug("int row=" + row + ", int column=" + column + " - start");

    	LOG.debug("int, int - end");
        return (column == 1);
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
    	LOG.debug("Object arg0=" + value + ", int row=" + row
                + ", int column=" + column + " - start");
        assert (column == 1);

        try {
            Method propertySetter = moreDescriptors[row].getWriteMethod();
            propertySetter.invoke(bean, new Object[] {value});
        } catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException ex) {
        	LOG.error("exception", ex); //$NON-NLS-1$
        }
        LOG.debug("Object, int, int - end");
    }


}
