package net.wohlfart.gui.properties.look.color;

import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.metal.DefaultMetalTheme;
import javax.swing.plaf.metal.MetalLookAndFeel;


class ColorThemeRed extends AbstractColorTheme implements IColorTheme {  
		
	// package private
	ColorThemeRed(final String id) {
		super(id);
	}
	
	@Override
	public void apply(final MetalLookAndFeel metalLookAndFeel) {
		MetalLookAndFeel.setCurrentTheme(new DefaultMetalTheme(){	    	
	    	@Override
			public String getName() { return ColorThemeRed.this.getName(); }  
	        // greenish colors  
	        private final ColorUIResource primary1 = new ColorUIResource(102, 51, 51);  
	        private final ColorUIResource primary2 = new ColorUIResource(153, 102, 102);  
	        private final ColorUIResource primary3 = new ColorUIResource(204, 153, 153);   
			private final ColorUIResource secondary1 = new ColorUIResource(102, 102, 102);
			private final ColorUIResource secondary2 = new ColorUIResource(153, 153, 153);
			private final ColorUIResource secondary3 = new ColorUIResource(204, 204, 204);

			@Override
			protected ColorUIResource getPrimary1() { return primary1; }    
			@Override
			protected ColorUIResource getPrimary2() { return primary2; }   
			@Override
			protected ColorUIResource getPrimary3() { return primary3; }  
		    @Override
			protected ColorUIResource getSecondary1() { return secondary1; }
		    @Override
			protected ColorUIResource getSecondary2() { return secondary2; }
		    @Override
			protected ColorUIResource getSecondary3() { return secondary3; }	
	    });  		
	}   
}  
