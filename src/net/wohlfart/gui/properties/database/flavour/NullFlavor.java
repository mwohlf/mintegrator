package net.wohlfart.gui.properties.database.flavour;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * null element not intended to be used by any datasoucre, just 
 * to have a select item if nothing was selected yet...
 * 
 * @author michael
 */
class NullFlavor implements IDatabaseFlavor {

	private final String id;
	
	// package private
	NullFlavor(final String id) {
		this.id = id;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getUrl(String host, Integer port, String dbName) {
		throw new UnsupportedOperationException("no valid database flavor selected"); //$NON-NLS-1$
	}

//	@Override
//	public String getFileExtension() {
//		throw new UnsupportedOperationException("no valid database flavor selected"); //$NON-NLS-1$
//	}

	@Override
	public String getDriverClassName() {
		throw new UnsupportedOperationException("no valid database flavor selected"); //$NON-NLS-1$
	}

	@Override
	public String getDefaultValue(String key, String defaultValue) {
		// this is called when switching flavors in order to fill textfields with default values
		// we just return null here
		return null;
	}

	@Override
	public void validateConnection(Connection connection) throws SQLException {
		throw new UnsupportedOperationException("no valid database flavor selected"); //$NON-NLS-1$
	}

	@Override
	public Properties getQueries(Class<?> clazz) {
		throw new UnsupportedOperationException("no valid database flavor selected"); //$NON-NLS-1$
	}

}
