package net.wohlfart;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.swing.ImageIcon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Application wide image repository.
 *
 * @author michael
 */
public final class ImageManager {
    /** Logger for this class. */
    private static final Logger LOG = LoggerFactory.getLogger(ImageManager.class);

    /**
     * The resource bundle for key to filename mappings.
     */
    private static final String BUNDLE_NAME = "net.wohlfart.images"; //$NON-NLS-1$

    /**
     * Load the resource bundle. It contains the paths to the images.
     */
    static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

    
    /** The working directory is the location in the file system from where the java command was invoked. */
    private static final String CURRENT_DIRECTORY = System.getProperty("user.dir"); //$NON-NLS-1$

    private static final String DEFAULT_GFX_DIRECTORY = "gfx"; //$NON-NLS-1$
    private static final String DEFAULT_GFX_ZIPFILE = "gfx.zip"; //$NON-NLS-1$

    private static final String FILE_SEPARATOR = System.getProperty("file.separator"); //$NON-NLS-1$

    private static final int BUFFER_SIZE = 1024;

    /**
     * The default image width if no key can be found.
     */
    private static final int IMG_WIDTH = 16;

    /**
     * The default image height if no key can be found.
     */
    private static final int IMG_HEIGHT = 16;


    /**
     * the default static image
     */
    private static final Image DEFAULT_IMAGE = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, BufferedImage.TYPE_INT_RGB);

    /**
     * A hashmap to store the images just a basic picture buffer, needs to be
     * volatile in order for the double check lock to work with, we also need JDK4 or better
     */
    private static volatile HashMap<String, ImageIcon> storage = null;


    /**
     * The hidden constructor.
     */
    private ImageManager() {
    	throw new AssertionError("Messages is not supposed to be instanciated, use the static helper methods please"); //$NON-NLS-1$
    }
    
    static void preloadImages() {
    	storage = loadImages();
    }

    /**
     * load all images from the ZipFile and store them into the hashmap
     */
    private static HashMap<String, ImageIcon> loadImages() {
        HashMap<String, ImageIcon> map = new HashMap<>();

        // read the ZipFile
        String filename = getFilename();
        LOG.warn("loadImages from '{}'", filename); //$NON-NLS-1$
        ZipEntry entry;
        try {
            ZipInputStream zipInput = new ZipInputStream(new FileInputStream(filename));

            byte[] buf = new byte[BUFFER_SIZE]; int len;
            while ((entry = zipInput.getNextEntry()) != null) {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                // skip directories
                if (entry.isDirectory()) {
                    continue;
                }

                
                while ((len = zipInput.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                byte[] b = out.toByteArray();
                ImageIcon icon = new ImageIcon(b);

                String name = entry.getName();
                map.put(name, icon);
                out.close();
            }
            
            zipInput.close();
        } catch (IOException ex) {
            LOG.error("error while reading image file, the file was expected at '{}'," //$NON-NLS-1$
            		+ " the map may be empty or incomplete", filename); //$NON-NLS-1$
        }
        return map;
    }


    /**  we try to find the resources in the conf directory. */
    private static String getFilename() {
        return CURRENT_DIRECTORY
        + FILE_SEPARATOR
        + DEFAULT_GFX_DIRECTORY
        + FILE_SEPARATOR
        + DEFAULT_GFX_ZIPFILE;
    }

    /**
     * Main Accessor Method to find Images in the Resources.
     *
     * @param key A String key to find a matching Image Object.
     *
     * @return An Image Object
     */
    public static Image getImage(final String key) {

    	// the famous DoubleCheckedLocking, see:
    	// http://www.cs.umd.edu/~pugh/java/memoryModel/DoubleCheckedLocking.html
    	// should work after JDK4
        if (storage == null) {
        	synchronized (ImageManager.class) {
        		if (storage == null) {
        			storage = loadImages();
        		}
        	}
        }

        Image image = null;
        try {
            String entrykey = RESOURCE_BUNDLE.getString(key);
            if (storage.containsKey(entrykey)) {
                image = storage.get(entrykey).getImage();
            } else {
                LOG.warn("Can't find image resource for key: '{}' in storage; creating empty image", key); //$NON-NLS-1$
                image = DEFAULT_IMAGE;
            }
        } catch (MissingResourceException ex) {
            LOG.warn("Can't find image resource for key: '{}' in resource bundle; creating empty image", key); //$NON-NLS-1$
            image = DEFAULT_IMAGE;
        }
        return image;
    }


    public static ImageIcon getImageIcon(final String key) {
        return new ImageIcon(getImage(key));
    }

}
