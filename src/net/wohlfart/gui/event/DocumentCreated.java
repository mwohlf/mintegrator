package net.wohlfart.gui.event;

import net.wohlfart.data.persis.DocumentTO;

/**
 * needed to fire events with a null candidate
 */
public class DocumentCreated {

	private final DocumentTO element;

	public DocumentCreated(DocumentTO element) {
		this.element = element;
	}

	public DocumentTO get() {
		return element;
	}

}
