package net.wohlfart.gui.document;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.UIManager;

import net.wohlfart.ImageManager;
import net.wohlfart.data.IDocumentTO;

public final class ThumbnailFactory {
    private static ThumbnailFactory thumbnailFactory;

    private static final  String DEFAULT_ICON = "unknownIcon60";
    private static final  String DEFAULT_ICON_FILE = "unknownIcon60.png";

    static {
        UIManager.put(DEFAULT_ICON,
                new ImageIcon(ThumbnailFactory.class.getResource(DEFAULT_ICON_FILE)));

    }

    public synchronized static ThumbnailFactory getInstance() {
        if (thumbnailFactory == null) {
            thumbnailFactory = new ThumbnailFactory();
        }
        return thumbnailFactory;
    }


    /**
     * This private constructor should never be used.
     */
    private ThumbnailFactory() {
        // static helper class
    }

    public Icon createThumbnail(IDocumentTO document) {

        String type = document.getType();
        if (type == null) {
            return ImageManager.getImageIcon("unknownThumbnail");
        }


        if (type.equalsIgnoreCase("JPG")) {
            return ImageManager.getImageIcon("jpgThumbnail");
        } else if (type.equalsIgnoreCase("GIF")) {
            return ImageManager.getImageIcon("gifThumbnail");
        } else if (type.equalsIgnoreCase("XLS")) {
            return ImageManager.getImageIcon("xlsThumbnail");
        } else if (type.equalsIgnoreCase("DOC")) {
            return ImageManager.getImageIcon("docThumbnail");
        } else if (type.equalsIgnoreCase("TXT")) {
            return ImageManager.getImageIcon("txtThumbnail");
        } else if (type.equalsIgnoreCase("PDF")) {
            return ImageManager.getImageIcon("pdfThumbnail");
        } else if (type.equalsIgnoreCase("BAT")) {
            return ImageManager.getImageIcon("batThumbnail");
        } else  {
            return ImageManager.getImageIcon("unknownThumbnail");
        }


    }

    /*
    private static Image createJPG(byte[] data) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(data, 0, data.length);
        JPEGImageDecoder dec = JPEGCodec.createJPEGDecoder(bis);
        BufferedImage bim = dec.decodeAsBufferedImage();
        return bim;
    }

    private static Image createUnknow() {
        return null;
    }
     */

}
