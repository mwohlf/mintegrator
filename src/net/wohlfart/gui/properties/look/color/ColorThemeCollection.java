package net.wohlfart.gui.properties.look.color;

import java.util.HashMap;

import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.plaf.metal.MetalLookAndFeel;


@SuppressWarnings("serial")
public final class ColorThemeCollection extends HashMap<String, IColorTheme> {

	private static ColorThemeCollection instance;
	
	private static final String ID_RED = "Red";
	private static final String ID_GREEN = "Green";
	private static final String ID_BLUE = "Blue";
	private static final String ID_PLAIN = "Ocean";



	private ColorThemeCollection() {
		super();
		addColorTheme(new ColorThemeRed(ID_RED));
		addColorTheme(new ColorThemeGreen(ID_GREEN));
		addColorTheme(new ColorThemeBlue(ID_BLUE));
		addColorTheme(new ColorThemePlain(ID_PLAIN));
	}

	public static synchronized ColorThemeCollection getInstance() {
		if (instance == null) {
			instance = new ColorThemeCollection();
		}
		return instance;
	}


	// we use the name as Id
	private void addColorTheme(IColorTheme colorTheme) {
		put(colorTheme.getId(), colorTheme);
	}
	

	public IColorTheme getCurrentColorTheme() {
		LookAndFeel lookAndFeel = UIManager.getLookAndFeel();
		if (lookAndFeel instanceof MetalLookAndFeel) {
			String themeId = MetalLookAndFeel.getCurrentTheme().getName();
			return get(themeId);
		}			
		return null;
	}

}
