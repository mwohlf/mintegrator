package net.wohlfart.worker;

import static net.wohlfart.Constants.DELAY_WORKERS;

import java.awt.EventQueue;
import java.sql.SQLException;
import java.util.List;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import net.wohlfart.CustomEventQueue;
import net.wohlfart.Messages;
import net.wohlfart.data.ICandidateTO;
import net.wohlfart.data.IDocumentTO;
import net.wohlfart.data.persis.DocumentTO;
import net.wohlfart.data.persis.FolderDAO;
import net.wohlfart.gui.ErrorPane;
import net.wohlfart.gui.event.DocumentCreated;
import net.wohlfart.util.GuiUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * worker for loading the document views
 * note there is no cancel or progress bar
 * 
 * @author michael
 */
public class DocumentFolderReadWorker extends SwingWorker<Void, IDocumentTO> {
	/** Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(DocumentFolderReadWorker.class);

	@Inject
	private FolderDAO folderDAO;
	@Inject 
	private Event<DocumentCreated> addedDocumentEvent;


	// custom event queue for blocking user interaction while SwingWorker is running
	private final CustomEventQueue queue = CustomEventQueue.getInstance();

	private volatile ICandidateTO candidate;

	public void doExecute(final ICandidateTO candidate) {
		// this must run in EDT
		if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The method doExecute() must be called from EDT."); //$NON-NLS-1$
		}

		// needed in the done() method to cleanup, even if we don't run the doBackground()
		this.candidate = candidate;

		if (candidate == null) {
			LOG.info("read was canceled"); //$NON-NLS-1$
			// cancel the future object, this is needed so the get() method doesn't block 
			cancel(true); // (actually it throws an exception now)
			// done(); // will be called anyways FIXME: done() might be called twice in other workers...
			return;				
		}

		GuiUtilities.setBusyCursor();

		// fire up the swing worker
		execute();
	}

	protected boolean isCanceled() {
		if (DELAY_WORKERS) { GuiUtilities.sleep(2000); }
		return false;
	}


	@Override
	protected Void doInBackground() {
		LOG.debug("doInBackground() - start"); //$NON-NLS-1$

		try {
			// FIXME: this is ugly, we should get a count first 
			// to publish them separately and to provide a progress bar to the user...
			List<DocumentTO> docs = folderDAO.readAllDocuments(candidate);			
			for (DocumentTO doc : docs) {
				publish(doc);				
			}			
		} catch (SQLException ex) {
			LOG.error("doInBackground()", ex);
			JOptionPane.showMessageDialog(
					null,
					ex.getMessage(),
					Messages.getString("UploadDocumentsWorker.SQLException"), //$NON-NLS-1$
					JOptionPane.ERROR_MESSAGE);
		} catch (Throwable th) {
			LOG.error("doInBackground()", th);
			GuiUtilities.setDefaultCursor();
			ErrorPane.showErrorDialog(th, Messages.getString("DeleteSelectedCandidatesWorker.Throwable")); //$NON-NLS-1$
		} finally {
			queue.clearBlockedComponents();
			GuiUtilities.setDefaultCursor();
			LOG.info("doInBackground() - finally");
		}

		return null; 
	}


	/**
	 * called in the EDT with the fetched data
	 */
	@Override
	protected void process(List<IDocumentTO> chunks) {	
		// FIXME: instead of directly calling the documentPanel we need to 
		// use an event here
		for (IDocumentTO doc : chunks) {
			addedDocumentEvent.fire(new DocumentCreated((DocumentTO)doc));
		}
	}

	@Override
	protected void done() {
		super.done();
		addedDocumentEvent.fire(new DocumentCreated(null));
	}

}
