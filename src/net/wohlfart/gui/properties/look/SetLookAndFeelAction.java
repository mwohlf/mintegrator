package net.wohlfart.gui.properties.look;


import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.metal.MetalLookAndFeel;

import net.wohlfart.gui.properties.look.color.ColorThemeCollection;
import net.wohlfart.gui.properties.look.color.IColorTheme;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * command implementation to switch the L&F and also do some tweaks
 * to get a nicer look...
 * 
 * this is not Weld Injected, initialize manually 
 * 
 * @author michael
 */
@SuppressWarnings("serial")
public class SetLookAndFeelAction extends AbstractAction {
	/**  Logger for this class */
	private static final Logger LOG = LoggerFactory.getLogger(SetLookAndFeelAction.class);

	/** name of the preferred L&F and the theme */
	public static final String PREFERRED_LOOK = "Nimbus";
	public static final String PREFERRED_THEME = "Red";

	private final LookAndFeelInfo lookAndFeelInfo;
	private final IColorTheme colorScheme;

	public SetLookAndFeelAction(
			final LookAndFeelInfo lookAndFeelInfo) {
		this(lookAndFeelInfo, null);
	}

	public SetLookAndFeelAction(
			final LookAndFeelInfo lookAndFeelInfo,
			final IColorTheme colorScheme) {
		this.lookAndFeelInfo = lookAndFeelInfo;
		this.colorScheme = colorScheme;
	}



	@Override
	public void actionPerformed(ActionEvent evt) {
		if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The Method actionPerformed() needs to be run in the EDT!"); //$NON-NLS-1$
		}
		LOG.info("actionPerformed, event is: {}", evt);
		try {
			String className = lookAndFeelInfo.getClassName();
			Class<?> lnfClass = Class.forName(className, true, Thread.currentThread().getContextClassLoader());
			LookAndFeel lookAndFeel = (LookAndFeel)lnfClass.newInstance();

			// only apply the color scheme if it is not null...
			if (colorScheme != null) {
				if (lookAndFeel instanceof MetalLookAndFeel) {
					colorScheme.apply((MetalLookAndFeel)lookAndFeel);
				}
			}

			setLookAndFeel(lookAndFeel);
			// update any part of the component tree that is visible, starting with the windows...
			//for (Window window : Frame.getOwnerlessWindows()) {
			for (Window window : Frame.getWindows()) {
				if (window.isVisible()) {
					LOG.info("updating visible window {}", window);
					SwingUtilities.updateComponentTreeUI(window);
				}
			}
		} catch (ClassNotFoundException | UnsupportedLookAndFeelException | InstantiationException | IllegalAccessException ex) {
			LOG.error("error setting look and feel, stacktrace follows, no UI indication for the user", ex);
		}
	}

	// override for testing
	protected void setLookAndFeel(LookAndFeel lookAndFeel) throws UnsupportedLookAndFeelException {
		UIManager.setLookAndFeel(lookAndFeel);
	}

	// the name of the class is used as id in LookAndFeelInfoCollection
	public static String getFallbackLookAndFeelId() {
		return UIManager.getCrossPlatformLookAndFeelClassName();
	}

	// no color theme by default
	public static String getFallbackThemeId() {
		return null;
	}



	static void setLookAndFeelTheme(
			final LookAndFeelInfo lookAndFeelInfo,
			final IColorTheme colorTheme) {
		// fire a custom action event
		final ActionEvent event = new ActionEvent(lookAndFeelInfo,  // source
				ActionEvent.ACTION_PERFORMED, 
				lookAndFeelInfo.getName());
		final SetLookAndFeelAction action = new SetLookAndFeelAction(lookAndFeelInfo, colorTheme);

		if (!EventQueue.isDispatchThread()) {
			EventQueue.invokeLater(new Runnable() {
				@Override
				public void run() {
					action.actionPerformed(event);
				}
			});
		} else {
			action.actionPerformed(event);
		}
	}


	/**
	 * setting the look and feel
	 * we perform a refresh on the component tree if there is already a frame visible
	 * 
	 * see: http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4843282
	 * see: http://www.webmaid.de/2010/02/uimanager-java-in-hubsch/
	 * 
	 * @param preferedLnF
	 */
	public static void setLookAndFeelTheme(
			final String lookAndFeelClassname,
			final String themeId) {

		setLookAndFeelTheme(
				LookAndFeelInfoCollection.getInstance().get(lookAndFeelClassname), 
				ColorThemeCollection.getInstance().get(themeId)
				);
	}




}
