package net.wohlfart.data;

/**
 * @author michael
 */
public interface IFolderTO {

    public abstract String getAttachment();

    public abstract Integer getCandidateId();

    public abstract Integer getDocId();

    public abstract Integer getDocumentId();

    public abstract String getGroup();

    public abstract Integer getId();

    public abstract Integer getSort1();

    public abstract Integer getSort2();

    public abstract String getTitle();

    public abstract void setAttachment(String attachment);

    public abstract void setCandidateId(Integer candidateId);

    public abstract void setDocId(Integer docId);

    public abstract void setDocumentId(Integer documentId);

    public abstract void setGroup(String group);

    public abstract void setId(Integer id);

    public abstract void setSort1(Integer sort1);

    public abstract void setSort2(Integer sort2);

    public abstract void setTitle(String title);

}
