/**
 *
 */
package net.wohlfart.gui.properties;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;

import net.wohlfart.ImageManager;
import net.wohlfart.Messages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An Action that triggers the new setup dialog
 * 
 * @author michael
 */
@SuppressWarnings("serial")
@Singleton
public class PropertiesSetupAction extends AbstractAction {
    /**  Logger for this class */
    private final static Logger LOG = LoggerFactory.getLogger(PropertiesSetupAction.class);
    
    
    @Inject
    private DefaultPropertySheets defaultPropertySheets;


    public PropertiesSetupAction() {
        putValue(Action.NAME, Messages.getString("PropertiesSetup.ActionName")); //$NON-NLS-1$
        putValue(Action.SMALL_ICON, ImageManager.getImageIcon("PropertiesSetupIcon16")); //$NON-NLS-1$
        putValue(Action.LARGE_ICON_KEY, ImageManager.getImageIcon("PropertiesSetupIcon24")); //$NON-NLS-1$
        putValue(Action.SHORT_DESCRIPTION, Messages.getString("PropertiesSetup.ActionShortDescription")); //$NON-NLS-1$
        putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
    }

    /**
     * runs in the EDT
     */
    @Override
	public void actionPerformed(final ActionEvent arg0) {
    	LOG.debug("PropertiesSetupAction triggered");
    	
    	new PropertySheetsDialog() {{
    		addPages(defaultPropertySheets);
    		pack();
    	}}.setVisible(true);
    }

  

    
}
