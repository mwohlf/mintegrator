/**
 *
 */
package net.wohlfart.mail.selection;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import net.wohlfart.Messages;
import net.wohlfart.properties.ApplicationPropertiesImpl;
import net.wohlfart.properties.PropertyKeys;
import net.wohlfart.util.GuiUtilities;


/**
 * @author michael
 *
 */
@SuppressWarnings("serial")
public class MailTable extends JPanel {

    private final JTable innerTable;

    private final MailTableModel tableDataModel;

    private final TableColumnModel columnModel = new DefaultTableColumnModel();


    /* package private */
    MailTable(MailTableModel tableData) {
        super();
        setLayout(new BorderLayout());
        tableDataModel = tableData;

        innerTable = new JTable(tableDataModel, columnModel);

        innerTable.setAutoCreateColumnsFromModel(false);
        innerTable.setAutoCreateRowSorter(true);

        // not needed here since it is called in the model: tableModel.fireTableStructureChanged();

        JScrollPane innerScrollPane = new JScrollPane();
        innerScrollPane.setViewportView(innerTable);
        add(innerScrollPane, BorderLayout.CENTER);


        JPopupMenu popup = createPopup();
        innerScrollPane.setComponentPopupMenu(popup);
        innerTable.setComponentPopupMenu(popup);
        setComponentPopupMenu(popup);


        // add a sorter -  not needed since auto sorter is enabled
        // innerTable.setRowSorter(new TableRowSorter<MailTableModel>(tableModel));

        GuiUtilities.loadColumnProperties(PropertyKeys.MAIL_TABLE, innerTable, tableDataModel);
        GuiUtilities.addColumnModelListener(PropertyKeys.MAIL_TABLE, innerTable);
    }


    /**
     * @param visibleProperties
     * @return
     */
    private JPopupMenu createPopup() {
        JPopupMenu jPopupMenu = new JPopupMenu();
        final ApplicationPropertiesImpl props = ApplicationPropertiesImpl.getInstance();

        // create a menu entry for each column
        int count = tableDataModel.getColumnCount();
        for (int column = 0; column < count; column++) {
            final JCheckBoxMenuItem menuItem = new JCheckBoxMenuItem(tableDataModel.getColumnName(column));
            final Integer modelIndex = column;
            boolean columnIsVisible = props.getColumnIsVisible(PropertyKeys.MAIL_TABLE, modelIndex);
            menuItem.addActionListener(new ActionListener() {
                @Override
				public void actionPerformed(final ActionEvent arg0) {
                    toggleColumn(modelIndex, menuItem.isSelected());
                }});
            menuItem.setSelected(columnIsVisible);
            jPopupMenu.add(menuItem);
        }

        jPopupMenu.addSeparator();

        // create a menu item for the vertical scrollbar
        final JCheckBoxMenuItem menuItem = new JCheckBoxMenuItem(Messages.getString("MailTable.Header.autoResize"));
        menuItem.addActionListener(new ActionListener() {
            @Override
			public void actionPerformed(final ActionEvent arg0) {
                if (menuItem.isSelected()) {
                    innerTable.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);
                } else {
                    innerTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                }
                props.setAutoResize(PropertyKeys.MAIL_TABLE, menuItem.isSelected());
            }});
        boolean autoResize = props.getAutoResize(PropertyKeys.MAIL_TABLE);
        // the table autoresize property is configured in the loadProperties call
        menuItem.setSelected(autoResize);
        jPopupMenu.add(menuItem);

        return jPopupMenu;
    }


    private void toggleColumn(final int modelIndex, final boolean isVisible) {
        EventQueue.invokeLater(new Runnable() {
            @Override
			public void run() {
                ApplicationPropertiesImpl props = ApplicationPropertiesImpl.getInstance();
                props.setColumnIsVisible(PropertyKeys.MAIL_TABLE, modelIndex, isVisible);

                //get the column
                int viewColumn = innerTable.convertColumnIndexToView(modelIndex);

                if (isVisible) {
                    // make column visible
                    assert viewColumn == -1: "column is already visible: " + modelIndex;
                    TableColumn column = new TableColumn(modelIndex);
                    innerTable.addColumn(column);
                } else {
                    // hide the column
                    assert viewColumn > -1: "column is not visible: " + modelIndex;
                    TableColumn column = innerTable.getColumnModel().getColumn(viewColumn);
                    innerTable.removeColumn(column);
                }
            }
        });
    }


    public int convertRowIndexToModel(int i) {
        return innerTable.convertRowIndexToModel(i);
    }
    public int convertRowIndexToView(Integer i) {
        return innerTable.convertRowIndexToView(i);
    }

    public void setAutoResizeMode(int mode) {
        innerTable.setAutoResizeMode(mode);
    }
    public void setListSelectionModel(ListSelectionModel selectionModel) {
        innerTable.setSelectionModel(selectionModel);
    }


}
