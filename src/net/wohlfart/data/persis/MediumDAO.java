package net.wohlfart.data.persis;

import java.awt.EventQueue;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.inject.Inject;

import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.data.IMediumTO;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author michael
 */
public class MediumDAO {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(MediumDAO.class);

	@Inject
	private IBasicDataSource basicDataSource;


	public MediumDAO() {
	}

	public List<MediumTO> readMedia() throws SQLException {
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The DataBase shouldn't be access in the EDT."); //$NON-NLS-1$
		}

		Properties properties = basicDataSource.getQueries(IMediumTO.class);

		BeanListHandler<MediumTO> handler = new BeanListHandler<>(MediumTO.class);
		// create Query Runner for a given DataSource
		QueryRunner run = new QueryRunner(basicDataSource);

		LOG.debug("loading {}", properties.getProperty("SELECT_ALL"));
		// returns result from the Handler
		ArrayList<MediumTO> result = (ArrayList<MediumTO>) run.query(
				properties.getProperty("SELECT_ALL"),
				handler,
				new Object[] {});
		LOG.debug("result {}", result);

		return result;
	}

}
