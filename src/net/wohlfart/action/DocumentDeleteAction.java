package net.wohlfart.action;

import java.awt.event.ActionEvent;
import java.sql.SQLException;

import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.AbstractAction;
import javax.swing.Action;

import net.wohlfart.ImageManager;
import net.wohlfart.Messages;
import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.data.IDocumentTO;
import net.wohlfart.data.persis.FolderDAO;
import net.wohlfart.gui.event.DocumentFocused;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author michael
 *
 */
@SuppressWarnings("serial")
@Singleton
public class DocumentDeleteAction extends AbstractAction {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(DocumentDeleteAction.class);

	@Inject
	private FolderDAO folderDAO; // = context.fab.getFolderDAO();
	@Inject
	private IBasicDataSource dataSource;
	@Inject 
	private Event<DocumentFocused> documentFocusEvent;

	private boolean isDataSourceConnected;
	private IDocumentTO document;


	public DocumentDeleteAction() {
		putValue(Action.NAME, Messages.getString("DeleteDocument.ActionName")); //$NON-NLS-1$
		putValue(Action.SMALL_ICON, ImageManager.getImageIcon("DeleteDocumentIcon16")); //$NON-NLS-1$
		putValue(Action.LARGE_ICON_KEY, ImageManager.getImageIcon("DeleteDocumentIcon24")); //$NON-NLS-1$
		putValue(Action.SHORT_DESCRIPTION, Messages.getString("DeleteDocument.ShortDescription")); //$NON-NLS-1$

		doEnableCheck();
	}

	@Override
	public void actionPerformed(final ActionEvent evt) {
		LOG.info("deleting document: {}", evt); //$NON-NLS-1$
		try {
			folderDAO.deleteDocument(document);
		} catch (SQLException ex) {
			LOG.error("trouble deleting document", ex); //$NON-NLS-1$
		} finally {
			// document no longer focused
			documentFocusEvent.fire(new DocumentFocused(null, dataSource));
		}
	}

	private void doEnableCheck() {
		setEnabled(isDataSourceConnected && (document != null));  	
	}


	public void onFocusedDocumentEvent(@Observes final DocumentFocused container) {
		LOG.info("onFocusedDocumentEvent called"); //$NON-NLS-1$
		document = (IDocumentTO) container.getContent();
		doEnableCheck();
	}


	public void onAnyDataSourceEvent(@Observes final IBasicDataSource basicDataSource) {
		LOG.debug("onAnyDataSourceEvent called, checking if the datasource is closed: '{}' " //$NON-NLS-1$
				+ "(from dataSource.isClosed())", basicDataSource.isClosed()); //$NON-NLS-1$
		isDataSourceConnected = !basicDataSource.isClosed();
		doEnableCheck();
	}

}
