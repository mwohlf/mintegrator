/**
 *
 */
package net.wohlfart.mail.outlook;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import net.wohlfart.mail.IMailAttachment;
import net.wohlfart.mail.IMailMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moyosoft.connector.com.ComponentObjectModelException;
import com.moyosoft.connector.ms.outlook.attachment.AttachmentsCollection;
import com.moyosoft.connector.ms.outlook.attachment.AttachmentsIterator;
import com.moyosoft.connector.ms.outlook.attachment.OutlookAttachment;
import com.moyosoft.connector.ms.outlook.item.FlagStatus;
import com.moyosoft.connector.ms.outlook.mail.OutlookMail;
import com.moyosoft.connector.ms.outlook.recipient.OutlookRecipient;
import com.moyosoft.connector.ms.outlook.recipient.RecipientsCollection;

/**
 * @author michael
 *
 */
public class OutlookMailData implements IMailMessage {
    /** Logger for this class */
    private final static Logger LOGGER = LoggerFactory.getLogger(OutlookMailData.class);

    private final static int INVALID_COUNT = -1;

    private final OutlookMail data;

    private int attachmentCount = INVALID_COUNT;
    private String senderAddress;
    private String subject;
    private Date receiveDate;
    private String senderName;

    private Boolean selected;



    OutlookMailData(OutlookMail mail) {
        data = mail;
    }

    @Override
	public Integer getAttachmentCount() {
        if (attachmentCount == INVALID_COUNT) {
            attachmentCount = data.getAttachmentsCount();
        }
        return attachmentCount;
    }

    @Override
	@SuppressWarnings("deprecation")
    public String getSenderAddress() {
        if (senderAddress == null) {

            // this is the easy way and works >= Outlook 2003
            try {
                senderAddress = data.getSenderEmailAddress();
                return senderAddress;
            } catch (ComponentObjectModelException ignore) {
                //System.err.println("err: " + ignore);
            }

            // this is by generating a reply email works < 2003
            try {
                senderAddress = getEmailFromReply(data);
                return senderAddress;
            } catch (ComponentObjectModelException ignore) {
                //System.err.println("err: " + ignore);
            }

            // last possible way is to try to parse the header
            // this is no longer possible in joc 2.1.3
            /*
            try {
                senderAddress = getEmailFromHeader(data);
                return senderAddress;
            } catch (ComponentObjectModelException ignore) {
                //System.err.println("err: " + ignore);
            }
            */
        }
        return senderAddress;
    }

    @Override
	public String getSubject() {
        if (subject == null) {
            subject = data.getSubject();
        }
        return subject;
    }

    @Override
	public ArrayList<IMailAttachment> getAttachments() {
        ArrayList<IMailAttachment> result = new ArrayList<>();

        AttachmentsCollection collection = data.getAttachments();
        for (AttachmentsIterator it = collection.iterator(); it.hasNext();) {
            Object object = it.next();
            if ( (object != null) && (object instanceof OutlookAttachment)) {
                OutlookAttachment attachment = (OutlookAttachment)object;
                result.add(new OutlookMailAttachment(attachment));
            }
        }
        return result;
    }

    @Override
	public String getSenderName() {
        if (senderName == null) {

            // this is the easy way and works >= Outlook 2003
            try {
                senderName = data.getSenderName();
                return senderName;
            } catch (ComponentObjectModelException ignore) {
                //System.err.println("err: " + ignore);
            }
        }
        return senderName;
    }

    @Override
	public Date getReceivedDate() {
        if (receiveDate == null) {
            receiveDate = data.getReceivedTime();
        }
        return receiveDate;
    }

    @Override
	public String getBody() {
        // we don'T buffer this since it is not shown in the select table
        // and would need too much memory...
        return data.getBody();
    }



    ///////////////////// special outlook hackery follows

/*
    protected static final String EMAIL_PATTERN_STRING
    = "^From: (?:.* ){0,1}[<]{0,1}([_\\-A-Za-z0-9\\.]*@[_\\-A-Za-z0-9\\.]*)[>]{0,1}.*$";
    //  (?:.* ){0,1}  -> a name or something else that may or may not be withing the from line in the header
    //                   (?:.* ) is a non-capturing group
    //  [<]{0,1} and [>]{0,1}  -> the obening and closing braktes that may or may not be in the from line
    //  ([\\-A-Za-z0-9\\.]*@[\\-A-Za-z0-9\\.]*)  -> the email and the matching group
    private String getEmailFromHeader(OutlookMail outlookMail) {
        //System.out.println("getting from header: " + outlookMail);
        String result = "";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN_STRING, Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
        String header = outlookMail.getTransportMessageHeaders();
        //System.out.println("header is: " + header);
        Matcher matcher = pattern.matcher(outlookMail.getTransportMessageHeaders());
        result = matcher.group(1);
        return result;
    }
*/

    @SuppressWarnings("rawtypes")
    private String getEmailFromReply(OutlookMail outlookMail) {
        String result = "";
        OutlookMail replyMail = outlookMail.reply();
        RecipientsCollection recipients = replyMail.getRecipients();

        for (Iterator it = recipients.iterator(); it.hasNext();) {
            Object object = it.next();
            if (object instanceof  OutlookRecipient) {
                // first try the address
                result = ((OutlookRecipient) object).getAddress();
                if ((result != null) && (result.length() > 0) && result.contains("@")) {
                    break;
                }
                // second try the name
                result = ((OutlookRecipient) object).getName();
                if ((result != null) && (result.length() > 0) && result.contains("@")) {
                    break;
                }
            }
        }
        replyMail.delete(); // clean up a bit
        return result;
    }


    @Override
	public Boolean getSelected() {
        return selected;
    }
    @Override
	public void setSelected(Boolean selected) {
        this.selected = selected;
    }


    @Override
	public Boolean getImported() {
        return
        (data.getFlagStatus() == FlagStatus.MARKED)
        || (data.getFlagStatus() == FlagStatus.COMPLETE);
    }
    @Override
	public void setImported(Boolean isImported) {
        LOGGER.debug("marking imported mail");
        data.setFlagStatus(FlagStatus.MARKED);
        data.save();
    }

}
