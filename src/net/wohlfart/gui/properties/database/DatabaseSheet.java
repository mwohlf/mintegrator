package net.wohlfart.gui.properties.database;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Arrays;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import javax.swing.text.AbstractDocument;

import net.wohlfart.ImageManager;
import net.wohlfart.Messages;
import net.wohlfart.gui.properties.AbstractPropertySheet;
import net.wohlfart.gui.properties.database.flavour.DbFlavourCollection;
import net.wohlfart.gui.properties.database.flavour.IDatabaseFlavor;
import net.wohlfart.properties.IApplicationProperties;
import net.wohlfart.properties.PropertyKeys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
@Singleton
public class DatabaseSheet extends AbstractPropertySheet {
	/**  Logger for this class */
	private static final Logger LOG = LoggerFactory.getLogger(DatabaseSheet.class);

	public static final String NAME = "DatabaseSheet"; // for lookup in the component hierarchy //$NON-NLS-1$

	// keys for the database properties to get a connection
	public static final String USER_PROPERTIES_KEY = "user"; //$NON-NLS-1$
	public static final String PASSWORD_PROPERTIES_KEY = "password"; //$NON-NLS-1$


	private final JComboBox<IDatabaseFlavor> databaseFlavorSelect;
	private final JTextField hostField;
	private final JTextField databaseField;
	private final JTextField portField;
	private final JTextField userField;
	private final JPasswordField passwordField;
	private final JCheckBox autoConnectCheck;
	// to display the error/success message when testing the connection
	private final JTextArea statusArea;
	// set to true if the current content can be overridden when selecting another DB flavor
	private final boolean doOverride = true;


	@Inject
	protected IApplicationProperties properties;


	DatabaseSheet() {
		super(ImageManager.getImageIcon("ConnectDBIcon24"),  //$NON-NLS-1$
				Messages.getString("DatabaseSheet.title"),  //$NON-NLS-1$
				Messages.getString("DatabaseSheet.tooltip")); //$NON-NLS-1$
		setName(NAME);
		// setup the finals
		databaseFlavorSelect = createDatabaseFlavourSelect();
		hostField =  new JTextField();
		databaseField =  new JTextField();
		portField =  new JTextField();
		userField =  new JTextField();
		passwordField = new JPasswordField();
		autoConnectCheck = new JCheckBox(Messages.getString("DatabaseSheet.autoConnectCheckLabel")); //$NON-NLS-1$
		statusArea = createStatusArea();
	}


	@PostConstruct
	protected void postConstruct() { // NO_UCD
		LOG.debug("postConstruct() called"); //$NON-NLS-1$
		initWiring();
		initComponents();
	}


	private void initWiring() {
		// reset the fields before wiring them
		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() throws Exception {
				resetAction(); // can't run in EDT, need a swingworker for this
				return null;
			}
		} .execute();

		// wire the actions
		databaseFlavorSelect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				LOG.debug("selection done"); //$NON-NLS-1$
				if (doOverride) {
					IDatabaseFlavor selectedFlavour = (IDatabaseFlavor) databaseFlavorSelect.getSelectedItem();
					setFlavourDefaults(selectedFlavour);
				}			
			}
		});

		passwordField.addFocusListener(new FocusAdapter(){
			@Override
			public void focusGained(FocusEvent e) {
				passwordField.selectAll();
			}		
		});

		// only numbers for the port field
		AbstractDocument doc = (AbstractDocument) portField.getDocument();				
		doc.setDocumentFilter(new PortDocumentFilter());
	}

	private void initComponents() {
		// do the layout
		setLayout(new GridBagLayout());
		setName(NAME);

		GridBagConstraints labelColumn = new GridBagConstraints();
		labelColumn.insets = new Insets(1,3,0,10);
		labelColumn.anchor = GridBagConstraints.WEST;

		GridBagConstraints editorColumn = new GridBagConstraints();
		editorColumn.insets = new Insets(1,0,0,10);
		editorColumn.anchor = GridBagConstraints.WEST;
		editorColumn.gridx = 1;
		editorColumn.fill = GridBagConstraints.HORIZONTAL;
		labelColumn.gridy = editorColumn.gridy=0;


		// --- start editors ---

		// database flavor
		add(new JLabel(Messages.getString("DatabaseSheet.databaseFlavorLabel")) //$NON-NLS-1$
		{{
			setLabelFor(databaseFlavorSelect);
		}}, labelColumn);
		add(databaseFlavorSelect, editorColumn);
		labelColumn.gridy = ++editorColumn.gridy;

		// server name
		add(new JLabel(Messages.getString("DatabaseSheet.hostLabel")) //$NON-NLS-1$
		{{
			setLabelFor(hostField);
		}}, labelColumn);
		add(hostField, editorColumn);	
		labelColumn.gridy = ++editorColumn.gridy;

		// database name
		add(new JLabel(Messages.getString("DatabaseSheet.databaseLabel")) //$NON-NLS-1$
		{{
			setLabelFor(databaseField);
		}}, labelColumn);
		add(databaseField, editorColumn);			
		labelColumn.gridy = ++editorColumn.gridy;

		// port
		add(new JLabel(Messages.getString("DatabaseSheet.portLabel")) //$NON-NLS-1$
		{{
			setLabelFor(portField);
		}}, labelColumn);
		add(portField, editorColumn);			
		labelColumn.gridy = ++editorColumn.gridy;

		// login
		add(new JLabel(Messages.getString("DatabaseSheet.loginLabel")) //$NON-NLS-1$
		{{
			setLabelFor(userField);
		}}, labelColumn);
		add(userField, editorColumn);			
		labelColumn.gridy = ++editorColumn.gridy;

		// password
		add(new JLabel(Messages.getString("DatabaseSheet.passwordLabel")) //$NON-NLS-1$
		{{
			setLabelFor(passwordField);
		}}, labelColumn);
		add(passwordField, editorColumn);			
		labelColumn.gridy = ++editorColumn.gridy;

		// autoConnect checkbox
		add(autoConnectCheck, editorColumn);			
		labelColumn.gridy = ++editorColumn.gridy;



		// ------------------- status, filler and button bar

		// add a status label
		GridBagConstraints buttons = new GridBagConstraints();
		buttons.gridy = labelColumn.gridy = editorColumn.gridy;		
		buttons.gridwidth = 2;
		buttons.fill = GridBagConstraints.BOTH;
		add(statusArea, buttons);
		buttons.gridy = labelColumn.gridy = ++editorColumn.gridy;

		// add a filler component	
		GridBagConstraints filler = new GridBagConstraints();
		filler.gridy = buttons.gridy = labelColumn.gridy = editorColumn.gridy;
		filler.gridwidth = 2;
		filler.weightx = filler.weighty = 0.5;
		add(Box.createGlue(), filler);
		filler.gridy = buttons.gridy = labelColumn.gridy = ++editorColumn.gridy;

		// finally "restore default" and "apply" buttons:
		JComponent buttonBar = createButtonBar();
		buttons.fill = GridBagConstraints.HORIZONTAL;
		add(buttonBar, buttons);

	}

	private JTextArea createStatusArea() {
		JTextArea result = new JTextArea() {
			// use the label background
			JLabel label;
			{
				setEditable(false);
				setColumns(50);
				setRows(5);
				setPreferredSize(getMinimumSize());
			}			
			@Override
			public Color getBackground() {
				if (label == null) {
					label = new JLabel();
				}
				return label.getBackground();
			}
		};
		result.setLineWrap(true);
		return result;
	}

	@SuppressWarnings("unchecked")
	private JComboBox<IDatabaseFlavor> createDatabaseFlavourSelect() {
		JComboBox<IDatabaseFlavor> result =  new JComboBox<>();
		result.setModel(new DbFlavourBoxModel());
		// pick the name to render the cell
		result.setRenderer(new BasicComboBoxRenderer() {
			@Override
			public Component getListCellRendererComponent(final JList list,
					Object value, int index, boolean isSelected,
					boolean cellHasFocus) {
				if (value instanceof IDatabaseFlavor) {
					String newValue = ((IDatabaseFlavor) value).getId();
					return super.getListCellRendererComponent(list, newValue, index, isSelected, cellHasFocus);
				} else {
					LOG.warn("invalid rendering value"); // just a fallback
					return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);					
				}
			}
		});		
		return result;	
	}





	private JComponent createButtonBar() {
		final Box result = Box.createHorizontalBox();
		// filler
		result.add(Box.createHorizontalGlue());
		// reset button
		result.add(new JButton(new TestAction()));
		// reset button
		result.add(new JButton(new ResetAction()));
		// apply button
		result.add(new JButton(new ApplyAction()));
		// some padding
		result.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
		return result;
	}




	private void indicateConnectionSuccess(final String url) {
		statusArea.setText(""); //$NON-NLS-1$
		statusArea.setForeground(Color.GREEN);
		statusArea.setFont(new JLabel() .getFont());
		statusArea.setText("" //$NON-NLS-1$
				+ "ConnectionURL: " + url + "\n" //$NON-NLS-1$ //$NON-NLS-2$
				+ "Successful Connected\n" //$NON-NLS-1$
				);
		statusArea.setSelectionStart(0);
		statusArea.setSelectionEnd(0);		
	}

	// called outside the EDT, we try to give the user a hint about what might have 
	// caused the connection problem, the better we do here the less hotline calls we get
	private void indicateConnectionFailure(final Throwable t, final String url) {	
		statusArea.setForeground(Color.RED);
		statusArea.setFont(new JLabel() .getFont());

		// print a nice error message for ClassNotFoundException
		if (t instanceof ClassNotFoundException) {
			String classname = t.getMessage();
			statusArea.setText("" //$NON-NLS-1$
					+ "ConnectionURL: " + url + "\n" //$NON-NLS-1$ //$NON-NLS-2$
					+ "Treiber kann nicht geladen werden: " + classname  + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		} else {
			// best we can do is just display the localized message
			String error = t.getLocalizedMessage();
			Throwable rootCause = t;
			while (rootCause.getCause() != null) {
				rootCause = rootCause.getCause();
			}
			String cause = rootCause.getLocalizedMessage();
			statusArea.setText("" //$NON-NLS-1$
					+ "ConnectionURL: " + url + "\n" //$NON-NLS-1$ //$NON-NLS-2$
					+ "Error: " + error  + "\n" //$NON-NLS-1$ //$NON-NLS-2$
					+ (t.equals(rootCause)?"\n":("Cause: " + cause  + "\n"))); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		statusArea.setSelectionStart(0);
		statusArea.setSelectionEnd(0);		

		// finally provide error feedback sound
		UIManager.getLookAndFeel().provideErrorFeedback(null);
	}

	private void setFlavourDefaults(final IDatabaseFlavor flavour) {
		hostField.setText(flavour.getDefaultValue(PropertyKeys.DATABASE_HOST, hostField.getText()));
		databaseField.setText(flavour.getDefaultValue(PropertyKeys.DATABASE_NAME, databaseField.getText()));
		portField.setText(flavour.getDefaultValue(PropertyKeys.DATABASE_PORT, portField.getText()));
		userField.setText(flavour.getDefaultValue(PropertyKeys.DATABASE_USER_NAME, userField.getText()));
		passwordField.setText(flavour.getDefaultValue(PropertyKeys.DATABASE_PASSWORD, new String(passwordField.getPassword())));
	}




	// this method is called outside the EDT to test a database connection
	protected void testAction() {
		LOG.info("testAction triggered in {} ", NAME); //$NON-NLS-1$
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The method resetAction() needs to run outside the EDT"); //$NON-NLS-1$
		}

		IDatabaseFlavor selectedFlavour = (IDatabaseFlavor) databaseFlavorSelect.getSelectedItem();
		LOG.info("selectedFlavour is {}", selectedFlavour);	 //$NON-NLS-1$

		String url = null;
		try {
			final String host = hostField.getText().trim();
			final String portString = portField.getText().trim();

			Integer port = null;
			if (portString.length() > 0) {
				port = Integer.valueOf(portString);
			}
			final String dbName = databaseField.getText().trim();
					
			url = selectedFlavour.getUrl(host, port, dbName);

			// seems like this is no longer necessary as of JDK7
			// however we get a nice ClassNotFoundException if the driver is not available
			Class.forName(selectedFlavour.getDriverClassName());

			final Properties props = new Properties();    
			final String user = userField.getText().trim();
			if (user.length() > 0) {
				props.put(USER_PROPERTIES_KEY, userField.getText().trim());
			}
			final char[] passwd = passwordField.getPassword();
			if (passwd.length > 0) {
				props.put(PASSWORD_PROPERTIES_KEY, new String(passwd));
			}
			Arrays.fill(passwd, '0');			
			final Connection connection = DriverManager.getConnection(url, props);

			// throws an error if the connction is invalid
			selectedFlavour.validateConnection(connection);			
			indicateConnectionSuccess(url);
		} catch (Throwable th) {
			LOG.info("failed to connect database in database sheet, stacktrace follows, the error is also shown in the UI", th); //$NON-NLS-1$
			indicateConnectionFailure(th, url);
		}		
	}


	@Override
	public void resetAction() {
		LOG.info("resetting values in {} ", NAME); //$NON-NLS-1$	
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The method resetAction() needs to run outside the EDT"); //$NON-NLS-1$
		}

		// we need to set the values in the EDT
		try {
			EventQueue.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					IDatabaseFlavor flavor = null;
					String flavorId = properties.getString(PropertyKeys.DATABASE_FLAVOR_ID, "");
					if (flavorId != null) {
					   flavor = DbFlavourCollection.getInstance().get(flavorId);
					} // fallback in case something went wrong with the properties
					if (flavor == null) {
						flavor = (IDatabaseFlavor)DbFlavourCollection.getInstance().getNullElement();
					}
					databaseFlavorSelect.setSelectedItem(flavor);					
					hostField.setText(properties.getString(PropertyKeys.DATABASE_HOST, ""));
					databaseField.setText(properties.getString(PropertyKeys.DATABASE_NAME, ""));
					Integer port = properties.getInteger(PropertyKeys.DATABASE_PORT, null);
					portField.setText((port!=null)?port.toString():"");
					userField.setText(properties.getString(PropertyKeys.DATABASE_USER_NAME, ""));
					String password = properties.getPassword();
					passwordField.setText((password!=null)?password:"");
					autoConnectCheck.setSelected(properties.getBoolean(PropertyKeys.AUTO_CONNECT, false));
				}});
		} catch (InvocationTargetException | InterruptedException ex) {
			LOG.warn("error setting values, ecpetion is ignored", ex);
		}
	}

	/**
	 * push the data into the application properties set
	 */
	@Override
	public void applyAction() {
		LOG.info("applying values in {} ", NAME); //$NON-NLS-1$
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The method resetAction() needs to run outside the EDT"); //$NON-NLS-1$
		}

		// we need to set the values in the EDT
		try {
			EventQueue.invokeAndWait(new Runnable() {
				@Override
				public void run() {

					IDatabaseFlavor databaseFlavor = (IDatabaseFlavor) databaseFlavorSelect.getSelectedItem();
					properties.setString(PropertyKeys.DATABASE_FLAVOR_ID, databaseFlavor.getId());

					String hostName = hostField.getText().trim();
					if (hostName.length() > 0) {
						properties.setString(PropertyKeys.DATABASE_HOST, hostName);
					} else {
						properties.delete(PropertyKeys.DATABASE_HOST);
					}

					String database = databaseField.getText().trim();
					if (database.length() > 0) {
						properties.setString(PropertyKeys.DATABASE_NAME, database);
					} else {
						properties.delete(PropertyKeys.DATABASE_NAME);
					}

					String port = portField.getText().trim();
					if (port.length() > 0) {
						properties.setInteger(PropertyKeys.DATABASE_PORT, Integer.valueOf(port));
					} else {
						properties.delete(PropertyKeys.DATABASE_PORT);
					}

					String user = userField.getText().trim();
					if (user.length() > 0) {
						properties.setString(PropertyKeys.DATABASE_USER_NAME, user);
					} else {
						properties.delete(PropertyKeys.DATABASE_USER_NAME);
					}

					String password = new String(passwordField.getPassword());
					if (password.length() > 0) {
						properties.setPassword(password);
					} else {
						properties.deletePassword();
					}

					properties.setBoolean(PropertyKeys.AUTO_CONNECT, autoConnectCheck.isSelected());
				}});
		} catch (InvocationTargetException | InterruptedException ex) {
			LOG.warn("error setting values, ecpetion is ignored", ex);
		}
	}



	// custom action to test the database
	protected class TestAction extends AbstractAction {

		public TestAction() {
			putValue(Action.NAME, Messages.getString("DatabaseSheet.TestAction.ActionName")); //$NON-NLS-1$
		}

		@Override
		public void actionPerformed(ActionEvent evt) {
			DatabaseSheet.this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			new SwingWorker<Void, Void>() {
				@Override
				protected Void doInBackground() throws Exception {
					try {
						testAction();
					} catch (Throwable th) {
						LOG.error("error while performing test action in database sheet", th); //$NON-NLS-1$
					} 
					return null;
				}
				@Override
				protected void done() {
					DatabaseSheet.this.setCursor(Cursor.getDefaultCursor());
				};				
			}.execute();
		}
	}

}
