package net.wohlfart.data;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


/**
 * custom annotation to map a bean property to a database field, this way we can
 * check the size of the field in the database... 
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface DatabaseColumn {

    String table();
    String column();
    
}
