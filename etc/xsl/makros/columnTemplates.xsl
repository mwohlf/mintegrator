<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:output
            method="text"
            encoding="utf-8"/>



<!---
about the dirty flag used in the Java Objects:

there are 3 possible values for each field:
  A, B, null

  field1  field2  synced
   A        A      true
   A        B      false
   A       null    false
   B        A      false
   B        B      true
   B       null    false
  null      A      false
  null      B      false
  null     null    true

the produced code:

            if ( ( this.bwrRefnr != null )
                 && ( !this.bwrRefnr.equals(bwrRefnr) ) ) {
                 setSynced(false);
            } else if ( ( this.bwrRefnr == null )
                 && ( bwrRefnr != null ) ) {
                 setSynced(false);
            }
-->
<xsl:template match="column" mode="setterBody">
  <xsl:param name="field" />
        if ( ( this.<xsl:value-of select="$field" />!= null )
             &amp;&amp; ( !this.<xsl:value-of select="$field" />.equals(<xsl:value-of select="$field" />) ) ) {
            setSynchronized(false);
        } else if ( ( this.<xsl:value-of select="$field" /> == null )
                    &amp;&amp; ( <xsl:value-of select="$field" /> != null ) ) {
            setSynchronized(false);
        }
        this.<xsl:value-of select="$field" /> = <xsl:value-of select="$field" />;
</xsl:template>




<!-- generates the variable types -->
<xsl:template match="column[@type = 'int']" mode="type">
  <xsl:text>Integer</xsl:text>
</xsl:template>

<xsl:template match="column[@type = 'varchar']" mode="type">
  <xsl:text>String</xsl:text>
</xsl:template>

<xsl:template match="column[@type = 'datetime']" mode="type">
  <xsl:text>Date</xsl:text>
</xsl:template>

<xsl:template match="column[@type = 'decimal']" mode="type">
  <xsl:text>Integer</xsl:text>
</xsl:template>

<xsl:template match="column[@type = 'text']" mode="type">
  <xsl:text>String</xsl:text>
</xsl:template>

<xsl:template match="column[@type = 'image']" mode="type">
  <xsl:text>byte[]</xsl:text>
</xsl:template>




<!-- generates the imports -->
<xsl:template match="column[@type = 'int']" mode="import">
  <xsl:text>import java.lang.Integer;</xsl:text>
  <xsl:text>&#10;</xsl:text>
</xsl:template>

<xsl:template match="column[@type = 'varchar']" mode="import">
  <xsl:text>import java.lang.String;</xsl:text>
  <xsl:text>&#10;</xsl:text>
</xsl:template>

<xsl:template match="column[@type = 'datetime']" mode="import">
  <xsl:text>import java.util.Date;</xsl:text>
  <xsl:text>&#10;</xsl:text>
</xsl:template>

<xsl:template match="table[@implements]" mode="import">
  <xsl:text>import net.wohlfart.data.</xsl:text>
  <xsl:value-of select='@implements'/>
  <xsl:text>;</xsl:text>
  <xsl:text>&#10;</xsl:text>
</xsl:template>




<!-- generates the variable field name -->
<xsl:template match="column" mode="field">
  <xsl:variable name='table' select='/Metadata/table/@name' />
  <xsl:variable name="column" select="@name" />
  <xsl:variable name="field" select="document('columnTransform.xml')/transform/table[@original = $table]/column[@original = $column]/@custom"/>
  <xsl:value-of select='translate(substring($field, 1, 1),"ABCDEFGHIJKLMNOPQRSTUVWXYZ","abcdefghijklmnopqrstuvwxyz")'/>
  <xsl:value-of select='substring($field, 2)'/>
</xsl:template>



<!-- generates the getter name-->
<xsl:template match="column" mode="getter">
  <xsl:variable name='table' select='/Metadata/table/@name' />
  <xsl:variable name="column" select="@name" />

  <xsl:text>get</xsl:text>
  <xsl:value-of select="document('columnTransform.xml')/transform/table[@original = $table]/column[@original = $column]/@custom"/>
</xsl:template>



<!-- generates the setter name -->
<xsl:template match="column" mode="setter">
  <xsl:variable name='table' select='/Metadata/table/@name' />
  <xsl:variable name="column" select="@name" />

  <xsl:text>set</xsl:text>
  <xsl:value-of select="document('columnTransform.xml')/transform/table[@original = $table]/column[@original = $column]/@custom"/>
</xsl:template>



<!-- transpose a string in the word parameter from THIS_IS_CAMEL_CASE to ThisIsCamelCase -->
<xsl:template name="camelCase">
  <xsl:param name='word' />

  <xsl:choose>
  <xsl:when test="contains($word,'_')">
    <xsl:value-of select='translate(substring(substring-before($word,"_"), 1, 1),"abcdefghijklmnopqrstuvwxyz","ABCDEFGHIJKLMNOPQRSTUVWXYZ")'/>
    <xsl:value-of select='translate(substring(substring-before($word,"_"), 2),"ABCDEFGHIJKLMNOPQRSTUVWXYZ","abcdefghijklmnopqrstuvwxyz")'/>
        <!-- recursion -->
    <xsl:call-template name='camelCase'>
       <xsl:with-param name='word' select='substring-after($word,"_")' />
    </xsl:call-template>
  </xsl:when>
  <xsl:otherwise>
    <xsl:value-of select="translate(substring($word, 1, 1),'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
    <xsl:value-of select="translate(substring($word, 2),'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')"/>
  </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="methodNameStub">
  <xsl:param name="name"/>
  <xsl:value-of select="translate(@name,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
</xsl:template>

</xsl:stylesheet>