package net.wohlfart.gui.table;

import java.util.Collection;
import java.util.Collections;

import net.wohlfart.data.ICandidateTO;

/**
 * needed to fire events with a null candidate
 */
public class ApplicationCollectionFocused {

	private Collection<ICandidateTO> elements = Collections.emptySet();

	public ApplicationCollectionFocused(final Collection<ICandidateTO> elements) {
		if (elements == null) {
			this.elements = Collections.emptySet();
		} else {
			this.elements = elements;
		}
	}

	public Collection<ICandidateTO> get() {
		return elements;
	}

}
