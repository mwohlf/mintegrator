/**
 *
 */
package net.wohlfart.gui;

import static net.wohlfart.util.GuiUtilities.getFrame;

import java.awt.EventQueue;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import net.wohlfart.Messages;
import net.wohlfart.gui.event.Shutdown;
import net.wohlfart.properties.IApplicationProperties;
import net.wohlfart.properties.PropertyKeys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Weld component for application shutdown, note that the shutdown doesn't work if the application
 * wasn't started with the core, also a SwingWorker can only be used once, we hold a static
 * counter here that makes sure whenever this worker was triggered twice (and confirmed) 
 * we do a System.exit() to make sure the application is not hanging.
 * 
 * 
 * 
 * @author michael
 */
public class ShutdownWorker extends SwingWorker<Void, Void> {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(ShutdownWorker.class);
	
	// we count how often the user ended the application to make sure the second time
	// really ends the application...
	private static volatile int invocationCount = 0;

	@Inject 
	private Event<Shutdown> shutdownEvent;

	@Inject
	private IApplicationProperties properties;
	
	// used in the validation dialog to store the user reply
	private volatile int reply = JOptionPane.NO_OPTION;

	@Override
	protected Void doInBackground() /* throws Exception */{
		LOG.info("doInBackground() - start"); //$NON-NLS-1$
		try {
			// check for emergency System.exit
			if (invocationCount > 0) {
				LOG.error("shutdown worker didn't quit in previous run, calling System.exit() now");
				System.exit(1);
			}

			if (confirmQuitIfNeeded()) {
				fireShutdownEvent();
			}
		} catch (Throwable ex) {
			LOG.error("exception during shutdown ", ex); //$NON-NLS-1$
			ErrorPane.showErrorDialog(ex, Messages.getString("ShutdownWorker.Throwable")); //$NON-NLS-1$
		} finally {
			LOG.debug("doInBackground() - finally"); //$NON-NLS-1$
		}
		return null;
	}


	/**
	 * invoke a confirm dialog
	 * @return true if the user really wants to quit the application
	 */
	private boolean confirmQuitIfNeeded() throws InvocationTargetException, InterruptedException {
		// read from the properties:
		boolean confirmQuit = properties.getBoolean(PropertyKeys.CONFIRM_QUIT, true);
		if (!confirmQuit)  {
			LOG.info("inc shutdown count");
			invocationCount ++;
			return true; // the settings say there is no confirm needed, just quit already			
		}

		EventQueue.invokeAndWait(new Runnable(){
			final boolean confirmQuit = properties.getBoolean(PropertyKeys.CONFIRM_QUIT, true);
			@Override
			public void run() {
				// need to be created in the EDT:
				final JCheckBox confirm = new JCheckBox(Messages.getString("ShutdownWorker.QueryCheckbox")); //$NON-NLS-1$
				confirm.setBorder(BorderFactory.createEmptyBorder(20, 1, 1, 1));
				confirm.setSelected(confirmQuit);
				reply = JOptionPane.showConfirmDialog(getFrame(),
						new Object[] { 
							Messages.getString("ShutdownWorker.QuitConfirm"),   //$NON-NLS-1$
							confirm 
						}, 
						Messages.getString("ShutdownWorker.DialogTitle"),  //$NON-NLS-1$
						JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE);
				// remember the confirm reply:
				properties.setBoolean(PropertyKeys.CONFIRM_QUIT, confirm.isSelected());
			}
		});
		
		LOG.info("user replied quit validation dialog: {}", reply); //$NON-NLS-1$
		// only quit if user selects yes
		if (reply == JOptionPane.YES_OPTION) {
			LOG.info("inc shutdown count");
			invocationCount ++; // increase invocation counter
			return true;
		} else {
			invocationCount = 0; // reset invocation counter
			return false;
		}
	}
	
	private void fireShutdownEvent() {		
		shutdownEvent.fire(new Shutdown(new Date()));
		// we do the shutdown by waking the main thread in the core
	}
	
}
