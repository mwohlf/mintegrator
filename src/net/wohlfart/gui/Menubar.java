package net.wohlfart.gui;

import java.awt.EventQueue;
import java.awt.event.KeyEvent;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import net.wohlfart.action.ApplicationCreateAction;
import net.wohlfart.action.ApplicationDeleteAction;
import net.wohlfart.action.ApplicationUpdateAction;
import net.wohlfart.action.DocumentCreateAction;
import net.wohlfart.action.DocumentDeleteAction;
import net.wohlfart.action.GlobalCopyAction;
import net.wohlfart.action.GlobalCutAction;
import net.wohlfart.action.GlobalPasteAction;
import net.wohlfart.action.HelpAction;
import net.wohlfart.action.ApplicationEmailImportAction;
import net.wohlfart.action.ShowAbout;
import net.wohlfart.data.ConnectDatabaseAction;
import net.wohlfart.data.DisconnectDatabaseAction;
import net.wohlfart.gui.properties.PropertiesSetupAction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author michael
 */
@SuppressWarnings("serial")
@Singleton
public class Menubar extends JMenuBar {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(Menubar.class);


	public static final String FILE_MENU_NAME = "file"; // for testing
	@Inject
	private ShutdownAction shutdown;
	public static final String MENU_ITEM_SHUTDOWN_NAME = "shutdown"; // for testing
	@Inject
	private ApplicationCreateAction createCandidate;
	@Inject
	private ApplicationDeleteAction deleteCandidate;
	@Inject
	private DocumentCreateAction createDocument;
	@Inject
	private DocumentDeleteAction deleteDocument;
	@Inject
	private ApplicationUpdateAction updateCandidate;
	@Inject
	private ApplicationEmailImportAction importEmailFromFolder;


	@Inject
	private GlobalCutAction globalCutAction;
	@Inject
	private GlobalCopyAction globalCopyAction;
	@Inject
	private GlobalPasteAction globalPasteAction;


	@Inject
	private PropertiesSetupAction propertiesSetupAction;
	public static final String MENU_ITEM_PROPERTIES_NAME = "properties"; // for testing
	//@Inject
	//private SetupOutlook setupOutlook;
	//    @Inject
	//    private SaveProperties saveProperties;
	//    @Inject
	//    private LoadProperties loadProperties;


	@Inject
	private ConnectDatabaseAction connectDatabaseAction; 
	@Inject
	private DisconnectDatabaseAction disconnectDatabaseAction;


	@Inject
	private HelpAction helpAction;
	@Inject
	private ShowAbout showAbout;



	/* package private */
	Menubar() {
		LOG.debug("Menubar() - init"); //$NON-NLS-1$
		if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("All swing components should be created in the EDT."); //$NON-NLS-1$
		}
	}


    @PostConstruct
    protected void postConstruct() { // NO_UCD
    	LOG.debug("postConstruct() called"); //$NON-NLS-1$
		initComponents();
	}

	private void initComponents() {
		add(new JMenu("Datei") {{
			setName(FILE_MENU_NAME);
			setMnemonic(KeyEvent.VK_D);
			add(new JMenuItem(shutdown) {{ setName(MENU_ITEM_SHUTDOWN_NAME); }}); // name for testing
			addSeparator();
			add(new JMenuItem(createCandidate));
			add(new JMenuItem(deleteCandidate));
			addSeparator();
			add(new JMenuItem(createDocument));
			add(new JMenuItem(deleteDocument));
			//add(new JMenuItem(saveDocument));
			addSeparator();
			add(new JMenuItem(updateCandidate));
			add(new JMenuItem(importEmailFromFolder));
		}});
		add(new JMenu("Edit") {{
			setMnemonic(KeyEvent.VK_E);
			add(new JMenuItem(globalCutAction));
			add(new JMenuItem(globalCopyAction));
			add(new JMenuItem(globalPasteAction));
			addSeparator();
			add(new JMenuItem(propertiesSetupAction) {{ setName(MENU_ITEM_PROPERTIES_NAME); }}); // name for testing       
		}});
					//        add(new JMenu("Einstellungen") {{
		//            setMnemonic(KeyEvent.VK_S);
		//            add(new JMenuItem(setupDatabase));
		//            add(new JMenuItem(setupIntegrator));
		//            add(new JMenuItem(setupOutlook));
		//            //add(new JMenuItem(saveProperties));
		//            //add(new JMenuItem(loadProperties));
		//        }});
		add(new JMenu("Datenbank") {{
			setMnemonic(KeyEvent.VK_B);
			add(new JMenuItem(connectDatabaseAction));
			add(new JMenuItem(disconnectDatabaseAction));
		}});
		add(new JMenu("Hilfe") {{
			setMnemonic(KeyEvent.VK_H);
			add(new JMenuItem(helpAction));
			add(new JMenuItem(showAbout));
		}});
	}

}
