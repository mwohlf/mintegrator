package net.wohlfart.gui.properties.database.flavour;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * class to create a database from scratch
 * 
 * @author michael
 */
public interface ITableLayout {

	public abstract void setupDatabaseTables(final Connection connection)
			throws SQLException;

	public abstract void teardownDatabaseTables(final Connection connection)
			throws SQLException;

	
}
