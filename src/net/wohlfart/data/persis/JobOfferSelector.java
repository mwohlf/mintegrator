package net.wohlfart.data.persis;

import java.util.ArrayList;
import java.util.HashMap;

import net.wohlfart.Messages;

/**
 * FIXME: this is ugly code
 * 
 * @author michael
 */
public class JobOfferSelector {
    private static HashMap<String, JobOfferSelector> hashMap = new HashMap<>();

    private final static String ALL_JOBOFFERS = "SELECT_ALL"; //$NON-NLS-1$
    private final static String UNDONE_JOBOFFERS = "UNDONE_JOBOFFERS"; //$NON-NLS-1$
    private final static String OPEN_JOBOFFERS = "OPEN_JOBOFFERS"; //$NON-NLS-1$

    static {
        hashMap.put(ALL_JOBOFFERS, new JobOfferSelector(ALL_JOBOFFERS, Messages.getString("JobOfferSelector.AllJobOffers"), "SELECT_ALL")); //$NON-NLS-1$ //$NON-NLS-2$
        hashMap.put(UNDONE_JOBOFFERS, new JobOfferSelector(UNDONE_JOBOFFERS, Messages.getString("JobOfferSelector.UnDoneJobOffers"), "SELECT_UNDONE")); //$NON-NLS-1$ //$NON-NLS-2$
        hashMap.put(OPEN_JOBOFFERS, new JobOfferSelector(OPEN_JOBOFFERS, Messages.getString("JobOfferSelector.OpenJobOffers"), "SELECT_OPEN")); //$NON-NLS-1$ //$NON-NLS-2$
    }


    public static JobOfferSelector[] getArray() {
        ArrayList<JobOfferSelector> result = new ArrayList<>();
        for (Object element : hashMap.keySet()) {
            result.add(hashMap.get(element));
        }
        return result.toArray(new JobOfferSelector[] {} );
    }

    public static JobOfferSelector getSelectorForKey(final String storageKey) {
        if (hashMap.containsKey(storageKey)) {
            return hashMap.get(storageKey);
        }
        return hashMap.get(ALL_JOBOFFERS); // select all by default
    }


    private final String view;
    private final String stmtKey;
    private final String hashKey;

    private JobOfferSelector(final String hashKey, final String view, final String stmtKey) {
        this.view = view;
        this.stmtKey = stmtKey;
        this.hashKey = hashKey;
    }


    public String getStatementKey() {
        return stmtKey;
    }

    public String getStorageKey() {
        return hashKey;
    }

    @Override
    public String toString() {
        return view;
    }
}
