/**
 *
 */
package net.wohlfart.splash;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics2D;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.SplashScreen;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.swing.BoundedRangeModel;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.JProgressBar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A package private class to handle splash screen modification,
 * in order for this to work, the application has to be started with the "-splash" option
 *
 * @author michael
 */
public class SplashScreenHandleImpl implements ISplashScreenHandle { // NO_UCD: this class is injected
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(SplashScreenHandleImpl.class);
	/** name of the splash image, must be in the same package/directory */
	protected static final String IMAGE_NAME = "splash.gif";

	/** The text position within the splashscreen. */
	protected final Point textLocation = new Point(17, 112);
	/** the scrollbar location within the splashscreen, 
	 * this is calculated as soon as we know the image height */
	protected int scrollBarY;


	/** The gfx context. */
	protected volatile Graphics2D graphics2D;
	/** The splashimage. */
	protected volatile BufferedImage image;
	/** Progress indicator */
	protected volatile JProgressBar progressBar = new JProgressBar();


	/** The splashimage object. */
	protected volatile SplashScreen splash;


	protected static final int PROGRESSBAR_HEIGHT = 5;
	protected static final int PROGRESSBAR_INSET = 3;
	protected BoundedRangeModel boundedRangeModel = new DefaultBoundedRangeModel();
	protected int imageHeight;
	protected int imageWidth;


	/** 
	 * A simple constructor the work is done in the init method below which is called by CDI
	 */
	public SplashScreenHandleImpl() {
		// do nothing
	}

    @PostConstruct
    protected void postConstruct() { // NO_UCD
    	LOG.debug("postConstruct() called"); //$NON-NLS-1$
		// returns null if there is no -splash option set on startup  or if the splash screen is already closed
		try {
			splash = SplashScreen.getSplashScreen(); // might throw a HeadlessException
			if (splash == null) {
				LOG.warn("no splash found, either already null or application started without splash screen,"
						+ " you can use the '-splash:gfx/splash.gif' option to set a splash screen on startup");
				return;
			}
		} catch (HeadlessException ex) {
			LOG.warn("we are running in a headless environment, no way to get a splashscreen here");
			return;
		}

		graphics2D = splash.createGraphics();
		graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		                            RenderingHints.VALUE_ANTIALIAS_ON);

		try {
			InputStream  inputStream = SplashScreenHandleImpl.class.getResourceAsStream(IMAGE_NAME);
			image = ImageIO.read(inputStream);
			inputStream.close();
			imageHeight = image.getHeight();
			imageWidth = image.getWidth();
			Rectangle bound = new Rectangle();
			bound.height = PROGRESSBAR_HEIGHT;
			bound.width = imageWidth - (PROGRESSBAR_INSET * 2);
			progressBar.setBounds(bound);
			progressBar.setModel(boundedRangeModel);       
			scrollBarY = imageHeight - PROGRESSBAR_HEIGHT - PROGRESSBAR_INSET;
			textLocation.setLocation(
					textLocation.getX(), 
					scrollBarY - (2 * PROGRESSBAR_INSET));
		} catch (IOException ex) {
			LOG.error("can't init SplashImage", ex);
		}
	}

	@Override
	public void setTotalSteps(int totalSteps) {
		if (splash == null) {
			LOG.error("splash is null, can't set totalSteps to {}", totalSteps);
		}
		boundedRangeModel.setMaximum(totalSteps);       
	}

	/**
	 *  method to set random text inside the splashimage.
	 *
	 * @param status a piece of text to show within splashscreen.
	 */
	@Override
	public void setStatus(final String status, final int currentStep) {
		if (boundedRangeModel.getMaximum() < currentStep) {
			LOG.warn("fixing model maximum, step is {}/{} this is likely a programmer's error, status is '{}'",
					new Object[] {currentStep, boundedRangeModel.getMaximum(), status});
			boundedRangeModel.setMaximum(currentStep);       
		}
		if (splash == null) {
			LOG.error("splash is null, can't set status, step is {}/{}, status is '{}'",
					new Object[] {currentStep, boundedRangeModel.getMaximum(), status});
			return;
		}
		if (graphics2D == null) {
			LOG.error("graphics2D is null, can't set status, step is {}/{}, status is '{}'",
					new Object[] {currentStep, boundedRangeModel.getMaximum(), status});
			return;
		}

		// seems like the first two parameters of the rectangle are not honored
		//progressBar.setBounds(new Rectangle(10,10,30,30));
		EventQueue.invokeLater(new Runnable() {
			// we use invoke later here because:
			// - we want to return to the caller asap
			// - no problems if we are called from the EDT
			// - we need to run in the EDT because we modify the gfx content 
			@Override
			public void run() {
				// reset the canvas with the image or logo
				graphics2D.drawImage(image, 0, 0, Color.WHITE, null);
				// remember transformation
				AffineTransform savedTransform = graphics2D.getTransform();
				graphics2D.translate(3, scrollBarY);
				boundedRangeModel.setValue(currentStep);
				progressBar.paint(graphics2D);
				// restore the transformation
				graphics2D.setTransform(savedTransform);
				graphics2D.setColor(Color.WHITE);
				graphics2D.drawString(status, textLocation.x, textLocation.y);
				// this must be the last action, update the splash with whatever we were drawing so far
				splash.update();
			}
		});
	}

	/**
	 * handy delay method...
	 * to give the user a chance to actually read the message
	 */
	public void delay(final double seconds) {
		try {
			Thread.sleep(Math.round(seconds * 1000));
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
	}



}
