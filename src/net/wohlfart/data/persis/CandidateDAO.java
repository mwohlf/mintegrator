package net.wohlfart.data.persis;

import static net.wohlfart.data.persis.Tools.getRefNr;
import static net.wohlfart.data.persis.Tools.sqlDate;

import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.inject.Inject;

import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.data.ICandidateTO;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Data Access Object for applications
 * 
 * @author michael
 */
public class CandidateDAO {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(CandidateDAO.class);

    @Inject
    private IBasicDataSource basicDataSource;

    
    public CandidateDAO() {
    }

    /**
     * create a single application in the database
     *  - fetch the id
     *  - put the candidate into the table
     *  - commit the changes
     *  - return the created object 
     */
    public ICandidateTO createCandidate() throws SQLException {
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The DataBase shouldn't be access in the EDT."); //$NON-NLS-1$
		}

    	Properties queries = basicDataSource.getQueries(ICandidateTO.class);

        Connection connection = null;

        try {
            // fetch the primary ID outside the create transaction to block the Z_REFNR table for a minimum time
            int refnr = getRefNr(basicDataSource);

            // set auto commit to false since we are doing a transaction
            connection = basicDataSource.getConnection();
            connection.setAutoCommit(false);  // setting to true in the final block
            
            // create a new candidate:
            ICandidateTO candidate = new CandidateTO();
            candidate.setId(refnr);

            // create Query Runner for a given DataSource
            QueryRunner queryRunner = new QueryRunner();

            queryRunner.update(
                    connection,
                    queries.getProperty("CREATE"), //$NON-NLS-1$
                    new Object[] {candidate.getId()});
            connection.commit();

            return candidate;

        } catch (Throwable th) {
            if (connection != null) {
                connection.rollback();
            }
            throw new CreationFailedException("Unable to create Object", th); //$NON-NLS-1$
        } finally {
            if (connection != null) {
                connection.setAutoCommit(true);
                connection.close(); // we need to close the connection in order to reuse it
            }
        }
    }

    /**
     * delete a candidate object in the database
     */
    public void deleteCandidate(final ICandidateTO candidate) throws SQLException {
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The DataBase shouldn't be access in the EDT."); //$NON-NLS-1$
		}
    	deleteCandidate(candidate.getId());
    }
    
    
    /**
     * delete a candidate object in the database
     * - delete all attachments for the application
     * - delete all links
     * - delete the application
     */
    protected void deleteCandidate(final Integer id) throws SQLException {
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The DataBase shouldn't be access in the EDT."); //$NON-NLS-1$
		}
    	Properties properties = basicDataSource.getQueries(ICandidateTO.class);

    	
    	LOG.info("delete triggered for candidate {}", id); //$NON-NLS-1$
        // create Query Runner for a given DataSource
        QueryRunner queryRunner = new QueryRunner(basicDataSource);
        // delete attachments first
        queryRunner.update( properties.getProperty("DELETE_ATTACHMENTS"), new Object[] {id}); //$NON-NLS-1$
        // delete folders
        queryRunner.update( properties.getProperty("DELETE_FOLDER"), new Object[] {id}); //$NON-NLS-1$
        // finally delete the candidate
        queryRunner.update( properties.getProperty("DELETE"), new Object[] {id}); //$NON-NLS-1$
    }

    
    /**
     * read the list of candidates, the read is done without the BEM field since it is LONG on oracle
     */
    public List<CandidateTO> readAllCandidates() throws SQLException {
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The DataBase shouldn't be access in the EDT."); //$NON-NLS-1$
		}
    	Properties queries = basicDataSource.getQueries(ICandidateTO.class);
    	
    	BeanListHandler<CandidateTO> handler = new BeanListHandler<>(CandidateTO.class);
        QueryRunner queryRunner = new QueryRunner(basicDataSource);
        
        // returns result from the Handler
        ArrayList<CandidateTO> result = (ArrayList<CandidateTO>) queryRunner.query(
                queries.getProperty("SELECT_ALL"), //$NON-NLS-1$
                handler,
                new Object[] {});
        
        // loop through all to update the BEM field
        for (CandidateTO candidate : result) {       	
        	String remark = queryRunner.query(
        			queries.getProperty("SELECT_SINGLE_BEM"), 
        			new ResultSetHandler<String>() {
						@Override
						public String handle(ResultSet rs) throws SQLException {
							rs.next();
							return rs.getString(1);
						}  				
        			},
        			candidate.getId());
        	candidate.setRemark(remark);
        }       
        return result;
    }
    

    /**
     * update an application in the database
     */
    public void updateCandidate(final ICandidateTO candidate) throws SQLException {
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The DataBase shouldn't be access in the EDT."); //$NON-NLS-1$
		}
    	Properties properties = basicDataSource.getQueries(ICandidateTO.class);
    	
        // create Query Runner for a given DataSource
        QueryRunner queryRunner = new QueryRunner(basicDataSource);

        Object[] args1 = new Object[] {
                candidate.getLastName(),
                candidate.getFirstName(),
                sqlDate(candidate.getDateOfBirth()),
                candidate.getPhoneNumber(),
                candidate.getStreetAndNumber(),
                candidate.getCountry(),
                candidate.getZipCode(),
                candidate.getCity(),
                candidate.getEmailAddress(),
                candidate.getGender(),
                // candidate.getRemark(),  doesn't work in Orcale
                sqlDate(candidate.getApplicationDate()),
                candidate.getApplicationTitle(),
                candidate.getJobOfferDescription(),
                candidate.getDivision(),
                candidate.getMedium(),
                candidate.getId()};
        queryRunner.update( properties.getProperty("UPDATE"), args1); //$NON-NLS-1$
        
        
        Object[] args2 = new Object[] {
                candidate.getRemark(),
                candidate.getId()};
        queryRunner.update( properties.getProperty("UPDATE_BEM"), args2); //$NON-NLS-1$
       
        basicDataSource.removeModified(candidate);
    }

}
