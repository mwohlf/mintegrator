package net.wohlfart.gui.properties.look.color;

import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.plaf.metal.OceanTheme;


class ColorThemePlain extends AbstractColorTheme  implements IColorTheme  {  
		
	// package private
	ColorThemePlain(final String id) {
		super(id);
	}  

	@Override
	public void apply(final MetalLookAndFeel metalLookAndFeel) {
		MetalLookAndFeel.setCurrentTheme(new OceanTheme());  		
	}   
}  