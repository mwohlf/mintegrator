package net.wohlfart.mail.outlook;

import java.awt.EventQueue;

import javax.inject.Singleton;

import net.wohlfart.mail.MailProviderError;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moyosoft.connector.com.ComponentObjectModelException;
import com.moyosoft.connector.exception.LibraryNotFoundException;
import com.moyosoft.connector.ms.outlook.Outlook;

/**
 * lazy outlook init using the moyosoft lib
 * 
 * @author michael
 */
@Singleton
public final class MoyosoftOutlookHandler {
	/** Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(MoyosoftOutlookHandler.class);

	private static volatile Outlook outlook;

	private MoyosoftOutlookHandler() {

	}


	/**
	 * throws an exception if something is wrong with the config, 
	 * this can not run in the EDT, might take forever to boot up outlook...
	 * 
	 * @return the outlook object, never null
	 */
	public static synchronized Outlook getOutlook() {
		if (EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("Outlook Database backend shouldn't be access in the EDT."); //$NON-NLS-1$
		}
		if (outlook != null) {          
			return outlook;
		}
		try {
			// this might trigger some kind of prompt to the user:
			outlook = new Outlook();
			LOG.info("found outlook, version string is: {}", outlook.getOutlookVersion()); //$NON-NLS-1$
			return outlook;
		} catch (ComponentObjectModelException | LibraryNotFoundException  ex) {
			throw new MailProviderError("can't bootup Outlook Object, check if Outlook is installed," //$NON-NLS-1$
					+ "also make sure the moyosoft lib is available", ex); //$NON-NLS-1$
		} 

	}

	public static synchronized  void disposeOutlook() {
		if (outlook != null) {
			outlook.dispose();
		}
	}

}

