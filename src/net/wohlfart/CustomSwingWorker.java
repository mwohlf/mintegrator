package net.wohlfart;

import static net.wohlfart.Constants.DELAY_WORKERS;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Frame;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.ProgressMonitor;
import javax.swing.SwingWorker;

import net.wohlfart.util.GuiUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * implements an enhanced swingworker, this class is ment for overriding
 * 
 * @author michael
 *
 * @param <T>  intermediate result
 * @param <E>  final result
 */
public abstract class CustomSwingWorker<T, E> extends SwingWorker<T, E> {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(CustomSwingWorker.class);

	protected static final String PROGRESS_PROPERTY_NAME = "progress";
	protected static final String STATE_PROPERTY_NAME = "state";

	// UI component ProgressMonitor
	protected ProgressMonitor progressMonitor = new ProgressMonitor(
			GuiUtilities.getFrame(),
			Messages.getString("DisconnectDatabaseWorker.Message"),  //$NON-NLS-1$
			Messages.getString("DisconnectDatabaseWorker.Note"),  //$NON-NLS-1$
			0, // start
			100); // end

	// custom event queue for blocking user interaction while SwingWorker is running
	protected final CustomEventQueue queue = CustomEventQueue.getInstance();


	
	/**
	 * @param note the text for the progress dialog
	 */
	protected void setNote(final String note) {
		EventQueue.invokeLater(new Runnable(){
			@Override
			public void run() {
				progressMonitor.setNote(note);
			}
		});
	}


	/**
	 * @return true if the user canceled the dialog
	 */
	protected boolean isCanceled() {
		if (DELAY_WORKERS) { GuiUtilities.sleep(2000); }
		return progressMonitor.isCanceled();
	}




	/**
	 * fire up the progress dialog and call the execute() on the swing worker
	 */
	protected void doExecute() {
		GuiUtilities.setBusyCursor();  // reset in the done() method

		// lets block some
		Component[] blocked = new Component[] {};
		Frame frame = GuiUtilities.getFrame();
		if (frame!= null) {
			blocked = frame.getComponents();
		}
		queue.setBlockedComponents(blocked);

		progressMonitor.setMillisToDecideToPopup(20);
		progressMonitor.setMillisToPopup(20);
		progressMonitor.setProgress(1);

		// link the progressMonitor to the SwingWorker 
		addPropertyChangeListener(new PropertyChangeListener() {			
			@Override
			public void propertyChange(final PropertyChangeEvent evt) {
				LOG.info("event: {}", evt);
				final String name = evt.getPropertyName();
				switch(name) {
				case PROGRESS_PROPERTY_NAME:
					int newValue = (int)evt.getNewValue();
					LOG.info("new progress value: {}, dialog is {}", evt.getNewValue(), progressMonitor); //$NON-NLS-1$
					progressMonitor.setProgress(newValue);
					break;
				case STATE_PROPERTY_NAME:
					if (StateValue.DONE.equals(evt.getNewValue())) {
						progressMonitor.close();
					}
					break;
				default:
					LOG.warn("unkown property name: {}", name);
				}
			}
		});

		// fire up the swing worker
		super.execute();
	}

	
	@Override
	protected void done() {
		GuiUtilities.setDefaultCursor();
		queue.clearBlockedComponents();
		progressMonitor.setProgress(100);
		progressMonitor.close();
		super.done();
	}

}
