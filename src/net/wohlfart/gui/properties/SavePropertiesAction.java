/**
 *
 */
package net.wohlfart.gui.properties;

import java.awt.event.ActionEvent;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.SwingWorker;

import net.wohlfart.Messages;
import net.wohlfart.gui.ErrorPane;
import net.wohlfart.properties.IApplicationProperties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Save the applications properties with included swingworker...
 * 
 * @author michael
 */
@SuppressWarnings("serial")
@Singleton
public class SavePropertiesAction extends AbstractAction {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(SavePropertiesAction.class);

	@Inject
	private IApplicationProperties properties;

	public SavePropertiesAction() {
		putValue(Action.NAME, Messages.getString("SaveProperties.ActionName")); //$NON-NLS-1$
		putValue(Action.SHORT_DESCRIPTION, Messages.getString("SaveProperties.ShortDescription")); //$NON-NLS-1$
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		saveProperties();
	}

	/**
	 * this method can be called from another class in order to trigger
	 * a save for the properties (e.g. during shutdown)
	 */
	public void saveProperties() {
		saveProperties(properties);
	}

	private void saveProperties(final IApplicationProperties properties) {
		// fire up a SwingWorker to do the job
		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() throws Exception {
				try {
					properties.save();
				} catch (Throwable th) {
					LOG.error("failed to store properties", th); //$NON-NLS-1$
					ErrorPane.showErrorDialog(th, Messages.getString("SaveProperties.Failed")); //$NON-NLS-1$
				}
				return null;
			}} .execute();
	}

}
