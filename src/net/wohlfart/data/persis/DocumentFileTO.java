package net.wohlfart.data.persis;

import java.io.File;

import net.wohlfart.data.IDocumentFileTO;

/**
 * @author michael
 */
public class DocumentFileTO extends DocumentTO implements IDocumentFileTO {

    private File file;

    public DocumentFileTO(File file, String name) {
        setFile(file);
        setName(name);
    }

    @Override
	public File getFile() {
        return file;
    }

    @Override
	public void setFile(final File file) {
        this.file = file;
    }

}
