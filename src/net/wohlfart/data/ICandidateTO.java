package net.wohlfart.data;

import java.util.Date;

/**
 * @author michael
 */
public interface ICandidateTO {

    public abstract Date getApplicationDate();

    public abstract String getApplicationTitle();

    public abstract String getCity();

    public abstract String getCountry();

    public abstract Date getDateOfBirth();

    public abstract String getDivision();

    public abstract String getEmailAddress();

    public abstract String getFirstName();

    public abstract String getGender();

    public abstract Integer getId();

    public abstract String getJobOfferDescription();

    public abstract String getLastName();

    public abstract String getMedium();

    public abstract String getPhoneNumber();

    public abstract String getRemark();

    public abstract String getStreetAndNumber();

    public abstract String getZipCode();

    public abstract void setApplicationDate(Date applicationDate);

    public abstract void setApplicationTitle(String applicationTitle);

    public abstract void setCity(String city);

    public abstract void setCountry(String country);

    public abstract void setDateOfBirth(Date dateOfBirth);

    public abstract void setDivision(String division);

    public abstract void setEmailAddress(String emailAddress);

    public abstract void setFirstName(String firstName);

    public abstract void setGender(String gender);

    public abstract void setId(Integer id);

    public abstract void setJobOfferDescription(String jobOfferDescription);
    
    public abstract void setLastName(String lastName);

    public abstract void setMedium(String medium);
   
    public abstract void setPhoneNumber(String phoneNumber);

    public abstract void setRemark(String remark);

    public abstract void setStreetAndNumber(String streetAndNumber);

    public abstract void setZipCode(String zipCode);

 }
