package net.wohlfart.gui.properties.look;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

import net.wohlfart.ImageManager;
import net.wohlfart.Messages;
import net.wohlfart.gui.properties.AbstractPropertySheet;
import net.wohlfart.gui.properties.look.color.ColorThemeCollection;
import net.wohlfart.gui.properties.look.color.IColorTheme;
import net.wohlfart.properties.IApplicationProperties;
import net.wohlfart.properties.PropertyKeys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
@Singleton
public class LookAndFeelSheet extends AbstractPropertySheet {
	/**  Logger for this class */
	private static final Logger LOG = LoggerFactory.getLogger(LookAndFeelSheet.class);

	public static final String NAME = "LookAndFeelSheet"; // for lookup in the component hierarchy

	private final JComboBox<LookAndFeelInfo> lookAndFeelChooser;
	private final JComboBox<IColorTheme> colorChooser;


	@Inject
	IApplicationProperties properties;


	LookAndFeelSheet() {
		super(ImageManager.getImageIcon("NewApplicationIcon24"), //$NON-NLS-1$   
			  Messages.getString("LookAndFeelSheet.title"),   //$NON-NLS-1$   
			  Messages.getString("LookAndFeelSheet.tooltip")); //$NON-NLS-1$   
		setName(NAME);
		// setup the finals
		lookAndFeelChooser = createLookAndFeelChooser();
		colorChooser =  createColorChooser();
	}

    @PostConstruct
    protected void postConstruct() { // NO_UCD
    	LOG.debug("postConstruct() called"); //$NON-NLS-1$

		// reset the values
		resetAction();

		// do the layout
		setName(NAME);
		setLayout(new GridBagLayout());

		GridBagConstraints labelColumn = new GridBagConstraints();
		labelColumn.insets = new Insets(1,3,0,10);
		labelColumn.anchor = GridBagConstraints.WEST;
		GridBagConstraints editorColumn = new GridBagConstraints();
		editorColumn.insets = new Insets(1,0,0,10);
		editorColumn.anchor = GridBagConstraints.WEST;
		editorColumn.gridx = 1;
		editorColumn.fill = GridBagConstraints.HORIZONTAL;
		labelColumn.gridy = editorColumn.gridy=0;


		// --- start editors ---

		// the look and feel settings		
		add(new JLabel("Look And Feel:"), labelColumn);
		add(lookAndFeelChooser, editorColumn);
		labelColumn.gridy = ++editorColumn.gridy;

		// the color scheme settings		
		add(new JLabel("Color Scheme:"), labelColumn);
		add(colorChooser, editorColumn);			
		labelColumn.gridy = ++editorColumn.gridy;



		// ------------------- filler and button bar

		// add a filler component	
		GridBagConstraints filler = new GridBagConstraints();
		filler.gridy = labelColumn.gridy = editorColumn.gridy;
		filler.gridwidth = 2;
		filler.weightx = filler.weighty = 0.5;
		add(Box.createGlue(), filler);

		// "restore default" and "apply" buttons:
		GridBagConstraints buttons = new GridBagConstraints();
		buttons.gridy = filler.gridy + 1;
		buttons.gridwidth = 2;
		buttons.fill = GridBagConstraints.HORIZONTAL;
		JComponent buttonBar = createButtonBar();
		add(buttonBar, buttons);

		// initially update the selections
		resetAction();
	}


	private JComponent createButtonBar() {
		Box result = Box.createHorizontalBox();
		// filler
		result.add(Box.createHorizontalGlue());
		// reset button
		result.add(new JButton(new ResetAction()));
		// apply button
		result.add(new JButton(new ApplyAction()));
		// some padding
		result.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
		return result;
	}


	@SuppressWarnings({"unchecked" })
	private JComboBox<LookAndFeelInfo> createLookAndFeelChooser() {
		JComboBox<LookAndFeelInfo> result =  new JComboBox<>();
		result.setModel(new LookAndFeelComboBoxModel());
		// pick the name to render the LookAndFeelInfo
		result.setRenderer(new BasicComboBoxRenderer() {
			@Override
			public Component getListCellRendererComponent(JList list,
					Object value, int index, boolean isSelected,
					boolean cellHasFocus) {
				String newValue = ((LookAndFeelInfo) value).getName();
				return super.getListCellRendererComponent(list, newValue, index, isSelected, cellHasFocus);
			}			
		});		
		return result;
	}


	@SuppressWarnings("unchecked")
	private JComboBox<IColorTheme> createColorChooser() {
		JComboBox<IColorTheme> result =  new JComboBox<>();
		result.setModel(new ThemeComboBoxModel());
		result.setRenderer(new BasicComboBoxRenderer() {
			@Override
			public Component getListCellRendererComponent(JList list,
					Object value, int index, boolean isSelected,
					boolean cellHasFocus) {
				IColorTheme theme = (IColorTheme) value;
				String newValue = theme==null?"":theme.getName();
				return super.getListCellRendererComponent(list, newValue, index, isSelected, cellHasFocus);
			}			
		});		
		return result;
	}


	/**
	 * might be called in the EDT
	 */
	@Override
	public void applyAction() {
		LOG.info("applying values in {} ", NAME);
		// set the color scheme and the look and feel
		LookAndFeelInfo lookAndFeelInfo = (LookAndFeelInfo) lookAndFeelChooser.getSelectedItem();
		IColorTheme colorTheme = (IColorTheme) colorChooser.getSelectedItem();
		
		SetLookAndFeelAction.setLookAndFeelTheme(lookAndFeelInfo, colorTheme);
		
		properties.setString(PropertyKeys.LOOK_AND_FEEL_INFO, lookAndFeelInfo.getClassName());
		properties.setString(PropertyKeys.LOOK_AND_FEEL_THEME, (colorTheme==null)?null:colorTheme.getId());
	};


	/**
	 * the current LookAndFeel from the properties overrides the selections and also the properties
	 * be careful not to implement any race conditions here since the application might still start up
	 * and we might have a different LookAndFeel than specified in the properties here
	 */
	@Override
	public void resetAction() {
		LOG.debug("resetting values in {} ", NAME);
		
		String lookAndFeelId = properties.getString(PropertyKeys.LOOK_AND_FEEL_INFO, 
				SetLookAndFeelAction.getFallbackLookAndFeelId());
		String themeId = properties.getString(PropertyKeys.LOOK_AND_FEEL_THEME, 
				SetLookAndFeelAction.getFallbackThemeId());

		LookAndFeelInfo lookAndFeelInfo = LookAndFeelInfoCollection.getInstance().get(lookAndFeelId);
		IColorTheme colorTheme = ColorThemeCollection.getInstance().get(themeId);

		lookAndFeelChooser.setSelectedItem(lookAndFeelInfo);	
		// colorTheme might be null if the L&F doesn't support themes
		colorChooser.setSelectedItem(colorTheme);	

	}


}
