package net.wohlfart.gui.properties;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.SwingWorker;

import net.wohlfart.Messages;

@SuppressWarnings("serial")
public abstract class AbstractPropertySheet extends JComponent implements IPropertySheet {

	protected final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

	protected final ImageIcon icon;
	protected final String title;
	protected final String toolTip;


	public AbstractPropertySheet(final ImageIcon icon, final String title, final String toolTip) {
		this.icon = icon;
		this.title = title;
		this.toolTip = toolTip;
	}


	// --- UI stuff
	
	@Override
	public ImageIcon getIcon() {
		return icon;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public String getToolTip() {
		return toolTip;
	}

	@Override
	public JComponent getPane() {
		return this;
	}

	
	// --- callback from the dialog sheets

	@Override
	public void applyAction() {
		// do nothing by default, override in a subclass if needed
	}


	@Override
	public void resetAction() {
		// do nothing by default, override in a subclass if needed
		// make sure this is called outside the EDT
	}


	
	// --- property change listener stuff

	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}
	
	@Override
	public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}
	
	@Override
	public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
	}
	
	
	// -- apply/reset action if the subclass needs them
	

	protected class ApplyAction extends AbstractAction {
		
		public ApplyAction() {
			super();
			putValue(Action.NAME, Messages.getString("AbstractPropertySheet.ApplyAction.ActionName")); //$NON-NLS-1$
		}

		@Override
		public void actionPerformed(ActionEvent evt) {
			new SwingWorker<Void, Void>() {
				@Override
				protected Void doInBackground() {
					applyAction();
					return null;
				}
			}.execute();
		}
	}

	protected class ResetAction extends AbstractAction {
		
		public ResetAction() {
			super();
			putValue(Action.NAME, Messages.getString("AbstractPropertySheet.ResetAction.ActionName")); //$NON-NLS-1$
		}

		@Override
		public void actionPerformed(ActionEvent evt) {
			new SwingWorker<Void, Void>() {
				@Override
				protected Void doInBackground() {
					resetAction();
					return null;
				}
			}.execute();
		}
	}

}
