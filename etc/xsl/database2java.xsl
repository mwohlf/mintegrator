<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:output
            method="text"
            encoding="utf-8"/>


  <xsl:param name="testparam"/>

  <xsl:include href="makros/columnTemplates.xsl" />


    <xsl:template match="/">


    <!-- xsl:value-of select="$testparam"/ -->

     <!-- package -->
     <xsl:text>package net.wohlfart.data.persis;</xsl:text>  <!-- this must match to the destination folder -->
      <xsl:text>&#10;</xsl:text>
      <xsl:text>&#10;</xsl:text>


      <!-- imports -->
      <xsl:for-each select="/Metadata/table/column[not(@type = preceding-sibling::column/@type)]">
        <xsl:sort select="@type"/>
        <xsl:apply-templates select="." mode="import" />
      </xsl:for-each>
      <xsl:text>&#10;</xsl:text>

      <xsl:variable name="dbtable" select="/Metadata/table/@name" />
      <xsl:variable name="transformtable"
                    select="document('makros/columnTransform.xml')/transform/table[@original = $dbtable]" />

      <xsl:for-each select="$transformtable[@implements]">
        <xsl:apply-templates select="." mode="import" />
        <xsl:value-of select="./table/@implements" />
        <xsl:text>&#10;</xsl:text>
      </xsl:for-each>



      <!-- class definition -->
      <xsl:text>public class </xsl:text>
      <xsl:value-of select="$transformtable[@original = $dbtable]/@custom" />
       <xsl:if test="document('makros/columnTransform.xml')/transform/table[@original = $dbtable]/@implements" >
        <xsl:text> implements </xsl:text>
        <xsl:value-of select="document('makros/columnTransform.xml')/transform/table[@original = $dbtable]/@implements" />
      </xsl:if>
      <xsl:text> {</xsl:text>
      <xsl:text>&#10;</xsl:text>


      <xsl:apply-templates select="/Metadata/table" />


      <xsl:text>&#10;</xsl:text>
      <xsl:text>}</xsl:text>
    </xsl:template>




    <xsl:template match="table">

      <xsl:variable name="dbtable" select="/Metadata/table/@name" />
      <xsl:variable name="transformtable"
                    select="document('makros/columnTransform.xml')/transform/table[@original = $dbtable]" />


        <!-- the variables -->
        <xsl:for-each select="column">
          <xsl:variable name="column" select="@name" />
          <xsl:if test = "$transformtable/column[@original = $column]">
            <xsl:text>    private </xsl:text>
            <xsl:apply-templates select="." mode="type" />
            <xsl:text> </xsl:text>
            <xsl:apply-templates select="." mode="field" />
            <xsl:text>;&#10;</xsl:text>
          </xsl:if>
        </xsl:for-each>
        <xsl:text>&#10;</xsl:text>


        <!-- synced flag -->

        <xsl:text>    private boolean isSynced = false;&#10;</xsl:text>
        <xsl:text>&#10;</xsl:text>



        <!-- synced getter/setter -->

        <xsl:text>    public boolean isSynchronized() {&#10;</xsl:text>
        <xsl:text>        return isSynced;&#10;</xsl:text>
        <xsl:text>    }&#10;</xsl:text>
        <xsl:text>&#10;</xsl:text>

        <xsl:text>    public void setSynchronized(boolean isSynced) {&#10;</xsl:text>
        <xsl:text>        this.isSynced = isSynced;&#10;</xsl:text>
        <xsl:text>    }&#10;</xsl:text>
        <xsl:text>&#10;</xsl:text>




        <!-- the getter methods  -->

        <xsl:for-each select="column">
          <xsl:variable name='table' select='/Metadata/table/@name' />
          <xsl:variable name="column" select="@name" />
          <xsl:if test = "document('makros/columnTransform.xml')/transform/table[@original = $table]/column[@original = $column]">
            <xsl:text>    public </xsl:text>
            <xsl:apply-templates select="." mode="type" />
            <xsl:text> </xsl:text>
            <xsl:apply-templates select="." mode="getter" />
            <xsl:text>() {&#10;        return this.</xsl:text>
            <xsl:apply-templates select="." mode="field" >
              <xsl:with-param name='table' select='/Metadata/table/@name' />
            </xsl:apply-templates>
            <xsl:text>;&#10;    }&#10;</xsl:text>
          </xsl:if>
        </xsl:for-each>

        <xsl:text>&#10;</xsl:text>


        <!-- the setter methods  -->

        <xsl:for-each select="column">
          <xsl:variable name='table' select='/Metadata/table/@name' />
          <xsl:variable name="column" select="@name" />
          <xsl:if test = "document('makros/columnTransform.xml')/transform/table[@original = $table]/column[@original = $column]">

          <xsl:variable name="field">
            <xsl:apply-templates select="." mode="field" >
              <xsl:with-param name='table' select='/Metadata/table/@name' />
            </xsl:apply-templates>
          </xsl:variable>

          <xsl:text>    public void </xsl:text>
          <xsl:apply-templates select="." mode="setter" />
          <xsl:text>(</xsl:text>
              <xsl:apply-templates select="." mode="type" /><xsl:text> </xsl:text><xsl:value-of select="$field" />
          <xsl:text>) {</xsl:text>
            <xsl:apply-templates select="." mode="setterBody">
              <xsl:with-param name="field">
                <xsl:value-of select="$field" />
              </xsl:with-param>
            </xsl:apply-templates>
          <xsl:text>    }&#10;</xsl:text>
          </xsl:if>
        </xsl:for-each>


    </xsl:template>

</xsl:stylesheet>









