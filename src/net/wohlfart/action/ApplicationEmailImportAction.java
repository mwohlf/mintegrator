package net.wohlfart.action;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;

import net.wohlfart.ImageManager;
import net.wohlfart.Messages;
import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.mail.ApplicationEmailImportWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * action to trigger an email import
 * 
 * @author michael
 */
@SuppressWarnings("serial")
@Singleton
public class ApplicationEmailImportAction extends AbstractAction {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(ApplicationEmailImportAction.class);
    
    @Inject
    private Instance<ApplicationEmailImportWorker> applicationEmailImportWorker;


    public ApplicationEmailImportAction() {
        putValue(Action.NAME, Messages.getString("ImportEmailFromFolder.ActionName")); //$NON-NLS-1$
        putValue(Action.SMALL_ICON, ImageManager.getImageIcon("CopyOutlookIcon16")); //$NON-NLS-1$
        putValue(Action.LARGE_ICON_KEY, ImageManager.getImageIcon("CopyOutlookIcon24")); //$NON-NLS-1$
        putValue(Action.SHORT_DESCRIPTION, Messages.getString("ImportEmailFromFolder.ShortDescription")); //$NON-NLS-1$
        putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
    }

    @Override
	public void actionPerformed(final ActionEvent evt) {
        applicationEmailImportWorker.get() .execute();
    }

    public void onAnyDataSourceEvent(@Observes final IBasicDataSource dataSource) {
        // we can create a candidate as soon as we have a datasource
        LOG.debug("onAnyDataSourceEvent called"); //$NON-NLS-1$
        // we can only import if we have a connected database
        setEnabled(!dataSource.isClosed());
    }

}
