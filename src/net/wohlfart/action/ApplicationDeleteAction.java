package net.wohlfart.action;

import java.awt.event.ActionEvent;
import java.util.Collection;
import java.util.List;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.AbstractAction;
import javax.swing.Action;

import net.wohlfart.ImageManager;
import net.wohlfart.Messages;
import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.data.ICandidateTO;
import net.wohlfart.data.persis.CandidateTO;
import net.wohlfart.gui.table.ApplicationCollectionFocused;
import net.wohlfart.gui.table.ApplicationListFull;
import net.wohlfart.worker.ApplicationDeleteWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author michael
 *
 */
@SuppressWarnings("serial")
@Singleton
public class ApplicationDeleteAction extends AbstractAction {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(ApplicationDeleteAction.class);

	@Inject
	private Instance<ApplicationDeleteWorker> deleteSelectedCandidatesWorker;

	private boolean isDataSourceConnected;
	private Collection<ICandidateTO> focusedApplications;
	private List<CandidateTO> allApplications;

	
	public ApplicationDeleteAction() {
		putValue(Action.NAME, Messages.getString("DeleteCandidate.ActionName")); //$NON-NLS-1$
		putValue(Action.SMALL_ICON, ImageManager.getImageIcon("DeleteApplicationIcon16")); //$NON-NLS-1$
		putValue(Action.LARGE_ICON_KEY, ImageManager.getImageIcon("DeleteApplicationIcon24")); //$NON-NLS-1$
		putValue(Action.SHORT_DESCRIPTION, Messages.getString("DeleteCandidate.ShortDescription")); //$NON-NLS-1$
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		// this creates a new instance
		deleteSelectedCandidatesWorker.get() .doExecute(focusedApplications, allApplications);
	}

    private void doEnableCheck() {
    	int count = focusedApplications==null?0:focusedApplications.size();
    	setEnabled(isDataSourceConnected && (count >= 1));  	
    	// TODO: fix the name
    	putValue(Action.NAME, Messages.getString("DeleteCandidate.ActionNameChoice", new Object[] {Integer.valueOf(count)})); //$NON-NLS-1$
    }
    	
	public void onFocusedCandidateCollectionEvent(@Observes final ApplicationCollectionFocused container) {
		LOG.debug("onFocusedCandidateCollectionEvent called, container is: '{}' ",  container); //$NON-NLS-1$
		focusedApplications = container.get();
		doEnableCheck();
	}
	
	public void onApplicationListFullEvent(@Observes final ApplicationListFull container) {
		LOG.debug("onApplicationListFullEvent called, container is: '{}' ",  container); //$NON-NLS-1$
		allApplications = container.get();
		doEnableCheck();
	}

    public void onAnyDataSourceEvent(@Observes final IBasicDataSource dataSource) {
        LOG.debug("onAnyDataSourceEvent called, checking if the datasource is closed: '{}' " //$NON-NLS-1$
        		+ "(from dataSource.isClosed())", dataSource.isClosed()); //$NON-NLS-1$
        isDataSourceConnected = !dataSource.isClosed();
        doEnableCheck();
    }

}
