package net.wohlfart.data;

import java.sql.SQLFeatureNotSupportedException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.logging.Logger;

import org.apache.commons.dbcp.BasicDataSource;

/**
 * implementing a DataSource that is able to keep track of changes to the database entities
 * 
 * @author michael
 */
abstract class AbstractBasicDataSource extends BasicDataSource implements IBasicDataSource {

    private final Collection<Object> modified = new HashSet<>();
 
	@Override
	public void addModified(final Object object) {
		modified.add(object);
	}

	@Override
	public void removeModified(final Object object) {
		modified.remove(object);
	}

	@Override
	public boolean isModified(Object object) {
		return modified.contains(object);
	}

	@Override
	public Collection<Object> getModified() {
		return Collections.unmodifiableCollection(modified);
	}

	@Override
	public void clearModified() {
		modified.clear();
	}

	@Override
	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		throw new SQLFeatureNotSupportedException();
	}

}
