package net.wohlfart.gui.table;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;

import net.wohlfart.action.ApplicationCreateAction;
import net.wohlfart.action.ApplicationDeleteAction;
import net.wohlfart.action.DocumentCreateAction;
import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.data.ICandidateTO;
import net.wohlfart.gui.event.ApplicationFocused;
import net.wohlfart.gui.properties.editors.DateCellEditor;
import net.wohlfart.gui.properties.editors.StringEditor;
import net.wohlfart.gui.renderer.DateCellRenderer;
import net.wohlfart.gui.renderer.IntegerRenderer;
import net.wohlfart.gui.renderer.StringRenderer;
import net.wohlfart.properties.PropertyKeys;
import net.wohlfart.util.GuiUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * a wrapper for a table component
 * 
 * see: http://www.chka.de/swing/table/faq.html
 * 
 * @author michael
 */
@SuppressWarnings("serial")
@Singleton
public class ApplicationDataTable extends JComponent {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(ApplicationDataTable.class);

	@Inject
	private ApplicationCreateAction applicationCreateAction;
	@Inject
	private ApplicationDeleteAction applicationDeleteAction;
	@Inject
	private DocumentCreateAction documentCreateAction;

	@Inject 
	private Event<ApplicationFocused> applicationFocusEvent;
	@Inject 
	private Event<ApplicationCollectionFocused> candidateCollectionEvent;



	private final LocalPropertyChangeListener propertyChangeDispatcher;
	private VetoableChangeSupport changeSupport; // we need to remember the last change support in order to derigister whenever 

	// this flag is used to prevent endless loops when firing events while listening on the same events...
	private volatile boolean isFiring = false;


	// the data model for the inner table
	@Inject 
	private ApplicationTableModel tableDataModel;
	@Inject 
	private ApplicationSelectionModel selectionModel;
	@Inject
	private IBasicDataSource dataSource; // to keep track of data object changes


	// real table, since we implements a table to display the table header within
	private final JTable innerTable;
	// wrapper for the inner table
	private final JScrollPane innerScrollPane;



	ApplicationDataTable() {
		LOG.debug("CandidateTable() - init"); //$NON-NLS-1$
		if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("All swing components should be created in the EDT."); //$NON-NLS-1$
		}
		innerTable = new CustomTable();
		innerScrollPane = new JScrollPane();
		// private inner class to dispatch property change events to the table grid
		propertyChangeDispatcher = new LocalPropertyChangeListener();
	}

    @PostConstruct
    protected void postConstruct() { // NO_UCD
    	LOG.debug("postConstruct() called"); //$NON-NLS-1$ 

		selectionModel.addListSelectionListener(new ListSelectionListener(){
			@Override
			public void valueChanged(ListSelectionEvent evt) {
				if (!evt.getValueIsAdjusting()) {
					fireCandidateCollectionEvent();
					fireCandidateEvent();
				}		
			}
		});

		innerTable.setModel(tableDataModel);
		innerTable.setSelectionModel(selectionModel);
		innerTable.setColumnModel(new DefaultTableColumnModel());


		// FIXME: check: http://tips4java.wordpress.com/2010/01/24/table-row-rendering/
		//
		// renderers and editors:

		// Date.class
		innerTable.setDefaultRenderer(Date.class, new DateCellRenderer(tableDataModel, dataSource));
		DateCellEditor dateCellEditor = new DateCellEditor();
		innerTable.setDefaultEditor(Date.class, dateCellEditor);

		// String class
		innerTable.setDefaultRenderer(String.class, new StringRenderer(tableDataModel, dataSource));
		StringEditor stringEditor = new StringEditor();
		innerTable.setDefaultEditor(String.class, stringEditor);

		// Integer class
		innerTable.setDefaultRenderer(Integer.class, new IntegerRenderer(tableDataModel, dataSource));

		initComponents();
	}


	private void initComponents() {
		// setup the layout    	
		setLayout(new BorderLayout());

		// some magic to stop editing when the focus is gone
		innerTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
		innerTable.setAutoCreateColumnsFromModel(false); // we do this manually
		innerTable.setAutoCreateRowSorter(true);
		//innerTable.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);
		//innerTable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);


		// Add the inner table to CENTER slot
		innerScrollPane.setViewportView(innerTable);
		add(innerScrollPane, BorderLayout.CENTER);
		innerScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		GuiUtilities.loadColumnProperties(PropertyKeys.CANDIDATE_TABLE, innerTable, tableDataModel);
		//GuiUtilities.addColumnModelListener(PropertyKeys.CANDIDATE_TABLE, innerTable);

		Enumeration<TableColumn> enumeration = innerTable.getColumnModel().getColumns();
		while (enumeration.hasMoreElements()) {
			TableColumn elem = enumeration.nextElement();			
			elem.setHeaderRenderer(new CustomHeaderRenderer());
		}	


		// --- some wiring

		// context menus
		final JPopupMenu columnPopup = createColumnMenu();
		// FIXME: find a nice icon for the component
		innerScrollPane.setCorner(ScrollPaneConstants.UPPER_RIGHT_CORNER, new JComponent() {{
			super.setComponentPopupMenu(columnPopup);
		}});
		final JPopupMenu actionPopup = createActionMenu();        
		innerScrollPane.setComponentPopupMenu(actionPopup);
		innerTable.setComponentPopupMenu(actionPopup);
	}

	private JPopupMenu createActionMenu() {
		return new JPopupMenu() {{
			add(applicationCreateAction);
			add(applicationDeleteAction);
			addSeparator();
			add(documentCreateAction);
		}};
	}

	private JPopupMenu createColumnMenu() {
		return new ColumnViewPopupMenu(innerTable);
	}


	public int convertRowIndexToModel(final int row) {
		return innerTable.convertRowIndexToModel(row);
	}

	public int convertRowIndexToView(final int row) {
		return innerTable.convertRowIndexToView(row);
	}

	private void showRow(final int row) {
		// we need to invoke this on the GUI EventQueue since there may be a fireTableDataChanged being triggered first
		// running this on the main thread may be faster than the event thread actions so the new data may not yet be stored in the model
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				Rectangle rect = innerTable.getCellRect(row, 1, true);
				innerTable.scrollRectToVisible(rect);
			}
		});
	}



	public void repaintRow(final ICandidateTO application) {
		int index = tableDataModel.getElementIndex(application);
		LOG.trace("repaintRow index: {}", index);
		int row = innerTable.convertRowIndexToView(index);
		LOG.trace("repaintRow row: {}", row);
		repaintRow(row);
	}

	protected void repaintRow(final int row) {			
		int x = 0;
		int y = getRowYStart(row);
		int width = innerTable.getColumnModel().getTotalColumnWidth();
		int height = innerTable.getRowHeight(row);
		Rectangle rect = new Rectangle(x, y, width, height);
		LOG.trace("repaint called for rec: {}", rect);
		innerTable.repaint(rect);
	}

	protected int getRowYStart(final int row) {
		int sum = 0;
		for (int i = 0 ; i < row; i++) {
			sum += innerTable.getRowHeight(i);
		}
		return sum;
	}


	//  this are user select events from the GUI




	private void fireCandidateCollectionEvent() {		
		// assemble a list of selected (highlighted) Candidates
		// and tell the model they are selected
		HashSet<ICandidateTO> candidateSet = new HashSet<>();
		int start = selectionModel.getMinSelectionIndex();
		int end = selectionModel.getMaxSelectionIndex();
		for (int i = start; i <= end; i++) {
			if (selectionModel.isSelectedIndex(i)) {
				int dataRow = innerTable.convertRowIndexToModel(i);
				ICandidateTO selected = tableDataModel.getValueAtRow(dataRow);
				candidateSet.add(selected);
			}
		}
		try {
			isFiring = true;
			candidateCollectionEvent.fire(new ApplicationCollectionFocused(candidateSet));
		} finally {
			isFiring = false;
		}
	}


	private void fireCandidateEvent() {
		ICandidateTO candidate = null;
		if (!selectionModel.isSelectionEmpty()) {
			// get the Candidate that was last clicked
			// this is the view row starting at count 0
			int anchorRow = selectionModel.getLeadSelectionIndex();
			// carefully here: the anchor selection may be a unselected row (e.g. by using ctrl)
			if ((anchorRow >= 0)
					&& (anchorRow < tableDataModel.getRowCount())
					&& (selectionModel.isSelectedIndex(anchorRow))) {
				int dataRow = innerTable.convertRowIndexToModel(anchorRow);
				candidate = tableDataModel.getValueAtRow(dataRow);
			}
		}
				
		try {
			isFiring = true;
			applicationFocusEvent.fire(new ApplicationFocused(candidate, dataSource));
		} finally {
			isFiring = false;
		}
	}


	public void onFocusedApplicationEvent(@Observes final ApplicationFocused container) {
		LOG.debug("onFocusedApplicationEvent called, container is: {} , isFiring: {}", 
				new Object[] {container, isFiring});

		// attach the property listener for the view updates
		if (container.isEmpty()) {
			if (changeSupport != null) {
				changeSupport.removeVetoableChangeListener(propertyChangeDispatcher); // remove from the old support
				changeSupport = null;
			}

		} else {
			changeSupport = container.getChangeSupport();
			changeSupport.addVetoableChangeListener(propertyChangeDispatcher);

		}

		// ignoreIncoming` mean we don't want to update the model since we might cuase a loop
		if (isFiring) {
			return;
		}

		// select the row and scroll it into the view
		int index = tableDataModel.getElementIndex((ICandidateTO)container.getContent());		
		selectionModel.setSelectionInterval(index, index);
		selectionModel.setAnchorSelectionIndex(index);
		showRow(index); // scroll to the row

	}


	public void onFocusedCandidateCollectionEvent(@Observes final ApplicationCollectionFocused container) {
		if (isFiring) {
			return;
		}
		LOG.debug("onFocusedCandidateCollectionEvent called, container is: '{}' ",  container);

	}


	/**
	 * for updating a table cell value and the table row rendering
	 * whenever a property value changes, we need to attach this
	 * to the selected bean this class fires a table cell update for the changed cell
	 * and a repaint for the whole row
	 */
	protected class LocalPropertyChangeListener implements VetoableChangeListener {

		@Override
		public void vetoableChange(final PropertyChangeEvent evt) {
			String propertyName = evt.getPropertyName();
			Object newValue = evt.getNewValue();
			Object oldValue = evt.getOldValue();
			LOG.debug("property change detected name is `{}`, old value is `{}` new value is '{}'", 
					new Object[] {propertyName, oldValue, newValue});
			//innerTable.revalidate();
			int column = tableDataModel.findColumn(propertyName);
			int row = selectionModel.getAnchorSelectionIndex();
			// update the value in the table grid:
			tableDataModel.fireTableCellUpdated(row, column);
			// change to use a bold font
			repaintRow(row);
		}

	}


}
