/**
 *
 */
package net.wohlfart;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.net.URL;

import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.help.HelpSetException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author michael
 */
public final class HelpBrokerAdapter {
	/**  Logger for this class.  */
	private static final Logger LOG = LoggerFactory.getLogger(HelpBrokerAdapter.class);
	// singleton
	private static HelpBrokerAdapter instance;

	private static final String HELP_SET = "main.hs";  // the HelpSet must be in the classpath //$NON-NLS-1$
	private transient HelpBroker helpBroker;
	private transient HelpSet helpSet;



	private HelpBrokerAdapter() {
	}


	public static synchronized HelpBrokerAdapter getInstance() {
		if (instance == null) {
			instance = new HelpBrokerAdapter();
		}
		return instance;
	}


	void initialize() {
		final URL helpResource = ClassLoader.getSystemClassLoader().getResource(HELP_SET);
		//URL helpSetUrl = .toURI().toURL();
		//URL currentWorkingDirectory = new File(System.getProperty("user.dir")).toURI().toURL();
		//URL helpSetUrl = new URL(currentWorkingDirectory, HelpSet);
		if (helpResource == null) {
			LOG.warn("help resource could not be found," //$NON-NLS-1$
					+ " the resources name is '{}' and must be in the classpath", HELP_SET); //$NON-NLS-1$
		} else {
			try {
				helpSet = new HelpSet(ClassLoader.getSystemClassLoader(), helpResource);
				//helpSet = new HelpSet(null, helpSetUrl);
				helpBroker = helpSet.createHelpBroker();		
			} catch (HelpSetException ex) {
				LOG.warn("the help set could not be created, resource name was {}", HELP_SET); //$NON-NLS-1$
			}
		}
	}



	public void showHelpForAction(final ActionEvent action) {
		if (helpBroker == null) {
			LOG.warn("help system not initialized"); //$NON-NLS-1$
		} else {
			new javax.help.CSH.DisplayHelpFromSource(helpBroker).actionPerformed(action);
		}
	}


	public void enableHelpKey(final Component component, final String string) {
		// the components register themselves with the help key
		if (helpBroker == null) {
			LOG.warn("help system not initialized, can't enable help key for: " + component); //$NON-NLS-1$
		} else {
			helpBroker.enableHelpKey(component, string, helpSet, "javax.help.SecondaryWindow", "mainSW"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

}
