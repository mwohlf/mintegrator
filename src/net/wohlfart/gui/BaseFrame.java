package net.wohlfart.gui;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.WindowConstants;

import net.wohlfart.ImageManager;
import net.wohlfart.Messages;
import net.wohlfart.gui.document.DocumentPanel;
import net.wohlfart.gui.properties.look.SetLookAndFeelAction;
import net.wohlfart.gui.table.ApplicationDataTable;
import net.wohlfart.properties.IApplicationProperties;
import net.wohlfart.properties.PropertyKeys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author michael
 */
@SuppressWarnings("serial")
@Singleton
public class BaseFrame extends JFrame  {
	/**  Logger for this class */
	private final static Logger LOG = LoggerFactory.getLogger(BaseFrame.class);

	// name for testing
	public static final String NAME = "BaseFrame";

	@Inject 
	private Statusbar statusbar; 
	@Inject
	private Toolbar toolbar;
	@Inject
	private Menubar menubar;
	@Inject
	private ApplicationDataTable candidateTable;
	@Inject
	private DocumentPanel documentPanel;
	@Inject
	private ApplicationForm candidatePropertiesPanel;

	@Inject
	private IApplicationProperties properties;

	// swing workers
	@Inject
	private Instance<ShutdownWorker> shutdownWorker;


	/* any UI init code must be called inside the EDT thread */
	/* package private */
	BaseFrame() {
		LOG.debug("BaseFrame() - init"); //$NON-NLS-1$
		if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("All swing components should be created in the EDT."); //$NON-NLS-1$
		}
	}

    @PostConstruct
    protected void postConstruct() { // NO_UCD
    	LOG.debug("postConstruct() called"); //$NON-NLS-1$
				
		setName(NAME); // for testing
		initLookAndFeel();
		initComponents();
	}


	private void initLookAndFeel() {
		
		String value = properties.getProperty(PropertyKeys.LOOK_AND_FEEL_INFO, "");
		LOG.info("look and feel value: " + value);
		
		String lookAndFeelId = properties.getString(PropertyKeys.LOOK_AND_FEEL_INFO, 
				SetLookAndFeelAction.getFallbackLookAndFeelId());
		String themeId = properties.getString(PropertyKeys.LOOK_AND_FEEL_THEME, 
				SetLookAndFeelAction.getFallbackThemeId());
		LOG.info("setting look and feel: {}/{}",
				new Object[] {lookAndFeelId, themeId});
		SetLookAndFeelAction.setLookAndFeelTheme(lookAndFeelId, themeId);
	}

	private void initComponents() {
		JPanel table = new JPanel();
		table.setLayout(new BorderLayout());
		JPanel formTop = new JPanel();
		formTop.setLayout(new BorderLayout());
		JPanel formBottom = new JPanel();
		formBottom.setLayout(new BorderLayout());

		final JSplitPane verticalSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		verticalSplitPane.add(formTop, JSplitPane.TOP);
		verticalSplitPane.add(formBottom, JSplitPane.BOTTOM);
		verticalSplitPane.setDividerSize(3);
		verticalSplitPane.setResizeWeight(0.5f); // resize evenly

		//final ApplicationPropertiesImpl properties = ApplicationPropertiesImpl.getInstance();

		Integer verticalSplitPanePosition = properties.getInteger(PropertyKeys.VERTICAL_SPLIT_PANE, null);
		if (verticalSplitPanePosition != null) {
			verticalSplitPane.setDividerLocation(verticalSplitPanePosition);
		}
		verticalSplitPane.addPropertyChangeListener("lastDividerLocation", new PropertyChangeListener() {
			@Override
			public void propertyChange(final PropertyChangeEvent evt) {
				LOG.debug("event {} triggered", evt);
				properties.setInteger(PropertyKeys.VERTICAL_SPLIT_PANE, verticalSplitPane.getDividerLocation());
			}
		});

		final JSplitPane horizontalSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		horizontalSplitPane.setResizeWeight(1.0f); // extra space goes to table
		horizontalSplitPane.add(verticalSplitPane, JSplitPane.RIGHT);
		horizontalSplitPane.add(table, JSplitPane.LEFT);

		Integer horizontalSplitPanePosition = properties.getInteger(PropertyKeys.HORIZONTAL_SPLIT_PANE, null);
		if (horizontalSplitPanePosition != null) {
			horizontalSplitPane.setDividerLocation(horizontalSplitPanePosition);
		}
		horizontalSplitPane.addPropertyChangeListener("lastDividerLocation", new PropertyChangeListener() {
			@Override
			public void propertyChange(final PropertyChangeEvent evt) {
				LOG.debug("event {} triggered", evt);
				properties.setInteger(PropertyKeys.HORIZONTAL_SPLIT_PANE, horizontalSplitPane.getDividerLocation());
			}
		});

		// default anyway, just to make sure
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(horizontalSplitPane, BorderLayout.CENTER);

		// disable default behavior, we want to cleanup on our own
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		// shutdown Engine on window close event
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent evt) {
				LOG.info("windowClosing(WindowEvent)"); //$NON-NLS-1$
				// inject a new one for each get() call
				shutdownWorker.get() .execute();
			}
		});

		setIconImage(new ImageIcon(ImageManager.getImage("Frame.Icon")).getImage());   //$NON-NLS-1$
		setTitle(Messages.getString("Frame.Title")); //$NON-NLS-1$

		//JRootPane rootPane = getRootPane();
		add(toolbar, BorderLayout.PAGE_START);        
		getContentPane().add(statusbar, BorderLayout.SOUTH);   
		setJMenuBar(menubar);

		table.add(candidateTable, BorderLayout.CENTER);
		formTop.add(candidatePropertiesPanel, BorderLayout.CENTER);
		formBottom.add(documentPanel, BorderLayout.CENTER);

		// a resize listener to keep track of the size and update the properties
		addComponentListener(new ComponentAdapter() {  
			@Override
			public void componentResized(ComponentEvent evt) {
				LOG.debug("event {} setting size to {}", new Object[]{evt, BaseFrame.this.getSize()});
				properties.setDimension(PropertyKeys.BASE_FRAME_DIMENSION, BaseFrame.this.getSize());
			}
		});

		LOG.debug("BaseFrame() - end"); //$NON-NLS-1$
	}


	@Override
	public void setVisible(final boolean makeVisible) {
		if (makeVisible) {
			pack();
			setSize(properties.getDimension(PropertyKeys.BASE_FRAME_DIMENSION, 
					new Dimension(800, 600)));
			// FIXME: remember position
			setLocationRelativeTo(null); // center
		}
		super.setVisible(makeVisible);
	}
}
