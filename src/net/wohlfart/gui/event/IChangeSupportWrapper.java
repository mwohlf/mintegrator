package net.wohlfart.gui.event;

import java.beans.VetoableChangeSupport;

interface IChangeSupportWrapper {

	public abstract VetoableChangeSupport getChangeSupport();

	public abstract Object getContent();

	public abstract boolean isEmpty();

}
