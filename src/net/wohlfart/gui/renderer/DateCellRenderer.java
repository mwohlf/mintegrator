package net.wohlfart.gui.renderer;

import java.awt.Component;
import java.awt.EventQueue;
import java.text.SimpleDateFormat;

import javax.swing.JTable;

import net.wohlfart.data.IBasicDataSource;
import net.wohlfart.gui.table.ApplicationTableModel;

@SuppressWarnings("serial")
public class DateCellRenderer extends DirtyCellRenderer {

	// date format is not thread safe, however it is used only in the EDT here so we should be fine
    public static final String DATE_FORMAT_STRING = "dd.MM.yyyy";

    public DateCellRenderer(final ApplicationTableModel model, final IBasicDataSource dataSource) {
        super(model, dataSource);
    }

    @Override
    public Component getTableCellRendererComponent(
            JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column) {
		if (!EventQueue.isDispatchThread()) {
			throw new IllegalAccessError("The DateCellRenderer shouldn't be access outside the EDT."); //$NON-NLS-1$
		}
   	
        try {
        	// FIXME: format is not thread safe, but creating a new instance in a renderer might be too slow...
            String string = new SimpleDateFormat(DATE_FORMAT_STRING).format(value);
            return super.getTableCellRendererComponent(table, string, isSelected, hasFocus, row, column);
        } catch (IllegalArgumentException ex) {
            // don't know what to do with it, pass it to the superclass
            return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
    }

}
